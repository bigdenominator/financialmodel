#ifndef INCLUDE_H_PBONDBEHAVIOR
#define INCLUDE_H_PBONDBEHAVIOR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "Schematics.h"

typedef CSmartArray<CVectorRealPtr, 3> LoanStatus;

SIMPLESTRUCT(SBehaviorPkt,
	(SBondBehaviorPtr msMyPtr;
	
	size_t miSims;
	CDate			mdtCurrent;

	CUniquePtr<LoanStatus> mcStatusBopPtr;
	CUniquePtr<LoanStatus> mcStatusEopPtr;

	CUniquePtr<CVectorReal>	mcProbStayPtr;
	CUniquePtr<CVectorReal>	mcSchedBalBopPtr;
	CUniquePtr<CVectorReal>	mcWorkingPtr;

	CVectorRealPtr	mcBalEopPtr;
	CVectorRealPtr	mcIntPtr;
	CVectorRealPtr	mcLgdPtr;
	CVectorRealPtr	mcPrinPtr;
	CVectorRealPtr	mcProbDfltPtr;
	CVectorRealPtr	mcProbPpmtPtr;));

DEF_NODE(PBondBehavior, SBondBehavior, (), (), ())
{
public:
	bool Initialize(void) 
	{
		msShelfPtr = CreateSmart<SBehaviorPkt>();
		return true; 
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		msShelfPtr->msMyPtr = inp_sSchematicPtr;

		MyBundle mcItrs(iterators::MakeBundleConst(
			(*inp_sSchematicPtr->mcDetailPtr)[METRICS::BAL_EOP],
			(*inp_sSchematicPtr->mcDetailPtr)[METRICS::INT],
			(*inp_sSchematicPtr->mcDetailPtr)[METRICS::LGD],
			(*inp_sSchematicPtr->mcDetailPtr)[METRICS::PRIN],
			(*inp_sSchematicPtr->mcDetailPtr)[METRICS::PROB_DFLT],
			(*inp_sSchematicPtr->mcDetailPtr)[METRICS::PROB_PPMT]));

		initialize(*msShelfPtr);

		mcItrs.begin();
		setCurrent(mcItrs, *msShelfPtr);
		settlement(msShelfPtr->mdtCurrent, msShelfPtr->miSims, *msShelfPtr->msMyPtr->mcDetailPtr);

		for (mcItrs++; !mcItrs.isEnd(); mcItrs++)
		{
			setCurrent(mcItrs, *msShelfPtr);
			normalPeriod(*msShelfPtr);
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	typedef BundleHlpr<CDataTable, CDataTable, CDataTable, CDataTable, CDataTable, CDataTable>::const_type MyBundle;
	enum :size_t { CURRENT, DEFAULT, PAIDOFF };

	enum HERE :size_t { BAL, INT, LGD, PRIN, PROBDFLT, PROBPPMT };

	SBehaviorPktPtr msShelfPtr;

	static void convertProbabilities(CVectorReal& inp_cProbStayCurr, CVectorReal& inp_cProbDflt, CVectorReal& inp_cProbPpmt)
	{
		inp_cProbDflt = std::exp(inp_cProbDflt);
		inp_cProbPpmt = std::exp(inp_cProbPpmt);
		
		inp_cProbStayCurr = 1 / (1 + inp_cProbDflt + inp_cProbPpmt);
		inp_cProbDflt *= inp_cProbStayCurr;
		inp_cProbPpmt *= inp_cProbStayCurr;
	}
	
	static void initialize(SBehaviorPkt& inp_sShelf)
	{
		inp_sShelf.mcSchedBalBopPtr = CreateUnique<CVectorReal>(*(*inp_sShelf.msMyPtr->mcDetailPtr)[METRICS::BAL_EOP]->front().second);
		inp_sShelf.miSims = inp_sShelf.mcSchedBalBopPtr->size();
		inp_sShelf.mcProbStayPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);
		inp_sShelf.mcWorkingPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);

		inp_sShelf.mcStatusBopPtr = CreateUnique<LoanStatus>();
		inp_sShelf.mcStatusEopPtr = CreateUnique<LoanStatus>();
		for (size_t i(0); i < LoanStatus::SIZE; i++)
		{
			(*inp_sShelf.mcStatusBopPtr)[i] = CreateSmart<CVectorReal>(inp_sShelf.miSims);
			(*inp_sShelf.mcStatusEopPtr)[i] = CreateSmart<CVectorReal>(inp_sShelf.miSims, 0.0);
		}

		*(*inp_sShelf.mcStatusEopPtr)[inp_sShelf.msMyPtr->mStatus] = 1.0;
	}

	static void normalPeriod(SBehaviorPkt& inp_sShelf)
	{
		CVectorRealPtr cDfltPtr(CreateSmart<CVectorReal>(inp_sShelf.miSims));
		CVectorRealPtr cLossPtr(CreateSmart<CVectorReal>(inp_sShelf.miSims));
		CVectorRealPtr cNetCfPtr(CreateSmart<CVectorReal>(inp_sShelf.miSims));
		CVectorRealPtr cPpmtPtr(CreateSmart<CVectorReal>(inp_sShelf.miSims));
		CVectorRealPtr cRecoveryPtr(CreateSmart<CVectorReal>(inp_sShelf.miSims));

		// transition
		convertProbabilities(*inp_sShelf.mcProbStayPtr, *inp_sShelf.mcProbDfltPtr, *inp_sShelf.mcProbPpmtPtr);
		std::swap(inp_sShelf.mcStatusBopPtr, inp_sShelf.mcStatusEopPtr);
		updateStatus(inp_sShelf);

		// loss & recovery
		*cDfltPtr = *(*inp_sShelf.mcStatusBopPtr)[CURRENT] * *inp_sShelf.mcProbDfltPtr * *inp_sShelf.mcSchedBalBopPtr;
		*cLossPtr = *cDfltPtr * *inp_sShelf.mcLgdPtr;
		*cRecoveryPtr = *cDfltPtr - *cLossPtr;

		// amortization & interest
		*inp_sShelf.mcWorkingPtr = (1.0 - *inp_sShelf.mcProbDfltPtr) * *(*inp_sShelf.mcStatusBopPtr)[CURRENT];
		*inp_sShelf.mcPrinPtr *= *inp_sShelf.mcWorkingPtr;
		*inp_sShelf.mcIntPtr *= *inp_sShelf.mcWorkingPtr;

		// prepayment and balance
		*inp_sShelf.mcSchedBalBopPtr = *inp_sShelf.mcBalEopPtr;
		*inp_sShelf.mcBalEopPtr *= *(*inp_sShelf.mcStatusEopPtr)[CURRENT];

		*cPpmtPtr = *(*inp_sShelf.mcStatusBopPtr)[CURRENT] * *inp_sShelf.mcProbPpmtPtr * *inp_sShelf.mcSchedBalBopPtr;

		// total payment
		*cNetCfPtr = *inp_sShelf.mcPrinPtr + *inp_sShelf.mcIntPtr + *cPpmtPtr + *cRecoveryPtr;

		(*inp_sShelf.msMyPtr->mcDetailPtr)[METRICS::DFLT]->add(inp_sShelf.mdtCurrent, cDfltPtr);
		(*inp_sShelf.msMyPtr->mcDetailPtr)[METRICS::LOSS]->add(inp_sShelf.mdtCurrent, cLossPtr);
		(*inp_sShelf.msMyPtr->mcDetailPtr)[METRICS::NET_CF]->add(inp_sShelf.mdtCurrent, cNetCfPtr);
		(*inp_sShelf.msMyPtr->mcDetailPtr)[METRICS::PPMT]->add(inp_sShelf.mdtCurrent, cPpmtPtr);
		(*inp_sShelf.msMyPtr->mcDetailPtr)[METRICS::RECOVERY]->add(inp_sShelf.mdtCurrent, cRecoveryPtr);
	}

	static void setCurrent(const MyBundle& inp_cItrs, SBehaviorPkt& inp_sShelf)
	{
		inp_sShelf.mdtCurrent = inp_cItrs.At<HERE::BAL>().key();

		inp_sShelf.mcBalEopPtr = inp_cItrs.At<HERE::BAL>().value();
		inp_sShelf.mcIntPtr = inp_cItrs.At<HERE::INT>().value();
		inp_sShelf.mcLgdPtr = inp_cItrs.At<HERE::LGD>().value();
		inp_sShelf.mcPrinPtr = inp_cItrs.At<HERE::PRIN>().value();
		inp_sShelf.mcProbDfltPtr = inp_cItrs.At<HERE::PROBDFLT>().value();
		inp_sShelf.mcProbPpmtPtr = inp_cItrs.At<HERE::PROBPPMT>().value();
	}

	static void settlement(CDate inp_dtSettle, size_t inp_iSims, CMetricDetail& inp_cDetail)
	{
		*inp_cDetail[METRICS::PROB_DFLT]->front().second = 0.0;
		*inp_cDetail[METRICS::PROB_PPMT]->front().second = 0.0;

		inp_cDetail[METRICS::DFLT]->add(inp_dtSettle, CreateSmart<CVectorReal>(inp_iSims, 0.0));
		inp_cDetail[METRICS::LOSS]->add(inp_dtSettle, CreateSmart<CVectorReal>(inp_iSims, 0.0));
		inp_cDetail[METRICS::PPMT]->add(inp_dtSettle, CreateSmart<CVectorReal>(inp_iSims, 0.0));
		inp_cDetail[METRICS::RECOVERY]->add(inp_dtSettle, CreateSmart<CVectorReal>(inp_iSims, 0.0));

		inp_cDetail[METRICS::NET_CF]->add(inp_dtSettle, CreateSmart<CVectorReal>(*inp_cDetail[METRICS::PRIN]->front().second + *inp_cDetail[METRICS::INT]->front().second));
	}

	static void updateStatus(SBehaviorPkt& inp_sShelf)
	{
		*(*inp_sShelf.mcStatusEopPtr)[CURRENT] = *(*inp_sShelf.mcStatusBopPtr)[CURRENT] * *inp_sShelf.mcProbStayPtr;
		*(*inp_sShelf.mcStatusEopPtr)[PAIDOFF] = *(*inp_sShelf.mcStatusBopPtr)[DEFAULT] + (*(*inp_sShelf.mcStatusBopPtr)[CURRENT] * *inp_sShelf.mcProbDfltPtr);
		*(*inp_sShelf.mcStatusEopPtr)[DEFAULT] = *(*inp_sShelf.mcStatusBopPtr)[PAIDOFF] + (*(*inp_sShelf.mcStatusBopPtr)[CURRENT] * *inp_sShelf.mcProbDfltPtr);
	}
};
#endif
