#ifndef INCLUDE_H_PBONDCONTRACT
#define INCLUDE_H_PBONDCONTRACT

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "FinTools.h"
#include "HolidayCalendar.h"
#include "Schematics.h"

static const string sBadHolidayLookup("No holiday calendar found");

SMARTSTRUCT(SWindow, (SCpnLst::size_type miStart; SCpnLst::size_type miEnd;), (miStart, miEnd));
typedef CSmartMap<CDate, SWindowPtr> CWindows;
typedef CSmartPtr<CWindows> CWindowsPtr;

SIMPLESTRUCT(SContractPkt,
	(size_t		miSims;
	CDate				mdtAsOf;
	CHolidayCalendarPtr	mcHolidaysPtr;

	SBondContractPtr	msMyPtr;
	SIrpPtr				msIrpPtr;

	CDate				mdtSettle;
	real				mdSettle;
	SCpnLst::size_type	miFirstPayPeriod;

	CVectorRealPtr		mcInitialBalPtr;
	CVectorRealPtr		mcInitialRatePtr;

	CVectorRealPtr		mcThisRatePtr;

	SCpnLstPtr			msCpnLstPtr;
	CWindowsPtr			mcResetKeyPtr;));

DEF_NODE(PBondContract, SBondContract, (SIrp), (), (SGlobals))
{
public:
	typedef PBondContract _Myt;

	bool Initialize(void)
	{
		SGlobalsPtr sGlobalsPtr;
		if (!View(sGlobalsPtr)) return false;

		return Initialize(sGlobalsPtr);
	}

	bool Initialize(SGlobalsPtr inp_sGlobalsPtr)
	{
		mcCalsItr.attach(inp_sGlobalsPtr->mcCalendarsPtr);

		mdtAsOf = inp_sGlobalsPtr->mdtMarket;
		miSims = inp_sGlobalsPtr->miScenarios * inp_sGlobalsPtr->miSimsPerScenario;

		return true; 
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		SContractPktPtr	sShelfPtr(CreateSmart<SContractPkt>());
		sShelfPtr->mdtAsOf = mdtAsOf;
		sShelfPtr->miSims = miSims;

		if (!mcCalsItr.find(inp_sSchematicPtr->msBondDefPtr->mHolidayCalendar))
		{
			FEEDBACK(GetFeedback(), 5001, sBadHolidayLookup + ": BondID(" + to_string(inp_sSchematicPtr->msBondDefPtr->mlBondId) + ") HolidayCalendar(" + to_string(inp_sSchematicPtr->msBondDefPtr->mHolidayCalendar) + ")");
			return PROCESS_STATUS::FAIL;
		}

		sShelfPtr->mcHolidaysPtr = mcCalsItr.value();
		sShelfPtr->msMyPtr = inp_sSchematicPtr;

		initialize(sShelfPtr);
		sShelfPtr->mcResetKeyPtr = calcResets(inp_sSchematicPtr->msBondDefPtr, sShelfPtr->msCpnLstPtr, sShelfPtr->miFirstPayPeriod);
		sShelfPtr->msIrpPtr = getSIrp(inp_sSchematicPtr->msBondDefPtr->mResetIndex, sShelfPtr->mcResetKeyPtr);

		if (sShelfPtr->mcResetKeyPtr->empty()) 
		{
			return makePayments(sShelfPtr); 
		}
		else
		{
			return Request(sShelfPtr->msIrpPtr, &_Myt::arm, sShelfPtr);
		}
	}

protected:
	typedef SCpnLst::size_type size_type;

	CDate	mdtAsOf;
	size_t	miSims;

	ItrSelector<CHolidayCalendarLst>::const_iterator	mcCalsItr;

	static inline PROCESS_STATUS arm(SContractPktPtr inp_sShelfPtr)
	{
		applyMargin(inp_sShelfPtr->msMyPtr->msBondDefPtr, inp_sShelfPtr->msIrpPtr->mcRatesPtr);
		applyCapsFloors(inp_sShelfPtr->msMyPtr->msBondDefPtr, !(inp_sShelfPtr->miFirstPayPeriod > inp_sShelfPtr->msMyPtr->msBondDefPtr->miFirstReset), inp_sShelfPtr->mcInitialRatePtr, inp_sShelfPtr->msIrpPtr->mcRatesPtr);

		return makePayments(inp_sShelfPtr);
	}

	static inline void applyCapsFloors(SBondDefPtr inp_sBondDefPtr, bool inp_bApplyFirstCapFloor, CVectorRealPtr inp_cInitialRatesPtr, CDataTablePtr inp_cRatesPtr)
	{
		auto itr(iterators::GetItr(inp_cRatesPtr));

		if (inp_sBondDefPtr->mdLifeRelCapFloor > 0)
		{
			const real dMax(inp_sBondDefPtr->mdRateOrigination + inp_sBondDefPtr->mdLifeRelCapFloor);
			const real dMin(inp_sBondDefPtr->mdRateOrigination - inp_sBondDefPtr->mdLifeRelCapFloor);

			LOOP(itr)
			{
				*itr.value() |= dMin;
				*itr.value() &= dMax;
			}
		}

		CVectorRealPtr cPriorPtr(inp_cInitialRatesPtr);

		itr.begin();
		if (inp_sBondDefPtr->mdFirstRelCapFloor > 0 && inp_bApplyFirstCapFloor) // 
		{
			*itr.value() |= (*cPriorPtr + inp_sBondDefPtr->mdFirstRelCapFloor);
			*itr.value() &= (*cPriorPtr - inp_sBondDefPtr->mdFirstRelCapFloor);

			cPriorPtr = itr.value();
			itr++;
		}

		if (inp_sBondDefPtr->mdPeriodicRelCapFloor > 0)
		{
			for (; !itr.isEnd(); itr++)
			{
				*itr.value() |= (*cPriorPtr + inp_sBondDefPtr->mdPeriodicRelCapFloor);
				*itr.value() &= (*cPriorPtr - inp_sBondDefPtr->mdPeriodicRelCapFloor);

				cPriorPtr = itr.value();
			}
		}
	}

	static inline void applyMargin(SBondDefPtr inp_sBondDefPtr, CDataTablePtr inp_cRatesPtr)
	{
		auto itr(iterators::GetItr(inp_cRatesPtr));
		auto foo = [&inp_sBondDefPtr](real& inp_dRate){ inp_dRate = CMath::roundMultiple(inp_dRate, inp_sBondDefPtr->mRoundingType, inp_sBondDefPtr->mdRoundingFactor); };

		LOOP(itr)
		{
			*itr.value() += inp_sBondDefPtr->mdMargin;
			itr.value()->foreach(foo);
		}
	}

	static inline CWindowsPtr calcResets(SBondDefPtr inp_sBondDefPtr, SCpnLstPtr inp_SCpnLstPtr, const size_type& inp_iFirstPayPeriod)
	{
		const uint32_t iLookback(inp_sBondDefPtr->miLookbackDays);
		const size_type iStep(inp_sBondDefPtr->miPayPeriodsPerReset);
		const size_type iPeriods(inp_sBondDefPtr->miPayPeriods);

		CWindowsPtr cResultPtr(CreateSmart<CWindows>());
		for (size_type iPeriod(inp_sBondDefPtr->miFirstReset); iPeriod < iPeriods; iPeriod += iStep)
		{
			if (iPeriod >= inp_iFirstPayPeriod)
			{
				SWindowPtr sWinPtr(CreateSmart<SWindow>());
				sWinPtr->miStart = iPeriod;
				sWinPtr->miEnd = min(iPeriod + iStep, iPeriods);

				cResultPtr->add((*inp_SCpnLstPtr)[iPeriod].mdtBop - iLookback, sWinPtr);
			}
		}

		return cResultPtr;
	}

	static inline void close(SContractPktPtr inp_sShelfPtr)
	{
		const CDate dtMaturity(inp_sShelfPtr->msMyPtr->msBondDefPtr->mdtMaturity);

		CVectorRealPtr cLastBalPtr(inp_sShelfPtr->msMyPtr->mcBalEopPtr->back().second);

		inp_sShelfPtr->msMyPtr->mcRatesPtr->add(dtMaturity, CreateSmart<CVectorReal>(*inp_sShelfPtr->mcThisRatePtr));
		inp_sShelfPtr->msMyPtr->mcPrinPtr->add(dtMaturity, CreateSmart<CVectorReal>(*cLastBalPtr));
		inp_sShelfPtr->msMyPtr->mcBalEopPtr->add(dtMaturity, CreateSmart<CVectorReal>(inp_sShelfPtr->miSims, 0.0));

		CVectorRealPtr cIntPtr(CreateSmart<CVectorReal>(inp_sShelfPtr->miSims));
		*cIntPtr = inp_sShelfPtr->msCpnLstPtr->back().mdCoupon * *inp_sShelfPtr->mcThisRatePtr * *cLastBalPtr;
		inp_sShelfPtr->msMyPtr->mcIntPtr->add(dtMaturity, cIntPtr);

	}

	static inline SIrpPtr getSIrp(MARKET_INDEX inp_MktIndex, CWindowsPtr inp_sWindowsPtr)
	{
		SIrpPtr sResultPtr(CreateSmart<SIrp>());
		sResultPtr->mMarketIndex = inp_MktIndex;
		sResultPtr->mcRatesPtr = CreateSmart<CDataTable>();

		inp_sWindowsPtr->foreach([&sResultPtr](CWindows::reference inp_Entry){ sResultPtr->mcRatesPtr->add(inp_Entry.first, CreateSmart<CVectorReal>()); });

		return sResultPtr;
	}

	static inline void initialize(SContractPktPtr inp_sShelfPtr)
	{
		SPeriodPtr		sPeriodPtr(CreateSmart<SPeriod>(inp_sShelfPtr->msMyPtr->msBondDefPtr->mFreq));
		CDateAdjPtr		cAdjPtr(CFinTools::CreateDateAdjustor(inp_sShelfPtr->msMyPtr->msBondDefPtr->mBusDayConv, inp_sShelfPtr->mcHolidaysPtr));
		CYearFracPtr	cFracPtr(CFinTools::CreateYearFrac(inp_sShelfPtr->msMyPtr->msBondDefPtr->mDayCountConv, *sPeriodPtr));

		inp_sShelfPtr->msCpnLstPtr = CreateSmart<SCpnLst>(inp_sShelfPtr->msMyPtr->msBondDefPtr->miPayPeriods);
		CCpnSchedPtr cSchedPtr(CFinTools::CreateCouponScheduler(inp_sShelfPtr->msMyPtr->msBondDefPtr->mDateGen, cAdjPtr, cFracPtr, inp_sShelfPtr->msMyPtr->msBondDefPtr->mdtDated, inp_sShelfPtr->msMyPtr->msBondDefPtr->mdtIssue, inp_sShelfPtr->msMyPtr->msBondDefPtr->mdtMaturity, *sPeriodPtr, inp_sShelfPtr->msCpnLstPtr));

		inp_sShelfPtr->mdtSettle = max(inp_sShelfPtr->msMyPtr->msBondDefPtr->mdtIssue, inp_sShelfPtr->mdtAsOf);
		inp_sShelfPtr->mdSettle = cSchedPtr->GetAccrual(inp_sShelfPtr->mdtSettle);
		inp_sShelfPtr->miFirstPayPeriod = static_cast<size_type>(cSchedPtr->GetAge(inp_sShelfPtr->mdtSettle)) + 1;

		inp_sShelfPtr->mcInitialBalPtr = CreateSmart<CVectorReal>(inp_sShelfPtr->miSims);
		*inp_sShelfPtr->mcInitialBalPtr = (inp_sShelfPtr->mdtSettle == inp_sShelfPtr->mdtAsOf ? inp_sShelfPtr->msMyPtr->msBondDefPtr->mdBalCurrent : inp_sShelfPtr->msMyPtr->msBondDefPtr->mdBalOrigination);

		inp_sShelfPtr->mcInitialRatePtr = CreateSmart<CVectorReal>(inp_sShelfPtr->miSims);
		*inp_sShelfPtr->mcInitialRatePtr = inp_sShelfPtr->msMyPtr->msBondDefPtr->mdRateCurrent;

		inp_sShelfPtr->mcThisRatePtr = inp_sShelfPtr->mcInitialRatePtr;
	}

	static inline PROCESS_STATUS makePayments(SContractPktPtr inp_sShelfPtr)
	{
		settle(inp_sShelfPtr);
		pay(inp_sShelfPtr);
		close(inp_sShelfPtr);

		return PROCESS_STATUS::DONE;
	}

	static inline void pay(SContractPktPtr inp_sShelfPtr)
	{
		CVectorRealPtr cPeriodicRatePtr(CreateSmart<CVectorReal>(inp_sShelfPtr->miSims));

		auto itrs(iterators::MakeBundleConst(inp_sShelfPtr->mcResetKeyPtr, inp_sShelfPtr->msIrpPtr->mcRatesPtr));

		*cPeriodicRatePtr = *inp_sShelfPtr->mcInitialRatePtr * (*inp_sShelfPtr->msCpnLstPtr)[inp_sShelfPtr->miFirstPayPeriod].mdCoupon;
		CAmortizerPtr cAmortPtr(CFinTools::CreateAmortizer(inp_sShelfPtr->msMyPtr->msBondDefPtr->mAmortType, inp_sShelfPtr->msMyPtr->msBondDefPtr->miAmortPeriods - inp_sShelfPtr->miFirstPayPeriod + 1, *cPeriodicRatePtr, *inp_sShelfPtr->mcInitialBalPtr, inp_sShelfPtr->msMyPtr->msBondDefPtr->mdPmtCurrent));

		size_type iNextStart(inp_sShelfPtr->mcResetKeyPtr->empty() ? inp_sShelfPtr->msMyPtr->msBondDefPtr->miPayPeriods + 1 : inp_sShelfPtr->mcResetKeyPtr->front().second->miStart);
		for (size_type iPayPeriod(inp_sShelfPtr->miFirstPayPeriod); iPayPeriod < inp_sShelfPtr->msMyPtr->msBondDefPtr->miPayPeriods; iPayPeriod++)
		{
			SCpn& sCpn((*inp_sShelfPtr->msCpnLstPtr)[iPayPeriod - 1]);

			if (iPayPeriod == iNextStart)
			{
				inp_sShelfPtr->mcThisRatePtr = itrs.At<1>().value();
				itrs++;
				iNextStart = itrs.At<0>().value()->miStart;

				*cPeriodicRatePtr = *inp_sShelfPtr->mcThisRatePtr * sCpn.mdCoupon;
				cAmortPtr->Recast(*cPeriodicRatePtr);
			}
			else
			{
				*cPeriodicRatePtr = *inp_sShelfPtr->mcThisRatePtr * sCpn.mdCoupon;
			}

			cAmortPtr->Age(*cPeriodicRatePtr);

			inp_sShelfPtr->msMyPtr->mcRatesPtr->add(sCpn.mdtEop, CreateSmart<CVectorReal>(*inp_sShelfPtr->mcThisRatePtr));
			inp_sShelfPtr->msMyPtr->mcIntPtr->add(sCpn.mdtEop, CreateSmart<CVectorReal>(cAmortPtr->GetInterest()));
			inp_sShelfPtr->msMyPtr->mcPrinPtr->add(sCpn.mdtEop, CreateSmart<CVectorReal>(cAmortPtr->GetPrincipal()));
			inp_sShelfPtr->msMyPtr->mcBalEopPtr->add(sCpn.mdtEop, CreateSmart<CVectorReal>(cAmortPtr->GetEndingBalance()));
		}
	}

	static inline void settle(SContractPktPtr inp_sShelfPtr)
	{
		inp_sShelfPtr->msMyPtr->mcRatesPtr->add(inp_sShelfPtr->mdtSettle, CreateSmart<CVectorReal>(*inp_sShelfPtr->mcInitialRatePtr));
		inp_sShelfPtr->msMyPtr->mcBalEopPtr->add(inp_sShelfPtr->mdtSettle, CreateSmart<CVectorReal>(*inp_sShelfPtr->mcInitialBalPtr));
		inp_sShelfPtr->msMyPtr->mcPrinPtr->add(inp_sShelfPtr->mdtSettle, CreateSmart<CVectorReal>(inp_sShelfPtr->miSims, 0.0));

		CVectorRealPtr cIntPtr(CreateSmart<CVectorReal>(inp_sShelfPtr->miSims));
		*cIntPtr = inp_sShelfPtr->mdSettle * *inp_sShelfPtr->mcInitialRatePtr * *inp_sShelfPtr->mcInitialBalPtr;
		inp_sShelfPtr->msMyPtr->mcIntPtr->add(inp_sShelfPtr->mdtSettle, cIntPtr);
	}
};
#endif
