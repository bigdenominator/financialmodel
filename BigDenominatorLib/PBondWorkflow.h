#ifndef INCLUDE_H_PBONDWORKFLOW
#define INCLUDE_H_PBONDWORKFLOW

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "Schematics.h"

SIMPLESTRUCT(SBondWorkflowPkt,
	(SBondDefPtr		msBondDefPtr;
	CMetricDetailPtr	mcDetailPtr;
	CVectorRealPtr		mcOasPtr;
	CVectorRealPtr		mcRiskFreePtr;
	CVectorRealPtr		mcValuesPtr;))

DEF_NODE(PBondWorkflow, SBondDef, (SBondContract, SPrepay, SDefault, SSeverity, SBondBehavior, SPriceOAS), (SBond), ())
{
public:
	typedef PBondWorkflow _Myt;

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		SBondWorkflowPktPtr	sShelfPtr(CreateSmart<SBondWorkflowPkt>());
		sShelfPtr->msBondDefPtr = inp_sSchematicPtr;
		sShelfPtr->mcDetailPtr = CreateSmart<CMetricDetail>();
		for (size_t i(0); i < METRIC_COUNT::value; i++)
		{
			(*sShelfPtr->mcDetailPtr)[i] = CreateSmart<CDataTable>();
		}
		sShelfPtr->mcOasPtr = CreateSmart<CVectorReal>();
		sShelfPtr->mcRiskFreePtr = CreateSmart<CVectorReal>();
		sShelfPtr->mcValuesPtr = CreateSmart<CVectorReal>();

		return requestContract(sShelfPtr);
	};

protected:
	PROCESS_STATUS requestBehavior(SBondWorkflowPktPtr inp_sShelfPtr)
	{
		SBondBehaviorPtr sTempPtr(CreateSmart<SBondBehavior>());
		sTempPtr->mStatus = inp_sShelfPtr->msBondDefPtr->mStatus;
		sTempPtr->mcDetailPtr = inp_sShelfPtr->mcDetailPtr;

		return Request(sTempPtr, &_Myt::requestPriceOas, *this, inp_sShelfPtr);
	}

	PROCESS_STATUS requestContract(SBondWorkflowPktPtr inp_sShelfPtr)
	{
		SBondContractPtr sTempPtr(CreateSmart<SBondContract>());
		sTempPtr->msBondDefPtr = inp_sShelfPtr->msBondDefPtr;
		sTempPtr->mcRatesPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::RATES];
		sTempPtr->mcBalEopPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::BAL_EOP];
		sTempPtr->mcIntPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::INT];
		sTempPtr->mcPrinPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::PRIN];

		return Request(sTempPtr, &_Myt::requestPrepay, *this, inp_sShelfPtr);
	}

	PROCESS_STATUS requestDefault(SBondWorkflowPktPtr inp_sShelfPtr)
	{
		SDefaultPtr sTempPtr(CreateSmart<SDefault>());
		sTempPtr->msBondDefPtr = inp_sShelfPtr->msBondDefPtr;
		sTempPtr->mcCfPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::BAL_EOP];
		sTempPtr->mcXBetaPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::PROB_DFLT];

		return Request(sTempPtr, &_Myt::requestSeverity, *this, inp_sShelfPtr);
	}

	PROCESS_STATUS requestPrepay(SBondWorkflowPktPtr inp_sShelfPtr)
	{
		SPrepayPtr sTempPtr(CreateSmart<SPrepay>());
		sTempPtr->msBondDefPtr = inp_sShelfPtr->msBondDefPtr;
		sTempPtr->mcCfPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::BAL_EOP];
		sTempPtr->mcXBetaPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::PROB_PPMT];

		return Request(sTempPtr, &_Myt::requestDefault, *this, inp_sShelfPtr);
	}

	PROCESS_STATUS requestPriceOas(SBondWorkflowPktPtr inp_sShelfPtr)
	{
		SPriceOASPtr sTempPtr(CreateSmart<SPriceOAS>());
		sTempPtr->msBondDefPtr = inp_sShelfPtr->msBondDefPtr;
		sTempPtr->mcNetCfPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::NET_CF];
		sTempPtr->mcOasPtr = inp_sShelfPtr->mcOasPtr;
		sTempPtr->mcRiskFreePtr = inp_sShelfPtr->mcRiskFreePtr;
		sTempPtr->mcValuesPtr = inp_sShelfPtr->mcValuesPtr;

		return Request(sTempPtr, &_Myt::sendBond, *this, inp_sShelfPtr);
	}

	PROCESS_STATUS requestSeverity(SBondWorkflowPktPtr inp_sShelfPtr)
	{
		SSeverityPtr sTempPtr(CreateSmart<SSeverity>());
		sTempPtr->mcCfPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::BAL_EOP];
		sTempPtr->mcSeverityPtr = (*inp_sShelfPtr->mcDetailPtr)[METRICS::LGD];

		return Request(sTempPtr, &_Myt::requestBehavior, *this, inp_sShelfPtr);
	}
	
	PROCESS_STATUS sendBond(SBondWorkflowPktPtr inp_sShelfPtr)
	{
		SBondPtr sTempPtr(CreateSmart<SBond>());
		sTempPtr->mlBondId = inp_sShelfPtr->msBondDefPtr->mlBondId;
		sTempPtr->mcDetailPtr = inp_sShelfPtr->mcDetailPtr;
		sTempPtr->mcOasPtr = inp_sShelfPtr->mcOasPtr;
		sTempPtr->mcRiskFreeValuesPtr = inp_sShelfPtr->mcRiskFreePtr;
		sTempPtr->mcValuesPtr = inp_sShelfPtr->mcValuesPtr;

		return Send(sTempPtr) ? PROCESS_STATUS::DONE : PROCESS_STATUS::FAIL;
	}
};
#endif