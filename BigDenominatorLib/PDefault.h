#ifndef INCLUDE_H_PDEFAULT
#define INCLUDE_H_PDEFAULT

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "Schematics.h"
#include "FinLib.h"

DEF_NODE(PDefault, SDefault, (), (), ())
{
public:
	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		auto itr(iterators::GetConstItr(inp_sSchematicPtr->mcCfPtr));

		const size_t iSims(itr.value()->size());
		uint32_t iPeriod(inp_sSchematicPtr->msBondDefPtr->miPayPeriods - static_cast<uint32_t>(itr.data()->size()) + 2);

		inp_sSchematicPtr->mcXBetaPtr->add(itr.key(), CreateSmart<CVectorReal>(iSims, dNOPPMTXBETA));
		for (itr++; !itr.isEnd(); itr++)
		{
			inp_sSchematicPtr->mcXBetaPtr->add(itr.key(), CreateSmart<CVectorReal>(iSims, getXBeta(iPeriod++)));
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	static inline real getXBeta(const uint32_t& inp_iPeriod)
	{
		return CLogistic::StandardInverseCDF(fin::Cpr2Smm(0.002));
	}
};
#endif
