#ifndef INCLUDE_H_PLOSSSEVERITY
#define INCLUDE_H_PLOSSSEVERITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "Schematics.h"

DEF_NODE(PLossSeverity, SSeverity, (), (), ())
{
public:
	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		auto itr(iterators::GetConstItr(inp_sSchematicPtr->mcCfPtr));

		inp_sSchematicPtr->mcSeverityPtr->add(itr.key(), CreateSmart<CVectorReal>(itr.value()->size(), 0.0));
		for (itr++; !itr.isEnd(); itr++)
		{
			inp_sSchematicPtr->mcSeverityPtr->add(itr.key(), CreateSmart<CVectorReal>(itr.value()->size(), getSeverity()));
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	static inline real getSeverity(void) { return 0.2; }
};
#endif
