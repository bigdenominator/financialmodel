#ifndef INCLUDE_H_PPREPAY
#define INCLUDE_H_PPREPAY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "FinLib.h"
#include "Schematics.h"


DEF_NODE(PPrepay, SPrepay, (), (), ())
{
public:
	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		auto itr(iterators::GetConstItr(inp_sSchematicPtr->mcCfPtr));

		const size_t iSims(itr.value()->size());
		uint32_t iPeriod(inp_sSchematicPtr->msBondDefPtr->miPayPeriods - static_cast<uint32_t>(itr.data()->size()) + 2);

		// Example: interest rate dependency
		//SIrpPtr sIrpPtr(SIrp>());
		//sIrpPtr->mRateIndex = inp_sPsaPtr->Member<SPsa::BOND_DEF>()->mRateIndex;
		//sIrpPtr->mcDatesPtr = XXXXX;
		//sIrpPtr->mcRatesPtr = XXXXX;
		//if (!Request(sIrpPtr)) return false;

		inp_sSchematicPtr->mcXBetaPtr->add(itr.key(), CreateSmart<CVectorReal>(iSims, dNOPPMTXBETA));
		for (itr++; !itr.isEnd(); itr++)
		{
			inp_sSchematicPtr->mcXBetaPtr->add(itr.key(), CreateSmart<CVectorReal>(iSims, getXBeta(iPeriod++)));
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	static inline real getXBeta(const uint32_t& inp_iPeriod)
	{
		switch (inp_iPeriod)
		{
		case 1: return -8.69843049593809;
		case 2: return -8.00419722075471;
		case 3: return -7.59764416893553;
		case 4: return -7.30887229690936;
		case 5: return -7.08463708334344;
		case 6: return -6.90122199476805;
		case 7: return -6.74597590673788;
		case 8: return -6.61134722255796;
		case 9: return -6.49246500502326;
		case 10: return -6.38600341015347;
		case 11: return -6.28959024675282;
		case 12: return -6.20147397469111;
		case 13: return -6.12032445333735;
		case 14: return -6.04510774172187;
		case 15: return -5.97500419777658;
		case 16: return -5.90935306392716;
		case 17: return -5.84761388184626;
		case 18: return -5.78933895284771;
		case 19: return -5.73415325414015;
		case 20: return -5.68173951260898;
		case 21: return -5.63182692411835;
		case 22: return -5.58418249946963;
		case 23: return -5.53860433563296;
		case 24: return -5.49491632009238;
		case 25: return -5.45296391694549;
		case 26: return -5.41261077996651;
		case 27: return -5.37373600521904;
		case 28: return -5.33623188355865;
		case 29: return -5.30000204770068;
		default: return -5.26495993354192;
		}
	}
};
#endif