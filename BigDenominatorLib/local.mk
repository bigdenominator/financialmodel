ifndef SETUP_FINBD
SETUP_FINBD = TRUE

# Financial model depends on framework
include ../../utilities/Make/common.mk
include ../financial.mk

# Specifics to build static / dynamic libraryi
FINBD_DIR	= $(FINANCIAL_ROOT)/BigDenominatorLib
INCLUDE_DIRS	+= -I$(FINBD_DIR)

FINBD			:= BigDenominatorLib.bd
FINBD_VER_MAJOR		= 1
FINBD_VER_MINOR		= 0
FINBD_VER_RELEASE	= 1
FINBD_EXT		= $(FINBD_VER_MAJOR).$(FINBD_VER_MINOR).$(FINBD_VER_RELEASE)

FINBD_REF_REL		= $(FIN_BIN_REL)/$(FINBD)
FINBD_LINK_REL		= $(FINBD_REF_REL).$(FINBD_EXT)

FINBD_REF_DBG		= $(FIN_BIN_DBG)/$(FINBD)
FINBD_LINK_DBG		= $(FINBD_REF_DBG).$(FINBD_EXT)

endif
