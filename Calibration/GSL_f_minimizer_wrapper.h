#ifndef GSL_F_MINIMIZER_WRAPPER_H
#define GSL_F_MINIMIZER_WRAPPER_H

#include <string>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multimin.h>
#include <iostream>

#include "VolatilityCalibration.h"

int GSL_f_minimizer_wrapper(double (*func) (const gsl_vector *, void *),
			    const int& inp_iDimension, SVolCalibrationInputPtr & inp_dFuncParamsPtr,
                            double inp_dStartPt[], const double inp_dSpan[],
                            const int& inp_iMaxIter, const double& inp_dTol,
			    const std::string& inp_minimizer="simplex2", const bool& inp_bVerbose=true, const int& inp_outputFrequency=1)
{
  // Using GSL routines to minimize a function without calculating the derivatives.
  //	The function is passed in as a function pointer. Its dimension and parameters are also passed in
  //		as arguments (inp_iDimension and inp_dFuncParams).
  //	inp_iDimension + 1 initial points are required to start the search process. They are determined
  //		using the single input point, inp_dStartPt, and its distance to any one of the rest of
  //		the initial points. The latter is provided by inp_dSpan.
  //    The stopping creterion is controlled by the maximum number of iteration, inp_iMaxIter, and the
  //		tolerance on the "size" of the search box, inp_dTol.
  //	Choice of minimizer can also be made through input: simple, simplex2, simplex2rand. Default is simplex2.
  //	inp_bVerbose is used to controll the amount of message written by this routine.
  //    The best fit parameters are passed back using the input starting point (inp_dStartPt[])
  //
  std::string simplex ("simplex");
  std::string simplex2 ("simplex2");
  std::string simplex2rand ("simplex2rand");

  const gsl_multimin_fminimizer_type *T;

  T = gsl_multimin_fminimizer_nmsimplex;
  if (simplex.compare(inp_minimizer) == 0)      { T = gsl_multimin_fminimizer_nmsimplex; }
  if (simplex2.compare(inp_minimizer) == 0)     { T = gsl_multimin_fminimizer_nmsimplex2; }
  if (simplex2rand.compare(inp_minimizer) == 0) { T = gsl_multimin_fminimizer_nmsimplex2rand; }

  gsl_multimin_fminimizer *s = NULL;
  gsl_vector *span, *x;
  gsl_multimin_function minex_func;

  size_t iter = 0;
  int status;
  double size;

  x = gsl_vector_alloc (inp_iDimension);		// Starting point
  span = gsl_vector_alloc (inp_iDimension);		// Span
  for (int i=0; i < inp_iDimension; i++)
  {
    gsl_vector_set (x, i, inp_dStartPt[i]);
    gsl_vector_set (span, i, inp_dSpan[i]);
  }

  /* Initialize method and iterate */
  minex_func.n = inp_iDimension;
  minex_func.f = func;
  minex_func.params = inp_dFuncParamsPtr;

  s = gsl_multimin_fminimizer_alloc (T, inp_iDimension);
  gsl_multimin_fminimizer_set (s, &minex_func, x, span);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);
      
      if (status) 
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, inp_dTol);

      if (status == GSL_SUCCESS)
        {
          std::cout << "converged to minimum at\n";
        }

      if ((inp_bVerbose && (iter%inp_outputFrequency == 0)) || status == GSL_SUCCESS)
      {
         std::cout << "\t" << iter;
	 for (int i=0; i < inp_iDimension; i++) { std::cout << "\t" << gsl_vector_get (s->x, i); }
         std::cout << "\tf() = " << s->fval << "\tsize = " << size << std::endl;
      }
    }
  while (status == GSL_CONTINUE && iter < inp_iMaxIter);

  // pass back the best fit parameters using the input starting point
  for (int i=0; i < inp_iDimension; i++) { inp_dStartPt[i] = gsl_vector_get (s->x, i); }
  
  gsl_vector_free(x);
  gsl_vector_free(span);
  gsl_multimin_fminimizer_free (s);

  return status;
}

typedef double(*gsl_min_function) (const gsl_vector *, void *);
int GSL_f_minimizer_wrapper_v2(gsl_min_function func,
	const int& inp_iDimension, SVolCalibrationInputPtr inp_dFuncParamsPtr,
	double inp_dStartPt[], const double inp_dSpan[],
	const int& inp_iMaxIter, const double& inp_dTol)
{
	// Using GSL routines to minimize a function without calculating the derivatives.
	//	The function is passed in as a function pointer. Its dimension and parameters are also passed in
	//		as arguments (inp_iDimension and inp_dFuncParams).
	//	inp_iDimension + 1 initial points are required to start the search process. They are determined
	//		using the single input point, inp_dStartPt, and its distance to any one of the rest of
	//		the initial points. The latter is provided by inp_dSpan.
	//    The stopping creterion is controlled by the maximum number of iteration, inp_iMaxIter, and the
	//		tolerance on the "size" of the search box, inp_dTol.
	//	Choice of minimizer can also be made through input: simple, simplex2, simplex2rand. Default is simplex2.
	//	inp_bVerbose is used to controll the amount of message written by this routine.
	//    The best fit parameters are passed back using the input starting point (inp_dStartPt[])
	//
	const gsl_multimin_fminimizer_type *T;
	T = gsl_multimin_fminimizer_nmsimplex2;
	
	gsl_multimin_fminimizer *s = NULL;
	gsl_vector *span, *x;
	gsl_multimin_function minex_func;

	double size;

	x = gsl_vector_alloc(inp_iDimension);		// Starting point
	span = gsl_vector_alloc(inp_iDimension);		// Span
	for (int i = 0; i < inp_iDimension; i++)
	{
		gsl_vector_set(x, i, inp_dStartPt[i]);
		gsl_vector_set(span, i, inp_dSpan[i]);
	}

	/* Initialize method and iterate */
	minex_func.n = inp_iDimension;
	minex_func.f = func;
	minex_func.params = inp_dFuncParamsPtr;

	s = gsl_multimin_fminimizer_alloc(T, inp_iDimension);
	gsl_multimin_fminimizer_set(s, &minex_func, x, span);

	int iStatus(GSL_CONTINUE), iIter;
	for (iIter = 1; iStatus == GSL_CONTINUE && iIter < inp_iMaxIter; iIter++)
	{
		iStatus = gsl_multimin_fminimizer_iterate(s);

		if (iStatus) break;

		size = gsl_multimin_fminimizer_size(s);
		iStatus = gsl_multimin_test_size(size, inp_dTol);
	}

	if (iStatus == GSL_SUCCESS)
	{
		std::cout << "converged to minimum at\n";
		std::cout << "\t" << iIter;
		for (int i(0); i < inp_iDimension; i++)
		{
			std::cout << "\t" << gsl_vector_get(s->x, i);
			inp_dStartPt[i] = gsl_vector_get(s->x, i);
		}
		std::cout << "\tf() = " << s->fval << "\tsize = " << size << std::endl;
	}
	
	gsl_vector_free(x);
	gsl_vector_free(span);
	gsl_multimin_fminimizer_free(s);

	return iStatus;
}

template<class Func> int gslMinimizer(Func inp_Func,
	const int& inp_iDimension, double inp_dStartPt[], double inp_dSpan[],
	const int& inp_iMaxIter, const double& inp_dTol)
{
	// Using GSL routines to minimize a function without calculating the derivatives.
	//	The function is passed in as a function pointer. Its dimension and parameters are also passed in
	//		as arguments (inp_iDimension and inp_dFuncParams).
	//	inp_iDimension + 1 initial points are required to start the search process. They are determined
	//		using the single input point, inp_dStartPt, and its distance to any one of the rest of
	//		the initial points. The latter is provided by inp_dSpan.
	//    The stopping creterion is controlled by the maximum number of iteration, inp_iMaxIter, and the
	//		tolerance on the "size" of the search box, inp_dTol.
	//	Choice of minimizer can also be made through input: simple, simplex2, simplex2rand. Default is simplex2.
	//	inp_bVerbose is used to controll the amount of message written by this routine.
	//    The best fit parameters are passed back using the input starting point (inp_dStartPt[])
	//
	const gsl_multimin_fminimizer_type *T;
	T = gsl_multimin_fminimizer_nmsimplex2;

	gsl_vector *span, *x;

	x = gsl_vector_alloc(inp_iDimension);		// Starting point
	span = gsl_vector_alloc(inp_iDimension);		// Span
	for (int i = 0; i < inp_iDimension; i++)
	{
		gsl_vector_set(x, i, inp_dStartPt[i]);
		gsl_vector_set(span, i, inp_dSpan[i]);
	}

	gsl_min_function foo = [](const gsl_vector * v, void * p)->double{
		const int iSize(static_cast<int>(v->size)); cout << "v->size = " << v->size << endl;
		CSmartPtr<double> dX(new double(iSize));
		for (int i(0); i < iSize; i++) dX.Get()[i] = gsl_vector_get(v, i);
		return (*static_cast<Func*>(p))(dX.Get()); };

	/* Initialize method and iterate */
	gsl_multimin_function minex_func;
	minex_func.n = inp_iDimension;
	minex_func.f = foo;
	minex_func.params = &inp_Func;

	gsl_multimin_fminimizer *s = NULL;
	s = gsl_multimin_fminimizer_alloc(T, inp_iDimension);
	gsl_multimin_fminimizer_set(s, &minex_func, x, span);

	double dTestSize;
	int iStatus(GSL_CONTINUE);
	for (int iIter(1); iStatus == GSL_CONTINUE && iIter < inp_iMaxIter; iIter++)
	{
		iStatus = gsl_multimin_fminimizer_iterate(s);

		if (iStatus) break;

		dTestSize = gsl_multimin_fminimizer_size(s);
		iStatus = gsl_multimin_test_size(dTestSize, inp_dTol);
	}

	if (iStatus == GSL_SUCCESS)
	{
		std::cout << "converged to minimum at\n";
		//std::cout << "\t" << iIter;
		for (int i(0); i < inp_iDimension; i++)
		{
			std::cout << "\t" << gsl_vector_get(s->x, i);
			inp_dStartPt[i] = gsl_vector_get(s->x, i);
		}
		std::cout << "\tf() = " << s->fval << "\tsize = " << dTestSize << std::endl;
	}

	gsl_vector_free(x);
	gsl_vector_free(span);
	gsl_multimin_fminimizer_free(s);

	return iStatus;
}
#endif
