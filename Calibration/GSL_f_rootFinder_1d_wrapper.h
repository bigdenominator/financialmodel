#ifndef GSL_F_ROOTFINDER_1D_WRAPPER_H
#define GSL_F_ROOTFINDER_1D_WRAPPER_H

#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>

#include <string>

//#include "demo_fn.h"
//#include "demo_fn.c"

int GSL_f_rootFinder_1d_wrapper (double (*func) (double, void *),
                            double inp_dFuncParams[],
                            double & x_lo, double & x_hi, const double & inp_dExpectedRoot,
                            const int& inp_iMaxIter, const double& epsabs, const double & epsrel,
			    double & out_dRoot,
                            const std::string& inp_rootFinder="BrentDekker", const bool& inp_bVerbose=true)
{
  std::string biSection ("biSection");
  std::string falsePositive ("falsePositive");
  std::string BrentDekker ("BrentDekker");

  const gsl_root_fsolver_type *T;

  T = gsl_root_fsolver_bisection;
  if (biSection.compare(inp_rootFinder) == 0)     { T = gsl_root_fsolver_bisection; }
  if (falsePositive.compare(inp_rootFinder) == 0) { T = gsl_root_fsolver_falsepos; }
  if (BrentDekker.compare(inp_rootFinder) == 0)   { T = gsl_root_fsolver_brent; }

  gsl_function F;

  F.function = func;
  F.params = inp_dFuncParams;

  gsl_root_fsolver *s;
  s = gsl_root_fsolver_alloc (T);

  gsl_root_fsolver_set (s, &F, x_lo, x_hi);

  int iter = 0;
  double r = 0;
  int status;

  if (inp_bVerbose)
  {
  	printf ("using %s method\n", gsl_root_fsolver_name (s));

  	printf ("%5s [%9s, %9s] %9s %10s %9s\n",
          "iter", "lower", "upper", "root", "err", "err(est)");
  }

  do
    {
      iter++;
      status = gsl_root_fsolver_iterate (s);
      r = gsl_root_fsolver_root (s);
      x_lo = gsl_root_fsolver_x_lower (s);
      x_hi = gsl_root_fsolver_x_upper (s);
      status = gsl_root_test_interval (x_lo, x_hi, epsabs, epsrel);

      /* if (status == GSL_SUCCESS) printf ("Root finding converged:\n");

      if (inp_bVerbose || status == GSL_SUCCESS)
      {
      	printf ("%5d [%.7f, %.7f] %.7f %+.7f %.7f\n",
              iter, x_lo, x_hi, r, r - inp_dExpectedRoot, x_hi - x_lo);
      } */
    }
  while (status == GSL_CONTINUE && iter < inp_iMaxIter);

  out_dRoot = r;

  gsl_root_fsolver_free (s);

  return status;
}

template<class Func> int gslRootFinder(Func inp_Func,
	double & dLow, double & dHigh,
	const int& inp_iMaxIter, const double& dToleranceAbsolute, const double & dToleranceRelative,
	double & out_dRoot)
{
	const gsl_root_fsolver_type *T;
	T = gsl_root_fsolver_brent;

	gsl_function F;
	F.function = [](double x, void * p)->double{return (*static_cast<Func*>(p))(x); };
	F.params = &inp_Func;

	gsl_root_fsolver *s;
	s = gsl_root_fsolver_alloc(T);

	gsl_root_fsolver_set(s, &F, dLow, dHigh);

	int iStatus(GSL_CONTINUE);
	for (int iIter(0); iStatus == GSL_CONTINUE && iIter < inp_iMaxIter; iIter++)
	{
		iStatus = gsl_root_fsolver_iterate(s);
		out_dRoot = gsl_root_fsolver_root(s);
		dLow = gsl_root_fsolver_x_lower(s);
		dHigh = gsl_root_fsolver_x_upper(s);
		iStatus = gsl_root_test_interval(dLow, dHigh, dToleranceAbsolute, dToleranceRelative);
	}
	gsl_root_fsolver_free(s);

	return iStatus;
}
#endif
