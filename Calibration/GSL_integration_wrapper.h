#ifndef GSL_INTEGRATION_WRAPPER_H
#define GSL_INTEGRATION_WRAPPER_H

#include <math.h>
#include <gsl/gsl_integration.h>

double GSL_integration_wrapper(double (*func) (double, void *), double inp_dFuncParams[],
                            const double inp_dLowerLimit, const double inp_dUpperLimit, size_t& inp_iMaxNumOfIntervals,
                            const double& epsabs, const double& epsrel, double& out_dAbsErr,
			    size_t& workSpaceSize, const int GaussKronrodPtsKey=3, const bool& inp_bVerbose=true)
{
  // Using GSL routines to do integration

  double	out_dResult;
  bool		bIntegratorDebug(false);

  gsl_integration_workspace * workSpace = gsl_integration_workspace_alloc (workSpaceSize);

  gsl_function F;
  F.function = func;
  F.params = inp_dFuncParams;

  gsl_integration_qag (&F, inp_dLowerLimit, inp_dUpperLimit, epsabs, epsrel, inp_iMaxNumOfIntervals,
                        GaussKronrodPtsKey, workSpace, &out_dResult, &out_dAbsErr);

  if (inp_bVerbose && bIntegratorDebug)
  {
    printf ("result          = % .18f\n", out_dResult);
    printf ("estimated error = % .18f\n", out_dAbsErr);
    printf ("intervals =  %d\n", workSpace->size);
  }

  gsl_integration_workspace_free (workSpace);

  return out_dResult;
}

#endif
