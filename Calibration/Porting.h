#ifndef VOL_CALIBRATION_PORT_H
#define VOL_CALIBRATION_PORT_H

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <iomanip>
#include <vector>
#include <omp.h>

#include <gsl/gsl_cdf.h> 	// cumulative distribution of N(0,1): gsl_cdf_ugaussian_P (x); Manual P260/276
#include "GSL_f_rootFinder_1d_wrapper.h"
#include "GSL_f_minimizer_wrapper.h"

#include "HolidayCalendar.h"
#include "YieldCurve.h"

using namespace std;


static const size_t					mlMaxSize = 1000;
static const ePaymentPeriods		mLastPaymentPeriodToUse = ePaymentPeriods::T10Y;
static const string					msDelimiter = "-";

const string sPmtPeriods[mlMaxSize] = { "T0", "T3M", "T6M", "T9M",
"T1Y", "T1Y3M", "T1Y6M", "T1Y9M",
"T2Y", "T2Y3M", "T2Y6M", "T2Y9M",
"T3Y", "T3Y3M", "T3Y6M", "T3Y9M",
"T4Y", "T4Y3M", "T4Y6M", "T4Y9M",
"T5Y", "T5Y3M", "T5Y6M", "T5Y9M",
"T6Y", "T6Y3M", "T6Y6M", "T6Y9M",
"T7Y", "T7Y3M", "T7Y6M", "T7Y9M",
"T8Y", "T8Y3M", "T8Y6M", "T8Y9M",
"T9Y", "T9Y3M", "T9Y6M", "T9Y9M",
"T10Y", "T11Y", "T12Y", "T13Y", "T14Y", "T15Y", "T16Y", "T17Y", "T18Y", "T19Y", "T20Y",
"TLast" };

static long mlCount;

class CCalibration
{
private:
	struct SBlackScholesParams
	{
		double dDiscountFactor;
		double dDeltaT;
		double dForwardPrice;
		double dStrikePrice;
		double dYearFracOfTexpire;
	};
	typedef CSmartPtr<SBlackScholesParams> SBlackScholesParamsPtr;

	static double BlackScholesPrice(const double& inp_dBlackVol, SBlackScholesParamsPtr inp_sParamsPtr)
	{
		double	d1, d2, dSigmaRootTime;

		dSigmaRootTime = inp_dBlackVol * sqrt(inp_sParamsPtr->dYearFracOfTexpire);
		d1 = (log(inp_sParamsPtr->dForwardPrice / inp_sParamsPtr->dStrikePrice)) / dSigmaRootTime + dSigmaRootTime / 2.0;
		d2 = d1 - dSigmaRootTime;

		return inp_sParamsPtr->dDiscountFactor * inp_sParamsPtr->dDeltaT *(inp_sParamsPtr->dForwardPrice * gsl_cdf_ugaussian_P(d1) - inp_sParamsPtr->dStrikePrice * gsl_cdf_ugaussian_P(d2));
	}

	static double BlackScholesVol(const double& inp_dPrice, SBlackScholesParamsPtr inp_sParamsPtr)
	{
		// calculate volatility for given price

		double	dVol(0), dLow(1.0e-6), dHigh(10.0);
		double	dToleranceRelative(1.0e-6), dToleranceAbsolute(1.0e-6);

		int iMaxIter(100);

		gslRootFinder([&](real inp_dVol){return BlackScholesPrice(inp_dVol, inp_sParamsPtr) - inp_dPrice; }, dLow, dHigh, iMaxIter, dToleranceAbsolute, dToleranceRelative, dVol);
		return dVol;
	}

	static void calcCapletsVol(CVolTimeSeriesPtr inp_CapletVolPtr, CVolTimeSeriesPtr inp_DiscountFuncPtr, CVolTimeSeriesPtr inp_ForwardLiborRatePtr,
		CVolTimeSeriesPtr inp_CapVolPtr, CVolTimeSeriesPtr inp_ATMcapStrikePtr, CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr,
		CVolTimeSeriesPtr inp_YearFractRelToMktPtr)
	{
		// Need: discount func, forward Libor rate during period i-1 to i, cap vol, cumulative pdf for N(0,1), ATM cap strike, year fractions (i-1 to i, relative to market day)
		//	 mcDiscountFuncPtr, mcForwardLiborRatePtr, mcCapVolPtr, gsl_cdf_ugaussian_P(x), mcATMcapStrikePtr, mcYearFractItoIplus1Ptr, mcYearFractRelToMktPtr

		double dLastCapletPrice(0.0);
		ePaymentPeriods	PayPeriodCurrent, PayPeriodPrior;
		for (int i(T6M); i < TLast; i++)	// loop through cap terms
		{
			PayPeriodCurrent = static_cast<ePaymentPeriods>(i);
			PayPeriodPrior = static_cast<ePaymentPeriods>(i - 1);

			dLastCapletPrice = calcCapPrice(PayPeriodCurrent, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_CapVolPtr, inp_ATMcapStrikePtr, inp_YearFractItoIplus1Ptr, inp_YearFractRelToMktPtr);
			dLastCapletPrice -= calcCapPriceFromCapletLessLast(PayPeriodCurrent, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_CapletVolPtr, inp_ATMcapStrikePtr, inp_YearFractItoIplus1Ptr, inp_YearFractRelToMktPtr);

			inp_CapletVolPtr->Add(PayPeriodPrior, calcLastCapletVol(dLastCapletPrice, PayPeriodPrior, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_ATMcapStrikePtr, inp_YearFractItoIplus1Ptr, inp_YearFractRelToMktPtr));
		}
	}

	static double calcCapMeritFunc(SVolCalibrationInputPtr volCalInputPtr)
	{
		double dResult(0.0), dDiff;
		double dUpperLimit(0.0);

		double dObs, dModel;

		for (int i(T3M); i < mLastPaymentPeriodToUse; i++)
		{
			dUpperLimit = volCalInputPtr->mcYearFractRelToMktPtr->GetValueByIndex(i);
			dObs = pow(volCalInputPtr->mcCapletVolPtr->GetValueByIndex(i), 2);
			dModel = IntegrateRebonatoVolFuncSquare_analytical(dUpperLimit, volCalInputPtr->RebonatoVolParams) / dUpperLimit;
			dDiff = dObs - dModel;
			dResult += dDiff * dDiff;
		}

		return dResult;
	}
	
	static double calcCapPrice(const ePaymentPeriods& ePP, CVolTimeSeriesPtr inp_cDiscountFuncPtr,
		CVolTimeSeriesPtr inp_cForwardLiborRatePtr,
		CVolTimeSeriesPtr inp_cCapVolPtr,
		CVolTimeSeriesPtr inp_cATMcapStrikePtr,
		CVolTimeSeriesPtr inp_cYearFractItoIplus1Ptr,
		CVolTimeSeriesPtr inp_cYearFractRelToMktPtr)
	{
		// calculated the price of a cap with term ePP using the constant cap volatility and
		// ATM strike. The price is the sum of prices from each period.

		double		dCapPrice(0.0), dCapVol(inp_cCapVolPtr->GetValueByIndex(ePP));

		SBlackScholesParamsPtr sParamsPtr(new SBlackScholesParams());
		sParamsPtr->dStrikePrice = inp_cATMcapStrikePtr->GetValueByIndex(ePP);

		for (int i(T3M); i < ePP; i++)
		{
			sParamsPtr->dYearFracOfTexpire = inp_cYearFractRelToMktPtr->GetValueByIndex(i);
			sParamsPtr->dDiscountFactor = inp_cDiscountFuncPtr->GetValueByIndex(i + 1);
			sParamsPtr->dDeltaT = inp_cYearFractItoIplus1Ptr->GetValueByIndex(i);
			sParamsPtr->dForwardPrice = inp_cForwardLiborRatePtr->GetValueByIndex(i);
			dCapPrice += BlackScholesPrice(dCapVol, sParamsPtr);
		}

		return dCapPrice;
	}

	static double calcCapPriceFromCaplet(const ePaymentPeriods& ePP, CVolTimeSeriesPtr inp_cDiscountFuncPtr,
		CVolTimeSeriesPtr inp_cForwardLiborRatePtr,
		CVolTimeSeriesPtr inp_cCapletVolPtr,
		CVolTimeSeriesPtr inp_cATMcapStrikePtr,
		CVolTimeSeriesPtr inp_cYearFractItoIplus1Ptr,
		CVolTimeSeriesPtr inp_cYearFractRelToMktPtr)
	{
		// calculate the price for a cap by summing up the contribution of all the caplets.
		// ePP is the term of the cap

		double		dCapPrice(0.0), dCapletVol;

		SBlackScholesParamsPtr sParamsPtr(new SBlackScholesParams());
		sParamsPtr->dStrikePrice = inp_cATMcapStrikePtr->GetValueByIndex(ePP);

		for (int i(T3M); i < ePP; i++)
		{
			sParamsPtr->dYearFracOfTexpire = inp_cYearFractRelToMktPtr->GetValueByIndex(i);
			sParamsPtr->dDiscountFactor = inp_cDiscountFuncPtr->GetValueByIndex(i + 1);
			sParamsPtr->dDeltaT = inp_cYearFractItoIplus1Ptr->GetValueByIndex(i);
			sParamsPtr->dForwardPrice = inp_cForwardLiborRatePtr->GetValueByIndex(i);
			dCapletVol = inp_cCapletVolPtr->GetValueByIndex(i);
			dCapPrice += BlackScholesPrice(dCapletVol, sParamsPtr);
		}
		return dCapPrice;
	}

	static double calcCapPriceFromCapletLessLast(const ePaymentPeriods& ePP, CVolTimeSeriesPtr inp_cDiscountFuncPtr,
		CVolTimeSeriesPtr inp_cForwardLiborRatePtr,
		CVolTimeSeriesPtr inp_cCapletVolPtr,
		CVolTimeSeriesPtr inp_cATMcapStrikePtr,
		CVolTimeSeriesPtr inp_cYearFractItoIplus1Ptr,
		CVolTimeSeriesPtr inp_cYearFractRelToMktPtr)
	{
		// without the last caplet; used by bootstrapping caplet volatility
		// ePP is the term of the cap

		double		dCapPrice(0.0), dCapletVol;

		SBlackScholesParamsPtr sParamsPtr(new SBlackScholesParams());
		sParamsPtr->dStrikePrice = inp_cATMcapStrikePtr->GetValueByIndex(ePP);

		for (int i(T3M); i < ePP - 1; i++)
		{
			sParamsPtr->dYearFracOfTexpire = inp_cYearFractRelToMktPtr->GetValueByIndex(i);
			sParamsPtr->dDiscountFactor = inp_cDiscountFuncPtr->GetValueByIndex(i + 1);
			sParamsPtr->dDeltaT = inp_cYearFractItoIplus1Ptr->GetValueByIndex(i);
			sParamsPtr->dForwardPrice = inp_cForwardLiborRatePtr->GetValueByIndex(i);
			dCapletVol = inp_cCapletVolPtr->GetValueByIndex(i);
			dCapPrice += BlackScholesPrice(dCapletVol, sParamsPtr);
		}

		return dCapPrice;
	}

	static void calcIRSpresentValueFloatingLegUnitPrinciple(double out_IRSpvFloatingLegArr[], CVolTimeSeriesPtr inp_DiscountFuncPtr, CVolTimeSeriesPtr inp_ForwardLiborRatePtr,
		CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr, const int& inp_startIndex, const int& inp_endIndex)
	{
		//        n
		// A_k = sum {P_(j+1)*f_j*dt_j}
		//       j=k

		double tmpDbl(0.0);

		for (int i = inp_startIndex; i <= inp_endIndex; i++)
		{
			tmpDbl += inp_DiscountFuncPtr->GetValueByIndex(inp_startIndex + 1) * inp_ForwardLiborRatePtr->GetValueByIndex(inp_startIndex) * inp_YearFractItoIplus1Ptr->GetValueByIndex(inp_startIndex);
		}
		out_IRSpvFloatingLegArr[inp_startIndex] = tmpDbl;

		for (int i = inp_startIndex + 1; i <= inp_endIndex; i++)
		{
			tmpDbl = inp_DiscountFuncPtr->GetValueByIndex(i + 1) * inp_ForwardLiborRatePtr->GetValueByIndex(i) * inp_YearFractItoIplus1Ptr->GetValueByIndex(i);
			tmpDbl = out_IRSpvFloatingLegArr[i - 1] - tmpDbl;
			out_IRSpvFloatingLegArr[i] = tmpDbl;
		}
	}

	static void calcIRSpresentValueFixedLegUnitPrincipleUnitRate(double out_IRSpvFixedLegArr[], CVolTimeSeriesPtr inp_DiscountFuncPtr,
		CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr, const int& inp_startIndex, const int& inp_endIndex)
	{
		//        n
		// B_k = sum {P_(j+1)*dt_j}
		//       j=k

		double dLeg(0.0);

		for (int i = inp_startIndex; i <= inp_endIndex; i++)
		{
			dLeg += inp_DiscountFuncPtr->GetValueByIndex(inp_startIndex + 1) * inp_YearFractItoIplus1Ptr->GetValueByIndex(inp_startIndex);
		}
		out_IRSpvFixedLegArr[inp_startIndex] = dLeg;

		for (int i = inp_startIndex + 1; i <= inp_endIndex; i++)
		{
			dLeg = inp_DiscountFuncPtr->GetValueByIndex(i + 1) * inp_YearFractItoIplus1Ptr->GetValueByIndex(i);
			dLeg = out_IRSpvFixedLegArr[i - 1] - dLeg;
			out_IRSpvFixedLegArr[i] = dLeg;
		}
	}

	static double calcLastCapletVol(const double& inp_dPrice, const ePaymentPeriods& ePP, CVolTimeSeriesPtr inp_DiscountFuncPtr,
		CVolTimeSeriesPtr inp_ForwardLiborRatePtr,
		CVolTimeSeriesPtr inp_ATMcapStrikePtr,
		CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr,
		CVolTimeSeriesPtr inp_YearFractRelToMktPtr)
	{
		// given the price of the last caplet (subtract all the other caplets from the cap price), this routine
		// calculate the volatility of the last caplet.

		SBlackScholesParamsPtr sParamsPtr(new SBlackScholesParams());
		sParamsPtr->dDiscountFactor = inp_DiscountFuncPtr->GetValueByIndex(ePP + 1);
		sParamsPtr->dDeltaT = inp_YearFractItoIplus1Ptr->GetValueByIndex(ePP);
		sParamsPtr->dForwardPrice = inp_ForwardLiborRatePtr->GetValueByIndex(ePP);
		sParamsPtr->dStrikePrice = inp_ATMcapStrikePtr->GetValueByIndex(ePP + 1);
		sParamsPtr->dYearFracOfTexpire = inp_YearFractRelToMktPtr->GetValueByIndex(ePP);

		return BlackScholesVol(inp_dPrice, sParamsPtr);
	}

	static double calcSwaptionMeritFunc(SVolCalibrationInputPtr volCalInputPtr)
	{
		// Loop over swaptions NxM
		// We only use the swaptions that have 4N+4M-1 < mLastPaymentPeriodToUse
		double dResult(0.0), dDiff, volSqObs, volSqMod;

		const long lMaxExpiry(static_cast<int>(volCalInputPtr->mcSwaptionData.size()));
		const long lMaxTenor(static_cast<int>(volCalInputPtr->mcSwaptionData[0].size()));

		int lIndxStart, lIndxEnd;

		for (long lExpiry(1); lExpiry < lMaxExpiry; lExpiry++)
		{
			lIndxStart = 4 * volCalInputPtr->mcSwaptionData[lExpiry][0];
			for (long lTenor(1); lTenor < lMaxTenor; lTenor++)
			{
				lIndxEnd = lIndxStart + 4 * volCalInputPtr->mcSwaptionData[0][lTenor] - 1;
				if (lIndxEnd < mLastPaymentPeriodToUse)
				{
					volSqObs = pow(volCalInputPtr->mcSwaptionData[lExpiry][lTenor], 2);
					volSqMod = swaptionVolSqUnderLMM(volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcForwardLiborRatePtr, volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcYearFractRelToMktPtr, lIndxStart, lIndxEnd, volCalInputPtr->RebonatoVolParams, volCalInputPtr->mcRebonatoVolParamNum);
					dDiff = volSqObs - volSqMod;
					dResult += dDiff * dDiff;
				}
			}
		}

		return dResult;
	}

	static void calcZeta(double out_zeta[], CVolTimeSeriesPtr inp_DiscountFuncPtr,
		CVolTimeSeriesPtr inp_ForwardLiborRatePtr, CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr, const int& inp_startIndex, const int& inp_endIndex)
	{
		double tmpDbl(0.0);
		double A1, Ai, B1, Bi, fi, dti, Pi1;

		double IRSpvFloatingLegArr[mLastPaymentPeriodToUse], IRSpvFixedLegArr[mLastPaymentPeriodToUse];

		calcIRSpresentValueFloatingLegUnitPrinciple(IRSpvFloatingLegArr, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_YearFractItoIplus1Ptr, inp_startIndex, inp_endIndex);
		calcIRSpresentValueFixedLegUnitPrincipleUnitRate(IRSpvFixedLegArr, inp_DiscountFuncPtr, inp_YearFractItoIplus1Ptr, inp_startIndex, inp_endIndex);

		A1 = IRSpvFloatingLegArr[inp_startIndex];
		B1 = IRSpvFixedLegArr[inp_startIndex];

		for (int i = inp_startIndex; i <= inp_endIndex; i++)
		{
			Ai = IRSpvFloatingLegArr[i];
			Bi = IRSpvFixedLegArr[i];
			Pi1 = inp_DiscountFuncPtr->GetValueByIndex(i + 1);
			fi = inp_ForwardLiborRatePtr->GetValueByIndex(i);
			dti = inp_YearFractItoIplus1Ptr->GetValueByIndex(i);

			tmpDbl = Pi1 + (A1*Bi / B1 - Ai) / (1.0 + fi*dti);	// with shape correction
			tmpDbl *= fi*dti / A1;
			out_zeta[i] = tmpDbl;
		}
	}

	static double CapAndSwaptionMeritFunc(const gsl_vector *v, void *params)
	{
		SVolCalibrationInputPtr volCalInputPtr = static_cast<SVolCalibrationInputPtr>(params);

		for (int i = 0; i < volCalInputPtr->mcSwaptionParamNum + volCalInputPtr->mcRebonatoVolParamNum; i++)
		{
			volCalInputPtr->RebonatoVolParams[i] = gsl_vector_get(v, i);
		}

		return calcCapMeritFunc(volCalInputPtr) + calcSwaptionMeritFunc(volCalInputPtr);
	}

	static void getATMcapStrike(CVolTimeSeriesPtr out_ATMcapStrikePtr, CVolTimeSeriesPtr inp_DiscountFuncPtr, CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr)
	{
		double	dCum(0.0);

		for (int i(T6M); i < TLast; i++)
		{
			dCum += inp_DiscountFuncPtr->GetValueByIndex(i) * inp_YearFractItoIplus1Ptr->GetValueByIndex(i - 1);
			out_ATMcapStrikePtr->Add(i, (inp_DiscountFuncPtr->GetValueByIndex(T3M) - inp_DiscountFuncPtr->GetValueByIndex(i)) / dCum);

			if (i == T6M)
			{
				cout << "T6M" << endl;
				cout << "DF(T3M): " << inp_DiscountFuncPtr->GetValueByIndex(T3M) << endl;
				cout << "DF(i): " << inp_DiscountFuncPtr->GetValueByIndex(i) << endl;
				cout << "DeltaT(i - 1): " << inp_YearFractItoIplus1Ptr->GetValueByIndex(i - 1) << endl;
			}
		}
	}

	static double getCorrelation_2Factors(const double& theta_i, const double& theta_j)
	{
		return cos(theta_i - theta_j);
	}

	static void getForwardLiborRate(CVolTimeSeriesPtr out_ForwardLiborRatePtr, CVolTimeSeriesPtr inp_DiscountFuncPtr, CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr)
	{
		for (int i(T0); i < TLast - 1; i++)
		{
			out_ForwardLiborRatePtr->Add(i, (inp_DiscountFuncPtr->GetValueByIndex(i) / inp_DiscountFuncPtr->GetValueByIndex(i + 1) - 1.0) / inp_YearFractItoIplus1Ptr->GetValueByIndex(i));
		}
	}

	static void getYearFractions(CVolTimeSeriesPtr out_YearFractItoIplus1Ptr, CVolTimeSeriesPtr out_YearFractRelToMktPtr, CListOfLongDatesPtr inp_CapPaymentCalendarDaysPtr)
	{
		CYearFracPtr cYearFracPtr(ToolFactory::CreateYearFrac(FREQUENCY::SINGLE, DAY_COUNT_CONVENTION::ACT_360));
		double	dYf;

		for (int i(T0); i < TLast - 1; i++)
		{
			dYf = cYearFracPtr->GetYearFrac(inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i + 1), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i + 1));
			out_YearFractItoIplus1Ptr->Add(i, dYf);
		}

		for (int i(T0); i < TLast; i++)
		{
			dYf = cYearFracPtr->GetYearFrac(inp_CapPaymentCalendarDaysPtr->GetValueByIndex(T0), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i));
			out_YearFractRelToMktPtr->Add(i, dYf);
		}
	}

	static double IntegrateRebonatoVolFunc_ixj_analytical(const double& inp_dLowerLimit, double inp_dParams[], double inp_dTimeIndex1, double inp_dTimeIndex2)
	{
		double dResult;
		double dT10(-inp_dLowerLimit);
		double V4dT10(inp_dParams[3] * dT10);

		double tmpVal, tmpVal2;
		tmpVal = exp(V4dT10);
		tmpVal2 = tmpVal*tmpVal;

		double V4_1, V4_2, V4_3;
		V4_1 = 1.0 / inp_dParams[3];
		V4_2 = V4_1 / inp_dParams[3];
		V4_3 = V4_2 / inp_dParams[3];

		double A, B, C, D, E;
		double P, Q, R, S;

		P = exp(-inp_dParams[3] * inp_dTimeIndex1);
		Q = exp(-inp_dParams[3] * inp_dTimeIndex2);
		R = inp_dParams[1] + inp_dParams[2] * inp_dTimeIndex1;
		S = inp_dParams[1] + inp_dParams[2] * inp_dTimeIndex2;

		A = inp_dParams[0] * inp_dParams[2] * (P + Q);
		B = inp_dParams[0] * (R*P + S*Q);
		C = inp_dParams[2] * inp_dParams[2] * P*Q;
		D = inp_dParams[2] * P*Q*(R + S);
		E = R*S*P*Q;

		dResult = inp_dParams[0] * inp_dParams[0] * dT10 + A*V4_2*(-1.0 + (1.0 - V4dT10)*tmpVal);
		dResult += B*V4_1*(tmpVal - 1.0);
		dResult += C*V4_3*0.25*(-1.0 + (2.0*V4dT10*V4dT10 - 2.0*V4dT10 + 1.0)*tmpVal2);
		dResult += D*V4_2*0.25*(-1.0 + (1.0 - 2.0*V4dT10)*tmpVal2);
		dResult += E*V4_1*0.5*(tmpVal2 - 1.0);

		return dResult;
	}

	static double IntegrateRebonatoVolFuncSquare_analytical(const double& inp_dUpperLimit, double inp_dParams[])
	{
		double dResult(0.0);
		double dTi0(inp_dUpperLimit);
		double v1, v2, v3, v4, tmpVal, tmpVal2;
		double V4dTi0, V4_1, V4_2, V4_3;

		v1 = inp_dParams[0];
		v2 = inp_dParams[1];
		v3 = inp_dParams[2];
		v4 = inp_dParams[3];

		V4dTi0 = v4 * dTi0;
		V4_1 = 1.0 / v4;
		V4_2 = V4_1 / v4;
		V4_3 = V4_2 / v4;

		tmpVal = exp(-V4dTi0);
		tmpVal2 = tmpVal * tmpVal;

		dResult = v1*v1*dTi0 - v2*v2*V4_1*0.5*(tmpVal2 - 1.0) - 0.5*v2*v3*V4_2*((2.0*V4dTi0 + 1.0)*tmpVal2 - 1.0);
		dResult += -0.25*v3*v3*V4_3*((2.0*V4dTi0*V4dTi0 + 2.0*V4dTi0 + 1.0)*tmpVal2 - 1.0);
		dResult += -2.0*v1*v2*V4_1*(tmpVal - 1.0) - 2.0*v1*v3*V4_2*((V4dTi0 + 1.0)*tmpVal - 1.0);

		return dResult;
	}

	static void interpolateCapVolandDF(CVolTimeSeriesPtr inp_CapVolPtr, CListOfLongDatesPtr inp_CapPaymentCalendarDaysPtr, CVolTimeSeriesPtr inp_DiscountFuncPtr) 
	{
		// use 1-year cap vol for 6 and 9 month caps.
		// this could be generalized to use the first avaliable cap volatility for all the shorter term caps.

		double dCap1Yvol(inp_CapVolPtr->GetValueByIndex(T1Y));
		for (int i(T6M); i <= T9M; i++)
		{
			inp_CapVolPtr->UpdateEntry(i, dCap1Yvol);
		}

		// interpolate the rest of the missing cap vols and discount factors.
		CDateDoubleMapPtr cDateCapVolPtr(CDateDoubleMap::CreateInstance()), cDateDFPtr(CDateDoubleMap::CreateInstance());		// these are used for interpolation; indexed by date, not by enum.

		int i(0);
		CDate	tmpDate;

		for (inp_CapVolPtr->First(); !inp_CapVolPtr->END(); inp_CapVolPtr->Next())
		{
			i = inp_CapVolPtr->GetCurrentIndex();
			tmpDate = inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i);
			cDateCapVolPtr->Add(tmpDate, inp_CapVolPtr->GetCurrentValue());
			cDateDFPtr->Add(tmpDate, inp_DiscountFuncPtr->GetValueByIndex(i));
		}

		for (int i = T1Y; i < T20Y; i++)
		{
			tmpDate = inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i);

			inp_CapVolPtr->Add(i, CMath::Interpolate(LINEAR, *cDateCapVolPtr, tmpDate));
			inp_DiscountFuncPtr->Add(i, CMath::Interpolate(LINEAR, *cDateDFPtr, tmpDate));
		}
	}

	static void populateCapPaymentCalendar(CListOfLongDatesPtr inp_CapPaymentCalendarDaysPtr, const ePaymentPeriods& inp_wholeYearStart)
	{
		// List of all the dates corresponding to the enum ePaymentPeriods.
		// inp_CapPaymentCalendarDaysPtr points to collection of (i, LongDate), where i is driven by the enum ePaymentPeriods.

		// SHolidayCalendarsPtr sHolidayCalendarPtr(new SHolidayCalendars());
		// sHolidayCalendarPtr->sInputPtr->dtStart = CDate(19500101);
		// sHolidayCalendarPtr->sInputPtr->dtEnd = CDate(20991231);
		// Get(SHOLIDAYCALENDARS, sHolidayCalendarPtr);

		// FDateAdjustor dateAdjustor(FOLLOWING, sHolidayCalendarPtr);

		CDate	dtStart(inp_CapPaymentCalendarDaysPtr->GetFirst());
		CDate	dtThis;
		for (int i = T3M; i < inp_wholeYearStart; i++)
		{
			dtThis = dtStart.Add(MONTH, i * 3);
			// dateAdjustor(tmpDate);
			inp_CapPaymentCalendarDaysPtr->Add(i, dtThis.ToLong());
		}

		for (int i = inp_wholeYearStart; i < TLast; i++)
		{
			dtThis = dtStart.Add(YEAR, (i - inp_wholeYearStart) + inp_wholeYearStart / 4);
			//tmpDate = capStartDate.Add(YEAR, (i-23)+5);
			// dateAdjustor(tmpDate);
			inp_CapPaymentCalendarDaysPtr->Add(i, dtThis.ToLong());
		}
	}

	static void readCapsData(const string& inp_fileName, CStringToEnumMapPtr inp_StrToEnumMapPtr,
		CListOfLongDatesPtr out_DatesPtr, CVolTimeSeriesPtr out_DiscountFuncPtr, CVolTimeSeriesPtr out_dCapVolPtr)
	{
		fstream       myFile;
		string        tmpStr, tmpDateStr;
		double        tmpDbl, dDF, dCapVol;

		string	sMarketDate, sCapStartDate, sTimeLapseCode;
		ePaymentPeriods ePP;

		LongDate	lMarketDate, lCapStartDate, tmpLDate;

		myFile.open(inp_fileName.c_str(), ios::in);

		if (myFile.is_open())
		{
			myFile >> tmpStr >> sMarketDate >> tmpDbl >> tmpStr;
			CDateMath::ExtractFormattedDateAsLong(sMarketDate, msDelimiter, lMarketDate);

			myFile >> sTimeLapseCode >> sCapStartDate >> dDF >> tmpStr;
			CDateMath::ExtractFormattedDateAsLong(sCapStartDate, msDelimiter, lCapStartDate);
			ePP = static_cast<ePaymentPeriods>(inp_StrToEnumMapPtr->GetValueByIndex(sTimeLapseCode));
			out_DatesPtr->Add(ePP, lCapStartDate);
			out_DiscountFuncPtr->Add(ePP, dDF);
			out_dCapVolPtr->Add(ePP, 0.0);

			for (int i = T3M; i <= T9M; i++)
			{
				myFile >> sTimeLapseCode >> tmpDateStr >> dDF >> tmpStr;
				CDateMath::ExtractFormattedDateAsLong(tmpDateStr, msDelimiter, tmpLDate);
				ePP = static_cast<ePaymentPeriods>(inp_StrToEnumMapPtr->GetValueByIndex(sTimeLapseCode));
				out_DatesPtr->Add(ePP, tmpLDate);
				out_DiscountFuncPtr->Add(ePP, dDF);
				out_dCapVolPtr->Add(ePP, 0.0);
			}

			int icount(T9M);
			while (myFile >> sTimeLapseCode >> tmpDateStr >> dDF >> dCapVol)
			{
				icount++;

				CDateMath::ExtractFormattedDateAsLong(tmpDateStr, msDelimiter, tmpLDate);
				ePP = static_cast<ePaymentPeriods>(inp_StrToEnumMapPtr->GetValueByIndex(sTimeLapseCode));
				out_DatesPtr->Add(ePP, tmpLDate);
				out_DiscountFuncPtr->Add(ePP, dDF);
				out_dCapVolPtr->Add(ePP, dCapVol);
			}

			myFile.close();
		}
		else
		{
			cout << "Unable to open file " << inp_fileName << endl;
			exit(1);
		}
	}

	static std::vector< std::vector<double> > readIn2dData(const char* filename)
	{
		/* Function takes a char* filename argument and returns a
		* 2d dynamic array containing the data
		*/

		std::vector< std::vector<double> > table;
		std::fstream ifs;

		/*  open file  */
		ifs.open(filename);

		if (ifs.is_open()) {

			while (true)
			{
				std::string line;
				double buf;

				getline(ifs, line);

				std::stringstream ss(line, std::ios_base::out | std::ios_base::in | std::ios_base::binary);

				if (!ifs)                  // mainly catch EOF
					break;

				if (line[0] == '#' || line.empty())   // catch empty lines or comment lines
					continue;

				std::vector<double> row;

				while (ss >> buf)
					row.push_back(buf);

				table.push_back(row);

			}
			ifs.close();

		}
		else {
			cout << "Unable to open file " << filename << endl;
			exit(1);
		}

		return table;
	}

	static double swaptionVolSqUnderLMM(CVolTimeSeriesPtr inp_DiscountFuncPtr, CVolTimeSeriesPtr inp_ForwardLiborRatePtr,
		CVolTimeSeriesPtr inp_YearFractItoIplus1Ptr, CVolTimeSeriesPtr inp_YearFractRelToMktPtr,
		int inp_startIndex, int inp_endIndex, double inp_RebonatoVolParams[], const int& inp_iRebonatoVolParamNum)
	{
		double	dRho;
		double	T1(inp_YearFractRelToMktPtr->GetValueByIndex(inp_startIndex));

		double	dLowerLimit(-T1);
		double	dIntegralIxJ;

		double dResult(0.0);

		double zetaArr[mLastPaymentPeriodToUse];

		calcZeta(zetaArr, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_YearFractItoIplus1Ptr, inp_startIndex, inp_endIndex);

		for (int index (inp_startIndex); index <= inp_endIndex; index++)
		{
			for (int jndex(inp_startIndex); jndex <= inp_endIndex; jndex++)
			{
				dRho = getCorrelation_2Factors(inp_RebonatoVolParams[index + inp_iRebonatoVolParamNum], inp_RebonatoVolParams[jndex + inp_iRebonatoVolParamNum]);
				dIntegralIxJ = IntegrateRebonatoVolFunc_ixj_analytical(dLowerLimit, inp_RebonatoVolParams, inp_YearFractRelToMktPtr->GetValueByIndex(index), inp_YearFractRelToMktPtr->GetValueByIndex(jndex));
				dResult += zetaArr[index] * zetaArr[jndex] * dRho * dIntegralIxJ;

				if (mlCount < 100)
				{
					std::cout << "T1: " << T1 << endl;
					std::cout << "dLowerLimit: " << dLowerLimit << endl;
					std::cout << "[0]: " << inp_RebonatoVolParams[0] << endl;
					std::cout << "[1]: " << inp_RebonatoVolParams[1] << endl;
					std::cout << "[2]: " << inp_RebonatoVolParams[2] << endl;
					std::cout << "[3]: " << inp_RebonatoVolParams[3] << endl;
					std::cout << "index: " << index << endl;
					std::cout << "jndex: " << jndex << endl;
					std::cout << "dIntegralIxJ: " << dIntegralIxJ << endl << endl;
					mlCount++;
				}
			}
		}

		dResult /= T1;

		return dResult;
	}

	static void outputEntries(CVolTimeSeriesPtr inp_CRealSeriesPtr, long inp_lEntriesToOutput, TSTRING inp_strLabel)
	{
		cout << inp_strLabel << endl;

		long lCnt(0);
		for (inp_CRealSeriesPtr->First(); !inp_CRealSeriesPtr->END(); inp_CRealSeriesPtr->Next(), lCnt++)
		{
			if (lCnt < inp_lEntriesToOutput) cout << inp_CRealSeriesPtr->GetCurrentIndex() << ": " << inp_CRealSeriesPtr->GetCurrent() << endl;
		}
	}

	// Below are swaption related routines
	//
	// The following diagram shows the index convention (f is forward rate)
	//
	// today         |--> f_0                                    |--> f_i                         |--> f_n
	// |-------------|----------|----------|----------|----------|----------|----------|----------|----------|
	//               t_0        t_1                              t_i                              t_n        t_(n+1)
	//                                                           |<- dt_i ->|
	//       zero coupon bond P_(i+1) or discount factor
	// |------------------------------------------------------------------->

public:
	static int Run(void)
	{
		clock_t startTime = clock(), endTime;

		SVolCalibrationInputPtr volCalInputPtr(new SVolCalibrationInput());

		mlCount = 0;

		volCalInputPtr->mcCapVolPtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcDiscountFuncPtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcCapletVolPtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcYearFracCapletVolSqPtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcCapPaymentCalendarDaysPtr = CListOfLongDates::CreateInstance();

		volCalInputPtr->mcEnumToStrMapPtr = CEnumToStringMap::CreateInstance();
		volCalInputPtr->mcStrToEnumMapPtr = CStringToEnumMap::CreateInstance();

		volCalInputPtr->mcYearFractItoIplus1Ptr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcYearFractRelToMktPtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcForwardLiborRatePtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcATMcapStrikePtr = CVolTimeSeries::CreateInstance();

		volCalInputPtr->mcIRSpvFloatingLegPtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcIRSpvFixedLegPtr = CVolTimeSeries::CreateInstance();
		volCalInputPtr->mcZetaPtr = CVolTimeSeries::CreateInstance();

		for (int i(T0); i != TLast; i++)
		{
			volCalInputPtr->mcStrToEnumMapPtr->Add(sPmtPeriods[i], static_cast<ePaymentPeriods>(i));
		}

		CCalibration::readCapsData("caps_01212005.dat", volCalInputPtr->mcStrToEnumMapPtr, volCalInputPtr->mcCapPaymentCalendarDaysPtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcCapVolPtr);
		CCalibration::populateCapPaymentCalendar(volCalInputPtr->mcCapPaymentCalendarDaysPtr, ePaymentPeriods::T10Y);
		CCalibration::interpolateCapVolandDF(volCalInputPtr->mcCapVolPtr, volCalInputPtr->mcCapPaymentCalendarDaysPtr, volCalInputPtr->mcDiscountFuncPtr);
		//cout << "Cap vol: " << volCalInputPtr->mcCapVolPtr->Size() << endl;
		//cout << "Pay calendar: " << volCalInputPtr->mcCapPaymentCalendarDaysPtr->Size() << endl;
		//cout << "Disc fn: " << volCalInputPtr->mcDiscountFuncPtr->Size() << endl;
		CCalibration::getYearFractions(volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcYearFractRelToMktPtr, volCalInputPtr->mcCapPaymentCalendarDaysPtr);
		CCalibration::getForwardLiborRate(volCalInputPtr->mcForwardLiborRatePtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcYearFractItoIplus1Ptr);
		CCalibration::getATMcapStrike(volCalInputPtr->mcATMcapStrikePtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcYearFractItoIplus1Ptr);

		// calculate the volatility of all caplets
		cout << "Bootstrapping caplet volatility from caps ... \n";
		CCalibration::calcCapletsVol(volCalInputPtr->mcCapletVolPtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcForwardLiborRatePtr, volCalInputPtr->mcCapVolPtr,
			volCalInputPtr->mcATMcapStrikePtr, volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcYearFractRelToMktPtr);

		//outputEntries(volCalInputPtr->mcYearFractRelToMktPtr, 20, "mcYearFractRelToMktPtr");
		//outputEntries(volCalInputPtr->mcYearFractItoIplus1Ptr, 20, "mcYearFractItoIplus1Ptr");
		//outputEntries(volCalInputPtr->mcDiscountFuncPtr, 20, "mcDiscountFuncPtr");
		//outputEntries(volCalInputPtr->mcForwardLiborRatePtr, 3, "mcForwardLiborRatePtr");
		//outputEntries(volCalInputPtr->mcATMcapStrikePtr, 3, "mcATMcapStrikePtr");
		//outputEntries(volCalInputPtr->mcCapletVolPtr, 3, "mcCapletVolPtr");
		//return 0;
		const int iVolParams(4);
		const int iSwaptionDimension(40);
		const int iCapAndSwaptionDimension(44);
		
		volCalInputPtr->mcRebonatoVolParamNum = iVolParams;
		volCalInputPtr->mcSwaptionParamNum = iSwaptionDimension;
		volCalInputPtr->mcSwaptionData = CCalibration::readIn2dData("swaption_0121205.dat");

		cout << "Calibrating Volatility Correlation function parameters using swaptions ... \n";

		const int iMaxIter(500000);
		const double dTol(1.0e-8);

		double dCapAndSwaptionStartPt[iCapAndSwaptionDimension];
		double dCapAndSwaptionSpan[iCapAndSwaptionDimension];
		for (int i = 0; i < iCapAndSwaptionDimension; i++)
		{
			dCapAndSwaptionStartPt[i] = 1.57;
			dCapAndSwaptionSpan[i] = 1.0;
		}

		auto objectiveFunction = [&](double inp_dParams[])->double
		{
			for (int i(0); i < iCapAndSwaptionDimension; i++)
			{
				volCalInputPtr->RebonatoVolParams[i] = inp_dParams[i];
			}

			return calcCapMeritFunc(volCalInputPtr) + calcSwaptionMeritFunc(volCalInputPtr);
		};

		GSL_f_minimizer_wrapper_v2(CCalibration::CapAndSwaptionMeritFunc, iCapAndSwaptionDimension, volCalInputPtr, dCapAndSwaptionStartPt, dCapAndSwaptionSpan, iMaxIter, dTol);
		//gslMinimizer(objectiveFunction, iCapAndSwaptionDimension, dCapAndSwaptionStartPt, dCapAndSwaptionSpan, iMaxIter, dTol);

		endTime = clock();
		cout << "\tTotal CPU time spent is " << ((endTime - startTime) / (double)CLOCKS_PER_SEC) << " seconds.\n";

		return 0;
	}
};

//class CUnused
//{
//private:
//	static void checkReadCapsData(CListOfLongDatesPtr & inp_DatesPtr, CVolTimeSeriesPtr & inp_DiscountFuncPtr, CVolTimeSeriesPtr & inp_dCapVolPtr)
//	{
//		LongDate	tmp_lDate;
//		CDate	my_cDate(20050125);
//
//		cout << "Output discount function and caps volatility ... \n";
//		int i(0);
//		inp_DatesPtr->First();
//		while (!inp_DatesPtr->END())
//		{
//			tmp_lDate = inp_DatesPtr->GetCurrentIndex();
//			cout << "\t" << inp_DatesPtr->GetCurrentValue()
//				<< "\t" << (my_cDate.Add(MONTH, 3 * i)).ToLong()
//				<< "\t" << inp_DiscountFuncPtr->GetValueByIndex(tmp_lDate)
//				<< "\t" << inp_dCapVolPtr->GetValueByIndex(tmp_lDate)
//				<< endl;
//			inp_DatesPtr->Next();
//			i++;
//		}
//	}
//
//	static void checkSigam_ixj_Integral(double dParamsSwap[])
//	{
//		cout << "testing the sigma_i*sigma_j integral ...\n";
//
//		double dLowerLimit(-1.0), dUpperLimit(0.0);
//		// double dParamsSwap[6];
//		double temp1, temp2;
//		// for (int i=0; i<4; i++) { dParamsSwap[i] = dStartPt[i]; }
//		dParamsSwap[4] = 2.0;
//		dParamsSwap[5] = 4.0;
//
//		temp1 = IntegrateRebonatoVolFunc_ixj(dLowerLimit, dUpperLimit, dParamsSwap);
//		temp2 = IntegrateRebonatoVolFunc_ixj_analytical(dLowerLimit, dUpperLimit, dParamsSwap);
//
//		cout << "\tOLD integral: " << temp1 << endl;
//		cout << "\tNEW integral: " << temp2 << endl;
//
//		dLowerLimit = 0.0;
//		dUpperLimit = 1.0;
//		temp1 = IntegrateRebonatoVolFuncSquare(dLowerLimit, dUpperLimit, dParamsSwap);
//		temp2 = IntegrateRebonatoVolFuncSquare_analytical(dLowerLimit, dUpperLimit, dParamsSwap);
//
//		cout << "\tOLD integral: " << temp1 << endl;
//		cout << "\tNEW integral: " << temp2 << endl;
//	}
//
//	static void createWorkSpace(CFakeMatrixPtr& out_dMatrxPtr) {}
//
//	static double getCorrelation(int& inp_index, int & inp_jndex, double inp_theta[], double inp_phi[], double & inp_ti0, double & inp_tj0)
//	{
//		double rho_ij(0.0);
//		double theta_i, phi_i;
//		double theta_j, phi_j;
//
//		theta_i = inp_theta[0] + (inp_theta[1] + inp_theta[2] * inp_ti0)*exp(-inp_theta[3] * inp_ti0);
//		phi_i = inp_phi[0] + (inp_phi[1] + inp_phi[2] * inp_ti0)*exp(-inp_phi[3] * inp_ti0);
//
//		theta_j = inp_theta[0] + (inp_theta[1] + inp_theta[2] * inp_tj0)*exp(-inp_theta[3] * inp_tj0);
//		phi_j = inp_phi[0] + (inp_phi[1] + inp_phi[2] * inp_tj0)*exp(-inp_phi[3] * inp_tj0);
//
//		rho_ij = cos(phi_i)*cos(phi_j) - sin(phi_i)*sin(phi_j)*(1.0 - cos(theta_i)*cos(theta_j));		// modified twice
//		// rho_ij = cos(phi_i)*cos(phi_j) - sin(phi_i)*sin(phi_j)*(1.0 - cos(theta_i-theta_j));		// modified once
//		// rho_ij = cos(phi_i - phi_j) - sin(phi_i)*sin(phi_j)*(1.0 - cos(theta_i-theta_j));		// in the manual
//
//		return rho_ij;
//	}
//
//	static double getCorrelation_JR(double beta[], double & inp_ti0, double & inp_tj0)
//	{
//		double rho_ij(0.0);
//		double Tmin(inp_ti0);
//
//		if (inp_ti0 > inp_tj0) Tmin = inp_tj0;
//
//		// rho_ij = beta[0];
//		// rho_ij = exp(-beta[0]*fabs(inp_ti0 - inp_tj0));
//		// rho_ij = exp(-beta[0]*fabs(inp_ti0 - inp_tj0)*exp(-beta[1]*Tmin));
//		// rho_ij = (beta[1]) + (1.0 - (beta[1]))*exp(-beta[0]*fabs(inp_ti0 - inp_tj0));
//		rho_ij = (beta[2]) + (1.0 - (beta[2]))*exp(-beta[0] * fabs(inp_ti0 - inp_tj0)*exp(-beta[1] * Tmin));
//
//		return rho_ij;
//	}
//
//
	//static double get_wall_time()
	//{
	//	//struct timeval time;
	//	//if (gettimeofday(&time,NULL)) { return 0; }
	//	//return (double)time.tv_sec + (double)time.tv_usec * 1.0e-6;
	//	return 0.0;
	//}
//
//	static double IntegrateRebonatoVolFunc_ixj(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[])
//	{
//		size_t iMaxNumOfIntervals(1000), workSpaceSize(1000);
//		double epsrel(1.0e-8), epsabs(1.0e-8);
//		double dResult, dAbsErr;
//		int GaussKronrodPtsKey(5);
//
//		dResult = GSL_integration_wrapper(RebonatoVolFunc_ixj, inp_dParams, inp_dLowerLimit, inp_dUpperLimit, iMaxNumOfIntervals, epsabs, epsrel, dAbsErr, workSpaceSize, GaussKronrodPtsKey, true);
//
//		return dResult;
//	}
//
//	static double IntegrateRebonatoVolFuncSquare(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[])
//	{
//		size_t iMaxNumOfIntervals(1000), workSpaceSize(1000);
//		double epsrel(1.0e-8), epsabs(0.0e-6);
//		double dResult, dAbsErr;
//		int GaussKronrodPtsKey(1);
//
//		/*  GSL_f_minimizer_wrapper(double (*func) (double *, void *), double inp_dFuncParams[],
//		const double inp_dLowerLimit, const double inp_dUpperLimit, size_t& inp_iMaxNumOfIntervals,
//		const double& epsabs, const double& epsrel, double& out_dAbsErr,
//		size_t& workSpaceSize, const int GaussKronrodPtsKey=3, const bool& inp_mbVerbose=true) */
//		dResult = GSL_integration_wrapper(RebonatoVolFuncSquare, inp_dParams, inp_dLowerLimit, inp_dUpperLimit, iMaxNumOfIntervals, epsabs, epsrel, dAbsErr, workSpaceSize, GaussKronrodPtsKey, true);
//
//		return dResult;
//	}
//
//	static double RebonatoVolFunc_ixj(double inp_dYearFraction, void * inp_dParams)
//	{
//		// The input parameters are: 4 Rebonato Vol parameters followed by the year fraction of Ti and Tj to market (T0)
//		// The integral is from 0 to T1, so the parameters are regrouped to call Rebonato Vol function.
//
//		double *p = (double *)inp_dParams;
//		double *pi;
//		double *pj;
//
//		pi[0] = p[0];
//		pj[0] = p[0];
//		pi[1] = (p[1] + p[2] * p[4]) * exp(-p[3] * p[4]);
//		pj[1] = (p[1] + p[2] * p[5]) * exp(-p[3] * p[5]);
//		pi[2] = p[2] * exp(-p[3] * p[4]);
//		pj[2] = p[2] * exp(-p[3] * p[5]);
//		pi[3] = p[3];
//		pj[3] = p[3];
//
//		double	dVol_ixj(0.0);
//		dVol_ixj = RebonatoVolFunc(inp_dYearFraction, pi) * RebonatoVolFunc(inp_dYearFraction, pj);
//		return dVol_ixj;
//	}
//
//	static double RebonatoVolFunc(double & inp_dYearFraction, double inp_dParams[])
//	{
//		double	dVol(0.0);
//		dVol = inp_dParams[0] + (inp_dParams[1] + inp_dParams[2] * inp_dYearFraction) * exp(-inp_dParams[3] * inp_dYearFraction);
//		return dVol;
//	}
//
//	static double RebonatoVolFuncSquare(double inp_dYearFraction, void * inp_dParams)
//	{
//		// this is not exactly the square of Rebonato volatility.
//		// First squared the volatility function then multiplied by 360.
//		// The purpose is to treat the squared vol as the integrand while the integral was transformed from t to year fraction dt/360.
//		// Correction: there shouldn't be a factor of 360! It was cancelled by the t in t*sigma^2
//
//		double *p = (double *)inp_dParams;
//
//		double	dVol2(0.0);
//		dVol2 = pow(RebonatoVolFunc(inp_dYearFraction, p), 2);
//		return dVol2;
//	}
//
//	static double SwaptionMeritFunc(const gsl_vector *v, void *params)
//	{
//		// MeritFunc += (YFrelToMkt*CapletVol**2 - IntegrateRebonatoValFuncSquare)**2
//
//		SVolCalibrationInputPtr volCalInputPtr = static_cast<SVolCalibrationInputPtr>(params);
//		double dResult(0.0);
//
//		//for (int i=0; i<8; i++) volCalInputPtr->RebonatoVolParams[i+4] = gsl_vector_get(v, i);	// theta and phi
//		for (int i = 0; i < volCalInputPtr->mcSwaptionParamNum; i++)
//		{
//			volCalInputPtr->RebonatoVolParams[i + 4] = gsl_vector_get(v, i);	// theta and phi
//			// cout << i << "\t" << volCalInputPtr->RebonatoVolParams[i+4] << "\t" << volCalInputPtr->mcSwaptionParamNum << endl;
//		}
//
//		dResult = calcSwaptionMeritFunc(volCalInputPtr);
//
//		return dResult;
//	}
//
//
//};
#endif
