
#include "Porting.h"

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <iomanip>
#include <vector>

#include <omp.h>

#include "SmartArray.h"
#include "SmartMap.h"
#include "UtilSmartPointers.h"
#include "ToolsMath.h"

#include "YearFrac.h"

//#include <gsl/gsl_vector.h>
//#include <gsl/gsl_blas.h>
#include <gsl/gsl_cdf.h> 	// cumulative distribution of N(0,1): gsl_cdf_ugaussian_P (x); Manual P260/276
// #include <gsl/gsl_roots.h>	// 1-d root finding; Manual P257/273; example on P365/381
#include "GSL_f_rootFinder_1d_wrapper.h"
#include "GSL_f_minimizer_wrapper.h"
#include "GSL_integration_wrapper.h"

#include "VolatilityCalibration.h"

using namespace std;

void readCapsData (const string& inp_fileName, CStringToEnumMapPtr& inp_StrToEnumMapPtr,
                   CListOfLongDatesPtr& out_DatesPtr, CVolTimeSeriesPtr& out_DiscountFuncPtr, CVolTimeSeriesPtr& out_dCapVolPtr,
		   bool bVerbose);
void checkReadCapsData (CListOfLongDatesPtr & inp_DatesPtr, CVolTimeSeriesPtr & inp_DiscountFuncPtr, CVolTimeSeriesPtr & inp_dCapVolPtr);

std::vector< std::vector<double> > readIn2dData(const char* filename);

const size_t maxArrSize(1000);
const string dateDelimeter("-");

const bool	bVerbose(false);

//enum ePaymentPeriods{T0=0,T3M,T6M,T9M,
//	    T1Y,T1Y3M,T1Y6M,T1Y9M,
//	    T2Y,T2Y3M,T2Y6M,T2Y9M,
//	    T3Y,T3Y3M,T3Y6M,T3Y9M,
//	    T4Y,T4Y3M,T4Y6M,T4Y9M,
//	    T5Y,T5Y3M,T5Y6M,T5Y9M,
//	    T6Y,T6Y3M,T6Y6M,T6Y9M,
//	    T7Y,T7Y3M,T7Y6M,T7Y9M,
//	    T8Y,T8Y3M,T8Y6M,T8Y9M,
//	    T9Y,T9Y3M,T9Y6M,T9Y9M,
//	    T10Y,T11Y,T12Y,T13Y,T14Y,T15Y,T16Y,T17Y,T18Y,T19Y,T20Y,
//	    TLast};

static const ePaymentPeriods		LastPaymentPeriodToUse(T10Y);

typedef vector< vector<double>>		CFakeMatrix;
typedef CSmartPtr<CFakeMatrix>		CFakeMatrixPtr;

const string sPaymentPeriods[maxArrSize] = {"T0","T3M","T6M","T9M",
            "T1Y","T1Y3M","T1Y6M","T1Y9M",
            "T2Y","T2Y3M","T2Y6M","T2Y9M",
            "T3Y","T3Y3M","T3Y6M","T3Y9M",
            "T4Y","T4Y3M","T4Y6M","T4Y9M",
            "T5Y","T5Y3M","T5Y6M","T5Y9M",
            "T6Y","T6Y3M","T6Y6M","T6Y9M",
            "T7Y","T7Y3M","T7Y6M","T7Y9M",
            "T8Y","T8Y3M","T8Y6M","T8Y9M",
            "T9Y","T9Y3M","T9Y6M","T9Y9M",
	    "T10Y","T11Y","T12Y","T13Y","T14Y","T15Y","T16Y","T17Y","T18Y","T19Y","T20Y",
	    "TLast"};

double calcCapPrice(ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_cDiscountFuncPtr,
					  CVolTimeSeriesPtr& inp_cForwardLiborRatePtr,
					  CVolTimeSeriesPtr& inp_cCapVolPtr,
					  CVolTimeSeriesPtr& inp_cATMcapStrikePtr,
					  CVolTimeSeriesPtr& inp_cYearFractItoIplus1Ptr,
					  CVolTimeSeriesPtr& inp_cYearFractRelToMktPtr);

double calcCapPriceFromCaplet(ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_cDiscountFuncPtr,
					  CVolTimeSeriesPtr& inp_cForwardLiborRatePtr,
					  CVolTimeSeriesPtr& inp_cCapletVolPtr,
					  CVolTimeSeriesPtr& inp_cATMcapStrikePtr,
					  CVolTimeSeriesPtr& inp_cYearFractItoIplus1Ptr,
					  CVolTimeSeriesPtr& inp_cYearFractRelToMktPtr);

double calcCapPriceFromCapletLessLast(ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_cDiscountFuncPtr,
					  CVolTimeSeriesPtr& inp_cForwardLiborRatePtr,
					  CVolTimeSeriesPtr& inp_cCapletVolPtr,
					  CVolTimeSeriesPtr& inp_cATMcapStrikePtr,
					  CVolTimeSeriesPtr& inp_cYearFractItoIplus1Ptr,
					  CVolTimeSeriesPtr& inp_cYearFractRelToMktPtr);
double BlackScholesPrice(double& inp_dDiscountFactor, double& inp_dDeltaT, double& inp_dForwardPrice, double& inp_dStrikePrice, double& inp_dBlackVol, double& inp_dYearFracOfTexpire);
double BlackScholesPriceDiff(double inp_dBlackVol, void *inp_params);
double BlackScholesVol(double & inp_dPrice, double& inp_dDiscountFactor, double& inp_dDeltaT, double& inp_dForwardPrice, double& inp_dStrikePrice, double& inp_dYearFracOfTexpire);

double calcLastCapletVol(double & inp_dPrice, ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_DiscountFuncPtr,
				CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
				CVolTimeSeriesPtr& inp_ATMcapStrikePtr,
				CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr,
				CVolTimeSeriesPtr& inp_YearFractRelToMktPtr);

void calcCapletsVol(CVolTimeSeriesPtr& inp_CapletVolPtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
		    CVolTimeSeriesPtr& inp_CapVolPtr, CVolTimeSeriesPtr& inp_ATMcapStrikePtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr,
		    CVolTimeSeriesPtr& inp_YearFractRelToMktPtr, CEnumToStringMapPtr& inp_EnumToStrMapPtr);

void populateCapPaymentCalendar (CListOfLongDatesPtr & inp_CapPaymentCalendarDaysPtr, ePaymentPeriods & inp_wholeYearStart);

void interpolateCapVolandDF (CVolTimeSeriesPtr& inp_CapVolPtr, CListOfLongDatesPtr& inp_CapPaymentCalendarDaysPtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CEnumToStringMapPtr& inp_EnumToStrMapPtr);
void getYearFractions(CVolTimeSeriesPtr& out_YearFractItoIplus1Ptr, CVolTimeSeriesPtr& out_YearFractRelToMktPtr, CListOfLongDatesPtr& inp_CapPaymentCalendarDaysPtr);
void getForwardLiborRate(CVolTimeSeriesPtr& out_ForwardLiborRatePtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr);
void getATMcapStrike(CVolTimeSeriesPtr& out_ATMcapStrikePtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, CEnumToStringMapPtr& inp_EnumToStrMapPtr);
void calcYearFracTimesCapletVolSq(CVolTimeSeriesPtr& out_YearFracCapletVolSqPtr, CVolTimeSeriesPtr& inp_CapletVolPtr, CVolTimeSeriesPtr& inp_YearFractRelToMktPtr, CEnumToStringMapPtr& inp_EnumToStrMapPtr);

double RebonatoVolFunc(double & inp_dYearFraction, double inp_dParams[]);
double RebonatoVolFuncSquare(double inp_dYearFraction, void * inp_dParams);
double IntegrateRebonatoVolFuncSquare(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[]);
double IntegrateRebonatoVolFuncSquare_analytical(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[]);

double CapMeritFunc(const gsl_vector *v, void *params);
double calcCapMeritFunc(SVolCalibrationInputPtr & volCalInputPtr);

void calcIRSpresentValueFloatingLegUnitPrinciple(double out_IRSpvFloatingLegArr[], CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
                    CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, int& inp_startIndex, int & inp_endIndex);
void calcIRSpresentValueFixedLegUnitPrincipleUnitRate(double out_IRSpvFixedLegArr[], CVolTimeSeriesPtr& inp_DiscountFuncPtr,
                    CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, int& inp_startIndex, int & inp_endIndex);
void calcZeta(double out_zeta[], CVolTimeSeriesPtr& inp_DiscountFuncPtr,
                    CVolTimeSeriesPtr& inp_ForwardLiborRatePtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, int& inp_startIndex, int & inp_endIndex);
double getCorrelation(int& inp_index, int & inp_jndex, double inp_theta[], double inp_phi[], double & inp_ti0, double & inp_tj0);
double getCorrelation_JR(double beta[], double & inp_ti0, double & inp_tj0);
double getCorrelation_2Factors(double & theta_i, double& theta_j);
double RebonatoVolFunc_ixj(double inp_dYearFraction, void * inp_dParams);
double IntegrateRebonatoVolFunc_ixj(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[]);
double IntegrateRebonatoVolFunc_ixj_analytical(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[]);
void checkSigam_ixj_Integral(double dParamsSwap[]);
double swaptionVolSqUnderLMM(CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
                    CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, CVolTimeSeriesPtr& inp_YearFractRelToMktPtr,
                    CVolTimeSeriesPtr& IRSpvFloatingLegPtr, CVolTimeSeriesPtr& IRSpvFixedLegPtr,
                    int& inp_startIndex, int & inp_endIndex, double inp_RebonatoVolParams[]);
double SwaptionMeritFunc(const gsl_vector *v, void *params);
double calcSwaptionMeritFunc(SVolCalibrationInputPtr & volCalInputPtr);
double CapAndSwaptionMeritFunc(const gsl_vector *v, void *params);
void createWorkSpace(CFakeMatrixPtr& out_dMatrxPtr);

double get_wall_time();

int zhaoming()
{
   clock_t startTime=clock(), endTime;
   double wallT0, wallT1;
   wallT0 = get_wall_time();

   SVolCalibrationInputPtr volCalInputPtr(new SVolCalibrationInput());

   volCalInputPtr->mcCapVolPtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcDiscountFuncPtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcCapletVolPtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcYearFracCapletVolSqPtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcCapPaymentCalendarDaysPtr = CListOfLongDates::CreateInstance();

   volCalInputPtr->mcEnumToStrMapPtr = CEnumToStringMap::CreateInstance();
   volCalInputPtr->mcStrToEnumMapPtr = CStringToEnumMap::CreateInstance();

   volCalInputPtr->mcYearFractItoIplus1Ptr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcYearFractRelToMktPtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcForwardLiborRatePtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcATMcapStrikePtr = CVolTimeSeries::CreateInstance();

   volCalInputPtr->mcIRSpvFloatingLegPtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcIRSpvFixedLegPtr = CVolTimeSeries::CreateInstance();
   volCalInputPtr->mcZetaPtr = CVolTimeSeries::CreateInstance();

   volCalInputPtr->mbOutput = false;

   for(int i=T0; i != TLast; i++)
   {
      volCalInputPtr->mcEnumToStrMapPtr->Add(static_cast<ePaymentPeriods>(i), sPaymentPeriods[i]);
      volCalInputPtr->mcStrToEnumMapPtr->Add(sPaymentPeriods[i], static_cast<ePaymentPeriods>(i));
   }

// read in cap data
   readCapsData ("caps_01212005.dat", volCalInputPtr->mcStrToEnumMapPtr, volCalInputPtr->mcCapPaymentCalendarDaysPtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcCapVolPtr, bVerbose);
   if (bVerbose) { checkReadCapsData(volCalInputPtr->mcCapPaymentCalendarDaysPtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcCapVolPtr); }

// set up all the dates correspond to the enum ePaymentPeriods; results are stored in mcCapPaymentCalendarDaysPtr as (int, LongDate)
   ePaymentPeriods	wholeYearStartEnum(T10Y);
   populateCapPaymentCalendar (volCalInputPtr->mcCapPaymentCalendarDaysPtr, wholeYearStartEnum);

// Interpolate Cap Volatilities and discount functions
   interpolateCapVolandDF (volCalInputPtr->mcCapVolPtr, volCalInputPtr->mcCapPaymentCalendarDaysPtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcEnumToStrMapPtr);

// year fractions and ATM cap strike

   getYearFractions(volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcYearFractRelToMktPtr, volCalInputPtr->mcCapPaymentCalendarDaysPtr);
   getForwardLiborRate(volCalInputPtr->mcForwardLiborRatePtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcYearFractItoIplus1Ptr);
   getATMcapStrike(volCalInputPtr->mcATMcapStrikePtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcEnumToStrMapPtr);

// calculate the volatility of all caplets
   cout << "Bootstrapping caplet volatility from caps ... \n";
   calcCapletsVol(volCalInputPtr->mcCapletVolPtr, volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcForwardLiborRatePtr, volCalInputPtr->mcCapVolPtr,
                  volCalInputPtr->mcATMcapStrikePtr, volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcYearFractRelToMktPtr, volCalInputPtr->mcEnumToStrMapPtr);
   calcYearFracTimesCapletVolSq(volCalInputPtr->mcYearFracCapletVolSqPtr, volCalInputPtr->mcCapletVolPtr, volCalInputPtr->mcYearFractRelToMktPtr, volCalInputPtr->mcEnumToStrMapPtr);

// setup merit function and minimize it to get volatility parameters.
   // RebonatoVolFunc();
   // IntegrateRebonatoVolFuncSquare();
   // double params[4] = {0.1, 0.1, 0.1, 0.1};
   // double dL(0.0), dU(mcYearFractRelToMktPtr->GetValueByIndex(T5Y));
   // double dResult;
   // dResult = IntegrateRebonatoVolFuncSquare(dL, dU, params);
   // cout << dResult << endl;
   // MeritFunc += (YFrelToMkt*CapletVol**2 - IntegrateRebonatoVolFuncSquare)**2 

   int iDimension(4), iMaxIter(500000), iOutputFrequency(1000);
   double dParams[2*TLast];
   double dStartPt[4] = {0.1, 0.1, 0.1, 0.1};
   double dSpan[4] = {1.0, 1.0, 1.0, 1.0};
   // double dStartPt[4] = {1.0, 1.0, 1.0, 1.0};
   // double dSpan[4] = {0.1, 0.1, 0.1, 0.1};
   double dTol(1.0e-8);
   string sMinimizer("simplex2");

/*   for (int i=T3M; i<LastPaymentPeriodToUse; i++)
   {
     // dParams[i] = mcYearFractRelToMktPtr->GetValueByIndex(i+1);		// testing error in eqn and table 9.15 on page 159/180 (compare to table 7.5 on page 77/98)
     dParams[i] = volCalInputPtr->mcYearFractRelToMktPtr->GetValueByIndex(i);
     dParams[i+TLast] = volCalInputPtr->mcYearFracCapletVolSqPtr->GetValueByIndex(i);
   }
*/

   cout << "Calibrating Rebonato Volatility function parameters using caplets ... \n";

   GSL_f_minimizer_wrapper(CapMeritFunc, iDimension, volCalInputPtr, dStartPt, dSpan, iMaxIter, dTol, sMinimizer, true, iOutputFrequency);
   // GSL_f_minimizer_wrapper(CapMeritFunc, iDimension, dParams, dStartPt, dSpan, iMaxIter, dTol, sMinimizer, true, iOutputFrequency);
   // for (int i=0; i<iDimension; i++) { std::cout << i << "\t" << dStartPt[i] << std::endl; }
   for (int i=0; i<iDimension; i++) { volCalInputPtr->RebonatoVolParams[i] = dStartPt[i]; }

// SWAPTIONS
// For swap NxM (start in N years and last for M years), say payment is made quarterly,
// I will be using the indexes defined by the enum ePaymentPeriods.
// the Libor rate that will be used starts at 4N, lasts 4M, so it ends as 4N+4M-1
//                                             0                           n=4M-1
// We only use the swaptions that have 4N+4M-1 < LastPaymentPeriodToUse
//
   //int iSwaptionDimension(8);
   // double dSwaptionStartPt[8] = {0.1, 0.1, -0.1, 0.3, 0.04, -0.04, 0.001, 0.001};
   // double dSwaptionStartPt[8] = {6.570105, -6.583219, 15.187660, 1.104976, 3.226231, -2.668102, -0.328448, 0.069869};
   // int iSwaptionDimension(1);
   // double dSwaptionStartPt[1] = {1.0};
   // double dSwaptionSpan[1] = {0.1};
   //int iSwaptionDimension(2);
   //double dSwaptionStartPt[2] = {1.0, 0.5};
   //double dSwaptionSpan[2] = {1.0, 1.0};
   // int iSwaptionDimension(3);
   // double dSwaptionStartPt[3] = {1.0, 1.0, 0.01};
   // double dSwaptionSpan[3] = {1.0, 1.0, 1.0};
   //double dSwaptionStartPt[8] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
   //double dSwaptionSpan[8] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
   // double dSwaptionSpan[8] = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};

   const int iSwaptionDimension(40);
   double dSwaptionStartPt[iSwaptionDimension];
   double dSwaptionSpan[iSwaptionDimension];
   for (int i=0; i<iSwaptionDimension; i++)
   {
      dSwaptionStartPt[i] = 1.57;
      dSwaptionSpan[i] = 1.0;
   }

   volCalInputPtr->mcSwaptionData = readIn2dData("swaption_0121205.dat");
   volCalInputPtr->mcSwaptionParamNum = iSwaptionDimension;

   cout << "Calibrating Volatility Correlation function parameters using swaptions ... \n";

   // GSL_f_minimizer_wrapper(SwaptionMeritFunc, iSwaptionDimension, volCalInputPtr, dSwaptionStartPt, dSwaptionSpan, iMaxIter, dTol, sMinimizer, true, iOutputFrequency);
   //for (int i=0; i<8; i++) volCalInputPtr->RebonatoVolParams[i+4] = dSwaptionStartPt[i];	// theta and phi
   // for (int i=0; i<iSwaptionDimension; i++) volCalInputPtr->RebonatoVolParams[i+4] = dSwaptionStartPt[i];	// theta and phi
   // calcSwaptionMeritFunc(volCalInputPtr);

   const int iCapAndSwaptionDimension(iSwaptionDimension+4);
   double dCapAndSwaptionStartPt[iCapAndSwaptionDimension];
   double dCapAndSwaptionSpan[iCapAndSwaptionDimension];
   for (int i=0; i<iCapAndSwaptionDimension; i++)
   {
      dCapAndSwaptionStartPt[i] = 1.57;
      dCapAndSwaptionSpan[i] = 1.0;
   }

   // checkSigam_ixj_Integral(dStartPt);

   GSL_f_minimizer_wrapper(CapAndSwaptionMeritFunc, iCapAndSwaptionDimension, volCalInputPtr, dCapAndSwaptionStartPt, dCapAndSwaptionSpan, iMaxIter, dTol, sMinimizer, true, iOutputFrequency);
   for (int i=0; i<iCapAndSwaptionDimension; i++) volCalInputPtr->RebonatoVolParams[i] = dCapAndSwaptionStartPt[i];
   volCalInputPtr->mbOutput = true;
   calcCapMeritFunc(volCalInputPtr);
   calcSwaptionMeritFunc(volCalInputPtr);

// Loop over swaptions NxM
/*   int  N(1), M(1);
   int	startIndex(4*N);
   int	endIndex(4*N+4*M-1);

   swaptionVolSqUnderLMM(volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcForwardLiborRatePtr, volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcYearFractRelToMktPtr, startIndex, endIndex, dStartPt);
*/

   // CFakeMatrixPtr mcFakeMatrixPtr(new CFakeMatrix());
   // createWorkSpace(mcFakeMatrixPtr);

   endTime = clock();
   cout << "\tTotal CPU time spent is " << ((endTime - startTime)/(double)CLOCKS_PER_SEC) << " seconds.\n";
   wallT1 = get_wall_time();
   cout << "\tWall time spent is " << wallT1 - wallT0 << " seconds." << endl;

   return 0;
}

int main()
{
	//zhaoming();
	CCalibration::Run();

	//gsl_multimin_function minex_func;

	//gsl_min_function foo = [](const gsl_vector * v, void * p){const int iSize(v->size); cout << iSize << endl; return 0.0; };
	//minex_func.f = foo;
	//gsl_vector * x;
	//x = gsl_vector_alloc(10);
	//
	//foo(x, x);
	return 0;
}

void readCapsData(const string& inp_fileName, CStringToEnumMapPtr& inp_StrToEnumMapPtr,
                   CListOfLongDatesPtr & out_DatesPtr, CVolTimeSeriesPtr & out_DiscountFuncPtr, CVolTimeSeriesPtr & out_dCapVolPtr,
		   bool bVerbose)
{
  fstream       myFile;
  string        tmpStr, tmpDateStr;
  double        tmpDbl, dDF, dCapVol;

  string	sMarketDate, sCapStartDate, sTimeLapseCode;
  ePaymentPeriods ePP;

  LongDate	lMarketDate, lCapStartDate, tmpLDate;

  myFile.open(inp_fileName.c_str(), ios::in);

  if (myFile.is_open())
  {
     if (bVerbose) cout << "\t opened file " << inp_fileName << " for input ... ";

     myFile >> tmpStr >> sMarketDate >> tmpDbl >> tmpStr;
     CDateMath::ExtractFormattedDateAsLong(sMarketDate,dateDelimeter,lMarketDate);

     myFile >> sTimeLapseCode >> sCapStartDate >> dDF >> tmpStr;
     CDateMath::ExtractFormattedDateAsLong(sCapStartDate,dateDelimeter,lCapStartDate);
     ePP = static_cast<ePaymentPeriods>(inp_StrToEnumMapPtr->GetValueByIndex(sTimeLapseCode));
     out_DatesPtr->Add(ePP, lCapStartDate);
     out_DiscountFuncPtr->Add(ePP, dDF);
     out_dCapVolPtr->Add(ePP, 0.0);

     if (bVerbose) cout << "\n";
     for (int i = T3M; i <= T9M; i++)
     {
       myFile >> sTimeLapseCode >> tmpDateStr >> dDF >> tmpStr;
       CDateMath::ExtractFormattedDateAsLong(tmpDateStr,dateDelimeter,tmpLDate);
       ePP = static_cast<ePaymentPeriods>(inp_StrToEnumMapPtr->GetValueByIndex(sTimeLapseCode));
       out_DatesPtr->Add(ePP, tmpLDate);
       out_DiscountFuncPtr->Add(ePP, dDF);
       out_dCapVolPtr->Add(ePP, 0.0);

       if (bVerbose) cout << "\t\t i = " << i << "\n";
     }
     
     int icount(T9M);
     while ( myFile >> sTimeLapseCode >> tmpDateStr >> dDF >> dCapVol )
     {
       icount++;

       CDateMath::ExtractFormattedDateAsLong(tmpDateStr,dateDelimeter,tmpLDate);
       ePP = static_cast<ePaymentPeriods>(inp_StrToEnumMapPtr->GetValueByIndex(sTimeLapseCode));
       out_DatesPtr->Add(ePP, tmpLDate);
       out_DiscountFuncPtr->Add(ePP, dDF);
       out_dCapVolPtr->Add(ePP, dCapVol);

       if (bVerbose) cout << "\t\t i = " << icount << "\n";
     }

     myFile.close();

     if (bVerbose) cout << "\t done!\n";
  }
  else
  {
     cout << "Unable to open file " << inp_fileName << endl;
     exit(1);
  }
}

void checkReadCapsData (CListOfLongDatesPtr & inp_DatesPtr, CVolTimeSeriesPtr & inp_DiscountFuncPtr, CVolTimeSeriesPtr & inp_dCapVolPtr)
{
   LongDate	tmp_lDate;
   CDate	my_cDate(20050125);

   cout << "Output discount function and caps volatility ... \n";
   int i(0);
   inp_DatesPtr->First();
   while ( ! inp_DatesPtr->END() )
   {
      tmp_lDate = inp_DatesPtr->GetCurrentIndex();
      cout << "\t" << inp_DatesPtr->GetCurrentValue()
	   << "\t" << (my_cDate.Add(MONTH, 3*i)).ToLong()
	   << "\t" << inp_DiscountFuncPtr->GetValueByIndex(tmp_lDate)
	   << "\t" << inp_dCapVolPtr->GetValueByIndex(tmp_lDate)
	   << endl;
      inp_DatesPtr->Next();
      i++;
   }
}


double calcCapPrice(ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_cDiscountFuncPtr,
					  CVolTimeSeriesPtr& inp_cForwardLiborRatePtr,
					  CVolTimeSeriesPtr& inp_cCapVolPtr,
					  CVolTimeSeriesPtr& inp_cATMcapStrikePtr,
					  CVolTimeSeriesPtr& inp_cYearFractItoIplus1Ptr,
					  CVolTimeSeriesPtr& inp_cYearFractRelToMktPtr)
{
   // calculated the price of a cap with term ePP using the constant cap volatility and
   // ATM strike. The price is the sum of prices from each period.

   ePaymentPeriods	ePPstart(T3M);
   double		dCapPrice(0.0), d1, d2;
   double		dDF, dDT, dFR, dYFrelToMkt;

   double		dATMcapStrike(inp_cATMcapStrikePtr->GetValueByIndex(ePP));
   double		dCapVol(inp_cCapVolPtr->GetValueByIndex(ePP));

   int			i(0);

   for (i=ePPstart; i < ePP; i++)
   {
      dYFrelToMkt = inp_cYearFractRelToMktPtr->GetValueByIndex(i);
      dDF = inp_cDiscountFuncPtr->GetValueByIndex(i+1);
      dDT = inp_cYearFractItoIplus1Ptr->GetValueByIndex(i);
      dFR = inp_cForwardLiborRatePtr->GetValueByIndex(i);
      dCapPrice += BlackScholesPrice(dDF, dDT, dFR, dATMcapStrike, dCapVol, dYFrelToMkt);
      if (bVerbose) { cout << "\t cap: " << i << "\t" << dCapPrice << "\t" << dCapVol <<  endl; }
   }

   return dCapPrice;
}

double BlackScholesPrice(double& inp_dDiscountFactor, double& inp_dDeltaT, double& inp_dForwardPrice, double& inp_dStrikePrice, double& inp_dBlackVol, double& inp_dYearFracOfTexpire)
{
   // calculate option price discounted to time zero for given volatility

   double	dPrice(0);
   double	d1, d2, dSigmaRootTime;

   dSigmaRootTime = inp_dBlackVol * sqrt(inp_dYearFracOfTexpire);
   d1 = (log(inp_dForwardPrice/inp_dStrikePrice))/dSigmaRootTime + dSigmaRootTime/2.0;
   d2 = d1 - dSigmaRootTime;
   dPrice = inp_dDiscountFactor * inp_dDeltaT *(inp_dForwardPrice * gsl_cdf_ugaussian_P(d1) - inp_dStrikePrice * gsl_cdf_ugaussian_P(d2));

   return dPrice;
}

struct SBlackSholesPricingParams
{
    double dPrice;
    double dDiscountFactor;
    double dDeltaT;
    double dForwardPrice;
    double dStrikePrice;
    double dBlackVol;
    double dYearFracOfTexpire;
};

double BlackScholesPriceDiff(double inp_dBlackVol, void *inp_params)
{
   // price differences of the input price and the one calculated using the input volatility and Black Scholes formula.

   struct SBlackSholesPricingParams *p = (struct SBlackSholesPricingParams *) inp_params;

   return (p->dPrice - BlackScholesPrice(p->dDiscountFactor, p->dDeltaT, p->dForwardPrice, p->dStrikePrice, inp_dBlackVol, p->dYearFracOfTexpire));
}

double BlackScholesVol(double & inp_dPrice, double& inp_dDiscountFactor, double& inp_dDeltaT, double& inp_dForwardPrice, double& inp_dStrikePrice, double& inp_dYearFracOfTexpire)
{
   // calculate volatility for given price

   double	dVol(0);
   int iMaxIter(100);
   double xLow(1.0e-6), xHigh(10.0), expectedRoot(0.1641);
   double epsrel(1.0e-6), epsabs(1.0e-6);
   string sRootFinder("BrentDekker");
   // string sRootFinder("biSection");
   // string sRootFinder("falsePositive");

   // Not sure how to make struct work.
   // struct SBlackSholesPricingParams sParams = { inp_dPrice, inp_dDiscountFactor, inp_dDeltaT, inp_dForwardPrice, inp_dStrikePrice, 0.0, inp_dYearFracOfTexpire};
   double dParams[7] = { inp_dPrice, inp_dDiscountFactor, inp_dDeltaT, inp_dForwardPrice, inp_dStrikePrice, 0.0, inp_dYearFracOfTexpire};

   GSL_f_rootFinder_1d_wrapper(BlackScholesPriceDiff, dParams, xLow, xHigh, expectedRoot, iMaxIter, epsabs, epsrel, dVol, sRootFinder, bVerbose);

   return dVol;
}

double calcCapPriceFromCaplet(ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_cDiscountFuncPtr,
					  CVolTimeSeriesPtr& inp_cForwardLiborRatePtr,
					  CVolTimeSeriesPtr& inp_cCapletVolPtr,
					  CVolTimeSeriesPtr& inp_cATMcapStrikePtr,
					  CVolTimeSeriesPtr& inp_cYearFractItoIplus1Ptr,
					  CVolTimeSeriesPtr& inp_cYearFractRelToMktPtr)
{
   // calculate the price for a cap by summing up the contribution of all the caplets.
   // ePP is the term of the cap

   ePaymentPeriods	ePPstart(T3M);
   double		dCapPrice(0.0), d1, d2;
   double		dDF, dDT, dFR, dYFrelToMkt;

   double		dATMcapStrike(inp_cATMcapStrikePtr->GetValueByIndex(ePP));
   double		dCapletVol;

   int			i(0);

   for (i=ePPstart; i < ePP; i++)
   {
      dYFrelToMkt = inp_cYearFractRelToMktPtr->GetValueByIndex(i);
      dDF = inp_cDiscountFuncPtr->GetValueByIndex(i+1);
      dDT = inp_cYearFractItoIplus1Ptr->GetValueByIndex(i);
      dFR = inp_cForwardLiborRatePtr->GetValueByIndex(i);
      dCapletVol = inp_cCapletVolPtr->GetValueByIndex(i);
      dCapPrice += BlackScholesPrice(dDF, dDT, dFR, dATMcapStrike, dCapletVol, dYFrelToMkt);
      if (bVerbose) { cout << "\t All caplets: " << i << "\t" << dCapPrice << "\t" << dCapletVol <<  endl; }
   }

   return dCapPrice;
}

double calcCapPriceFromCapletLessLast(ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_cDiscountFuncPtr,
					  CVolTimeSeriesPtr& inp_cForwardLiborRatePtr,
					  CVolTimeSeriesPtr& inp_cCapletVolPtr,
					  CVolTimeSeriesPtr& inp_cATMcapStrikePtr,
					  CVolTimeSeriesPtr& inp_cYearFractItoIplus1Ptr,
					  CVolTimeSeriesPtr& inp_cYearFractRelToMktPtr)
{
   // without the last caplet; used by bootstrapping caplet volatility
   // ePP is the term of the cap

   ePaymentPeriods	ePPstart(T3M);
   double		dCapPrice(0.0), d1, d2;
   double		dDF, dDT, dFR, dYFrelToMkt;

   double		dATMcapStrike(inp_cATMcapStrikePtr->GetValueByIndex(ePP));
   double		dCapletVol;

   int			i(0);

   for (i=ePPstart; i < ePP-1; i++)
   {
      dYFrelToMkt = inp_cYearFractRelToMktPtr->GetValueByIndex(i);
      dDF = inp_cDiscountFuncPtr->GetValueByIndex(i+1);
      dDT = inp_cYearFractItoIplus1Ptr->GetValueByIndex(i);
      dFR = inp_cForwardLiborRatePtr->GetValueByIndex(i);
      dCapletVol = inp_cCapletVolPtr->GetValueByIndex(i);
      dCapPrice += BlackScholesPrice(dDF, dDT, dFR, dATMcapStrike, dCapletVol, dYFrelToMkt);
      if (bVerbose) { cout << "\t caplet: " << i << "\t" << dCapPrice << "\t" << dCapletVol << endl; }
   }

   return dCapPrice;
}

double calcLastCapletVol(double & inp_dPrice, ePaymentPeriods& ePP, CVolTimeSeriesPtr& inp_DiscountFuncPtr,
				CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
				CVolTimeSeriesPtr& inp_ATMcapStrikePtr,
				CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr,
				CVolTimeSeriesPtr& inp_YearFractRelToMktPtr)
{
   // given the price of the last caplet (subtract all the other caplets from the cap price), this routine
   // calculate the volatility of the last caplet.

   double dDiscountFactor(inp_DiscountFuncPtr->GetValueByIndex(ePP+1));
   double dDeltaT(inp_YearFractItoIplus1Ptr->GetValueByIndex(ePP));
   double dForwardPrice(inp_ForwardLiborRatePtr->GetValueByIndex(ePP));
   double dStrikePrice(inp_ATMcapStrikePtr->GetValueByIndex(ePP+1));
   double dYearFracOfTexpire(inp_YearFractRelToMktPtr->GetValueByIndex(ePP));

   double dBlackVol;

   dBlackVol = BlackScholesVol(inp_dPrice, dDiscountFactor, dDeltaT, dForwardPrice, dStrikePrice, dYearFracOfTexpire);

   return dBlackVol;
}

void calcCapletsVol(CVolTimeSeriesPtr& inp_CapletVolPtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
		    CVolTimeSeriesPtr& inp_CapVolPtr, CVolTimeSeriesPtr& inp_ATMcapStrikePtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr,
		    CVolTimeSeriesPtr& inp_YearFractRelToMktPtr, CEnumToStringMapPtr& inp_EnumToStrMapPtr)
{
// Need: discount func, forward Libor rate during period i-1 to i, cap vol, cumulative pdf for N(0,1), ATM cap strike, year fractions (i-1 to i, relative to market day)
//	 mcDiscountFuncPtr, mcForwardLiborRatePtr, mcCapVolPtr, gsl_cdf_ugaussian_P(x), mcATMcapStrikePtr, mcYearFractItoIplus1Ptr, mcYearFractRelToMktPtr

   double dCapPrice(0.0), dCapPriceFromCapletWOlastOne(0.0), dCapPriceFromCaplet(0.0), dCapletVol(0.0), dLastCapletPrice(0.0);
   ePaymentPeriods	ePP(T6M), ePPlastCaplet(T3M);
   cout << "\t" << "Tenor" << "\t Price(cap)" << "\t Price(w/o last caplet)" << "\t Price(caplets)" << "\t Caplet Vol" << "\t Cap Vol\n";
   cout.precision(6);
   cout.setf(ios::fixed);
   int	iCoutFieldWidth(12);
   for (int i = T6M; i < TLast; i++)	// loop through cap terms
   {
     ePP = static_cast<ePaymentPeriods>(i);
     ePPlastCaplet = static_cast<ePaymentPeriods>(i-1);
     dCapPrice = calcCapPrice(ePP, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_CapVolPtr, inp_ATMcapStrikePtr, inp_YearFractItoIplus1Ptr, inp_YearFractRelToMktPtr);
     dCapPriceFromCapletWOlastOne = calcCapPriceFromCapletLessLast(ePP, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_CapletVolPtr, inp_ATMcapStrikePtr,
									inp_YearFractItoIplus1Ptr, inp_YearFractRelToMktPtr);
     dLastCapletPrice = dCapPrice - dCapPriceFromCapletWOlastOne;
     dCapletVol = calcLastCapletVol(dLastCapletPrice, ePPlastCaplet, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_ATMcapStrikePtr, inp_YearFractItoIplus1Ptr, inp_YearFractRelToMktPtr);
     inp_CapletVolPtr->Add(ePPlastCaplet, dCapletVol);
     dCapPriceFromCaplet = calcCapPriceFromCaplet(ePP, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_CapletVolPtr, inp_ATMcapStrikePtr, inp_YearFractItoIplus1Ptr, inp_YearFractRelToMktPtr);
     cout << "\t" << inp_EnumToStrMapPtr->GetValueByIndex(ePP) << "\t" << setw(iCoutFieldWidth) << dCapPrice
						       << "\t" << setw(iCoutFieldWidth) << dCapPriceFromCapletWOlastOne
						       << "\t" << setw(iCoutFieldWidth) << dCapPriceFromCaplet
						       << "\t" << setw(iCoutFieldWidth) << dCapletVol
						       << "\t" << setw(iCoutFieldWidth) << inp_CapVolPtr->GetValueByIndex(i) << endl;
   }
}

/* void populateCapPaymentCalendar_orig (CListOfLongDatesPtr & inp_CapPaymentCalendarDaysPtr)
{
   // List of all the dates corresponding to the enum ePaymentPeriods.
   // inp_CapPaymentCalendarDaysPtr points to collection of (i, LongDate), where i is driven by the enum ePaymentPeriods.

   // SHolidayCalendarsPtr sHolidayCalendarPtr(new SHolidayCalendars());
   // sHolidayCalendarPtr->sInputPtr->dtStart = CDate(19500101);
   // sHolidayCalendarPtr->sInputPtr->dtEnd = CDate(20991231);
   // Get(SHOLIDAYCALENDARS, sHolidayCalendarPtr);

   // FDateAdjustor dateAdjustor(FOLLOWING, sHolidayCalendarPtr);

   CDate	capStartDate(inp_CapPaymentCalendarDaysPtr->GetFirst());
   CDate	tmpDate;
   for (int i=T3M; i <= T5Y9M; i++)
   {
      tmpDate = capStartDate.Add(MONTH, i*3);
      // dateAdjustor(tmpDate);
      inp_CapPaymentCalendarDaysPtr->Add(i, tmpDate.ToLong());
      if (bVerbose) { cout << "\t" << i << "\t" << inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i) << endl; }
   }

   for (int i=T6Y; i <= T20Y; i++)
   {
      tmpDate = capStartDate.Add(YEAR, (i-23)+5);
      // dateAdjustor(tmpDate);
      inp_CapPaymentCalendarDaysPtr->Add(i, tmpDate.ToLong());
      if (bVerbose) { cout << "\t" << i << "\t" << inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i) << endl; }
   }
} */

void populateCapPaymentCalendar (CListOfLongDatesPtr & inp_CapPaymentCalendarDaysPtr, ePaymentPeriods & inp_wholeYearStart)
{
   // List of all the dates corresponding to the enum ePaymentPeriods.
   // inp_CapPaymentCalendarDaysPtr points to collection of (i, LongDate), where i is driven by the enum ePaymentPeriods.

   // SHolidayCalendarsPtr sHolidayCalendarPtr(new SHolidayCalendars());
   // sHolidayCalendarPtr->sInputPtr->dtStart = CDate(19500101);
   // sHolidayCalendarPtr->sInputPtr->dtEnd = CDate(20991231);
   // Get(SHOLIDAYCALENDARS, sHolidayCalendarPtr);

   // FDateAdjustor dateAdjustor(FOLLOWING, sHolidayCalendarPtr);

   CDate	capStartDate(inp_CapPaymentCalendarDaysPtr->GetFirst());
   CDate	tmpDate;
   for (int i=T3M; i < inp_wholeYearStart; i++)
   {
      tmpDate = capStartDate.Add(MONTH, i*3);
      // dateAdjustor(tmpDate);
      inp_CapPaymentCalendarDaysPtr->Add(i, tmpDate.ToLong());
      if (bVerbose) { cout << "\t" << i << "\t" << inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i) << endl; }
   }

   for (int i=inp_wholeYearStart; i < TLast; i++)
   {
      tmpDate = capStartDate.Add(YEAR, (i-inp_wholeYearStart)+inp_wholeYearStart/4);
      //tmpDate = capStartDate.Add(YEAR, (i-23)+5);
      // dateAdjustor(tmpDate);
      inp_CapPaymentCalendarDaysPtr->Add(i, tmpDate.ToLong());
      if (bVerbose) { cout << "\t" << i << "\t" << inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i) << endl; }
   }
}

void interpolateCapVolandDF (CVolTimeSeriesPtr& inp_CapVolPtr, CListOfLongDatesPtr& inp_CapPaymentCalendarDaysPtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CEnumToStringMapPtr& inp_EnumToStrMapPtr)
{
  // use 1-year cap vol for 6 and 9 month caps.
  // this could be generalized to use the first avaliable cap volatility for all the shorter term caps.

   double dCap1Yvol(inp_CapVolPtr->GetValueByIndex(T1Y));
   for (int i = T6M; i <= T9M; i++)
   {
     inp_CapVolPtr->UpdateEntry(i, dCap1Yvol);
     if (bVerbose) { cout << "\t" << i << "\t" << inp_CapVolPtr->GetValueByIndex(i) << endl; }
   }

  // interpolate the rest of the missing cap vols and discount factors.
   double dCapVol(0);
   CDateDoubleMapPtr cDateCapVolPtr(CDateDoubleMap::CreateInstance()), cDateDFPtr(CDateDoubleMap::CreateInstance());		// these are used for interpolation; indexed by date, not by enum.

   int i(0);
   double dDF(0);
   CDate	tmpDate;

   inp_CapVolPtr->First();
   while ( ! inp_CapVolPtr->END())
   {
     i = inp_CapVolPtr->GetCurrentIndex();
     tmpDate = inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i);
     cDateCapVolPtr->Add(tmpDate, inp_CapVolPtr->GetCurrentValue());
	 cDateDFPtr->Add(tmpDate, inp_DiscountFuncPtr->GetValueByIndex(i));
     inp_CapVolPtr->Next();
   }

   for (int i = T1Y; i < T20Y; i++)
   {
     tmpDate = inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i);
     dCapVol = CMath::Interpolate(LINEAR, *cDateCapVolPtr, tmpDate); 
     inp_CapVolPtr->Add(i, dCapVol);

	 dDF = CMath::Interpolate(LINEAR, *cDateDFPtr, tmpDate);
     inp_DiscountFuncPtr->Add(i, dDF);
   }

   for (int i = T0; i < TLast; i++)
   {
     if (bVerbose) { cout << "\tINTERP: " << inp_EnumToStrMapPtr->GetValueByIndex(i) << "\t" << inp_CapVolPtr->GetValueByIndex(i) << "\t\t" << inp_DiscountFuncPtr->GetValueByIndex(i) << endl; }
   }
}

void getYearFractions(CVolTimeSeriesPtr& out_YearFractItoIplus1Ptr, CVolTimeSeriesPtr& out_YearFractRelToMktPtr, CListOfLongDatesPtr& inp_CapPaymentCalendarDaysPtr)
{
   // create list of year fractions of the dates listed in inp_CapPaymentCalendarDaysPtr.
   // dt(i, i+1): out_YearFractItoIplus1Ptr
   // dt(t0, i) : out_YearFractRelToMktPtr

   // FYearFrac	fYearFrac(SINGLE, ACT_360);
	CYearFracPtr cYFACT360(CYearFracActual360::CreateInstance(SINGLE));
   double	tmpDbl;

   for (int i = T0; i < TLast-1; i++)
   {
     // tmpDbl = fYearFrac(inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i+1), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i+1));
     tmpDbl = cYFACT360->GetYearFrac(inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i+1), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i+1));
     out_YearFractItoIplus1Ptr->Add(i, tmpDbl);
   }

   for (int i = T0; i < TLast; i++)
   {
     // tmpDbl = fYearFrac(inp_CapPaymentCalendarDaysPtr->GetValueByIndex(T0), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i));
     tmpDbl = cYFACT360->GetYearFrac(inp_CapPaymentCalendarDaysPtr->GetValueByIndex(T0), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i), inp_CapPaymentCalendarDaysPtr->GetValueByIndex(i));
     out_YearFractRelToMktPtr->Add(i, tmpDbl);
   }
}

void getForwardLiborRate(CVolTimeSeriesPtr& out_ForwardLiborRatePtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr)
{
   // [DF(i)/DF(i+1) - 1]/dt(i, i+1)

   double	tmpDbl;

   for (int i = T0; i < TLast-1; i++)
   {
     tmpDbl = inp_YearFractItoIplus1Ptr->GetValueByIndex(i);
     out_ForwardLiborRatePtr->Add(i, (inp_DiscountFuncPtr->GetValueByIndex(i)/inp_DiscountFuncPtr->GetValueByIndex(i+1) - 1.0)/tmpDbl);
   }
}

void getATMcapStrike(CVolTimeSeriesPtr& out_ATMcapStrikePtr, CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, CEnumToStringMapPtr& inp_EnumToStrMapPtr)
{
   // indexed using the term of the cap

   double	cumSum(0.0);

   for (int i = T6M; i < TLast; i++)
   {
     cumSum += inp_DiscountFuncPtr->GetValueByIndex(i) * inp_YearFractItoIplus1Ptr->GetValueByIndex(i-1);
     out_ATMcapStrikePtr->Add(i, (inp_DiscountFuncPtr->GetValueByIndex(T3M)-inp_DiscountFuncPtr->GetValueByIndex(i))/cumSum);
     if (bVerbose) { cout << "\tATM cap Strike: " << inp_EnumToStrMapPtr->GetValueByIndex(i) << "\t" << out_ATMcapStrikePtr->GetValueByIndex(i) << endl; }
   }
}

void calcYearFracTimesCapletVolSq(CVolTimeSeriesPtr& out_YearFracCapletVolSqPtr, CVolTimeSeriesPtr& inp_CapletVolPtr, CVolTimeSeriesPtr& inp_YearFractRelToMktPtr, CEnumToStringMapPtr& inp_EnumToStrMapPtr)
{
   double	tmpDbl;

   for (int i = T3M; i < TLast-1; i++)
   {
     // tmpDbl = inp_YearFractRelToMktPtr->GetValueByIndex(i+1);		// testing error in eqn and table 9.15 on page 159/180 (compare to table 7.5 on page 77/98)
     tmpDbl = inp_YearFractRelToMktPtr->GetValueByIndex(i);
     tmpDbl *= pow(inp_CapletVolPtr->GetValueByIndex(i), 2);
     out_YearFracCapletVolSqPtr->Add(i, tmpDbl);
     // if (bVerbose) { cout << "\t YearFraction x CapletVolSquare: " << inp_EnumToStrMapPtr->GetValueByIndex(i) << "\t" << out_YearFracCapletVolSqPtr->GetValueByIndex(i) << endl; }
     if (bVerbose) { cout << "\t YearFraction x CapletVolSquare: " << inp_EnumToStrMapPtr->GetValueByIndex(i) << "\t" << out_YearFracCapletVolSqPtr->GetValueByIndex(i)
                          << "\t" << inp_YearFractRelToMktPtr->GetValueByIndex(i) << "\t" << inp_CapletVolPtr->GetValueByIndex(i) << endl; }
   }
}

double RebonatoVolFunc(double & inp_dYearFraction, double inp_dParams[])
{
  double	dVol(0.0);
  dVol = inp_dParams[0] + (inp_dParams[1] + inp_dParams[2]*inp_dYearFraction) * exp(-inp_dParams[3]*inp_dYearFraction);
  return dVol;
}

double RebonatoVolFuncSquare(double inp_dYearFraction, void * inp_dParams)
{
  // this is not exactly the square of Rebonato volatility.
  // First squared the volatility function then multiplied by 360.
  // The purpose is to treat the squared vol as the integrand while the integral was transformed from t to year fraction dt/360.
  // Correction: there shouldn't be a factor of 360! It was cancelled by the t in t*sigma^2

  double *p = (double *) inp_dParams;

  double	dVol2(0.0);
  dVol2 = pow(RebonatoVolFunc(inp_dYearFraction, p), 2);
  return dVol2;
}

double IntegrateRebonatoVolFuncSquare(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[])
{
  size_t iMaxNumOfIntervals(1000), workSpaceSize(1000);
  double epsrel(1.0e-8), epsabs(0.0e-6);
  double dResult, dAbsErr;
  int GaussKronrodPtsKey(1);

/*  GSL_f_minimizer_wrapper(double (*func) (double *, void *), double inp_dFuncParams[],
                            const double inp_dLowerLimit, const double inp_dUpperLimit, size_t& inp_iMaxNumOfIntervals,
                            const double& epsabs, const double& epsrel, double& out_dAbsErr,
                            size_t& workSpaceSize, const int GaussKronrodPtsKey=3, const bool& inp_bVerbose=true) */
  dResult = GSL_integration_wrapper(RebonatoVolFuncSquare, inp_dParams, inp_dLowerLimit, inp_dUpperLimit, iMaxNumOfIntervals, epsabs, epsrel, dAbsErr, workSpaceSize, GaussKronrodPtsKey, true);

  return dResult;
}

double IntegrateRebonatoVolFuncSquare_analytical(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[])
{
  double dResult(0.0);
  double dTi0(inp_dUpperLimit);
  double v1, v2, v3, v4, tmpVal, tmpVal2;
  double V4dTi0, V4_1, V4_2, V4_3;

  v1 = inp_dParams[0];
  v2 = inp_dParams[1];
  v3 = inp_dParams[2];
  v4 = inp_dParams[3];

  V4dTi0 = v4 * dTi0;
  V4_1 = 1.0/v4;
  V4_2 = V4_1/v4;
  V4_3 = V4_2/v4;

  tmpVal = exp(-V4dTi0);
  tmpVal2 = tmpVal * tmpVal;

  dResult = v1*v1*dTi0 - v2*v2*V4_1*0.5*(tmpVal2 - 1.0) - 0.5*v2*v3*V4_2*((2.0*V4dTi0 + 1.0)*tmpVal2 - 1.0);
  dResult += -0.25*v3*v3*V4_3*((2.0*V4dTi0*V4dTi0 + 2.0*V4dTi0 + 1.0)*tmpVal2 - 1.0);
  dResult += -2.0*v1*v2*V4_1*(tmpVal - 1.0) - 2.0*v1*v3*V4_2*((V4dTi0 + 1.0)*tmpVal - 1.0);

  return dResult;
}

double calcCapMeritFunc(SVolCalibrationInputPtr & volCalInputPtr)
{
   double dResult(0.0), dDiff;
   double dLowerLimit(0.0), dUpperLimit(0.0);

   double obsVal, modelVal;

  // omp_set_num_threads(2);
  int i, chunk(1), TID, numOfThreads(1);
  #pragma omp parallel for num_threads(numOfThreads) default(shared) private(i, dUpperLimit, dDiff, TID) schedule(static,chunk) reduction(+:dResult)
  for (i=T3M; i<LastPaymentPeriodToUse; i++)
  {
    dUpperLimit = volCalInputPtr->mcYearFractRelToMktPtr->GetValueByIndex(i);
    // dDiff = volCalInputPtr->mcYearFracCapletVolSqPtr->GetValueByIndex(i) - IntegrateRebonatoVolFuncSquare_analytical(dLowerLimit, dUpperLimit, volCalInputPtr->RebonatoVolParams);
    obsVal = volCalInputPtr->mcYearFracCapletVolSqPtr->GetValueByIndex(i)/dUpperLimit;
    modelVal = IntegrateRebonatoVolFuncSquare_analytical(dLowerLimit, dUpperLimit, volCalInputPtr->RebonatoVolParams)/dUpperLimit;
    dDiff = obsVal - modelVal;
    dResult += dDiff * dDiff;

    if (volCalInputPtr->mbOutput) 
    {
       obsVal = sqrt(obsVal);
       modelVal = sqrt(modelVal);
       cout << obsVal << ", " << modelVal << "\t" << (1.0 - modelVal/obsVal) << "\t" << (1.0 - obsVal/modelVal) << endl;;
    }
  }

   return dResult;
}

double CapMeritFunc(const gsl_vector *v, void *params)
{
  // MeritFunc += (YFrelToMkt*CapletVol**2 - IntegrateRebonatoValFuncSquare)**2
  // params are year fraction relative to market day and yearFracTimesCapletVolSquare stacked together in one array (length 2*(TLast+1))
     // dParams[i] = volCalInputPtr->mcYearFractRelToMktPtr->GetValueByIndex(i);
     // dParams[i+TLast] = volCalInputPtr->mcYearFracCapletVolSqPtr->GetValueByIndex(i);

  SVolCalibrationInputPtr p = static_cast<SVolCalibrationInputPtr>(params);
  // double *p = (double *)params;
  double dResult(0.0);

  for (int i=0; i<4; i++) p->RebonatoVolParams[i] = gsl_vector_get(v, i);

  dResult = calcCapMeritFunc(p);

  return dResult;
}

// Below are swaption related routines
//
// The following diagram shows the index convention (f is forward rate)
//
// today         |--> f_0                                    |--> f_i                         |--> f_n
// |-------------|----------|----------|----------|----------|----------|----------|----------|----------|
//               t_0        t_1                              t_i                              t_n        t_(n+1)
//                                                           |<- dt_i ->|
//       zero coupon bond P_(i+1) or discount factor
// |------------------------------------------------------------------->

void calcIRSpresentValueFloatingLegUnitPrinciple(double out_IRSpvFloatingLegArr[], CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
                    CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, int& inp_startIndex, int & inp_endIndex)
{
   //        n
   // A_k = sum {P_(j+1)*f_j*dt_j}
   //       j=k

   double tmpDbl(0.0);

   for (int i=inp_startIndex; i <= inp_endIndex; i++)
   {
      tmpDbl += inp_DiscountFuncPtr->GetValueByIndex(inp_startIndex+1) * inp_ForwardLiborRatePtr->GetValueByIndex(inp_startIndex) * inp_YearFractItoIplus1Ptr->GetValueByIndex(inp_startIndex);
   }
   out_IRSpvFloatingLegArr[inp_startIndex] = tmpDbl;

   for (int i=inp_startIndex+1; i <= inp_endIndex; i++)
   {
      tmpDbl = inp_DiscountFuncPtr->GetValueByIndex(i+1) * inp_ForwardLiborRatePtr->GetValueByIndex(i) * inp_YearFractItoIplus1Ptr->GetValueByIndex(i);
      tmpDbl = out_IRSpvFloatingLegArr[i-1] - tmpDbl;
      out_IRSpvFloatingLegArr[i] = tmpDbl;
   }
}

void calcIRSpresentValueFixedLegUnitPrincipleUnitRate(double out_IRSpvFixedLegArr[], CVolTimeSeriesPtr& inp_DiscountFuncPtr,
                    CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, int& inp_startIndex, int & inp_endIndex)
{
   //        n
   // B_k = sum {P_(j+1)*dt_j}
   //       j=k

   double tmpDbl(0.0);

   for (int i=inp_startIndex; i <= inp_endIndex; i++)
   {
      tmpDbl += inp_DiscountFuncPtr->GetValueByIndex(inp_startIndex+1) * inp_YearFractItoIplus1Ptr->GetValueByIndex(inp_startIndex);
   } 
   out_IRSpvFixedLegArr[inp_startIndex] = tmpDbl;

   for (int i=inp_startIndex+1; i <= inp_endIndex; i++)
   {
      tmpDbl = inp_DiscountFuncPtr->GetValueByIndex(i+1) * inp_YearFractItoIplus1Ptr->GetValueByIndex(i);
      tmpDbl = out_IRSpvFixedLegArr[i-1] - tmpDbl;
      out_IRSpvFixedLegArr[i] = tmpDbl;
   }
}

void calcZeta(double out_zeta[], CVolTimeSeriesPtr& inp_DiscountFuncPtr,
                    CVolTimeSeriesPtr& inp_ForwardLiborRatePtr, CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, int& inp_startIndex, int & inp_endIndex)
{
   double tmpDbl(0.0);
   double A1, Ai, B1, Bi, fi, dti, Pi1;

   double IRSpvFloatingLegArr[LastPaymentPeriodToUse], IRSpvFixedLegArr[LastPaymentPeriodToUse];

   calcIRSpresentValueFloatingLegUnitPrinciple(IRSpvFloatingLegArr, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_YearFractItoIplus1Ptr, inp_startIndex, inp_endIndex);
   calcIRSpresentValueFixedLegUnitPrincipleUnitRate(IRSpvFixedLegArr, inp_DiscountFuncPtr, inp_YearFractItoIplus1Ptr, inp_startIndex, inp_endIndex);

   // A1 = inp_IRSpvFloatingLegPtr->GetValueByIndex(inp_startIndex);
   // B1 = inp_IRSpvFixedLegPtr->GetValueByIndex(inp_startIndex);
   A1 = IRSpvFloatingLegArr[inp_startIndex];
   B1 = IRSpvFixedLegArr[inp_startIndex];

   for (int i=inp_startIndex; i <= inp_endIndex; i++)
   {
      // Ai = inp_IRSpvFloatingLegPtr->GetValueByIndex(i);
      // Bi = inp_IRSpvFixedLegPtr->GetValueByIndex(i);
      Ai = IRSpvFloatingLegArr[i];
      Bi = IRSpvFixedLegArr[i];
      Pi1 = inp_DiscountFuncPtr->GetValueByIndex(i+1);
      fi  = inp_ForwardLiborRatePtr->GetValueByIndex(i);
      dti = inp_YearFractItoIplus1Ptr->GetValueByIndex(i);

      // tmpDbl = Pi1;					// without shape correction
      tmpDbl = Pi1 + (A1*Bi/B1 - Ai)/(1.0 + fi*dti);	// with shape correction
      tmpDbl *= fi*dti/A1;
      // out_zeta->Add(i, tmpDbl);
      out_zeta[i] = tmpDbl;
   }
}

double getCorrelation_JR(double beta[], double & inp_ti0, double & inp_tj0)
{
   double rho_ij(0.0);
   double Tmin(inp_ti0);

   if (inp_ti0 > inp_tj0) Tmin=inp_tj0;

   // rho_ij = beta[0];
   // rho_ij = exp(-beta[0]*fabs(inp_ti0 - inp_tj0));
   // rho_ij = exp(-beta[0]*fabs(inp_ti0 - inp_tj0)*exp(-beta[1]*Tmin));
   // rho_ij = (beta[1]) + (1.0 - (beta[1]))*exp(-beta[0]*fabs(inp_ti0 - inp_tj0));
   rho_ij = (beta[2]) + (1.0 - (beta[2]))*exp(-beta[0]*fabs(inp_ti0 - inp_tj0)*exp(-beta[1]*Tmin));

   return rho_ij;
}

double getCorrelation(int& inp_index, int & inp_jndex, double inp_theta[], double inp_phi[], double & inp_ti0, double & inp_tj0)
{
   double rho_ij(0.0);
   double theta_i, phi_i;
   double theta_j, phi_j;

   theta_i = inp_theta[0] + (inp_theta[1] + inp_theta[2]*inp_ti0)*exp(-inp_theta[3]*inp_ti0);
   phi_i   = inp_phi[0] + (inp_phi[1] + inp_phi[2]*inp_ti0)*exp(-inp_phi[3]*inp_ti0);

   theta_j = inp_theta[0] + (inp_theta[1] + inp_theta[2]*inp_tj0)*exp(-inp_theta[3]*inp_tj0);
   phi_j   = inp_phi[0] + (inp_phi[1] + inp_phi[2]*inp_tj0)*exp(-inp_phi[3]*inp_tj0);

   rho_ij = cos(phi_i)*cos(phi_j) - sin(phi_i)*sin(phi_j)*(1.0 - cos(theta_i)*cos(theta_j));		// modified twice
   // rho_ij = cos(phi_i)*cos(phi_j) - sin(phi_i)*sin(phi_j)*(1.0 - cos(theta_i-theta_j));		// modified once
   // rho_ij = cos(phi_i - phi_j) - sin(phi_i)*sin(phi_j)*(1.0 - cos(theta_i-theta_j));		// in the manual

   return rho_ij;
}

double getCorrelation_2Factors(double & theta_i, double& theta_j)
{
   return cos(theta_i - theta_j);
}

double RebonatoVolFunc_ixj(double inp_dYearFraction, void * inp_dParams)
{
  // The input parameters are: 4 Rebonato Vol parameters followed by the year fraction of Ti and Tj to market (T0)
  // The integral is from 0 to T1, so the parameters are regrouped to call Rebonato Vol function.

  double *p = (double *) inp_dParams;
  double *pi;
  double *pj;

  pi[0] = p[0];
  pj[0] = p[0];
  pi[1] = (p[1] + p[2]*p[4]) * exp(-p[3]*p[4]);
  pj[1] = (p[1] + p[2]*p[5]) * exp(-p[3]*p[5]);
  pi[2] = p[2] * exp(-p[3]*p[4]);
  pj[2] = p[2] * exp(-p[3]*p[5]);
  pi[3] = p[3];
  pj[3] = p[3];

  double	dVol_ixj(0.0);
  dVol_ixj = RebonatoVolFunc(inp_dYearFraction, pi) * RebonatoVolFunc(inp_dYearFraction, pj);
  return dVol_ixj;
}

double IntegrateRebonatoVolFunc_ixj(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[])
{
  size_t iMaxNumOfIntervals(1000), workSpaceSize(1000);
  double epsrel(1.0e-8), epsabs(1.0e-8);
  double dResult, dAbsErr;
  int GaussKronrodPtsKey(5);

  dResult = GSL_integration_wrapper(RebonatoVolFunc_ixj, inp_dParams, inp_dLowerLimit, inp_dUpperLimit, iMaxNumOfIntervals, epsabs, epsrel, dAbsErr, workSpaceSize, GaussKronrodPtsKey, true);

  return dResult;
}

double IntegrateRebonatoVolFunc_ixj_analytical(double & inp_dLowerLimit, double & inp_dUpperLimit, double inp_dParams[])
{
  double dResult;
  double dT10(-inp_dLowerLimit);
  double V4dT10(inp_dParams[3]*dT10);

  double tmpVal, tmpVal2;
  tmpVal = exp(V4dT10);
  tmpVal2 = tmpVal*tmpVal;

  double V4_1, V4_2, V4_3;
  V4_1 = 1.0/inp_dParams[3];
  V4_2 = V4_1/inp_dParams[3];
  V4_3 = V4_2/inp_dParams[3];

  double A, B, C, D, E;
  double P, Q, R, S;

  P = exp(-inp_dParams[3]*inp_dParams[4]);
  Q = exp(-inp_dParams[3]*inp_dParams[5]);
  R = inp_dParams[1] + inp_dParams[2]*inp_dParams[4];
  S = inp_dParams[1] + inp_dParams[2]*inp_dParams[5];

  A = inp_dParams[0]*inp_dParams[2]*(P + Q);
  B = inp_dParams[0]*(R*P + S*Q);
  C = inp_dParams[2]*inp_dParams[2]*P*Q;
  D = inp_dParams[2]*P*Q*(R + S);
  E = R*S*P*Q;

  dResult = inp_dParams[0]*inp_dParams[0]*dT10 + A*V4_2*(-1.0 + (1.0 - V4dT10)*tmpVal);
  dResult += B*V4_1*(tmpVal - 1.0);
  dResult += C*V4_3*0.25*(-1.0 + (2.0*V4dT10*V4dT10 - 2.0*V4dT10 + 1.0)*tmpVal2);
  dResult += D*V4_2*0.25*(-1.0 + (1.0 - 2.0*V4dT10)*tmpVal2);
  dResult += E*V4_1*0.5*(tmpVal2 - 1.0);

  return dResult;
}

void checkSigam_ixj_Integral(double dParamsSwap[])
{
   cout << "testing the sigma_i*sigma_j integral ...\n";

   double dLowerLimit(-1.0), dUpperLimit(0.0);
   // double dParamsSwap[6];
   double temp1, temp2;
   // for (int i=0; i<4; i++) { dParamsSwap[i] = dStartPt[i]; }
   dParamsSwap[4] = 2.0;
   dParamsSwap[5] = 4.0;

   temp1 = IntegrateRebonatoVolFunc_ixj(dLowerLimit, dUpperLimit, dParamsSwap);
   temp2 = IntegrateRebonatoVolFunc_ixj_analytical(dLowerLimit, dUpperLimit, dParamsSwap);

   cout << "\tOLD integral: " << temp1 << endl;
   cout << "\tNEW integral: " << temp2 << endl;

   dLowerLimit=0.0;
   dUpperLimit=1.0;
   temp1 =  IntegrateRebonatoVolFuncSquare(dLowerLimit, dUpperLimit, dParamsSwap);
   temp2 =  IntegrateRebonatoVolFuncSquare_analytical(dLowerLimit, dUpperLimit, dParamsSwap);

   cout << "\tOLD integral: " << temp1 << endl;
   cout << "\tNEW integral: " << temp2 << endl;
}

double swaptionVolSqUnderLMM(CVolTimeSeriesPtr& inp_DiscountFuncPtr, CVolTimeSeriesPtr& inp_ForwardLiborRatePtr,
                    CVolTimeSeriesPtr& inp_YearFractItoIplus1Ptr, CVolTimeSeriesPtr& inp_YearFractRelToMktPtr,
                    CVolTimeSeriesPtr& IRSpvFloatingLegPtr, CVolTimeSeriesPtr& IRSpvFixedLegPtr,
                    int& inp_startIndex, int & inp_endIndex, double inp_RebonatoVolParams[])
{
   const int		iDimension(4);
   double	theta[iDimension], phi[iDimension];
   double	ti0, tj0;
   double	rho_ij;
   double	T1;
   int		index(T0), jndex(T0);

   double zetaArr[LastPaymentPeriodToUse];

   calcZeta(zetaArr, inp_DiscountFuncPtr, inp_ForwardLiborRatePtr, inp_YearFractItoIplus1Ptr, inp_startIndex, inp_endIndex);

   T1 = inp_YearFractRelToMktPtr->GetValueByIndex(inp_startIndex);

   double	dLowerLimit(-T1), dUpperLimit(0.0);
   double	dParamsSwap[6];
   double	integralVol_ixj;

   // need to assign vol parameters to dParamsSwap[0-3]
   // for (int i=0; i<iDimension; i++) {
   //   dParamsSwap[i] = inp_RebonatoVolParams[i];
   //   theta[i] = inp_RebonatoVolParams[i+4];
   //   phi[i] = inp_RebonatoVolParams[i+8];
   // }

   const int chunk(1), numOfThreads(1);
   int TID;
   double sum[numOfThreads];
   double dResult(0.0);

   for (TID=0; TID < numOfThreads; TID++) sum[TID]=0.0;

   // #pragma omp parallel for default(shared) private(index, jndex, ti0, tj0, dParamsSwap, rho_ij, integralVol_ixj, TID) schedule(static,chunk) reduction(+:sum)
   // #pragma omp parallel num_threads(numOfThreads) default(shared) private(index, jndex, ti0, tj0, dParamsSwap, rho_ij, integralVol_ixj, TID)
   {
      // TID = omp_get_thread_num();
      TID = 0;

      for (int i=0; i<iDimension; i++) { dParamsSwap[i] = inp_RebonatoVolParams[i]; }

      for (index=inp_startIndex+TID; index <= inp_endIndex; index += numOfThreads)
      {
         for (jndex=inp_startIndex; jndex <= inp_endIndex; jndex++)
         {
            // ti0 = inp_YearFractRelToMktPtr->GetValueByIndex(index);
            // tj0 = inp_YearFractRelToMktPtr->GetValueByIndex(jndex);
            //rho_ij = getCorrelation(index, jndex, theta, phi, ti0, tj0);
            //rho_ij = getCorrelation_JR(theta, ti0, tj0);
            rho_ij = getCorrelation_2Factors(inp_RebonatoVolParams[index], inp_RebonatoVolParams[jndex]);
            // dParamsSwap[4] = ti0;
            // dParamsSwap[5] = tj0;
            dParamsSwap[4] = inp_YearFractRelToMktPtr->GetValueByIndex(index);
            dParamsSwap[5] = inp_YearFractRelToMktPtr->GetValueByIndex(jndex);
            integralVol_ixj = IntegrateRebonatoVolFunc_ixj_analytical(dLowerLimit, dUpperLimit, dParamsSwap);
            // integralVol_ixj = IntegrateRebonatoVolFunc_ixj(dLowerLimit, dUpperLimit, dParamsSwap);
            sum[TID] += zetaArr[index] * zetaArr[jndex] * rho_ij * integralVol_ixj;
            // cout << index << "\t" << jndex << "\t" << sum << "\t" << integralVol_ixj << "\t" << rho_ij << endl;
          }
         // if (TID == 0) cout << inp_startIndex << "\t" << inp_endIndex << "\t" << TID << ": " << sum << endl;
       }
   }
   // #pragma omp barrier

   for (TID=0; TID < numOfThreads; TID++) dResult += sum[TID];
   dResult /= T1;

   // sum /= T1;	// this is done when taking the difference between observable and model

   // cout << inp_startIndex << "\t" << inp_endIndex << "\t" << sum[0]*T1 << endl;

   return dResult;
}

double SwaptionMeritFunc(const gsl_vector *v, void *params)
{
  // MeritFunc += (YFrelToMkt*CapletVol**2 - IntegrateRebonatoValFuncSquare)**2

  SVolCalibrationInputPtr volCalInputPtr = static_cast<SVolCalibrationInputPtr>(params);
  double dResult(0.0);

  //for (int i=0; i<8; i++) volCalInputPtr->RebonatoVolParams[i+4] = gsl_vector_get(v, i);	// theta and phi
  for (int i=0; i<volCalInputPtr->mcSwaptionParamNum; i++) 
  {
     volCalInputPtr->RebonatoVolParams[i+4] = gsl_vector_get(v, i);	// theta and phi
     // cout << i << "\t" << volCalInputPtr->RebonatoVolParams[i+4] << "\t" << volCalInputPtr->mcSwaptionParamNum << endl;
  }

  dResult = calcSwaptionMeritFunc(volCalInputPtr);

  return dResult;
}

double calcSwaptionMeritFunc(SVolCalibrationInputPtr & volCalInputPtr)
{
// Loop over swaptions NxM
// We only use the swaptions that have 4N+4M-1 < LastPaymentPeriodToUse
   double dResult(0.0), dDiff, volSqObs, volSqMod, T1;
   int  N, M, iExpiry, jIRSlength, NExpiry, MIRSlength;
   int	startIndex;
   int	endIndex;

   NExpiry = volCalInputPtr->mcSwaptionData.size();
   MIRSlength = volCalInputPtr->mcSwaptionData[0].size();

   const int numOfThreads(1);
   int TID;
   double sum[numOfThreads];

   // double wallT0, wallT1;

   for (TID=0; TID < numOfThreads; TID++) sum[TID]=0.0;

   #pragma omp parallel num_threads(numOfThreads) default(shared) private(TID, iExpiry, jIRSlength, startIndex, endIndex, M, N, dDiff, volSqObs, volSqMod, T1)
   {
      // ************* Threading here won't work, b/c threads would collide in swaptionVolSqUnderLMM. ***************

      TID = omp_get_thread_num();

      for (iExpiry = TID + 1; iExpiry < NExpiry; iExpiry += numOfThreads)
      {
         // wallT0 = get_wall_time();
         N = volCalInputPtr->mcSwaptionData[iExpiry][0];
         startIndex = 4*N;
         T1 = volCalInputPtr->mcYearFractRelToMktPtr->GetValueByIndex(startIndex);
         for (jIRSlength=1; jIRSlength < MIRSlength; jIRSlength++)
         {
             M = volCalInputPtr->mcSwaptionData[0][jIRSlength];
             endIndex = 4*N+4*M-1;
             if (endIndex < LastPaymentPeriodToUse)
             {
                volSqObs = pow(volCalInputPtr->mcSwaptionData[iExpiry][jIRSlength], 2);
                volSqMod = swaptionVolSqUnderLMM(volCalInputPtr->mcDiscountFuncPtr, volCalInputPtr->mcForwardLiborRatePtr,
                                            volCalInputPtr->mcYearFractItoIplus1Ptr, volCalInputPtr->mcYearFractRelToMktPtr,
                                            volCalInputPtr->mcIRSpvFloatingLegPtr, volCalInputPtr->mcIRSpvFixedLegPtr,
                                            startIndex, endIndex, volCalInputPtr->RebonatoVolParams);
                dDiff = volSqObs - volSqMod;
                // dDiff = volSqObs*T1 - volSqMod;
                sum[TID] += dDiff * dDiff;

                // if (inp_bOutput) cout << volSqObs << ", " << volSqMod << "\t";
                   // cout << volSqObs << ", " << volSqMod << "\t" << (1.0 - volSqMod/volSqObs) << "\t" << (1.0 - volSqObs/volSqMod) << endl;;
                if (volCalInputPtr->mbOutput) 
                {
                   double obsVal, modelVal;
                   obsVal = sqrt(volSqObs);
                   modelVal = sqrt(volSqMod);
                   cout << obsVal << ", " << modelVal << "\t" << (1.0 - modelVal/obsVal) << "\t" << (1.0 - obsVal/modelVal) << endl;;
                }
             }
         }
         // wallT1 = get_wall_time();
         // cout << "iExpiry = " << iExpiry << "\tWall time spent is " << wallT1 - wallT0 << " seconds." << endl;
         // if (inp_bOutput) cout << "\n";
      }
   }

   for (TID=0; TID < numOfThreads; TID++) dResult += sum[TID];

   return dResult;
}

double CapAndSwaptionMeritFunc(const gsl_vector *v, void *params)
{
  SVolCalibrationInputPtr volCalInputPtr = static_cast<SVolCalibrationInputPtr>(params);
  double dResult(0.0);

  for (int i = 0; i<volCalInputPtr->mcSwaptionParamNum + volCalInputPtr->mcRebonatoVolParamNum; i++)
  {
     volCalInputPtr->RebonatoVolParams[i] = gsl_vector_get(v, i);
  }

  dResult = calcCapMeritFunc(volCalInputPtr);

  dResult += calcSwaptionMeritFunc(volCalInputPtr);

  return dResult;
}

void createWorkSpace(CFakeMatrixPtr& out_dMatrxPtr)
{
}

std::vector< std::vector<double> > readIn2dData(const char* filename)
{
    /* Function takes a char* filename argument and returns a
     * 2d dynamic array containing the data
     */

    std::vector< std::vector<double> > table;
    std::fstream ifs;

    /*  open file  */
    ifs.open(filename);

    if (ifs.is_open()) {

      while (true)
      {
          std::string line;
          double buf;

          getline(ifs, line);

          std::stringstream ss(line, std::ios_base::out|std::ios_base::in|std::ios_base::binary);

          if (!ifs)                  // mainly catch EOF
              break;

          if (line[0] == '#' || line.empty())   // catch empty lines or comment lines
              continue;

          std::vector<double> row;

          while (ss >> buf)
              row.push_back(buf);

          table.push_back(row);

      }
      ifs.close();

    }
    else {
      cout << "Unable to open file " << filename << endl;
      exit(1);
    }

    return table;
}

double get_wall_time()
{
   //struct timeval time;
   //if (gettimeofday(&time,NULL)) { return 0; }
   //return (double)time.tv_sec + (double)time.tv_usec * 1.0e-6;
	return 0.0;
}
