#ifndef VOL_CALIBRATION_COMMONDEFS_H
#define VOL_CALIBRATION_COMMONDEFS_H

#include "FinancialDefs.h"
#include "FinanceFactory.h"
#include "VolatilityCalibration.h"

enum ePaymentPeriods {
	T0 = 0, T3M, T6M, T9M,
	T1Y, T1Y3M, T1Y6M, T1Y9M,
	T2Y, T2Y3M, T2Y6M, T2Y9M,
	T3Y, T3Y3M, T3Y6M, T3Y9M,
	T4Y, T4Y3M, T4Y6M, T4Y9M,
	T5Y, T5Y3M, T5Y6M, T5Y9M,
	T6Y, T6Y3M, T6Y6M, T6Y9M,
	T7Y, T7Y3M, T7Y6M, T7Y9M,
	T8Y, T8Y3M, T8Y6M, T8Y9M,
	T9Y, T9Y3M, T9Y6M, T9Y9M,
	T10Y, T11Y, T12Y, T13Y, T14Y, T15Y, T16Y, T17Y, T18Y, T19Y, T20Y,
	TLast
};


typedef CSmartMap<int, double>          CVolTimeSeries;
typedef CSmartPtr<CVolTimeSeries>       CVolTimeSeriesPtr;

typedef CSmartMap<int, LongDate>        CListOfLongDates;
typedef CSmartPtr<CListOfLongDates>     CListOfLongDatesPtr;

typedef CSmartMap<string, int>          CStringToEnumMap;
typedef CSmartPtr<CStringToEnumMap>     CStringToEnumMapPtr;

typedef CSmartMap<int, string>          CEnumToStringMap;
typedef CSmartPtr<CEnumToStringMap>     CEnumToStringMapPtr;

typedef CSmartMap<CDate, double>        CDateDoubleMap;
typedef CSmartPtr<CDateDoubleMap>       CDateDoubleMapPtr;

struct SVolCalibrationInput
{
   CVolTimeSeriesPtr mcCapVolPtr;
   CVolTimeSeriesPtr mcDiscountFuncPtr;
   CVolTimeSeriesPtr mcCapletVolPtr;
   CVolTimeSeriesPtr mcYearFracCapletVolSqPtr;
   CListOfLongDatesPtr mcCapPaymentCalendarDaysPtr;

   CEnumToStringMapPtr mcEnumToStrMapPtr;
   CStringToEnumMapPtr mcStrToEnumMapPtr;

   CVolTimeSeriesPtr mcYearFractItoIplus1Ptr;
   CVolTimeSeriesPtr mcYearFractRelToMktPtr;
   CVolTimeSeriesPtr mcForwardLiborRatePtr;
   CVolTimeSeriesPtr mcATMcapStrikePtr;

   CVolTimeSeriesPtr mcIRSpvFloatingLegPtr;
   CVolTimeSeriesPtr mcIRSpvFixedLegPtr;
   CVolTimeSeriesPtr mcZetaPtr;

   // double	RebonatoVolParams[12];	// a, b, c, d, theta[4], phi[4]
   double	RebonatoVolParams[44];	// a, b, c, d, theta[40]
   int		mcRebonatoVolParamNum;
   int		mcSwaptionParamNum;
   bool		mbOutput;		// print result

   std::vector< std::vector<double> >	mcSwaptionData;
};
typedef SVolCalibrationInput* SVolCalibrationInputPtr;

struct SVolCalibrationOutput
{
  double	dRebonatoVolParams[4];
  double	dCorrTheta[4];
  double	dCorrPhi[4];
};
typedef CSmartPtr<SVolCalibrationOutput> SVolCalibrationOutputPtr;

#endif
