#ifndef INCLUDE_H_AMORTIZER
#define INCLUDE_H_AMORTIZER

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"
#include "FinLib.h"
#include "RateCompounder.h"

class IAmort
{
public:
	void Age(const CVectorReal& inp_cPeriodicRates)
	{
		calcInterest(inp_cPeriodicRates);

		if (miRemainingPeriods > 1) { pay(); }
		else { close(); }

		miRemainingPeriods--;
	}

	CVectorReal& GetEndingBalance(void) const { return *mcBalPtr; }

	CVectorReal& GetInterest(void) const { return *mcIntPtr; }

	CVectorReal& GetPayment(void) const { return *mcPmtPtr; }

	CVectorReal& GetPrincipal(void) const { return *mcPrinPtr; }

	size_t GetRemainingPeriods(void) const { return miRemainingPeriods; }

	void Recast(const CVectorReal& inp_cPeriodicRates) { cast(inp_cPeriodicRates, -1.0); }

protected:
	const size_t	miSims;

	size_t	miRemainingPeriods;
	
	CUniquePtr<CVectorReal>	mcIntPtr;
	CUniquePtr<CVectorReal>	mcPrinPtr;
	CUniquePtr<CVectorReal>	mcPmtPtr;
	CUniquePtr<CVectorReal>	mcBalPtr;
	
	IAmort(const size_t& inp_iPeriods, const CVectorReal& inp_cBalancesBop)
		:miRemainingPeriods(inp_iPeriods), miSims(inp_cBalancesBop.size())
	{
		mcIntPtr = CreateUnique<CVectorReal>(miSims);
		mcPrinPtr = CreateUnique<CVectorReal>(miSims);
		mcPmtPtr = CreateUnique<CVectorReal>(miSims);
		mcBalPtr = CreateUnique<CVectorReal>(inp_cBalancesBop);
	}

	void calcInterest(const CVectorReal& inp_cPeriodicRates)
	{
		*mcIntPtr = *mcBalPtr * inp_cPeriodicRates;
	}

	void close(void)
	{
		*mcPrinPtr = *mcBalPtr;
		*mcPmtPtr = *mcIntPtr + *mcPrinPtr;
		*mcBalPtr = 0.0;
	}

	virtual void cast(const CVectorReal& inp_cPeriodicRates, const real& inp_dFixedPmt) = 0;

	virtual void pay(void) = 0;
};

template<AMORTIZATIONTYPE TAmort> struct CAmort;
template<> struct CAmort<AMORTIZATIONTYPE::ANNUITY> : public IAmort
{
	typedef IAmort								_Mybase;
	typedef CAmort<AMORTIZATIONTYPE::ANNUITY>	_Myt;

	CAmort(const size_t& inp_iPeriods, const CVectorReal& inp_cPeriodicRates, const CVectorReal& inp_cBalancesBop, const real& inp_dFixedPmt)
		:_Mybase(inp_iPeriods, inp_cBalancesBop)
	{
		cast(inp_cPeriodicRates, inp_dFixedPmt);
	}

protected:
	typedef CRateCmpd<COMPOUNDTYPE::COMPOUND> cmpd_t;
	void cast(const CVectorReal& inp_cPeriodicRates, const real& inp_dFixedPmt)
	{
		if (inp_dFixedPmt < 0)
		{
			*mcPmtPtr = fin::AnnuityPmtsPerPv(cmpd_t::CalcDiscountFactor(inp_cPeriodicRates, 1.0), miRemainingPeriods) * *mcBalPtr;
		}
		else 
		{ 
			*mcPmtPtr = inp_dFixedPmt;
		}
	}

	void pay(void)
	{
		*mcPmtPtr &= *mcBalPtr + *mcIntPtr;
		*mcPrinPtr = *mcPmtPtr - *mcIntPtr;
		*mcBalPtr -= *mcPrinPtr;
	}
};
template<> struct CAmort<AMORTIZATIONTYPE::BULLET> : public IAmort
{
	typedef IAmort									_Mybase;
	typedef CAmort<AMORTIZATIONTYPE::STRAIGHTLINE>	_Myt;

	CAmort(const size_t& inp_iPeriods, const CVectorReal& inp_cPeriodicRates, const CVectorReal& inp_cBalancesBop, const real& inp_dFixedPmt)
		:_Mybase(inp_iPeriods, inp_cBalancesBop)
	{
		cast(inp_cPeriodicRates, inp_dFixedPmt);
	}

	void cast(const CVectorReal& inp_cPeriodicRates, const real& inp_dFixedPm) { *mcPrinPtr = 0.0; }

	void pay(void) { *mcPmtPtr = *mcIntPtr; }

};
template<> struct CAmort<AMORTIZATIONTYPE::STRAIGHTLINE> : public IAmort
{
	typedef IAmort								_Mybase;
	typedef CAmort<AMORTIZATIONTYPE::STRAIGHTLINE>	_Myt;

	CAmort(const size_t& inp_iPeriods, const CVectorReal& inp_cPeriodicRates, const CVectorReal& inp_cBalancesBop, const real& inp_dFixedPmt)
		:_Mybase(inp_iPeriods, inp_cBalancesBop)
	{
		cast(inp_cPeriodicRates, inp_dFixedPmt);
	}

protected:
	void cast(const CVectorReal& inp_cPeriodicRates, const real& inp_dFixedPmt)
	{
		*mcPrinPtr = *mcBalPtr * (1.0 / static_cast<real>(miRemainingPeriods));
	}

	void pay(void) 
	{
		*mcPmtPtr = *mcIntPtr + *mcPrinPtr;
		*mcBalPtr -= *mcPrinPtr;
	}
};

typedef CSmartPtr<IAmort> CAmortizerPtr;
namespace CFinTools
{
	static CAmortizerPtr CreateAmortizer(const AMORTIZATIONTYPE& inp_AmortizationType, const size_t& inp_iPeriods, const CVectorReal& inp_cPeriodicRates, const CVectorReal& inp_cBalancesBop, const real& inp_dFixedPmt = -1.0)
	{
		switch (inp_AmortizationType)
		{
		case AMORTIZATIONTYPE::STRAIGHTLINE: return CreateSmart<CAmort<AMORTIZATIONTYPE::STRAIGHTLINE>>(inp_iPeriods, inp_cPeriodicRates, inp_cBalancesBop, inp_dFixedPmt);
		case AMORTIZATIONTYPE::ANNUITY: return CreateSmart<CAmort<AMORTIZATIONTYPE::ANNUITY>>(inp_iPeriods, inp_cPeriodicRates, inp_cBalancesBop, inp_dFixedPmt);
		default: return CreateSmart<CAmort<AMORTIZATIONTYPE::BULLET>>(inp_iPeriods, inp_cPeriodicRates, inp_cBalancesBop, inp_dFixedPmt);
		}
	}
};
#endif