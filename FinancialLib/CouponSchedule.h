#ifndef INCLUDE_H_COUPONSCHED
#define INCLUDE_H_COUPONSCHED

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"
#include "FinLib.h"
#include "DateAdjustor.h"
#include "YearFrac.h"

class ICpnSched
{
public:
	typedef SCpnLst::size_type size_type;

	real GetAccrual(const CDate& inp_dtAccrual) const
	{
		if (miLastPeriod == 0) return mcYearFracPtr->GetYearFrac(mdtDated, mdtFirst, inp_dtAccrual);

		if (inp_dtAccrual < mdtFirst)
		{
			{return (*msCpnLstPtr)[0].mdCoupon - calcYearFracThruFirstDate(inp_dtAccrual); }
		}
		else if (inp_dtAccrual > mdtNextToLast)
		{
			return calcYearFracFromNextToLastDate(inp_dtAccrual);
		}
		else
		{
			auto itr(iterators::GetItr(msCpnLstPtr));
			for (itr.begin(); itr->mdtEop < inp_dtAccrual; itr++);

			if (itr->mdtEop == inp_dtAccrual) { return itr->mdCoupon; }
			else { return mcYearFracPtr->GetYearFrac(itr->mdtBop, itr->mdtEop, inp_dtAccrual); }
		}
	}

	size_type GetAge(const CDate& inp_dtAsOf) const
	{
		size_type iAge(0);

		auto itr(iterators::GetItr(msCpnLstPtr));
		LOOP(itr)
		{
			if (inp_dtAsOf == itr->mdtEop) { return ++iAge; }
			else if (inp_dtAsOf < itr->mdtEop) { return iAge; }

			iAge++;
		}

		return msCpnLstPtr->size();
	}

	SCpnLstPtr GetCoupons(void) const { return msCpnLstPtr; }

protected:
	CDate			mdtDated;
	CDate			mdtIssue;
	CDate			mdtMaturity;

	CDate			mdtFirst;
	CDate			mdtNextToLast;

	const size_type miLastPeriod;

	CDateAdjPtr		mcDateAdjPtr;
	CYearFracPtr	mcYearFracPtr;

	SPeriodPtr		msPeriodPtr;
	SCpnLstPtr		msCpnLstPtr;

	ICpnSched(CDateAdjPtr inp_cDateAdjPtr, CYearFracPtr inp_cYearFracPtr, const CDate& inp_dtDated, const CDate& inp_dtIssue, const CDate& inp_dtMaturity, const SPeriod& inp_sPeriod, SCpnLstPtr inp_sCpnLstPtr)
		:mdtDated(inp_dtDated), mdtIssue(inp_dtIssue), mdtMaturity(inp_dtMaturity), miLastPeriod(inp_sCpnLstPtr->size() - 1),
		mcDateAdjPtr(inp_cDateAdjPtr), mcYearFracPtr(inp_cYearFracPtr), 
		msPeriodPtr(CreateUnique<SPeriod>(inp_sPeriod)), msCpnLstPtr(inp_sCpnLstPtr) {}

	void build(void)
	{
		switch (msCpnLstPtr->size())
		{
		case 1:
			mdtFirst = mdtMaturity;
			mdtNextToLast = mdtDated;

			loadFirst();
			break;
		case 2:
			mdtFirst = mdtNextToLast = getDate(1);

			loadFirst();
			loadLast();
			break;
		default:
			mdtFirst = getDate(1);
			mdtNextToLast = getDate(miLastPeriod);

			loadFirst();
			loadInterim();
			loadLast();
		}
	}

	real calcYearFracThruFirstDate(const CDate& inp_dtBeforeFirstDate) const
	{
		real dFrac(0.0);
		CDate dtBop(mdtFirst), dtEop;

		if (inp_dtBeforeFirstDate < mdtFirst)
		{
			for (int64_t iPeriod(0); inp_dtBeforeFirstDate < dtBop; iPeriod--)
			{
				dtEop = dtBop;
				dtBop = getDate(iPeriod);

				dFrac += mcYearFracPtr->GetCoupon(dtBop, dtEop);
			}
			dFrac -= mcYearFracPtr->GetYearFrac(dtBop, dtEop, inp_dtBeforeFirstDate);
		}
		return dFrac;
	}

	real calcYearFracFromNextToLastDate(const CDate& inp_dtAfterNextToLast) const
	{
		real dFrac(0.0);
		CDate dtBop, dtEop(mdtNextToLast);

		for (int64_t iPeriod(msCpnLstPtr->size());; iPeriod++)
		{
			dtBop = dtEop;
			dtEop = getDate(iPeriod);

			if (inp_dtAfterNextToLast > dtEop) dFrac += mcYearFracPtr->GetCoupon(dtBop, dtEop);
			else if (inp_dtAfterNextToLast > dtBop)
			{
				dFrac += mcYearFracPtr->GetYearFrac(dtBop, dtEop, inp_dtAfterNextToLast); break;
			}
			else break;
		}
		return dFrac;
	}

	void loadFirst(void)
	{
		(*msCpnLstPtr)[0].mdtBop = mdtDated;
		(*msCpnLstPtr)[0].mdtEop = mdtFirst;
		(*msCpnLstPtr)[0].mdCoupon = calcYearFracThruFirstDate(mdtDated);
	}

	void loadInterim(void)
	{
		CDate dtBop, dtEop(mdtFirst);

		for (size_type iPeriod(1); iPeriod < miLastPeriod; iPeriod++)
		{
			(*msCpnLstPtr)[iPeriod].mdtBop = dtBop = dtEop;
			(*msCpnLstPtr)[iPeriod].mdtEop = dtEop = getDate(iPeriod + 1);
			(*msCpnLstPtr)[iPeriod].mdCoupon = mcYearFracPtr->GetCoupon(dtBop, dtEop);
		}
	}

	void loadLast(void)
	{
		(*msCpnLstPtr)[miLastPeriod].mdtBop = mdtNextToLast;
		(*msCpnLstPtr)[miLastPeriod].mdtEop = mdtMaturity;
		(*msCpnLstPtr)[miLastPeriod].mdCoupon = calcYearFracFromNextToLastDate(mdtMaturity);
	}

	virtual CDate getDate(const int64_t& inp_iPeriod) const = 0;
};

template<DATE_GENERATION TDateGen> struct CCpnSched;
template<> struct CCpnSched<DATE_GENERATION::BACKWARD> : public ICpnSched
{
	typedef ICpnSched								_Mybase;
	typedef CCpnSched<DATE_GENERATION::BACKWARD>	_Myt;

	CCpnSched(CDateAdjPtr inp_cDateAdjPtr, CYearFracPtr inp_cYearFracPtr, const CDate& inp_dtDated, const CDate& inp_dtIssue, const CDate& inp_dtMaturity, const SPeriod& inp_sPeriod, SCpnLstPtr inp_sCpnLstPtr)
		:_Mybase(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr)
	{
		msPeriodPtr->miUnitCount *= -1;

		build();
	}

protected:
	CDate getDate(const int64_t& inp_iPeriod) const
	{
		return(mcDateAdjPtr->Adjust(mdtMaturity.Add(msPeriodPtr->mUnitType, static_cast<int32_t>(msPeriodPtr->miUnitCount * (msCpnLstPtr->size() - inp_iPeriod)), false)));
	}
};
template<> struct CCpnSched<DATE_GENERATION::EOM> : public ICpnSched
{
	typedef ICpnSched								_Mybase;
	typedef CCpnSched<DATE_GENERATION::EOM>	_Myt;

	CCpnSched(CDateAdjPtr inp_cDateAdjPtr, CYearFracPtr inp_cYearFracPtr, const CDate& inp_dtDated, const CDate& inp_dtIssue, const CDate& inp_dtMaturity, const SPeriod& inp_sPeriod, SCpnLstPtr inp_sCpnLstPtr)
		:_Mybase(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr)
	{
		build();
	}

protected:
	CDate getDate(const int64_t& inp_iPeriod) const
	{
		return(mcDateAdjPtr->Adjust(mdtIssue.Add(msPeriodPtr->mUnitType, msPeriodPtr->miUnitCount * inp_iPeriod, true)));
	}
};
template<> struct CCpnSched<DATE_GENERATION::FORWARD> : public ICpnSched
{
	typedef ICpnSched								_Mybase;
	typedef CCpnSched<DATE_GENERATION::FORWARD>	_Myt;

	CCpnSched(CDateAdjPtr inp_cDateAdjPtr, CYearFracPtr inp_cYearFracPtr, const CDate& inp_dtDated, const CDate& inp_dtIssue, const CDate& inp_dtMaturity, const SPeriod& inp_sPeriod, SCpnLstPtr inp_sCpnLstPtr)
		:_Mybase(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr)
	{
		build();
	}

protected:
	CDate getDate(const int64_t& inp_iPeriod) const
	{
		return(mcDateAdjPtr->Adjust(mdtIssue.Add(msPeriodPtr->mUnitType, msPeriodPtr->miUnitCount * inp_iPeriod, false)));
	}
};
template<> struct CCpnSched<DATE_GENERATION::THIRDWED> : public ICpnSched
{
	typedef ICpnSched								_Mybase;
	typedef CCpnSched<DATE_GENERATION::THIRDWED>	_Myt;

	CCpnSched(CDateAdjPtr inp_cDateAdjPtr, CYearFracPtr inp_cYearFracPtr, const CDate& inp_dtDated, const CDate& inp_dtIssue, const CDate& inp_dtMaturity, const SPeriod& inp_sPeriod, SCpnLstPtr inp_sCpnLstPtr)
		:_Mybase(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr)
	{
		CDate dtTest(mdtDated.Year(), MONTHSOFYEAR::MARCH, DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD);
		while (dtTest <= mdtDated)
		{
			forwardPeriod(1, dtTest);
		}

		mdtDated = dtTest;
		forwardPeriod(msCpnLstPtr->size(), dtTest);
		mdtMaturity = dtTest;

		build();
	}

protected:
	CDate getDate(const int64_t& inp_iPeriod) const
	{
		CDate dtResult(mdtDated);

		if (inp_iPeriod == 0) return dtResult;
		else if (inp_iPeriod > 0)
		{
			forwardPeriod(inp_iPeriod, dtResult);
		}
		else
		{
			priorPeriod(inp_iPeriod, dtResult);
		}

		return dtResult;
	}

	static void forwardPeriod(int64_t inp_iPeriod, CDate& inp_dtForward)
	{
		for (; inp_iPeriod > 0; inp_iPeriod--)
		{
			inp_dtForward.Move(PERIODS::MONTH, 3);
			inp_dtForward = CDate(inp_dtForward.Year(), inp_dtForward.Month(), DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD);
		}
	}

	static void priorPeriod(int64_t inp_iPeriod, CDate& inp_dtPrior)
	{
		for (; inp_iPeriod > 0; inp_iPeriod++)
		{
			inp_dtPrior.Move(PERIODS::MONTH, -3);
			inp_dtPrior = CDate(inp_dtPrior.Year(), inp_dtPrior.Month(), DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD);
		}
	}

	static inline void nextDate(CDate& inp_dtThis)
	{
		inp_dtThis.Move(PERIODS::MONTH, 3);
		inp_dtThis = CDate(inp_dtThis.Year(), inp_dtThis.Month(), DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD);
	}

	static inline void priorDate(CDate& inp_dtThis)
	{
		inp_dtThis.Move(PERIODS::MONTH, -3);
		inp_dtThis = CDate(inp_dtThis.Year(), inp_dtThis.Month(), DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD);
	}
};

typedef CSmartPtr<ICpnSched> CCpnSchedPtr;
namespace CFinTools
{
	static CCpnSchedPtr CreateCouponScheduler(const DATE_GENERATION& inp_DateGeneration, CDateAdjPtr inp_cDateAdjPtr, CYearFracPtr inp_cYearFracPtr,
		const CDate& inp_dtDated, const CDate& inp_dtIssue, CDate& inp_dtMaturity, const SPeriod& inp_sPeriod, SCpnLstPtr inp_sCpnLstPtr)
	{
		switch (inp_DateGeneration)
		{
		case DATE_GENERATION::BACKWARD: return CreateSmart<CCpnSched<DATE_GENERATION::BACKWARD>>(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr);
		case DATE_GENERATION::EOM: return CreateSmart<CCpnSched<DATE_GENERATION::EOM>>(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr);
		case DATE_GENERATION::THIRDWED: return CreateSmart<CCpnSched<DATE_GENERATION::THIRDWED>>(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr);
		default: return CreateSmart<CCpnSched<DATE_GENERATION::FORWARD>>(inp_cDateAdjPtr, inp_cYearFracPtr, inp_dtDated, inp_dtIssue, inp_dtMaturity, inp_sPeriod, inp_sCpnLstPtr);
		}
	}
};
#endif