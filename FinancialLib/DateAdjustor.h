#ifndef INCLUDE_H_DATEADJUSTOR
#define INCLUDE_H_DATEADJUSTOR

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"
#include "FinLib.h"

class IDateAdj
{
public:
	CDate AddBusinessDays(const CDate& inp_dtThis, const uint32_t& inp_iDaysToAdd) const
	{
		CDate dtReturn(inp_dtThis);
		fin::AddBusinessDays(dtReturn, inp_iDaysToAdd, *mcHolidayCalendarPtr);
		return dtReturn;
	}

	virtual CDate Adjust(const CDate& inp_dtThis) const = 0;

	CDate SubtractBusinessDays(const CDate& inp_dtThis, const uint32_t& inp_iDaysToSubtract) const
	{
		CDate dtReturn(inp_dtThis);
		fin::SubtractBusinessDays(dtReturn, inp_iDaysToSubtract, *mcHolidayCalendarPtr);
		return dtReturn;
	}

protected:
	CHolidayCalendarPtr	mcHolidayCalendarPtr;

	IDateAdj(const CHolidayCalendarPtr inp_cHolidayCalendarPtr) :mcHolidayCalendarPtr(inp_cHolidayCalendarPtr) {}
};

template<BUSINESS_DAY_CONVENTION TBusDayConv> struct CDateAdj;
template<> struct CDateAdj<BUSINESS_DAY_CONVENTION::FOLLOWING> : public IDateAdj
{
	typedef IDateAdj										_Mybase;
	typedef CDateAdj<BUSINESS_DAY_CONVENTION::FOLLOWING>	_Myt;

	CDateAdj(CHolidayCalendarPtr inp_cHolidayCalendarPtr) :_Mybase(inp_cHolidayCalendarPtr) {}

	CDate Adjust(const CDate& inp_dtThis) const
	{
		CDate dtAdj(inp_dtThis);
		fin::AdjustDateFollowing(dtAdj, *mcHolidayCalendarPtr);

		return dtAdj;
	}
};
template<> struct CDateAdj<BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING> : public IDateAdj
{
	typedef IDateAdj												_Mybase;
	typedef CDateAdj<BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING>	_Myt;

	CDateAdj(CHolidayCalendarPtr inp_cHolidayCalendarPtr) :_Mybase(inp_cHolidayCalendarPtr) {}

	CDate Adjust(const CDate& inp_dtThis) const
	{
		CDate dtAdj(inp_dtThis);
		fin::AdjustDateModifiedFollowing(dtAdj, *mcHolidayCalendarPtr);

		return dtAdj;
	}
};
template<> struct CDateAdj<BUSINESS_DAY_CONVENTION::UNADJUSTED> : public IDateAdj
{
	typedef IDateAdj										_Mybase;
	typedef CDateAdj<BUSINESS_DAY_CONVENTION::UNADJUSTED>	_Myt;

	CDateAdj(CHolidayCalendarPtr inp_cHolidayCalendarPtr) :_Mybase(inp_cHolidayCalendarPtr) {}

	CDate Adjust(const CDate& inp_dtThis) const { return inp_dtThis; }
};
template<> struct CDateAdj<BUSINESS_DAY_CONVENTION::PRECEDING> : public IDateAdj
{
	typedef IDateAdj										_Mybase;
	typedef CDateAdj<BUSINESS_DAY_CONVENTION::PRECEDING>	_Myt;

	CDateAdj(CHolidayCalendarPtr inp_cHolidayCalendarPtr) :_Mybase(inp_cHolidayCalendarPtr) {}

	CDate Adjust(const CDate& inp_dtThis) const
	{
		CDate dtAdj(inp_dtThis);
		fin::AdjustDatePreceding(dtAdj, *mcHolidayCalendarPtr);

		return dtAdj;
	}
};

typedef CSmartPtr<IDateAdj> CDateAdjPtr;
namespace CFinTools
{
	static CDateAdjPtr CreateDateAdjustor(const BUSINESS_DAY_CONVENTION& inp_BusinessDayConvention, CHolidayCalendarPtr inp_cHolidayCalendarPtr)
	{
		switch (inp_BusinessDayConvention)
		{
		case BUSINESS_DAY_CONVENTION::FOLLOWING: return CreateSmart<CDateAdj<BUSINESS_DAY_CONVENTION::FOLLOWING>>(inp_cHolidayCalendarPtr);
		case BUSINESS_DAY_CONVENTION::PRECEDING: return CreateSmart<CDateAdj<BUSINESS_DAY_CONVENTION::PRECEDING>>(inp_cHolidayCalendarPtr);
		case BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING: return CreateSmart<CDateAdj<BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING>>(inp_cHolidayCalendarPtr);
		default: return CreateSmart<CDateAdj<BUSINESS_DAY_CONVENTION::UNADJUSTED>>(inp_cHolidayCalendarPtr);
		}
	}
};
#endif