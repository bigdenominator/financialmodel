#ifndef INCLUDE_H_FINTOOLS
#define INCLUDE_H_FINTOOLS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"
#include "HolidayCalendar.h"

struct fin
{
	// business days
	static bool IsBusinessDay(const CDate& inp_dtThis, const CHolidayCalendar& inp_cCalendar)
	{
		return inp_dtThis.IsWeekend() ? false : (inp_cCalendar.find(inp_dtThis) == inp_cCalendar.cend());
	}

	static int32_t BusinessDays(const CDate& inp_dtFrom, const CDate& inp_dtTo, const CHolidayCalendar& inp_cCalendar)
	{
		CDate dtThis(inp_dtFrom), dtMax(inp_dtTo);
		int32_t iDir(1), iResult(0);

		if (dtThis > dtMax)
		{
			std::swap(dtThis, dtMax);
			iDir = -1;
		}

		for (; dtThis < dtMax; dtThis++)
		{
			if (IsBusinessDay(dtThis, inp_cCalendar)) iResult++;
		}

		return iResult * iDir;
	}

	static void AddBusinessDays(CDate& inp_dtThis, uint32_t inp_iDaysToAdd, const CHolidayCalendar& inp_cHolidayCalendar)
	{
		while (inp_iDaysToAdd > 0)
		{
			if (IsBusinessDay(++inp_dtThis, inp_cHolidayCalendar)) { inp_iDaysToAdd--; }
		}
	}

	static void SubtractBusinessDays(CDate& inp_dtThis, uint32_t inp_iDaysToSubtract, const CHolidayCalendar& inp_cCalendar)
	{
		while (inp_iDaysToSubtract > 0)
		{
			if (IsBusinessDay(--inp_dtThis, inp_cCalendar)) inp_iDaysToSubtract--;
		}
	}

	// day counts
	static int32_t DayCount30(const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		return 360 * (inp_dtTo.Year() - inp_dtFrom.Year()) + 30 * (static_cast<int32_t>(inp_dtTo.Month()) - static_cast<int32_t>(inp_dtFrom.Month())) + inp_dtTo.Day() - inp_dtFrom.Day();
	}

	static int32_t DayCountActual(const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		return inp_dtTo - inp_dtFrom;
	}

	// year fracs
	static real Actual360(const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		return static_cast<real>(DayCountActual(inp_dtFrom, inp_dtTo)) / 360.0;
	}

	static real Actual365F(const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		return static_cast<real>(DayCountActual(inp_dtFrom, inp_dtTo)) / 365.0;
	}

	static real ActualActualICMA_PeriodFrac(const CDate& inp_dtCpnPeriodStart, const CDate& inp_dtCpnPeriodEnd, const CDate& inp_dtThis)
	{
		if (inp_dtCpnPeriodEnd == inp_dtThis) return 1.0;
		return static_cast<real>(DayCountActual(inp_dtCpnPeriodStart, inp_dtThis)) / static_cast<real>(DayCountActual(inp_dtCpnPeriodStart, inp_dtCpnPeriodEnd));
	}

	static real ActualActualICMA_YearFrac(const CDate& inp_dtCpnPeriodStart, const CDate& inp_dtCpnPeriodEnd, const CDate& inp_dtThis, const FREQUENCY& inp_Frequency)
	{
		return ActualActualICMA_PeriodFrac(inp_dtCpnPeriodStart, inp_dtCpnPeriodEnd, inp_dtThis) / static_cast<real>(SPeriod::PeriodsPerYear(inp_Frequency));
	}

	static real ActualActualISDA(const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		return static_cast<real>(inp_dtTo.DayOfYear() - 1) / static_cast<real>(inp_dtTo.DaysInYear()) - static_cast<real>(inp_dtFrom.DayOfYear() - 1) / static_cast<real>(inp_dtFrom.DaysInYear()) + static_cast<real>(inp_dtTo.Diff(inp_dtFrom, PERIODS::YEAR));
	}

	static real BondBasis(const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		CDate dtRefPeriodStartAdj(inp_dtFrom), dtThisAdj(inp_dtTo);

		switch (dtRefPeriodStartAdj.Day())
		{
		case 31:
			dtRefPeriodStartAdj--;
		case 30:
			if (dtRefPeriodStartAdj.Day() == 31) dtThisAdj--;
			break;
		}

		return(DayCount30(dtRefPeriodStartAdj, dtThisAdj) / 360.0);
	}

	static real EurobondBasis(const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		CDate dtFromAdj(inp_dtFrom), dtToAdj(inp_dtTo);

		if (dtFromAdj.Day() == 31) dtFromAdj--;
		if (dtToAdj.Day() == 31) dtToAdj--;

		return(static_cast<real>(DayCount30(dtFromAdj, dtToAdj)) / 360.0);
	}

	// date adjustments
	static void AdjustDateFollowing(CDate& inp_dtThis, const CHolidayCalendar& inp_cHolidayCalendar)
	{
		while (!IsBusinessDay(inp_dtThis, inp_cHolidayCalendar)) { inp_dtThis++; }
	}

	static void AdjustDatePreceding(CDate& inp_dtThis, const CHolidayCalendar& inp_cHolidayCalendar)
	{
		while (!IsBusinessDay(inp_dtThis, inp_cHolidayCalendar)) { inp_dtThis--; }
	}

	static void AdjustDateModifiedFollowing(CDate& inp_dtThis, const CHolidayCalendar& inp_cHolidayCalendar)
	{
		CDate dtTemp(inp_dtThis);

		AdjustDateFollowing(inp_dtThis, inp_cHolidayCalendar);
		if (dtTemp.Month() == inp_dtThis.Month()) return;

		inp_dtThis = dtTemp;
		AdjustDatePreceding(inp_dtThis, inp_cHolidayCalendar);
	}


	// options
	static real Black76Call(const real& inp_dExpiry, const real& inp_dDiscountFactor, const real& inp_dPeriodicForwardRate, const real& inp_dPeriodicStrikeRate, const real& inp_dBlackVol)
	{
		real dSigmaRootTime(max(0.0, inp_dBlackVol) * sqrt(inp_dExpiry));

		if (dSigmaRootTime == 0.0) return max(inp_dDiscountFactor * (inp_dPeriodicForwardRate - inp_dPeriodicStrikeRate), 0.0);

		real dRatio(inp_dPeriodicForwardRate / inp_dPeriodicStrikeRate);
		real d1(CMath::ln(dRatio) / dSigmaRootTime + dSigmaRootTime / 2.0);
		real d2(d1 - dSigmaRootTime);

		return inp_dDiscountFactor * inp_dPeriodicStrikeRate * (dRatio * CNormal::StandardCDF(d1) - CNormal::StandardCDF(d2));
	}

	static real Black76Put(const real& inp_dExpiry, const real& inp_dDiscountFactor, const real& inp_dPeriodicForwardRate, const real& inp_dPeriodicStrikeRate, const real& inp_dBlackVol)
	{
		real dSigmaRootTime(max(0.0, inp_dBlackVol) * sqrt(inp_dExpiry));

		if (dSigmaRootTime == 0.0) return max(inp_dDiscountFactor * (inp_dPeriodicForwardRate - inp_dPeriodicStrikeRate), 0.0);

		real dRatio(inp_dPeriodicForwardRate / inp_dPeriodicStrikeRate);
		real d1(CMath::ln(inp_dPeriodicForwardRate / inp_dPeriodicStrikeRate) / dSigmaRootTime + dSigmaRootTime / 2.0);
		real d2(d1 - dSigmaRootTime);

		return inp_dDiscountFactor * inp_dPeriodicStrikeRate * (CNormal::StandardCDF(-d2) - dRatio * CNormal::StandardCDF(-d1));
	}

	static real BlackScholesCall(const real& inp_dExpiry, const real& inp_dRiskFreeRate, const real& inp_dForwardPrice, const real& inp_dStrikePrice, const real& inp_dBlackVol)
	{
		real dSigmaRootTime(max(0.0, inp_dBlackVol) * sqrt(inp_dExpiry));
		real dRt(inp_dExpiry * inp_dRiskFreeRate);
		real dDf(CMath::exp(-dRt));

		if (dSigmaRootTime == 0.0) return max(dDf * (inp_dForwardPrice - inp_dStrikePrice), 0.0);

		real d1((CMath::ln(inp_dForwardPrice / inp_dStrikePrice) + dRt) / dSigmaRootTime + dSigmaRootTime / 2.0);
		real d2(d1 - dSigmaRootTime);

		return inp_dForwardPrice * CNormal::StandardCDF(d1) - inp_dStrikePrice * dDf * CNormal::StandardCDF(d2);
	}

	static real BlackScholesPut(const real& inp_dExpiry, const real& inp_dRiskFreeRate, const real& inp_dForwardPrice, const real& inp_dStrikePrice, const real& inp_dBlackVol)
	{
		real dSigmaRootTime(max(0.0, inp_dBlackVol) * sqrt(inp_dExpiry));
		real dRt(inp_dExpiry * inp_dRiskFreeRate);
		real dDf(CMath::exp(-dRt));

		if (dSigmaRootTime == 0.0) return max(dDf * (inp_dForwardPrice - inp_dStrikePrice), 0.0);

		real d1((CMath::ln(inp_dForwardPrice / inp_dStrikePrice) + dRt) / dSigmaRootTime + dSigmaRootTime / 2.0);
		real d2(d1 - dSigmaRootTime);

		return inp_dStrikePrice * dDf * CNormal::StandardCDF(-d2) - inp_dForwardPrice * CNormal::StandardCDF(-d1);
	}

	// bond math
	template<typename T> static auto PriceFromBondEquivYld(const T& inp_dBondEquivalentYield, const int32_t& inp_iPeriods, const real& inp_dFrac) -> decltype(std::pow(1.0 + inp_dBondEquivalentYield * 0.5, -inp_iPeriods) / (1.0 + inp_dBondEquivalentYield * inp_dFrac))
	{
		return std::pow(1.0 + inp_dBondEquivalentYield * 0.5, -inp_iPeriods) / (1.0 + inp_dBondEquivalentYield * inp_dFrac);
	}

	template<typename T> static auto PriceFromBondEquivYld(const T& inp_dBondEquivalentYield, const CDate& inp_dtPrice, const CDate& inp_dtMaturity) -> decltype(priceFromBondEquivalentYield(inp_dBondEquivalentYield, (int32_t)1, (real)1))
	{
		real dFrac(ActualActualISDA(inp_dtPrice, inp_dtMaturity));
		real dRound(CMath::roundMultiple(dFrac, ROUNDINGTYPE::DOWN, 0.5));

		return PriceFromBondEquivYld(inp_dBondEquivalentYield, static_cast<int32_t>(2.0 * dRound), dFrac - dRound);
	}

	static real BondEquivYldFromPrice(const real& inp_dPriceDecimal, const CDate& inp_dtPrice, const CDate& inp_dtMaturity)
	{
		real dFrac(ActualActualISDA(inp_dtPrice, inp_dtMaturity));
		real dRound(CMath::roundMultiple(dFrac, ROUNDINGTYPE::DOWN, 0.5));

		auto foo = [&](const real& inp_dBEY) {return PriceFromBondEquivYld(inp_dBEY, static_cast<int32_t>(2.0 * dRound), dFrac - dRound) - inp_dPriceDecimal; };

		real dRoot(0.0);
		Solvers::root(foo, 0.0001, 0.25, 0.000001, dRoot);

		return dRoot;
	}

	static real BondEquivYldFromBankDiscount(const real& inp_dBankDiscountRate, const CDate& inp_dtFrom, const CDate& inp_dtTo)
	{
		return BondEquivYldFromPrice(inp_dtFrom, inp_dtTo, PriceFromBondEquivYld(inp_dBankDiscountRate, inp_dtFrom, inp_dtTo));
	}

	template<typename T> static auto BankDiscountFromPrice(const T& inp_dPriceDecimal, const CDate& inp_dtPrice, const CDate& inp_dtMaturity) -> decltype((1.0 - inp_dPriceDecimal) / Actual360(inp_dtPrice, inp_dtMaturity))
	{
		return (1.0 - inp_dPriceDecimal) * (1 / Actual360(inp_dtPrice, inp_dtMaturity));
	}

	template<typename T> static auto BankDiscountFromBondEquivYld(const T& inp_dBondEquivalentYield, const CDate& inp_dtFrom, const CDate& inp_dtTo) ->decltype(BankDiscountFromPrice(inp_dtFrom, inp_dtTo, PriceFromBondEquivYld(inp_dtFrom, inp_dtTo, inp_dBondEquivalentYield)))
	{
		return BankDiscountFromPrice(inp_dtFrom, inp_dtTo, PriceFromBondEquivYld(inp_dtFrom, inp_dtTo, inp_dBondEquivalentYield));
	}

	template<typename T> static auto PriceFromBankDiscount(const T& inp_dDiscountYieldDecimal, const CDate& inp_dtPrice, const CDate& inp_dtMaturity) -> decltype(priceFromBondEquivalentYield(inp_dDiscountYieldDecimal, (int32_t)1, (real)1))
	{
		return 1.0 - inp_dDiscountYieldDecimal * Actual360(inp_dtPrice, inp_dtMaturity);
	}

	// compounding
	template<typename T> static auto GetCmpGrowthFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(std::pow(1.0 + inp_dPeriodicRateDecimal, inp_dPeriods))
	{
		return std::pow(1.0 + inp_dPeriodicRateDecimal, inp_dPeriods);
	}

	template<typename T> static auto GetContGrowthFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(std::exp(inp_dPeriodicRateDecimal * inp_dPeriods))
	{
		return std::exp(inp_dPeriodicRateDecimal * inp_dPeriods);
	}

	template<typename T> static auto GetSmplGrowthFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(1.0 + inp_dPeriodicRateDecimal * inp_dPeriods)
	{
		return 1.0 + inp_dPeriodicRateDecimal * inp_dPeriods;
	}

	template<typename T> static auto ImplyPeriodicRateFromCmpGrowth(const T& inp_dGrowthFactor, const real& inp_dPeriods) -> decltype(std::pow(inp_dGrowthFactor, 1.0 / inp_dPeriods) - 1.0)
	{
		return std::pow(inp_dGrowthFactor, 1.0 / inp_dPeriods) - 1.0;
	}

	template<typename T> static auto ImplyPeriodicRateFromContGrowth(const T& inp_dGrowthFactor, const real& inp_dPeriods)-> decltype(std::ln(inp_dGrowthFactor) / inp_dPeriods)
	{
		return std::ln(inp_dGrowthFactor) / inp_dPeriods;
	}

	template<typename T> static auto ImplyPeriodicRateFromSmplGrowth(const T& inp_dGrowthFactor, const real& inp_dPeriods)-> decltype((inp_dGrowthFactor - 1.0) / inp_dPeriods)
	{
		return (inp_dGrowthFactor - 1.0) / inp_dPeriods;
	}

	static real ImplyPeriodicRateFromCmpGrowth(const real& inp_dGrowthFactor, const real& inp_dPeriods)
	{
		if (inp_dGrowthFactor == 1.0 || inp_dPeriods == 0.0) { return 0.0; }

		return (pow(inp_dGrowthFactor, 1.0 / inp_dPeriods) - 1.0);
	}

	static real ImplyPeriodicRateFromContGrowth(const real& inp_dGrowthFactor, const real& inp_dPeriods)
	{
		if (inp_dGrowthFactor == 1.0 || inp_dPeriods == 0.0) { return  0.0; }

		return CMath::ln(inp_dGrowthFactor) / inp_dPeriods;
	}

	static real ImplyPeriodicRateFromSmplGrowth(const real& inp_dGrowthFactor, const real& inp_dPeriods)
	{
		if (inp_dPeriods == 0.0) { return 0.0; }

		return (inp_dGrowthFactor - 1.0) / inp_dPeriods;
	}

	// mortgage
	static real AnnuityPmtPerPv(const real& inp_dPeriodDf, const size_t& inp_iPeriods)
	{
		if (inp_dPeriodDf == 1.0) return (1.0 / inp_iPeriods);

		return (1.0 - inp_dPeriodDf) / (inp_dPeriodDf - pow(inp_dPeriodDf, inp_iPeriods + 1));
	}

	template<typename T> static auto AnnuityPmtsPerPv(const T& inp_dPeriodDf, const size_t inp_iPeriods) -> decltype(std::transform(inp_dPeriodDf, MakeParameterized(fin::AnnuityPmtPerPv, inp_iPeriods)))
	{
		return std::transform(inp_dPeriodDf, MakeParameterized(fin::AnnuityPmtPerPv, inp_iPeriods));
	}

	template<typename T> static auto Cpr2Smm(const T& inp_cCpr) -> decltype(1 - std::pow(1 - inp_cCpr, 0.0833333333333333))
	{
		return 1 - std::pow(1 - inp_cCpr, 0.0833333333333333);
	}

	template<typename T> static auto Smm2Cpr(const T& inp_dSmm) ->decltype(1 - std::pow(1 - inp_dSmm, 12))
	{
		return 1 - std::pow(1 - inp_dSmm, 12);
	}
};
#endif