#ifndef INCLUDE_H_HOLIDAY
#define INCLUDE_H_HOLIDAY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

class CHolidays
{
public:
	static CHolidayCalendarPtr GetCalendar(HOLIDAYCALENDARS inp_HolidayCalendar, const int32_t& inp_iYearStart, const int32_t& inp_iYearEnd)
	{
		switch (inp_HolidayCalendar)
		{
		case HOLIDAYCALENDARS::LONDON: return createCalendar(inp_iYearStart, inp_iYearEnd, getLondonHolidays());
		case HOLIDAYCALENDARS::NYSE: return createCalendar(inp_iYearStart, inp_iYearEnd, getNyseHolidays());
		case HOLIDAYCALENDARS::PUBLIC: return createCalendar(inp_iYearStart, inp_iYearEnd, getPublicHolidays());
		default: // BOND
			return createCalendar(inp_iYearStart, inp_iYearEnd, getBondHolidays());
		}
	}

	static CHolidayCalendarLstPtr GetAll(const int32_t& inp_iYearStart, const int32_t& inp_iYearEnd)
	{
		CHolidayCalendarLstPtr cAllPtr(CreateSmart<CHolidayCalendarLst>());
		cAllPtr->add(HOLIDAYCALENDARS::BOND, GetCalendar(HOLIDAYCALENDARS::BOND, inp_iYearStart, inp_iYearEnd));
		cAllPtr->add(HOLIDAYCALENDARS::FNMA, GetCalendar(HOLIDAYCALENDARS::FNMA, inp_iYearStart, inp_iYearEnd));
		cAllPtr->add(HOLIDAYCALENDARS::FHLMC, GetCalendar(HOLIDAYCALENDARS::FHLMC, inp_iYearStart, inp_iYearEnd));
		cAllPtr->add(HOLIDAYCALENDARS::GNMA, GetCalendar(HOLIDAYCALENDARS::GNMA, inp_iYearStart, inp_iYearEnd));
		cAllPtr->add(HOLIDAYCALENDARS::LONDON, GetCalendar(HOLIDAYCALENDARS::LONDON, inp_iYearStart, inp_iYearEnd));
		cAllPtr->add(HOLIDAYCALENDARS::NYSE, GetCalendar(HOLIDAYCALENDARS::NYSE, inp_iYearStart, inp_iYearEnd));
		cAllPtr->add(HOLIDAYCALENDARS::PUBLIC, GetCalendar(HOLIDAYCALENDARS::PUBLIC, inp_iYearStart, inp_iYearEnd));

		return cAllPtr;
	}

private:
	enum class OBSERVANCERULE { ACTUAL, NEAREST, BACK, FWD, BOXING, NYD };

	typedef CSimpleMap<HOLIDAYS, OBSERVANCERULE> CHolidayList;
	typedef CSmartPtr<CHolidayList> CHolidayListPtr;

	static CHolidayCalendarPtr createCalendar(const int32_t& inp_iYearStart, const int32_t& inp_iYearEnd, CHolidayListPtr inp_cHolidayListPtr)
	{
		CHolidayCalendarPtr	cThisHolidayCalendarPtr(CreateSmart<CHolidayCalendar>());
		CDate				cThisDate;

		for (int32_t iYear(inp_iYearStart); iYear <= inp_iYearEnd; iYear++)
		{
			auto itr(iterators::GetItr(inp_cHolidayListPtr));
			LOOP(itr)
			{
				cThisDate = getHoliday(iYear, itr.key());
				observeHoliday(cThisDate, itr.value());
				cThisHolidayCalendarPtr->add(cThisDate, itr.key());
			}
		}
		return cThisHolidayCalendarPtr;
	}

	static CDate getHoliday(const int32_t& inp_iYear, const HOLIDAYS& inp_iHoliday)
	{
		int32_t iCalc, iTemp, iMonth, iDay;
		int32_t iCenturies(inp_iYear / 100);
		int32_t iYearRemain19(inp_iYear % 19);

		switch (inp_iHoliday)
		{
		case HOLIDAYS::BOXING_DAY:
			return CDate(inp_iYear, MONTHSOFYEAR::DECEMBER, 26);
			break;
		case HOLIDAYS::CHRISTMAS:
			return CDate(inp_iYear, MONTHSOFYEAR::DECEMBER, 25);
			break;
		case HOLIDAYS::COLUMBUS_DAY:
			return CDate(inp_iYear, MONTHSOFYEAR::OCTOBER, DAYSOFWEEK::MONDAY, POSITIONINMONTH::SECOND);
			break;
		case HOLIDAYS::EASTER:
			// see http://aa.usno.navy.mil/faq/docs/easter.php#compute
			iCalc = iCenturies - iCenturies / 4 - (iCenturies - (iCenturies - 17) / 25) / 3 + 19 * iYearRemain19 + 15;
			iCalc = iCalc % 30;
			iCalc = iCalc - (iCalc / 28) * (1 - (iCalc / 28) * (29 / (iCalc + 1)) * ((21 - iYearRemain19) / 11));

			iTemp = iCalc + inp_iYear + inp_iYear / 4 + 2 - iCenturies + iCenturies / 4;

			iCalc -= (iTemp % 7);
			iMonth = 3 + (iCalc + 40) / 44;
			iDay = iCalc + 28 - 31 * (iMonth / 4);

			return CDate(inp_iYear, static_cast<MONTHSOFYEAR>(iMonth), iDay);
			break;
		case HOLIDAYS::GOOD_FRIDAY:
			return getHoliday(inp_iYear, HOLIDAYS::EASTER).Add(PERIODS::DAY, -2, false);
			break;
		case HOLIDAYS::JULY4:
			return CDate(inp_iYear, MONTHSOFYEAR::JULY, 4);
			break;
		case HOLIDAYS::LABOR_DAY:
			return CDate(inp_iYear, MONTHSOFYEAR::SEPTEMBER, DAYSOFWEEK::MONDAY, POSITIONINMONTH::FIRST);
			break;
		case HOLIDAYS::MLK_DAY:
			return CDate(inp_iYear, MONTHSOFYEAR::JANUARY, DAYSOFWEEK::MONDAY, POSITIONINMONTH::THIRD);
			break;
		case HOLIDAYS::MEMORIAL_DAY:
			return CDate(inp_iYear, MONTHSOFYEAR::MAY, DAYSOFWEEK::MONDAY, POSITIONINMONTH::LAST);
			break;
		case HOLIDAYS::PRESIDENTS_DAY:
			return CDate(inp_iYear, MONTHSOFYEAR::FEBRUARY, DAYSOFWEEK::MONDAY, POSITIONINMONTH::THIRD);
			break;
		case HOLIDAYS::THANKSGIVING:
			return CDate(inp_iYear, MONTHSOFYEAR::NOVEMBER, DAYSOFWEEK::THURSDAY, POSITIONINMONTH::FOURTH);
			break;
		case HOLIDAYS::THANKSGIVING_DAYAFTER:
			return getHoliday(inp_iYear, HOLIDAYS::THANKSGIVING).Add(PERIODS::DAY, 1, false);
			break;
		case HOLIDAYS::EARLY_MAY_BANK:
			return CDate(inp_iYear, MONTHSOFYEAR::MAY, DAYSOFWEEK::MONDAY, POSITIONINMONTH::FIRST);
			break;
		case HOLIDAYS::LATE_MAY_BANK:
			return CDate(inp_iYear, MONTHSOFYEAR::MAY, DAYSOFWEEK::MONDAY, POSITIONINMONTH::LAST);
			break;
		case HOLIDAYS::SUMMER_BANK:
			return CDate(inp_iYear, MONTHSOFYEAR::AUGUST, DAYSOFWEEK::MONDAY, POSITIONINMONTH::LAST);
			break;
		case HOLIDAYS::VETERANS_DAY:
			return CDate(inp_iYear, MONTHSOFYEAR::NOVEMBER, 11);
			break;
		default: //New Years Day
			return CDate(inp_iYear, MONTHSOFYEAR::JANUARY, 1);
			break;
		}
	}

	static CHolidayListPtr getBondHolidays(void)
	{
		CHolidayListPtr cThisHolidayListPtr(CreateSmart<CHolidayList>());

		cThisHolidayListPtr->add(HOLIDAYS::NEWYEARS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::MLK_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::PRESIDENTS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::GOOD_FRIDAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::MEMORIAL_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::JULY4, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::LABOR_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::COLUMBUS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::VETERANS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::THANKSGIVING, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::CHRISTMAS, OBSERVANCERULE::NEAREST);

		return cThisHolidayListPtr;
	}

	static CHolidayListPtr getFnmaHolidays(void)
	{
		CHolidayListPtr cThisHolidayListPtr(CreateSmart<CHolidayList>());

		cThisHolidayListPtr->add(HOLIDAYS::NEWYEARS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::MLK_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::PRESIDENTS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::MEMORIAL_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::JULY4, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::LABOR_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::VETERANS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::THANKSGIVING, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::THANKSGIVING_DAYAFTER, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::CHRISTMAS, OBSERVANCERULE::NEAREST);

		return cThisHolidayListPtr;
	}

	static CHolidayListPtr getFhlmcHolidays(void)
	{
		CHolidayListPtr cThisHolidayListPtr(CreateSmart<CHolidayList>());

		cThisHolidayListPtr->add(HOLIDAYS::NEWYEARS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::MLK_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::MEMORIAL_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::JULY4, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::LABOR_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::VETERANS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::THANKSGIVING, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::CHRISTMAS, OBSERVANCERULE::NEAREST);

		return cThisHolidayListPtr;
	}

	static CHolidayListPtr getGnmaHolidays(void)
	{
		CHolidayListPtr cThisHolidayListPtr(CreateSmart<CHolidayList>());

		cThisHolidayListPtr->add(HOLIDAYS::NEWYEARS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::MLK_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::PRESIDENTS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::MEMORIAL_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::JULY4, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::LABOR_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::COLUMBUS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::VETERANS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::THANKSGIVING, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::CHRISTMAS, OBSERVANCERULE::NEAREST);

		return cThisHolidayListPtr;
	}

	static CHolidayListPtr getLondonHolidays(void)
	{
		CHolidayListPtr cThisHolidayListPtr(CreateSmart<CHolidayList>());

		cThisHolidayListPtr->add(HOLIDAYS::NEWYEARS_DAY, OBSERVANCERULE::FWD);
		cThisHolidayListPtr->add(HOLIDAYS::GOOD_FRIDAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::EASTER, OBSERVANCERULE::FWD);
		cThisHolidayListPtr->add(HOLIDAYS::EARLY_MAY_BANK, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::LATE_MAY_BANK, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::SUMMER_BANK, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::CHRISTMAS, OBSERVANCERULE::FWD);
		cThisHolidayListPtr->add(HOLIDAYS::BOXING_DAY, OBSERVANCERULE::BOXING);

		return cThisHolidayListPtr;
	}

	static CHolidayListPtr getNyseHolidays(void)
	{
		CHolidayListPtr cThisHolidayListPtr(CreateSmart<CHolidayList>());
		
		cThisHolidayListPtr->add(HOLIDAYS::NEWYEARS_DAY, OBSERVANCERULE::NYD);
		cThisHolidayListPtr->add(HOLIDAYS::MLK_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::PRESIDENTS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::GOOD_FRIDAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::MEMORIAL_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::JULY4, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::LABOR_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::THANKSGIVING, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::CHRISTMAS, OBSERVANCERULE::NEAREST);

		return cThisHolidayListPtr;
	}

	static CHolidayListPtr getPublicHolidays(void)
	{
		CHolidayListPtr cThisHolidayListPtr(CreateSmart<CHolidayList>());

		cThisHolidayListPtr->add(HOLIDAYS::NEWYEARS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::MLK_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::PRESIDENTS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::MEMORIAL_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::JULY4, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::LABOR_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::COLUMBUS_DAY, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::VETERANS_DAY, OBSERVANCERULE::NEAREST);
		cThisHolidayListPtr->add(HOLIDAYS::THANKSGIVING, OBSERVANCERULE::ACTUAL);
		cThisHolidayListPtr->add(HOLIDAYS::CHRISTMAS, OBSERVANCERULE::NEAREST);

		return cThisHolidayListPtr;
	}

	static void observeHoliday(CDate& inp_cHoliday, const OBSERVANCERULE& inp_iObservanceRule)
	{
		DAYSOFWEEK	iDayOfWeek(inp_cHoliday.DayOfWeek());
		int32_t		iDayShift(0);

		switch (inp_iObservanceRule)
		{
		case OBSERVANCERULE::NEAREST:
			if (iDayOfWeek == DAYSOFWEEK::SATURDAY){ iDayShift = -1; }
			else if (iDayOfWeek == DAYSOFWEEK::SUNDAY){ iDayShift = 1; }
			break;
		case OBSERVANCERULE::BACK:
			if (iDayOfWeek == DAYSOFWEEK::SATURDAY){ iDayShift = -1; }
			else if (iDayOfWeek == DAYSOFWEEK::SUNDAY){ iDayShift = -2; }
			break;
		case OBSERVANCERULE::FWD:
			if (iDayOfWeek == DAYSOFWEEK::SATURDAY){ iDayShift = 2; }
			else if (iDayOfWeek == DAYSOFWEEK::SUNDAY){ iDayShift = 1; }
			break;
		case OBSERVANCERULE::BOXING:
			if (iDayOfWeek == DAYSOFWEEK::SATURDAY){ iDayShift = 2; }
			else if (iDayOfWeek == DAYSOFWEEK::SUNDAY){ iDayShift = 2; }
			else if (iDayOfWeek == DAYSOFWEEK::MONDAY){ iDayShift = 1; }
			break;
		case OBSERVANCERULE::NYD:
			if (iDayOfWeek == DAYSOFWEEK::SUNDAY) { iDayShift = 1; }
			break;
		}

		inp_cHoliday.Move(PERIODS::DAY, iDayShift, false);
	}
};
#endif