#ifndef INCLUDE_H_OPTIONS
#define INCLUDE_H_OPTIONS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

template<typename TUnderlying, OPTIONTYPE TOptionType> class COption : public CContainer<FRM_TYPELIST::NONSCHEMATIC>
{
public:
	typedef COption			_Myt;
	typedef TUnderlying		underlying_type;

	virtual real CalcAtm(void) const = 0;
	virtual real CalcPrice(real) const = 0;

	CDate	GetExpiry(void) const { return mdtExpiry; }
	real	GetStrike(void) const { return mdStrike; }
	CSmartPtr<underlying_type> GetUnderlying(void) const { return mcUnderlyingPtr; }

	virtual real ImputeVol(real) const = 0;

	virtual void SetExpiry(CDate inp_dtExpiry) { mdtExpiry = inp_dtExpiry; }
	virtual void SetStrike(real inp_dStrike) { mdStrike = inp_dStrike; }
	virtual void SetUnderlying(CSmartPtr<underlying_type> inp_cUnderlyingPtr) { mcUnderlyingPtr = inp_cUnderlyingPtr; }

protected:
	CDate						mdtExpiry;
	real						mdStrike;
	CSmartPtr<underlying_type>	mcUnderlyingPtr;

	COption(void) {}

	SPUD(mdtExpiry, mdStrike, mcUnderlyingPtr)
};
#endif