#ifndef INCLUDE_H_COMPOUND
#define INCLUDE_H_COMPOUND

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"
#include "FinLib.h"

template<COMPOUNDTYPE TComp> struct CRateCmpd;
template<> struct CRateCmpd<COMPOUNDTYPE::COMPOUND>
{
	template<typename T> static inline auto CalcGrowthFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(fin::GetCmpGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods))
	{
		return fin::GetCmpGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods);
	}
	template<typename T> static inline auto CalcDiscountFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(1.0 / CalcGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods))
	{
		return 1.0 / CalcGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods);
	}

	template<typename T> static inline auto ImplyPeriodicRateFromGrowth(const T& inp_dGrowthFactor, const real& inp_dPeriods) -> decltype(fin::ImplyPeriodicRateFromCmpGrowth(inp_dGrowthFactor, inp_dPeriods))
	{
		return fin::ImplyPeriodicRateFromCmpGrowth(inp_dGrowthFactor, inp_dPeriods);
	}
	template<typename T> static inline auto ImplyPeriodicRateFromDiscount(const T& inp_dDiscountFactor, const real& inp_dPeriods) -> decltype(ImplyPeriodicRateFromGrowth(1.0 / inp_dDiscountFactor, inp_dPeriods))
	{
		return ImplyPeriodicRateFromGrowth(1.0 / inp_dDiscountFactor, inp_dPeriods);
	}
};

template<> struct CRateCmpd<COMPOUNDTYPE::CONTINUOUS>
{
	template<typename T> static inline auto CalcGrowthFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(fin::GetContGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods))
	{
		return fin::GetContGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods);
	}
	template<typename T> static inline auto CalcDiscountFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(1.0 / CalcGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods))
	{
		return 1.0 / CalcGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods);
	}

	template<typename T> static inline auto ImplyPeriodicRateFromGrowth(const T& inp_dGrowthFactor, const real& inp_dPeriods) -> decltype(fin::ImplyPeriodicRateFromContGrowth(inp_dGrowthFactor, inp_dPeriods))
	{
		return fin::ImplyPeriodicRateFromContGrowth(inp_dGrowthFactor, inp_dPeriods);
	}
	template<typename T> static inline auto ImplyPeriodicRateFromDiscount(const T& inp_dDiscountFactor, const real& inp_dPeriods) -> decltype(ImplyPeriodicRateFromGrowth(1.0 / inp_dDiscountFactor, inp_dPeriods))
	{
		return ImplyPeriodicRateFromGrowth(1.0 / inp_dDiscountFactor, inp_dPeriods);
	}
};

template<> struct CRateCmpd<COMPOUNDTYPE::SIMPLE>
{
	template<typename T> static inline auto CalcGrowthFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(fin::GetSmplGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods))
	{
		return fin::GetSmplGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods);
	}
	template<typename T> static inline auto CalcDiscountFactor(const T& inp_dPeriodicRateDecimal, const real& inp_dPeriods) -> decltype(1.0 / CalcGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods))
	{
		return 1.0 / CalcGrowthFactor(inp_dPeriodicRateDecimal, inp_dPeriods);
	}

	template<typename T> static inline auto ImplyPeriodicRateFromGrowth(const T& inp_dGrowthFactor, const real& inp_dPeriods) -> decltype(fin::ImplyPeriodicRateFromSmplGrowth(inp_dGrowthFactor, inp_dPeriods))
	{
		return fin::ImplyPeriodicRateFromSmplGrowth(inp_dGrowthFactor, inp_dPeriods);
	}
	template<typename T> static inline auto ImplyPeriodicRateFromDiscount(const T& inp_dDiscountFactor, const real& inp_dPeriods) -> decltype(ImplyPeriodicRateFromGrowth(1.0 / inp_dDiscountFactor, inp_dPeriods))
	{
		return ImplyPeriodicRateFromGrowth(1.0 / inp_dDiscountFactor, inp_dPeriods);
	}
};
#endif