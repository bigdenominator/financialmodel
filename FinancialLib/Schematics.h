#ifndef INCLUDE_H_SCHEMATICS
#define INCLUDE_H_SCHEMATICS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

SCHEMATIC(SBondDef, TYPELIST::SBONDDEF,
	(IdInt mlBondId; IdInt mlCounterpartyId; IdInt mlPropertyId;
	CDate mdtDated; CDate mdtIssue; CDate mdtMaturity;
	BUSINESS_DAY_CONVENTION mBusDayConv; DATE_GENERATION mDateGen; FREQUENCY mFreq; HOLIDAYCALENDARS mHolidayCalendar; DAY_COUNT_CONVENTION mDayCountConv; AMORTIZATIONTYPE mAmortType; MARKET_INDEX mResetIndex; ROUNDINGTYPE mRoundingType; LOANORIGINATORTYPE mLoanOriginatorType; LOANPURPOSETYPE mLoanPurposeType; DEFAULT_SUBMODELS mDfltSubModel; RTPP_SUBMODELS mRtppSubModel; LOANSTATUS mStatus; OAS_CALC_TYPE mOasCalcType;
	uint32_t miAmortPeriods; uint32_t miPayPeriods; uint32_t miFirstReset; uint32_t miPayPeriodsPerReset; uint32_t miLookbackDays;
	real mdMargin; real mdRoundingFactor; real mdFirstRelCapFloor; real mdPeriodicRelCapFloor; real mdLifeRelCapFloor; real mdBalOrigination; real mdRateOrigination; real mdBalCurrent; real mdRateCurrent; real mdPmtCurrent; real mdOas; real mdValue),
	(mlBondId, mlCounterpartyId, mlPropertyId,
	mdtDated, mdtIssue, mdtMaturity,
	mBusDayConv, mDateGen, mFreq, mHolidayCalendar, mDayCountConv, mAmortType, mResetIndex, mRoundingType, mLoanOriginatorType, mLoanPurposeType, mDfltSubModel, mRtppSubModel, mStatus, mOasCalcType,
	miAmortPeriods, miPayPeriods, miFirstReset, miPayPeriodsPerReset, miLookbackDays,
	mdMargin, mdRoundingFactor, mdFirstRelCapFloor, mdPeriodicRelCapFloor, mdLifeRelCapFloor, mdBalOrigination, mdRateOrigination, mdBalCurrent, mdRateCurrent, mdPmtCurrent, mdOas, mdValue))

//SCOUNTERPARTY
SCHEMATIC(SCounterparty, TYPELIST::SCOUNTERPARTY, (IdInt mlCounterpartyId; real mdFico;), (mlCounterpartyId, mdFico))

//SPROPERTY
SCHEMATIC(SProperty, TYPELIST::SPROPERTY,
	(IdInt mlPropertyId; STATE_CODES mFipsStateCode; PROJECTLEGALSTRUCTURETYPE mProjectLegalStructureType; PROPERTYUSAGETYPE mPropertyUsageType; CDate mdtValue; real mdValue),
	(mlPropertyId, mFipsStateCode, mProjectLegalStructureType, mPropertyUsageType, mdtValue, mdValue))


SCHEMATIC(SBond, TYPELIST::SBOND, (IdInt mlBondId; CMetricDetailPtr mcDetailPtr; CVectorRealPtr mcOasPtr; CVectorRealPtr mcRiskFreeValuesPtr; CVectorRealPtr mcValuesPtr;), (mlBondId, mcDetailPtr, mcOasPtr, mcRiskFreeValuesPtr, mcValuesPtr))
SCHEMATIC(SBondBehavior, TYPELIST::SBONDBEHAVIOR, (LOANSTATUS mStatus; CMetricDetailPtr mcDetailPtr;), (mStatus, mcDetailPtr))
SCHEMATIC(SBondContract, TYPELIST::SBONDCONTRACT, (SBondDefPtr msBondDefPtr; CDataTablePtr mcRatesPtr; CDataTablePtr mcBalEopPtr; CDataTablePtr mcIntPtr; CDataTablePtr mcPrinPtr;), (msBondDefPtr, mcRatesPtr, mcBalEopPtr, mcIntPtr, mcPrinPtr))

//SDEFAULT
SCHEMATIC(SDefault, TYPELIST::SDEFAULT, (SBondDefPtr msBondDefPtr; CDataTablePtr mcCfPtr; CDataTablePtr mcXBetaPtr;), (mcCfPtr, mcXBetaPtr))

//SGLOBALS
SCHEMATIC(SGlobals, TYPELIST::SGLOBALS, (CDate mdtMarket; size_t miScenarios; size_t miSimsPerScenario; CHolidayCalendarLstPtr mcCalendarsPtr), (mdtMarket, miScenarios, miSimsPerScenario, mcCalendarsPtr))

////SIRP
SCHEMATIC(SIrp, TYPELIST::SIRP, (MARKET_INDEX mMarketIndex; CDataTablePtr mcRatesPtr;), (mMarketIndex, mcRatesPtr))

//SPRICEOAS
SCHEMATIC(SPriceOAS, TYPELIST::SPRICEOAS, (SBondDefPtr msBondDefPtr; CDataTablePtr mcNetCfPtr; CVectorRealPtr mcOasPtr; CVectorRealPtr mcRiskFreePtr; CVectorRealPtr mcValuesPtr;), (msBondDefPtr, mcNetCfPtr, mcOasPtr, mcRiskFreePtr, mcValuesPtr))

//SPSA
SCHEMATIC(SPrepay, TYPELIST::SPREPAY, (SBondDefPtr msBondDefPtr; CDataTablePtr mcCfPtr; CDataTablePtr mcXBetaPtr;), (msBondDefPtr, mcCfPtr, mcXBetaPtr));

//SSEVERITY
SCHEMATIC(SSeverity, TYPELIST::SSEVERITY, (CDataTablePtr mcCfPtr; CDataTablePtr mcSeverityPtr;), (mcCfPtr, mcSeverityPtr))
#endif