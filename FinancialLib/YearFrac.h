#ifndef INCLUDE_H_YEARFRAC
#define INCLUDE_H_YEARFRAC

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"
#include "FinLib.h"

class IYearFrac
{
public:
	virtual real GetCoupon(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd) const = 0;

	virtual real GetYearFrac(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd, const CDate& inp_dtThis) const = 0;
	
protected:
	IYearFrac(void) {}
};

template<DAY_COUNT_CONVENTION TDayCntConv> struct CYearFrac;
template<> struct CYearFrac<DAY_COUNT_CONVENTION::ACT_360> : public IYearFrac
{
	typedef IYearFrac									_Mybase;
	typedef CYearFrac<DAY_COUNT_CONVENTION::ACT_360>	_Myt;

	CYearFrac(void) :_Mybase() {}

	real GetCoupon(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd) const
	{
		return fin::Actual360(inp_dtPeriodStart, inp_dtPeriodEnd);
	}

	real GetYearFrac(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd, const CDate& inp_dtThis) const
	{
		return fin::Actual360(inp_dtPeriodStart, inp_dtThis);
	}
};
template<> struct CYearFrac<DAY_COUNT_CONVENTION::ACT_365F> : public IYearFrac
{
	typedef IYearFrac									_Mybase;
	typedef CYearFrac<DAY_COUNT_CONVENTION::ACT_365F>	_Myt;

	CYearFrac(void) :_Mybase() {}

	real GetCoupon(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd) const
	{
		return fin::Actual365F(inp_dtPeriodStart, inp_dtPeriodEnd);
	}

	real GetYearFrac(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd, const CDate& inp_dtThis) const
	{
		return fin::Actual365F(inp_dtPeriodStart, inp_dtThis);
	}
};
template<> struct CYearFrac<DAY_COUNT_CONVENTION::ACT_ACT_ICMA> : public IYearFrac
{
	typedef IYearFrac									_Mybase;
	typedef CYearFrac<DAY_COUNT_CONVENTION::ACT_ACT_ICMA>	_Myt;

	CYearFrac(const SPeriod& inp_sPeriod) :_Mybase(),
		mdCpnFrac(inp_sPeriod.miUnitCount / static_cast<real>(SPeriod::PeriodsPerYear(inp_sPeriod.mUnitType))) {}

	real GetCoupon(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd) const
	{
		return mdCpnFrac;
	}

	real GetYearFrac(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd, const CDate& inp_dtThis) const
	{
		return fin::ActualActualICMA_PeriodFrac(inp_dtPeriodStart, inp_dtPeriodEnd, inp_dtThis) * mdCpnFrac;
	}

protected:
	real mdCpnFrac;


};
template<> struct CYearFrac<DAY_COUNT_CONVENTION::ACT_ACT_ISDA> : public IYearFrac
{
	typedef IYearFrac									_Mybase;
	typedef CYearFrac<DAY_COUNT_CONVENTION::ACT_ACT_ISDA>	_Myt;

	CYearFrac(void) :_Mybase() {}

	real GetCoupon(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd) const
	{
		return fin::ActualActualISDA(inp_dtPeriodStart, inp_dtPeriodEnd);
	}

	real GetYearFrac(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd, const CDate& inp_dtThis) const
	{
		return fin::ActualActualISDA(inp_dtPeriodStart, inp_dtThis);
	}
};
template<> struct CYearFrac<DAY_COUNT_CONVENTION::BOND_BASIS> : public IYearFrac
{
	typedef IYearFrac									_Mybase;
	typedef CYearFrac<DAY_COUNT_CONVENTION::BOND_BASIS>	_Myt;

	CYearFrac(void) :_Mybase() {}

	real GetCoupon(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd) const
	{
		return fin::BondBasis(inp_dtPeriodStart, inp_dtPeriodEnd);
	}

	real GetYearFrac(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd, const CDate& inp_dtThis) const
	{
		return fin::BondBasis(inp_dtPeriodStart, inp_dtThis);
	}
};
template<> struct CYearFrac<DAY_COUNT_CONVENTION::EUROBOND_BASIS> : public IYearFrac
{
	typedef IYearFrac									_Mybase;
	typedef CYearFrac<DAY_COUNT_CONVENTION::EUROBOND_BASIS>	_Myt;

	CYearFrac(void) :_Mybase() {}

	real GetCoupon(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd) const
	{
		return fin::EurobondBasis(inp_dtPeriodStart, inp_dtPeriodEnd);
	}

	real GetYearFrac(const CDate& inp_dtPeriodStart, const CDate& inp_dtPeriodEnd, const CDate& inp_dtThis) const
	{
		return fin::EurobondBasis(inp_dtPeriodStart, inp_dtThis);
	}
};

typedef CSmartPtr<IYearFrac> CYearFracPtr;
namespace CFinTools
{
	static CYearFracPtr CreateYearFrac(const DAY_COUNT_CONVENTION& inp_DayCountConvention, const SPeriod& inp_sPeriod)
	{
		switch (inp_DayCountConvention)
		{
		case DAY_COUNT_CONVENTION::ACT_360: return CreateSmart<CYearFrac<DAY_COUNT_CONVENTION::ACT_360>>();
		case DAY_COUNT_CONVENTION::ACT_365F: return CreateSmart<CYearFrac<DAY_COUNT_CONVENTION::ACT_365F>>();
		case DAY_COUNT_CONVENTION::ACT_ACT_ISDA: return CreateSmart<CYearFrac<DAY_COUNT_CONVENTION::ACT_ACT_ISDA>>();
		case DAY_COUNT_CONVENTION::ACT_ACT_ICMA: return CreateSmart<CYearFrac<DAY_COUNT_CONVENTION::ACT_ACT_ICMA>>(inp_sPeriod);
		case DAY_COUNT_CONVENTION::EUROBOND_BASIS: return CreateSmart<CYearFrac<DAY_COUNT_CONVENTION::EUROBOND_BASIS>>();
		default: return CreateSmart<CYearFrac<DAY_COUNT_CONVENTION::BOND_BASIS>>();
		}
	}
};
#endif