#ifndef INCLUDE_H_CONSTANTRATEMODEL
#define INCLUDE_H_CONSTANTRATEMODEL

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateModel.h"

struct CConstantRates : public IRateModel
{
	typedef IRateModel		_Mybase;
	typedef CConstantRates	_Myt;

	CConstantRates() :_Mybase() {}

	bool Run(const CYieldCurve& inp_cInitialCurve, CYieldCurve& inp_cPathDiscount, CYieldCurveLst& inp_cCurves)
	{
		CYieldCurvePtr cBasePtr(CreateSmart<CYieldCurve>(inp_cInitialCurve));
		cBasePtr->ExpandSimulations(inp_cPathDiscount.GetSims() / inp_cInitialCurve.GetSims());
		inp_cCurves.add(cBasePtr->GetAsOfDate(), cBasePtr);

		inp_cPathDiscount = *cBasePtr;
		
		return true;
	}
};
#endif