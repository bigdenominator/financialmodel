#ifndef INCLUDE_H_FORWARDROLLMODEL
#define INCLUDE_H_FORWARDROLLMODEL

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateModel.h"

struct CForwardRoll : public IRateModel
{
	typedef IRateModel		_Mybase;
	typedef CForwardRoll	_Myt;

	CForwardRoll() :_Mybase() {}

	bool Run(const CYieldCurve& inp_cInitialCurve, CYieldCurve& inp_cPathDiscount, CYieldCurveLst& inp_cCurves)
	{
		CYieldCurvePtr cBasePtr(CreateSmart<CYieldCurve>(inp_cInitialCurve));
		cBasePtr->ExpandSimulations(inp_cPathDiscount.GetSims() / inp_cInitialCurve.GetSims());
		inp_cCurves.add(cBasePtr->GetAsOfDate(), cBasePtr);

		inp_cPathDiscount = *cBasePtr;

		CDataTablePtr cDiscFactorsPtr(inp_cPathDiscount.AsDiscountRates());
		while (cDiscFactorsPtr->size() > 1)
		{
			CYieldCurvePtr ycPtr(rollForward(*cDiscFactorsPtr));
			inp_cCurves.add(ycPtr->GetAsOfDate(), ycPtr);
		}

		return true;
	}

protected:
	static CYieldCurvePtr rollForward(CDataTable& inp_cData)
	{
		auto itr(inp_cData.begin());

		const CDate dtAsOf(itr->first);
		CVectorRealPtr cBasePtr(itr->second);
		CYieldCurvePtr cResultPtr(CreateSmart<CYieldCurve>(dtAsOf, cBasePtr->size()));

		inp_cData.erase(dtAsOf);
		for (itr = inp_cData.begin(); itr != inp_cData.end(); itr++)
		{
			cResultPtr->AddDiscountFactors(dtAsOf, itr->first, *itr->second /= *cBasePtr);
		}
		
		return cResultPtr;
	}
};
#endif