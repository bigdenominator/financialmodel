#ifndef INCLUDE_H_LIBORDERIVATIVES
#define INCLUDE_H_LIBORDERIVATIVES

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "LiborIndexes.h"
#include "RateDerivative.h"

//using namespace fin;
typedef CRateOption < CLiborRate, OPTIONTYPE::CALL, fin::Black76Call > CCaplet;
typedef CSmartPtr<CCaplet> CCapletPtr;
typedef CRateOption < CLiborRate, OPTIONTYPE::PUT, fin::Black76Put > CFloorlet;
typedef CSmartPtr<CFloorlet> CFloorletPtr;

typedef CRateOptionChain < CLiborRate, OPTIONTYPE::CALL, fin::Black76Call > CCap;
typedef CSmartPtr<CCap> CCapPtr;
typedef CRateOptionChain < CLiborRate, OPTIONTYPE::PUT, fin::Black76Put > CFloor;
typedef CSmartPtr<CFloor> CFloorPtr;

typedef CRateOption < CSwapRate, OPTIONTYPE::CALL, fin::Black76Call > CSwaption;
typedef CSmartPtr<CSwaption> CSwaptionPtr;

typedef CSmartMap<MARKET_INDEX, CSwaptionPtr> CSwaptionLst;
typedef CSmartMap<MARKET_INDEX, CCapPtr> CCapLst;

typedef CSmartPtr<CSwaptionLst> CSwaptionLstPtr;
typedef CSmartPtr<CCapLst> CCapLstPtr;

SMARTSTRUCT(SLiborDerivativeLsts, (CCapLstPtr mcCapLstPtr; CSwaptionLstPtr mcSwaptionLstPtr;), (mcCapLstPtr, mcSwaptionLstPtr))

namespace RateDerivativeTools
{
	static void	LoadLiborCaps(const CLiborLst& inp_cLiborLst, const CYieldCurve& inp_cDiscount, CCapLst& inp_cCapLst)
	{
		if (inp_cLiborLst.find(MARKET_INDEX::LIBOR_3M) == inp_cLiborLst.end()) { return; }

		CLiborRatePtr cLibor3Ptr(inp_cLiborLst.at(MARKET_INDEX::LIBOR_3M));
		cLibor3Ptr->SetSchedule(inp_cDiscount.GetAsOfDate());

		CDate dtExpiry(cLibor3Ptr->GetCoupons().front().mdtEop);
		fin::SubtractBusinessDays(dtExpiry, cLibor3Ptr->GetConfig().miSettleDays, *cLibor3Ptr->GetConfig().mcHolidaysPtr);

		const uint32_t iStart(static_cast<uint32_t>(MARKET_INDEX::CAP1));
		const uint32_t iEnd(static_cast<uint32_t>(MARKET_INDEX::CAP3));

		for (uint32_t iExpiryYears(1), iIndx(iStart); iIndx <= iEnd; iExpiryYears++, iIndx++)
		{
			CCapPtr cThisPtr(CreateSmart<CCap>(cLibor3Ptr, 4 * iExpiryYears - 1));
			cThisPtr->SetExpiry(dtExpiry);
			cThisPtr->SetPricingTerms(inp_cDiscount);
			cThisPtr->SetStrike(cThisPtr->CalcAtm());

			inp_cCapLst.add(static_cast<MARKET_INDEX>(iIndx), cThisPtr);
		}
	}

	static void	LoadLiborSwaptions(const CSwapLst& inp_cSwapLst, const CYieldCurve& inp_cDiscount, CSwaptionLst& inp_cSwaptionLst)
	{
		const uint32_t iStart(static_cast<uint32_t>(MARKET_INDEX::SWPTN01X01));
		const uint32_t iStartTenor(static_cast<uint32_t>(MARKET_INDEX::SWAP_1Y));
		const uint32_t iEndTenor(static_cast<uint32_t>(MARKET_INDEX::SWAP_10Y));

		for (uint32_t iTenor(iStartTenor), iIndx(iStart); iTenor <= iEndTenor; iTenor++)
		{
			MARKET_INDEX MktIndx(static_cast<MARKET_INDEX>(iTenor));
			if (inp_cSwapLst.find(MktIndx) != inp_cSwapLst.end())
			{
				CSwapRatePtr cSwapPtr(inp_cSwapLst.at(MktIndx));
				for (uint32_t iExpiry(1); iExpiry <= 10; iExpiry++, iIndx++)
				{
					CSwaptionPtr cThisPtr(CreateSmart<CSwaption>(cSwapPtr));
					cThisPtr->SetExpiry(cSwapPtr->GetDateAdj()->Adjust(inp_cDiscount.GetAsOfDate() + SPeriod(PERIODS::YEAR, iExpiry)));
					cThisPtr->SetPricingTerms(inp_cDiscount);
					cThisPtr->SetStrike(cThisPtr->CalcAtm());

					inp_cSwaptionLst.add(static_cast<MARKET_INDEX>(iIndx), cThisPtr);
				}
			}
		}
	}

	static void LoadLiborDerivatives(const SLiborLsts& inp_sLiborLsts, const CYieldCurve& inp_cDiscount, SLiborDerivativeLsts& inp_sLiborDerivativeLsts)
	{
		inp_sLiborDerivativeLsts.mcCapLstPtr = CreateSmart<CCapLst>();
		LoadLiborCaps(*inp_sLiborLsts.mcLiborLstPtr, inp_cDiscount, *inp_sLiborDerivativeLsts.mcCapLstPtr);

		inp_sLiborDerivativeLsts.mcSwaptionLstPtr = CreateSmart<CSwaptionLst>();
		LoadLiborSwaptions(*inp_sLiborLsts.mcSwapLstPtr, inp_cDiscount, *inp_sLiborDerivativeLsts.mcSwaptionLstPtr);
	}
}
#endif
