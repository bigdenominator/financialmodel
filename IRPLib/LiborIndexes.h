#ifndef INCLUDE_H_LIBORINDEXES
#define INCLUDE_H_LIBORINDEXES

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateIndex.h"

class CLiborRate : public IRateIndex
{
public:
	typedef IRateIndex	_Mybase;
	typedef CLiborRate	_Myt;

	CLiborRate(void) :_Myt(SPeriod(FREQUENCY::QUARTERLY)) {}

	CLiborRate(const SPeriod& inp_sTenor) :_Mybase(1)
	{
		msConfigPtr->mBusDayConv = BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING;
		msConfigPtr->mDateGen = DATE_GENERATION::EOM;
		msConfigPtr->mDayCountConv = DAY_COUNT_CONVENTION::ACT_360;
		msConfigPtr->mcHolidaysPtr = CreateSmart<CHolidayCalendar>();

		SetPeriod(inp_sTenor);
	}

	CLiborRate(const SPeriod& inp_sTenor, const CHolidayCalendarLst& inp_cCalendarLst) :_Myt(inp_sTenor)
	{
		SetHolidays(inp_cCalendarLst);
	}

	virtual inline void Bootstrap(real inp_dQuoteRate, CYieldCurve& inp_cBootstrap) const
	{
		SetSchedule(inp_cBootstrap.GetAsOfDate());

		inp_cBootstrap.AddDiscountFactor(msCouponsPtr->front().mdtBop, msCouponsPtr->front().mdtEop, 1.0 / fin::GetSmplGrowthFactor(inp_dQuoteRate, msCouponsPtr->front().mdCoupon));
	}

	inline void Calibrate(real inp_dQuoteRate, const CYieldCurve& inp_cDiscount)
	{
		SetSchedule(inp_cDiscount.GetAsOfDate());

		auto tmp((growthRate(msCouponsPtr->front(), inp_cDiscount) - 1.0) / msCouponsPtr->front().mdCoupon);

		mdSpread = inp_dQuoteRate  - reinterpret_cast<real*>(&tmp[0])[0];
		if (!HasSpread()) { mdSpread = 0.0; }
	}

	inline void GetPvFactors(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cFactors) const
	{
		inp_cFactors = inp_cDiscount.GetDiscountFactors(msCouponsPtr->front().mdtEop) * msCouponsPtr->front().mdCoupon;
	}

	inline real GetSpread(void) const { return mdSpread; }

	inline real HasSpread(void) const { return (fabs(mdSpread) > 0.00005); }

	inline void ImputeAnnualizedRates(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cResult) const
	{
		inp_cResult = imputePeriodic(msCouponsPtr->front(), inp_cDiscount) / msCouponsPtr->front().mdCoupon;
	}

	inline void ImputePeriodicRates(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cResult) const
	{
		inp_cResult = imputePeriodic(msCouponsPtr->front(), inp_cDiscount);
	}

	inline void SetHolidays(const CHolidayCalendarLst& inp_cCalendarLst)
	{
		if (inp_cCalendarLst.find(HOLIDAYCALENDARS::LONDON) != inp_cCalendarLst.end())
		{
			msConfigPtr->mcHolidaysPtr = inp_cCalendarLst.at(HOLIDAYCALENDARS::LONDON);
			_Mybase::init();
		}
	}

	inline void SetPeriod(const SPeriod& inp_sTenor)
	{
		msConfigPtr->miSettleDays = (inp_sTenor.mUnitType == PERIODS::DAY && inp_sTenor.miUnitCount == 1 ? 0 : 2);
		msConfigPtr->msPeriod = inp_sTenor;
		msConfigPtr->msTenor = inp_sTenor;

		mdSpread = 0.0;

		_Mybase::init();
	}

protected:
	real	mdSpread;

	virtual void setCoupons(CDate inp_dtSettlement, CDate inp_dtMaturity) const
	{
		msCouponsPtr->front().mdtBop = inp_dtSettlement;
		msCouponsPtr->front().mdtEop = inp_dtMaturity;
		msCouponsPtr->front().mdCoupon = mcYearFracPtr->GetYearFrac(inp_dtSettlement, inp_dtMaturity, inp_dtMaturity);
	}
	
	static CVectorReal growthRate(const SCpn& inp_sCpn, const CYieldCurve& inp_cDiscount)
	{
		return CVectorReal(inp_cDiscount.GetDiscountFactors(inp_sCpn.mdtBop) / inp_cDiscount.GetDiscountFactors(inp_sCpn.mdtEop));
	}

	virtual CVectorReal imputePeriodic(const SCpn& inp_sCpn, const CYieldCurve& inp_cDiscount) const
	{
		return growthRate(inp_sCpn, inp_cDiscount) += (mdSpread * inp_sCpn.mdCoupon - 1.0);
	}

	DERIVED_SPUD(mdSpread)
};
typedef CSmartPtr<CLiborRate> CLiborRatePtr;

class CEdfRate : public CLiborRate
{
public:
	typedef CLiborRate	_Mybase;
	typedef CEdfRate	_Myt;

	typedef CSmartSet<CDate>	CDateLst;
	typedef CSmartPtr<CDateLst>	CDateLstPtr;

	CEdfRate(void) :_Myt(1) {}

	CEdfRate(uint32_t inp_iOrdinal, bool inp_bAdjConvexity = false) :_Myt(inp_iOrdinal, inp_bAdjConvexity, BuildDates()) {}

	CEdfRate(uint32_t inp_iOrdinal, bool inp_bAdjConvexity, CDateLstPtr inp_cDateLstPtr) :_Mybase(SPeriod(FREQUENCY::QUARTERLY)), mcDateLstPtr(inp_cDateLstPtr)
	{
		msConfigPtr->mBusDayConv = BUSINESS_DAY_CONVENTION::UNADJUSTED;
		msConfigPtr->mDateGen = DATE_GENERATION::FORWARD;
		init();

		SetTerms(inp_iOrdinal, inp_bAdjConvexity);
	}

	CEdfRate(uint32_t inp_iOrdinal, bool inp_bAdjConvexity, const CHolidayCalendarLst& inp_cCalendarLst, CDateLstPtr inp_cDateLstPtr)
		:_Myt(inp_iOrdinal, inp_bAdjConvexity, inp_cDateLstPtr)
	{
		SetHolidays(inp_cCalendarLst);
	}

	inline void Bootstrap(real inp_dQuoteRate, CYieldCurve& inp_cBootstrap) const
	{
		SetSchedule(inp_cBootstrap.GetAsOfDate());

		inp_cBootstrap.AddDiscountFactor(msCouponsPtr->front().mdtBop, msCouponsPtr->front().mdtEop, (mbAdjConvexity ? convexityAdjDiscFac(inp_cBootstrap.GetAsOfDate(), msCouponsPtr->front().mdtBop) : 1.0) / fin::GetSmplGrowthFactor(inp_dQuoteRate, msCouponsPtr->front().mdCoupon));
	}

	static CDateLstPtr BuildDates(void)
	{
		CDateLstPtr cDateLstPtr(CreateSmart<CDateLst>());
		{
			for (int32_t iYear(iSTART); iYear < iEND; iYear++)
			{
				cDateLstPtr->insert(CDate(iYear, MONTHSOFYEAR::MARCH, DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD));
				cDateLstPtr->insert(CDate(iYear, MONTHSOFYEAR::JUNE, DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD));
				cDateLstPtr->insert(CDate(iYear, MONTHSOFYEAR::SEPTEMBER, DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD));
				cDateLstPtr->insert(CDate(iYear, MONTHSOFYEAR::DECEMBER, DAYSOFWEEK::WEDNESDAY, POSITIONINMONTH::THIRD));
			}
		}
		return cDateLstPtr;
	}

	inline void CalcDates(CDate inp_dtQuote, CDate& inp_dtEffective, CDate& inp_dtMaturity) const
	{
		inp_dtQuote = mcDateAdjPtr->AddBusinessDays(inp_dtQuote, 2);
		auto itr(mcDateLstPtr->lower_bound(++inp_dtQuote));
		for (uint32_t iOrdinal(1); iOrdinal < miOrdinal; iOrdinal++, itr++) {}

		inp_dtEffective = *itr++;
		inp_dtMaturity = *itr;
	}

	inline void SetPeriod(const SPeriod&) = delete;

	inline void SetTerms(uint32_t inp_iOrdinal, bool inp_bAdjConvexity)
	{
		miOrdinal = inp_iOrdinal;
		mbAdjConvexity = inp_bAdjConvexity;
	}

protected:
	enum :int { iSTART = 1950, iYEARS = 200, iEND = iSTART + iYEARS };

	bool		mbAdjConvexity;
	uint32_t	miOrdinal;

	CDateLstPtr	mcDateLstPtr;

	real convexityAdjDiscFac(CDate inp_dtCurrent, CDate inp_dtEffective) const
	{
		real dFrac(fin::ActualActualISDA(inp_dtCurrent, inp_dtEffective));

		// 0.999928002591938 = exp(-0.5 * 0.012 * 0.012)
		return pow(0.999928002591938, dFrac * dFrac * (dFrac + 0.25));
	}

	inline CVectorReal imputePeriodicRate(const SCpn& inp_sCpn, const CYieldCurve& inp_cDiscount) const
	{
		CVectorReal cResult(growthRate(inp_sCpn, inp_cDiscount));
		if (mbAdjConvexity)
		{
			cResult = cResult / convexityAdjDiscFac(inp_cDiscount.GetAsOfDate(), inp_sCpn.mdtBop) - 1.0;
		}
		else
		{
			cResult -= 1.0;
		}

		return cResult;
	}

	DERIVED_SPUD(mbAdjConvexity, miOrdinal)
};
typedef CSmartPtr<CEdfRate> CEdfRatePtr;

class CSwapRate : public IRateIndex
{
public:
	typedef IRateIndex	_Mybase;
	typedef CSwapRate	_Myt;

	CSwapRate(void) :_Myt(10, CreateSmart<CLiborRate>()) {}

	CSwapRate(uint32_t inp_iTenorInYears, CLiborRatePtr inp_cLiborPtr) :_Mybase(2 * inp_iTenorInYears)
	{
		msFloatLegPtr = CreateSmart<SCpnLst>();

		msConfigPtr->mDateGen = DATE_GENERATION::BACKWARD;
		msConfigPtr->mDayCountConv = DAY_COUNT_CONVENTION::BOND_BASIS;
		msConfigPtr->msPeriod = SPeriod(FREQUENCY::SEMIANNUAL);
		msConfigPtr->msTenor = SPeriod(PERIODS::YEAR, inp_iTenorInYears);

		SetTerms(inp_iTenorInYears, inp_cLiborPtr);
	}

	inline void Bootstrap(real inp_dQuoteRate, CYieldCurve& inp_cBootstrap) const
	{
		SetSchedule(inp_cBootstrap.GetAsOfDate());

		const CDate dtMaturity(msCouponsPtr->back().mdtEop);
		CVectorReal cWorking(inp_cBootstrap.GetSims());

		auto foo = [&](real inp_dTestZero)
		{
			CYieldCurve cYc(inp_cBootstrap);
			cYc.AddZeroRate(dtMaturity, inp_dTestZero);

			ImputeAnnualizedRates(cYc, cWorking);

			return cWorking[0] - inp_dQuoteRate;
		};

		real dNewZero(inp_dQuoteRate);
		if (Solvers::root(foo, 0.0, inp_dQuoteRate, 0.000001, dNewZero))
		{
			inp_cBootstrap.AddZeroRate(dtMaturity, dNewZero);
		}
	}

	inline void Calibrate(real, const CYieldCurve&) {}

	inline void GetPvFactors(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cFactors) const
	{
		inp_cFactors = getPvFactors(*msCouponsPtr, inp_cDiscount);
	}

	inline SCpnLst& GetFloatingLegs(void) const { return *msFloatLegPtr; }

	inline CLiborRatePtr GetLibor(void) const { return mcLiborPtr; }

	inline void ImputeAnnualizedRates(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cResult) const
	{
		ImputePeriodicRates(inp_cDiscount, inp_cResult);
		inp_cResult /= getPvFactors(*msCouponsPtr, inp_cDiscount);
	}

	inline void ImputePeriodicRates(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cResult) const
	{
		inp_cResult = imputePeriodic(inp_cDiscount);
		
		if (mcLiborPtr->HasSpread())
		{
			inp_cResult += (getPvFactors(*msFloatLegPtr, inp_cDiscount) * mcLiborPtr->GetSpread());
		}
	}

	inline void SetTerms(uint32_t inp_iTenorInYears, CLiborRatePtr inp_cLiborPtr)
	{
		mcLiborPtr = inp_cLiborPtr;

		const uint32_t iFixedPmts(inp_iTenorInYears * SPeriod::PeriodsPerYear<uint32_t>(msConfigPtr->msPeriod));
		const uint32_t iFloatPmts(inp_iTenorInYears * SPeriod::PeriodsPerYear<uint32_t>(mcLiborPtr->GetConfig().msTenor));

		msCouponsPtr->resize(iFixedPmts);
		msFloatLegPtr->resize(iFloatPmts);
		
		msConfigPtr->mBusDayConv = mcLiborPtr->GetConfig().mBusDayConv;
		msConfigPtr->mcHolidaysPtr = mcLiborPtr->GetConfig().mcHolidaysPtr;
		msConfigPtr->miSettleDays = mcLiborPtr->GetConfig().miSettleDays;

		init();
	}

protected:
	CLiborRatePtr	mcLiborPtr;
	SCpnLstPtr		msFloatLegPtr;

	inline void calcCoupons(CDate inp_dtSettlement, CDate inp_dtMaturity) const
	{
		CFinTools::CreateCouponScheduler(msConfigPtr->mDateGen, mcDateAdjPtr, mcYearFracPtr, inp_dtSettlement, inp_dtSettlement, inp_dtMaturity, msConfigPtr->msPeriod, msCouponsPtr);
		CFinTools::CreateCouponScheduler(mcLiborPtr->GetConfig().mDateGen, mcLiborPtr->GetDateAdj(), mcLiborPtr->GetYearFrac(), inp_dtSettlement, inp_dtSettlement, inp_dtMaturity, mcLiborPtr->GetConfig().msTenor, msFloatLegPtr);
	}

	static inline CVectorReal getPvFactors(const SCpnLst& inp_sCpnLst, const CYieldCurve& inp_cDiscount)
	{
		auto itr(inp_sCpnLst.begin());

		CVectorReal cResult(inp_cDiscount.GetDiscountFactors(itr->mdtEop) * itr->mdCoupon);
		for(itr++; itr != inp_sCpnLst.end(); itr++)
		{
			cResult += inp_cDiscount.GetDiscountFactors(itr->mdtEop) * itr->mdCoupon;
		}
		return cResult;
	}

	inline CVectorReal imputePeriodic(const CYieldCurve& inp_cDiscount) const
	{
		CVectorReal cResult(inp_cDiscount.GetDiscountFactors(msCouponsPtr->front().mdtBop));
		return cResult -= inp_cDiscount.GetDiscountFactors(msCouponsPtr->back().mdtEop);
	}

	DERIVED_SPUD(mcLiborPtr, msFloatLegPtr)
};
typedef CSmartPtr<CSwapRate> CSwapRatePtr;

typedef CSmartMap<MARKET_INDEX, CLiborRatePtr> CLiborLst;
typedef CSmartMap<MARKET_INDEX, CEdfRatePtr> CEdfLst;
typedef CSmartMap<MARKET_INDEX, CSwapRatePtr> CSwapLst;

typedef CSmartPtr<CLiborLst>	CLiborLstPtr;
typedef CSmartPtr<CEdfLst>		CEdfLstPtr;
typedef CSmartPtr<CSwapLst>		CSwapLstPtr;

SMARTSTRUCT(SLiborLsts, (CLiborLstPtr mcLiborLstPtr; CEdfLstPtr mcEdfLstPtr; CSwapLstPtr mcSwapLstPtr;), (mcLiborLstPtr, mcEdfLstPtr, mcSwapLstPtr))

namespace RateIndexes
{
	static inline void LoadLiborRates(const CHolidayCalendarLst& inp_cCalendarLst, CLiborLst& inp_cLiborLst)
	{
		inp_cLiborLst.add(MARKET_INDEX::LIBOR_ON, CreateSmart<CLiborRate>(SPeriod(PERIODS::DAY, 1), inp_cCalendarLst));
		inp_cLiborLst.add(MARKET_INDEX::LIBOR_1M, CreateSmart<CLiborRate>(SPeriod(PERIODS::MONTH, 1), inp_cCalendarLst));
		inp_cLiborLst.add(MARKET_INDEX::LIBOR_3M, CreateSmart<CLiborRate>(SPeriod(PERIODS::MONTH, 3), inp_cCalendarLst));
		inp_cLiborLst.add(MARKET_INDEX::LIBOR_6M, CreateSmart<CLiborRate>(SPeriod(PERIODS::MONTH, 6), inp_cCalendarLst));
		inp_cLiborLst.add(MARKET_INDEX::LIBOR_9M, CreateSmart<CLiborRate>(SPeriod(PERIODS::MONTH, 9), inp_cCalendarLst));
		inp_cLiborLst.add(MARKET_INDEX::LIBOR_12M, CreateSmart<CLiborRate>(SPeriod(PERIODS::MONTH, 12), inp_cCalendarLst));
	}

	static inline void LoadEurodollarFutureRates(bool inp_bAdjConvexity, const CHolidayCalendarLst& inp_cCalendarLst, CEdfLst& inp_cEdfLst)
	{
		const uint32_t iFirst(static_cast<uint32_t>(MARKET_INDEX::EDF1));
		const uint32_t iLast(static_cast<uint32_t>(MARKET_INDEX::EDF40));

		auto datePtr(CEdfRate::BuildDates());
		for (uint32_t iEDF(iFirst), iOrdinal(1); iEDF <= iLast; iEDF++, iOrdinal++)
		{
			inp_cEdfLst.add(static_cast<MARKET_INDEX>(iEDF), CreateSmart<CEdfRate>(iOrdinal, inp_bAdjConvexity, inp_cCalendarLst, datePtr));
		}
	}

	static inline void LoadSwapRates(const CLiborLst& inp_cLiborLst, CSwapLst& inp_cSwapLst)
	{
		CLiborRatePtr cLiborPtr(inp_cLiborLst.at(MARKET_INDEX::LIBOR_3M));

		inp_cSwapLst.add(MARKET_INDEX::SWAP_1Y, CreateSmart<CSwapRate>(1, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_2Y, CreateSmart<CSwapRate>(2, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_3Y, CreateSmart<CSwapRate>(3, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_4Y, CreateSmart<CSwapRate>(4, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_5Y, CreateSmart<CSwapRate>(5, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_6Y, CreateSmart<CSwapRate>(6, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_7Y, CreateSmart<CSwapRate>(7, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_8Y, CreateSmart<CSwapRate>(8, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_9Y, CreateSmart<CSwapRate>(9, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_10Y, CreateSmart<CSwapRate>(10, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_11Y, CreateSmart<CSwapRate>(11, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_12Y, CreateSmart<CSwapRate>(12, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_13Y, CreateSmart<CSwapRate>(13, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_14Y, CreateSmart<CSwapRate>(14, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_15Y, CreateSmart<CSwapRate>(15, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_20Y, CreateSmart<CSwapRate>(20, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_25Y, CreateSmart<CSwapRate>(25, cLiborPtr));
		inp_cSwapLst.add(MARKET_INDEX::SWAP_30Y, CreateSmart<CSwapRate>(30, cLiborPtr));
	}

	static inline void LoadLiborLsts(bool inp_bAdjConvexity, const CHolidayCalendarLst& inp_cCalendarLst, SLiborLsts& inp_sLiborLsts)
	{
		inp_sLiborLsts.mcLiborLstPtr = CreateSmart<CLiborLst>();
		LoadLiborRates(inp_cCalendarLst, *inp_sLiborLsts.mcLiborLstPtr);

		inp_sLiborLsts.mcEdfLstPtr = CreateSmart<CEdfLst>();
		LoadEurodollarFutureRates(inp_bAdjConvexity, inp_cCalendarLst, *inp_sLiborLsts.mcEdfLstPtr);

		inp_sLiborLsts.mcSwapLstPtr = CreateSmart<CSwapLst>();
		LoadSwapRates(*inp_sLiborLsts.mcLiborLstPtr, *inp_sLiborLsts.mcSwapLstPtr);
	}

	static inline void LoadMaster(const SLiborLsts& inp_sLiborLsts, CRateMaster& inp_cMaster)
	{
		inp_sLiborLsts.mcLiborLstPtr->foreach([&inp_cMaster](CLiborLst::reference inp_cDictEntry){ inp_cMaster.add(inp_cDictEntry.first, inp_cDictEntry.second.get()); });
		inp_sLiborLsts.mcEdfLstPtr->foreach([&inp_cMaster](CEdfLst::reference inp_cDictEntry){ inp_cMaster.add(inp_cDictEntry.first, inp_cDictEntry.second.get()); });
		inp_sLiborLsts.mcSwapLstPtr->foreach([&inp_cMaster](CSwapLst::reference inp_cDictEntry){ inp_cMaster.add(inp_cDictEntry.first, inp_cDictEntry.second.get()); });
	}
}
#endif