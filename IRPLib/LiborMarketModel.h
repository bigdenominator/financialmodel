#ifndef INCLUDE_H_LMM
#define INCLUDE_H_LMM

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "LiborDerivatives.h"
#include "RateModel.h"

class LmmVolSurface 
{
public:
	virtual void GetFactors(const real& inp_dAsOf, const real& inp_dExpiry, real& inp_dFactor1, real& inp_dFactor2) const = 0;
};

class CLiborMarketModel : public IRateModel
{
public:
	typedef IRateModel			_Mybase;
	typedef CLiborMarketModel	_Myt;

	enum :uint32_t { FACTORS = 2 };

	CLiborMarketModel(const uint64_t& inp_lRandomSeed, CLiborRatePtr inp_cLiborPtr, LmmVolSurface& inp_cLmmVolSurface)
		:_Mybase(), mlRandomSeed(inp_lRandomSeed), miCaplets(_Myt::YEARS * SPeriod::PeriodsPerYear<uint32_t>(inp_cLiborPtr->GetConfig().msTenor)),
		mcCapPtr(CreateSmart<CCap>()), mcLmmVolSurface(inp_cLmmVolSurface), mcRandomGen(mlRandomSeed)
	{
		mcCapPtr->SetUnderlying(inp_cLiborPtr, miCaplets);
	}

	bool Run(const CYieldCurve& inp_cInitialCurve, CYieldCurve& inp_cPathDiscount, CYieldCurveLst& inp_cCurves)
	{
		CYieldCurvePtr cBasePtr(CreateSmart<CYieldCurve>(inp_cInitialCurve));
		cBasePtr->ExpandSimulations(inp_cPathDiscount.GetSims() / inp_cInitialCurve.GetSims());
		inp_cCurves.add(cBasePtr->GetAsOfDate(), cBasePtr);

		initialize(inp_cInitialCurve, inp_cPathDiscount.GetSims());
		buildForwards(*cBasePtr);
		for (uint32_t iCaplet(1); iCaplet < miCaplets; iCaplet++)
		{
			updateRemainingForwards(iCaplet);

			CYieldCurvePtr cYldPtr(createYieldCurve(iCaplet));
			inp_cCurves.add(cYldPtr->GetAsOfDate(), cYldPtr);
		}

		inp_cPathDiscount = *createYieldCurve(0);

		return true;
	}

protected:
	enum :uint32_t { YEARS = 30 };

	const uint64_t	mlRandomSeed;
	const uint32_t	miCaplets;

	CCapPtr			mcCapPtr;
	LmmVolSurface&	mcLmmVolSurface;
	CMatrixRealPtr	mcPeriodicForwardsPtr;

	CRandomNumberGenerator mcRandomGen;

	CVectorRealPtr	mcDriftCum1Ptr;
	CVectorRealPtr	mcFactor1Ptr;
	CVectorRealPtr	mcRandom1Ptr;

	CVectorRealPtr	mcDriftCum2Ptr;
	CVectorRealPtr	mcFactor2Ptr;
	CVectorRealPtr	mcRandom2Ptr;

	CVectorRealPtr	mcDriftPtr;
	CVectorRealPtr	mcVolPtr;

	void buildForwards(const CYieldCurve& inp_cYieldCurve)
	{
		CVectorReal cInitial(inp_cYieldCurve.GetDiscountFactors(mcCapPtr->GetSchedule().front().mdtBop));
		for (uint32_t iCaplet(0); iCaplet < miCaplets; iCaplet++)
		{
			(*mcPeriodicForwardsPtr)[iCaplet] = (cInitial / inp_cYieldCurve.GetDiscountFactors(mcCapPtr->GetSchedule()[iCaplet].mdtEop)) - 1.0;
		}
	}
	
	CYieldCurvePtr createYieldCurve(const uint32_t& inp_iCapletStart) const
	{
		const CDate dtStart(mcCapPtr->GetSchedule()[inp_iCapletStart].mdtBop);

		CYieldCurvePtr cResultPtr(CreateSmart<CYieldCurve>(mcCapPtr->GetResetLst()[inp_iCapletStart], (*mcPeriodicForwardsPtr)[inp_iCapletStart].size()));

		CVectorReal cDf(cResultPtr->GetSims(), 1.0);
		for (uint32_t iCaplet(inp_iCapletStart); iCaplet < miCaplets; iCaplet++)
		{
			cDf /= (*mcPeriodicForwardsPtr)[iCaplet] + 1.0;

			cResultPtr->AddDiscountFactors(dtStart, mcCapPtr->GetSchedule()[iCaplet].mdtEop, cDf);
		}
		return cResultPtr;
	}

	void initialize(const CYieldCurve& inp_cInitialCurve, const size_t& inp_iTotalSims)
	{
		const size_t iSimsPerScenario(inp_iTotalSims / inp_cInitialCurve.GetSims());

		mcPeriodicForwardsPtr = CreateSmart<CMatrixReal>(miCaplets);
		for (uint32_t iCaplet(0); iCaplet < miCaplets; iCaplet++)
		{
			(*mcPeriodicForwardsPtr)[iCaplet].resize(inp_iTotalSims);
		}

		mcDriftCum1Ptr = CreateSmart<CVectorReal>(inp_iTotalSims);
		mcFactor1Ptr = CreateSmart<CVectorReal>(inp_iTotalSims);
		mcRandom1Ptr = CreateSmart<CVectorReal>(iSimsPerScenario);

		mcDriftCum2Ptr = CreateSmart<CVectorReal>(inp_iTotalSims);
		mcFactor2Ptr = CreateSmart<CVectorReal>(inp_iTotalSims);
		mcRandom2Ptr = CreateSmart<CVectorReal>(iSimsPerScenario);

		mcDriftPtr = CreateSmart<CVectorReal>(inp_iTotalSims);
		mcVolPtr = CreateSmart<CVectorReal>(inp_iTotalSims);

		mcRandomGen = CRandomNumberGenerator(mlRandomSeed);

		CDate dtExpiry(mcCapPtr->GetUnderlying()->GetCoupons().front().mdtEop);
		fin::SubtractBusinessDays(dtExpiry, mcCapPtr->GetUnderlying()->GetConfig().miSettleDays, *mcCapPtr->GetUnderlying()->GetConfig().mcHolidaysPtr);
		mcCapPtr->SetExpiry(dtExpiry);
	}

	void updateRandoms(void)
	{
		mcRandomGen.GetNext(*mcRandom1Ptr);
		mcRandomGen.GetNext(*mcRandom2Ptr);
		for (size_t iIndx(0); iIndx < mcRandom1Ptr->size(); iIndx++)
		{
			CMath::BoxMuller((*mcRandom1Ptr)[iIndx], (*mcRandom2Ptr)[iIndx], (*mcRandom1Ptr)[iIndx], (*mcRandom2Ptr)[iIndx]);
		}
	}

	void updateRemainingForwards(const uint32_t& inp_iCapletStart)
	{
		const real dAsOf(mcCapPtr->GetPricingTermsLst()[inp_iCapletStart - 1]->mdExpiry);
		real dFactor1, dFactor2;

		updateRandoms();
		mcFactor1Ptr->repeat(*mcRandom1Ptr);
		mcFactor2Ptr->repeat(*mcRandom2Ptr);

		*mcDriftCum1Ptr = 0.0;
		*mcDriftCum2Ptr = 0.0;

		for (uint32_t iCaplet(inp_iCapletStart); iCaplet < miCaplets; iCaplet++)
		{
			mcLmmVolSurface.GetFactors(dAsOf, mcCapPtr->GetPricingTermsLst()[iCaplet]->mdExpiry, dFactor1, dFactor2);

			real dDelta(mcCapPtr->GetSchedule()[iCaplet].mdCoupon);

			*mcDriftPtr = (*mcPeriodicForwardsPtr)[iCaplet] / ((*mcPeriodicForwardsPtr)[iCaplet] + 1.0);
			*mcDriftCum1Ptr += *mcDriftPtr * dFactor1;
			*mcDriftCum2Ptr += *mcDriftPtr * dFactor2;

			*mcDriftPtr = (*mcDriftCum1Ptr * (dFactor1 * dDelta)) + (*mcDriftCum2Ptr * (dFactor2 * dDelta)) - 0.5 * dDelta * (dFactor1 * dFactor1 + dFactor2 * dFactor2);
			*mcVolPtr = *mcFactor1Ptr * (dFactor1 * sqrt(dDelta)) + *mcFactor2Ptr * (dFactor2 * sqrt(dDelta));

			(*mcPeriodicForwardsPtr)[iCaplet] *= std::exp(*mcDriftPtr + *mcVolPtr);
		}
	}
};
#endif