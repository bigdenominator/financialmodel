#ifndef INCLUDE_H_PIRP
#define INCLUDE_H_PIRP

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateSchematics.h"
#include "Schematics.h"

#include "ConstantRateModel.h"
#include "ForwardRollModel.h"
#include "LiborMarketModel.h"
#include "RateTools.h"

static const string sBadMarketIndexLookup("No market index found");


DEF_NODE(PIrp, SIrp, (SRebonato), (SPriceOasConfig), (SGlobals, SIrpConfig))
{
public:
	bool Initialize(void)
	{
		SGlobalsPtr		sGlobalsPtr;
		SIrpConfigPtr	sConfigPtr;

		if (!View(sGlobalsPtr)) return false;
		if (!View(sConfigPtr)) return false;
		
		return Initialize(sGlobalsPtr, sConfigPtr);
	}

	bool Initialize(SGlobalsPtr inp_sGlobalsPtr, SIrpConfigPtr inp_sConfigPtr)
	{
		CYieldCurvePtr cInitialPtr(CreateSmart<CYieldCurve>(inp_sGlobalsPtr->mdtMarket, 1));
		CYieldCurvePtr cShocksPtr(CreateSmart<CYieldCurve>(inp_sGlobalsPtr->mdtMarket, inp_sGlobalsPtr->miScenarios));

		mcHistoryItr.attach(inp_sConfigPtr->mcHistoryPtr);
		mcCurvesItr.attach(CreateSmart<CYieldCurveLst>());
		mcMasterItr.attach(CreateSmart<CRateMaster>());

		msLiborRatesPtr = CreateSmart<SLiborLsts>();
		mcIndexDataPtr = CreateSmart<CIndexDataTables>();
		mcWorkingPtr = CreateSmart<CVectorReal>(inp_sGlobalsPtr->miScenarios * inp_sGlobalsPtr->miSimsPerScenario);

		RateIndexes::LoadLiborLsts(inp_sConfigPtr->mbAdjConvexity, *inp_sGlobalsPtr->mcCalendarsPtr, *msLiborRatesPtr);
		RateTools::InitializeRates(*inp_sConfigPtr->mcQuotesPtr, *msLiborRatesPtr, *cInitialPtr);
		RateIndexes::LoadMaster(*msLiborRatesPtr, *mcMasterItr.data());

		RateTools::BuildShocks(*inp_sConfigPtr->mcShockLstPtr, *cShocksPtr);
		cInitialPtr->ExpandSimulations(cShocksPtr->GetSims());
		*cInitialPtr += *cShocksPtr;

		CRateModelPtr cModelPtr; SRebonatoPtr sRebonatoPtr;
		switch (inp_sConfigPtr->mlModelId)
		{
		case RATE_MODELS::CONST_RATES:
			cModelPtr = CreateSmart<CConstantRates>();
			break;
		case RATE_MODELS::LMM_REBONATO:
			sRebonatoPtr = CreateSmart<SRebonato>();
			sRebonatoPtr->mcLiborDerivativeLstsPtr = CreateSmart<SLiborDerivativeLsts>();
			sRebonatoPtr->mcQuotesPtr = inp_sConfigPtr->mcQuotesPtr;
			sRebonatoPtr->mcPricingPtr = cInitialPtr;
			sRebonatoPtr->mcRebonatoPtr = CreateSmart<CRebonato>(*inp_sConfigPtr->mcRebonatoParamsPtr);

			RateDerivativeTools::LoadLiborDerivatives(*msLiborRatesPtr, *cInitialPtr, *sRebonatoPtr->mcLiborDerivativeLstsPtr);

			if (!Request(sRebonatoPtr)) return false;
			cModelPtr = CreateSmart< CLiborMarketModel>(inp_sConfigPtr->mlSeed, (*msLiborRatesPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_3M], *sRebonatoPtr->mcRebonatoPtr);
			break;
		default:
			cModelPtr = CreateSmart<CForwardRoll>();
		}
		
		SPriceOasConfigPtr sOasConfigPtr(CreateSmart<SPriceOasConfig>());
		sOasConfigPtr->miSimsPerScenario = inp_sGlobalsPtr->miSimsPerScenario;
		sOasConfigPtr->mcDiscountPtr = CreateSmart<CYieldCurve>(inp_sGlobalsPtr->mdtMarket, inp_sGlobalsPtr->miScenarios * inp_sGlobalsPtr->miSimsPerScenario);

		cModelPtr->Run(*cInitialPtr, *sOasConfigPtr->mcDiscountPtr, *mcCurvesItr.data());

#ifndef UNITTEST
		Send(sOasConfigPtr);
#endif
		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		const MARKET_INDEX MktIndx(inp_sSchematicPtr->mMarketIndex);

		if (!mcMasterItr.find(MktIndx))
		{
			FEEDBACK(GetFeedback(), 5021, sBadMarketIndexLookup + ": MarketIndex(" + to_string(MktIndx) + ")");
			return PROCESS_STATUS::FAIL;
		}
		if (!(bool)(*mcIndexDataPtr)[MktIndx])
		{
			(*mcIndexDataPtr)[MktIndx] = CreateSmart<CDataTable>();
			buildRate(MktIndx);
			addHistory(MktIndx);
		}

		auto itr(iterators::GetItr(inp_sSchematicPtr->mcRatesPtr));
		LOOP(itr)
		{
			*itr.value() = Solvers::InterpolateLinear(*(*mcIndexDataPtr)[MktIndx], itr.key(), false);
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	typedef CSmartArray<CDataTablePtr, MARKET_INDEX_COUNT::value> CIndexDataTables;
	typedef CSmartPtr<CIndexDataTables> CIndexDataTablesPtr;

	typedef const_MapItr<CIndexHistory> HistItr;
	typedef const_MapItr<CYieldCurveLst>	YcItr;
	typedef const_MapItr<CRateMaster>	RateItr;

	HistItr	mcHistoryItr;
	YcItr	mcCurvesItr;
	RateItr	mcMasterItr;

	SLiborLstsPtr		msLiborRatesPtr;
	CIndexDataTablesPtr	mcIndexDataPtr;

	CVectorRealPtr		mcWorkingPtr;

	inline void addHistory(const MARKET_INDEX& inp_MktIndx)
	{
		for (mcHistoryItr.find(inp_MktIndx); !mcHistoryItr.isEnd(); mcHistoryItr++)
		{
			if (mcHistoryItr.key() != inp_MktIndx) break;

			(*mcIndexDataPtr)[inp_MktIndx]->add(mcHistoryItr.value()->mdtAsOf, CreateSmart<CVectorReal>(mcWorkingPtr->size(), mcHistoryItr.value()->mdRate));
		}
	}

	inline void buildRate(const MARKET_INDEX& inp_MktIndx)
	{
		LOOP(mcCurvesItr)
		{
			mcMasterItr.value()->SetSchedule(mcCurvesItr.key());
			mcMasterItr.value()->ImputeAnnualizedRates(*mcCurvesItr.value(), *mcWorkingPtr);

			(*mcIndexDataPtr)[inp_MktIndx]->add(mcCurvesItr.key(), CreateSmart<CVectorReal>(*mcWorkingPtr));
		}
	}
};
#endif
