#ifndef INCLUDE_H_PPRICEOAS
#define INCLUDE_H_PPRICEOAS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateSchematics.h"
#include "Schematics.h"
#include "FinTools.h"
#include <iostream>

static const real dOASDEFAULT = 0.0;


DEF_NODE(PPriceOAS, SPriceOAS, (), (), (SPriceOasConfig))
{
public:
	bool Initialize(void)
	{
		SPriceOasConfigPtr	sConfigPtr;

		if (!View(sConfigPtr)) return false;
		
		return Initialize(sConfigPtr);
	}

	bool Initialize(SPriceOasConfigPtr inp_sConfigPtr)
	{
		mcYearFracPtr = CreateSmart<CYearFrac<DAY_COUNT_CONVENTION::ACT_ACT_ISDA>>();
		mcDiscHlprPtr = CreateUnique<CDiscHlpr>(inp_sConfigPtr->mcDiscountPtr);
		mdtAsOf = inp_sConfigPtr->mcDiscountPtr->GetAsOfDate();
		miSimsPerScenario = inp_sConfigPtr->miSimsPerScenario;

		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		CVectorRealPtr cCpnFracsPtr(calcCouponFracs(*inp_sSchematicPtr->mcNetCfPtr, *mcYearFracPtr, mdtAsOf));
		CMatrixRealPtr cAvgDiscPtr(calcAvgDiscounted(*inp_sSchematicPtr->mcNetCfPtr, *mcDiscHlprPtr, miSimsPerScenario));

		inp_sSchematicPtr->mcOasPtr->resize(cAvgDiscPtr->size());
		inp_sSchematicPtr->mcRiskFreePtr->resize(cAvgDiscPtr->size());
		inp_sSchematicPtr->mcValuesPtr->resize(cAvgDiscPtr->size());

		switch (inp_sSchematicPtr->msBondDefPtr->mOasCalcType)
		{
		case OAS_CALC_TYPE::OAS2PRICE:
			*inp_sSchematicPtr->mcOasPtr = inp_sSchematicPtr->msBondDefPtr->mdOas;
			for (size_t i(0); i < cAvgDiscPtr->size(); i++)
			{
				(*inp_sSchematicPtr->mcRiskFreePtr)[i] = std::sum((*cAvgDiscPtr)[i]);
				(*inp_sSchematicPtr->mcValuesPtr)[i] = calcValue((*cAvgDiscPtr)[i], *cCpnFracsPtr, inp_sSchematicPtr->msBondDefPtr->mdOas);
			}
			break;
		default:
			*inp_sSchematicPtr->mcValuesPtr = inp_sSchematicPtr->msBondDefPtr->mdValue;
			for (size_t i(0); i < cAvgDiscPtr->size(); i++)
			{
				(*inp_sSchematicPtr->mcRiskFreePtr)[i] = std::sum((*cAvgDiscPtr)[i]);
				(*inp_sSchematicPtr->mcOasPtr)[i] = imputeOas((*cAvgDiscPtr)[i], *cCpnFracsPtr, inp_sSchematicPtr->msBondDefPtr->mdValue);
			}
			break;
		}

		return PROCESS_STATUS::DONE;
	}

private:
	class CDiscHlpr
	{
	public:
		typedef CDiscHlpr _Myt;

		CDiscHlpr(CYieldCurvePtr inp_cYcPtr) :mcYcPtr(inp_cYcPtr), mcFactorItr(iterators::GetConstItr(CreateSmart<CDataTable>())) {}

		CVectorRealPtr GetFactors(const CDate& inp_dtEvent)
		{
			if (!mcFactorItr.find(inp_dtEvent))
			{
				CVectorRealPtr cDfPtr(CreateSmart<CVectorReal>(mcYcPtr->GetDiscountFactors(inp_dtEvent)));

				mcFactorItr.data()->add(inp_dtEvent, cDfPtr);

				return cDfPtr;
			}
			else return mcFactorItr.value();
		}

		CVectorReal::size_type Sims(void) const { return mcYcPtr->GetSims(); }

	private:
		CYieldCurvePtr					mcYcPtr;
		const_MapItr<CDataTable>		mcFactorItr;
	};
	typedef CUniquePtr<CDiscHlpr> CDiscHlprPtr;

	CYearFracPtr	mcYearFracPtr;
	CDiscHlprPtr	mcDiscHlprPtr;

	CDate	mdtAsOf;

	size_t	miSimsPerScenario;

	static CVectorRealPtr calcCouponFracs(const CDataTable& inp_cData, const IYearFrac& inp_cYf, const CDate& inp_dtAsOf)
	{
		CVectorRealPtr cResultPtr(CreateSmart<CVectorReal>(inp_cData.size()));

		auto itr(inp_cData.begin());
		for (size_t iDate(0); iDate < inp_cData.size(); iDate++)
		{
			(*cResultPtr)[iDate] = inp_cYf.GetCoupon(inp_dtAsOf, itr->first);

			itr++;
		}

		return cResultPtr;
	}

	static CMatrixRealPtr calcAvgDiscounted(const CDataTable& inp_cData, CDiscHlpr& inp_cDiscHlpr, const size_t& inp_iSimsPerScenario)
	{
		const size_t iPeriods(inp_cData.size());
		const size_t iSims(inp_cData.front().second->size());
		const size_t iScenarios(iSims / inp_iSimsPerScenario);
		const real dScenarioAvgFrac(1.0 / static_cast<real>(inp_iSimsPerScenario));

		CMatrixRealPtr cResultPtr(CreateSmart<CMatrixReal>(iScenarios, CVectorReal(iPeriods, 0.0)));
		CVectorReal cDiscounted(iSims);
		auto itrAvg(iterators::GetItr(cResultPtr));
		auto itrCf(inp_cData.begin());
		
		for(size_t iPeriod(0); iPeriod < iPeriods; iPeriod++, itrCf++)
		{
			cDiscounted = *itrCf->second * *inp_cDiscHlpr.GetFactors(itrCf->first);
			LOOP(itrAvg)
			{
				size_t iSim(0);
				for (size_t iSubSim(0); iSubSim < inp_iSimsPerScenario; iSubSim++)
				{
					(*itrAvg)[iPeriod] += cDiscounted[iSim++];
				}
			}
		}

		LOOP(itrAvg)
		{
			(*itrAvg) *= dScenarioAvgFrac;
		}

		return cResultPtr;
	}

	template<typename T> static auto calcOasRiskFactors(const T& inp_cCpnFracs, real inp_dOas) -> decltype(std::exp(inp_cCpnFracs * (-inp_dOas)))
	{
		return std::exp(inp_cCpnFracs * (-inp_dOas));
	}

	template<typename T> static real calcValue(const T& inp_cData, const CVectorReal& inp_cCpnFracs, const real& inp_dOas)
	{
		return std::sum(inp_cData * calcOasRiskFactors(inp_cCpnFracs, inp_dOas));
	}

	template<typename T> static real imputeOas(const T& inp_cData, const CVectorReal& inp_cCpnFracs, const real& inp_dValue)
	{
		auto foo = [&](real inp_dOas)->real
		{
			return std::sum(std::exp(inp_cCpnFracs * (-inp_dOas)) * inp_cData) - inp_dValue;
		};

		real dOas(0.001);
		return (Solvers::root(foo, 0.0, 0.1, 0.000001, dOas) ? dOas : dOASDEFAULT);
	}
};
#endif