#ifndef INCLUDE_H_PREBONATO
#define INCLUDE_H_PREBONATO

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateSchematics.h"
#include "Schematics.h"

#include "LiborDerivatives.h"
#include "RebonatoVolatility.h"


static const string	sCalibrationError("Rebonato volatility calibration error: Simplex minimization did not converge");

SMARTSTRUCT(SCapHandler,
	(CCapPtr mcCapPtr; real mdVolQuote; CRebonatoPtr mcRebonatoPtr;),
	(mcCapPtr, mdVolQuote, mcRebonatoPtr));
SMARTSTRUCT(SSwaptionHandler,
	(CSwaptionPtr mcSwaptionPtr; real mdVolQuote; CRebonatoPtr mcRebonatoPtr; CVectorRealPtr mcExpiriesPtr; CVectorRealPtr mcModGammaPtr;),
	(mcSwaptionPtr, mdVolQuote, mcRebonatoPtr, mcExpiriesPtr, mcModGammaPtr));

typedef CSmartMap<MARKET_INDEX, SCapHandlerPtr> SCapHandlerLst;
typedef CSmartMap<MARKET_INDEX, SSwaptionHandlerPtr> SSwaptionHandlerLst;

typedef CSmartPtr<SCapHandlerLst> SCapHandlerLstPtr;
typedef CSmartPtr<SSwaptionHandlerLst> SSwaptionHandlerLstPtr;

typedef CGridDims<real, CRebonatoParams::SIZE> RebGrid;
typedef CSmartPtr<RebGrid> RebGridPtr;

SIMPLESTRUCT(SRebonatoPkt,
	(SRebonatoPtr			msSchemaPtr;
	SCapHandlerLstPtr		msCapsPtr;
	SSwaptionHandlerLstPtr	msSwaptionsPtr;
	RebGridPtr				mcGridPtr;
	real					mdMin;
	uint32_t				miIterations;
	CUniquePtr<CRebonatoParams>	mcMinPointPtr;
	CUniquePtr<CRebonatoParams>	mcSimplexStepPtr));

DEF_NODE(PRebonato, SRebonato, (), (), ())
{
public:
	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		SRebonatoPktPtr	msShelfPtr(CreateSmart<SRebonatoPkt>());
		msShelfPtr->msSchemaPtr = inp_sSchematicPtr;
		msShelfPtr->msCapsPtr = CreateSmart<SCapHandlerLst>();
		msShelfPtr->msSwaptionsPtr = CreateSmart<SSwaptionHandlerLst>();
		msShelfPtr->mcGridPtr = CreateSmart<RebGrid>();
		msShelfPtr->mcMinPointPtr = CreateUnique<CRebonatoParams>();
		msShelfPtr->mcSimplexStepPtr = CreateUnique<CRebonatoParams>();

		buildHandlerLsts(*inp_sSchematicPtr, *msShelfPtr->msCapsPtr, *msShelfPtr->msSwaptionsPtr);
		buildMinimizerTools(*inp_sSchematicPtr->mcRebonatoPtr, *msShelfPtr->mcSimplexStepPtr, *msShelfPtr->mcGridPtr);

		auto cObjFn(MakeParameterized(evalParams, std::cref(*msShelfPtr)));
		msShelfPtr->mdMin = Solvers::SearchGrid(cObjFn, *msShelfPtr->mcGridPtr, *msShelfPtr->mcMinPointPtr);

		Log("Grid Search:" + sEOL + summarize(*msShelfPtr));

		if (!Solvers::MinimizeSimplex(cObjFn, miMaxIterations, mdTolerance, *msShelfPtr->mcMinPointPtr, *msShelfPtr->mcSimplexStepPtr, msShelfPtr->miIterations, msShelfPtr->mdMin))
		{
			FEEDBACK(GetFeedback(), 6000, sCalibrationError)
		}
		else
		{
			inp_sSchematicPtr->mcRebonatoPtr->Set(*msShelfPtr->mcMinPointPtr);
		}

		Log("Simplex iterations: " + to_string(msShelfPtr->miIterations) + sEOL + summarize(*msShelfPtr));

		return GetFeedback() ? PROCESS_STATUS::DONE : PROCESS_STATUS::FAIL;
	}

protected:
	const uint32_t miMaxIterations = 100000;
	const real mdTolerance = 0.000001;

	static inline void buildHandlerLsts(const SSchematic& inp_sSchematic, SCapHandlerLst& inp_sCapLst, SSwaptionHandlerLst& inp_sSwaptionLst)
	{
		auto itrQuotes(iterators::GetConstItr(inp_sSchematic.mcQuotesPtr));

		auto itrCaps(iterators::GetConstItr(inp_sSchematic.mcLiborDerivativeLstsPtr->mcCapLstPtr));
		LOOP(itrCaps)
		{
			if (itrQuotes.find(itrCaps.key()))
			{
				SCapHandlerPtr sCapPtr(CreateSmart<SCapHandler>());
				sCapPtr->mcCapPtr = itrCaps.value();
				sCapPtr->mdVolQuote = itrQuotes.value();
				sCapPtr->mcRebonatoPtr = inp_sSchematic.mcRebonatoPtr;

				inp_sCapLst.add(itrCaps.key(), sCapPtr);
			}
		}

		auto itrSwaptions(iterators::GetConstItr(inp_sSchematic.mcLiborDerivativeLstsPtr->mcSwaptionLstPtr));
		LOOP(itrSwaptions)
		{
			if (itrQuotes.find(itrSwaptions.key()))
			{
				SSwaptionHandlerPtr sSwaptionPtr(CreateSmart<SSwaptionHandler>());
				sSwaptionPtr->mcSwaptionPtr = itrSwaptions.value();
				sSwaptionPtr->mdVolQuote = itrQuotes.value();
				sSwaptionPtr->mcRebonatoPtr = inp_sSchematic.mcRebonatoPtr;
				buildSwaptionHandler(*sSwaptionPtr, *inp_sSchematic.mcPricingPtr);

				inp_sSwaptionLst.add(itrSwaptions.key(), sSwaptionPtr);
			}
		}
	}

	static inline void buildMinimizerTools(const CRebonato& inp_cRebonato, CRebonatoParams& inp_cSimplexSteps, RebGrid& inp_cGrid)
	{
		real dMax[] = { 1.0, 0.2, 0.6, 0.5 };
		CRebonatoParams::size_type iSteps[] = { 20, 4, 6, 5 };
		
		for (CRebonatoParams::size_type iParam(0); iParam < CRebonatoParams::SIZE; iParam++)
		{
			if (inp_cRebonato.GetParams()[iParam] > -10.0)
			{
				dMax[iParam] = inp_cRebonato.GetParams()[iParam];
				iSteps[iParam] = 0;
				inp_cSimplexSteps[iParam] = 0.0;
			}
			else
			{
				inp_cSimplexSteps[iParam] = dMax[iParam] / static_cast<real>(iSteps[iParam]);
			}

			inp_cGrid[iParam].resize(iSteps[iParam] + 1);

			Solvers::BuildGridDim(0.0, dMax[iParam], inp_cGrid[iParam]);
		}
	}

	static inline void buildSwaptionHandler(SSwaptionHandler& inp_sHndlr, const CYieldCurve& inp_cPricing)
	{
		CSwaptionPtr cSwaptionPtr(inp_sHndlr.mcSwaptionPtr);
		CLiborRatePtr cLiborPtr(cSwaptionPtr->GetUnderlying()->GetLibor());
		SCpnLst& sFloats(cSwaptionPtr->GetUnderlying()->GetFloatingLegs());

		const SCpnLst::size_type iCaplets(sFloats.size());
		inp_sHndlr.mcExpiriesPtr = CreateSmart<CVectorReal>(iCaplets);
		inp_sHndlr.mcModGammaPtr = CreateSmart<CVectorReal>(iCaplets);

		inp_sHndlr.mcModGammaPtr->repeatElements(gamma(cSwaptionPtr->GetUnderlying()->GetCoupons(), inp_cPricing));

		CVectorReal cPeriodic(inp_cPricing.GetSims());
		for (SCpnLst::size_type iCaplet(0); iCaplet < iCaplets; iCaplet++)
		{
			CDate dtExpiry(sFloats[iCaplet].mdtBop);
			fin::SubtractBusinessDays(dtExpiry, cLiborPtr->GetConfig().miSettleDays, *cLiborPtr->GetConfig().mcHolidaysPtr);
			(*inp_sHndlr.mcExpiriesPtr)[iCaplet] = fin::ActualActualISDA(inp_cPricing.GetAsOfDate(), dtExpiry);

			cLiborPtr->SetSchedule(dtExpiry);
			cLiborPtr->ImputePeriodicRates(inp_cPricing, cPeriodic);

			(*inp_sHndlr.mcModGammaPtr)[iCaplet] *= cPeriodic[0] / (1.0 + cPeriodic[0]);
		}
	}

	static inline real calcModelVol(SCapHandler& inp_sHndlr)
	{
		const CCapPtr cCapPtr(inp_sHndlr.mcCapPtr);
		const uint32_t iCaplets(cCapPtr->GetCapletCount());
		const real mdStrike(cCapPtr->GetStrike());

		real dPrice(0.0);
		for (uint32_t iCaplet(0); iCaplet < iCaplets; iCaplet++)
		{
			real dExpiry(cCapPtr->GetPricingTermsLst()[iCaplet]->mdExpiry);
			real dBlackVol(sqrt(inp_sHndlr.mcRebonatoPtr->IntegrateVariance(0.0, dExpiry, dExpiry) / dExpiry));
			dPrice += (CCap::PriceFn())(dExpiry, 1.0, (*cCapPtr->GetPricingTermsLst()[iCaplet]->mcImpliedPvPtr)[0], (*cCapPtr->GetPricingTermsLst()[iCaplet]->mcPvFactorsPtr)[0] * mdStrike, dBlackVol);
		}

		return inp_sHndlr.mcCapPtr->ImputeVol(dPrice);
	}

	static inline real calcModelVol(SSwaptionHandler& inp_sHndlr)
	{
		const real dExpiry(inp_sHndlr.mcSwaptionPtr->GetPricingTerms().mdExpiry);
		auto fnEval(MakeParameterized(evalSwaption, std::cref(inp_sHndlr)));

		uint32_t iPts(dExpiry * 12);
		if (iPts % 2 == 1) { iPts++; }

		return sqrt(Solvers::SimpsonsRule(fnEval, 0.0, dExpiry, iPts) / dExpiry);
	}

	static inline real evalParams(const CRebonatoParams& inp_cParams, const SRebonatoPkt& inp_sShelf)
	{
		inp_sShelf.msSchemaPtr->mcRebonatoPtr->Set(inp_cParams);
		real dCum(0.0), dThis;

		auto itrCaps(iterators::GetConstItr(inp_sShelf.msCapsPtr));
		LOOP(itrCaps)
		{
			dThis = calcModelVol(*itrCaps.value()) - itrCaps.value()->mdVolQuote;
			dCum += dThis * dThis;
		}

		auto itrSwaptions(iterators::GetConstItr(inp_sShelf.msSwaptionsPtr));
		LOOP(itrSwaptions)
		{
			dThis = calcModelVol(*itrSwaptions.value()) - itrSwaptions.value()->mdVolQuote;
			dCum += dThis * dThis;
		}

		return dCum;
	}

	static inline real evalSwaption(real inp_dAsOf, const SSwaptionHandler& inp_sHndlr)
	{
		real dF1, dF2, dSum1(0.0), dSum2(0.0);
		for (uint32_t iIndx(0); iIndx < inp_sHndlr.mcModGammaPtr->size(); iIndx++)
		{
			inp_sHndlr.mcRebonatoPtr->GetFactors(inp_dAsOf, (*inp_sHndlr.mcExpiriesPtr)[iIndx], dF1, dF2);
			dSum1 += dF1 * (*inp_sHndlr.mcModGammaPtr)[iIndx];
			dSum2 += dF2 * (*inp_sHndlr.mcModGammaPtr)[iIndx];
		}

		return dSum1 * dSum1 + dSum2 * dSum2;
	}

	static inline CVectorReal gamma(const SCpnLst& inp_sCpnLst, const CYieldCurve& inp_cDiscount)
	{
		const CVectorReal::size_type iPeriods(inp_sCpnLst.size());
		const real dDfStart(inp_cDiscount.GetDiscountFactors(inp_sCpnLst.front().mdtBop)[0]);

		CVectorReal cResult(iPeriods);

		real dSum(0.0), dThis(0.0);
		for (CVectorReal::size_type iPeriod(0); iPeriod < iPeriods; iPeriod++)
		{
			dThis = inp_cDiscount.GetDiscountFactors(inp_sCpnLst[iPeriod].mdtEop)[0];
			cResult[iPeriod] = dSum += inp_sCpnLst[iPeriod].mdCoupon * dThis;
		}
		cResult = dDfStart / (dDfStart - dThis) - cResult / dSum;

		return cResult;
	}

	static inline string summarize(const SRebonatoPkt& inp_sShelf)
	{
		string sResult = "Minimum: " + to_string(inp_sShelf.mdMin);
		sResult += sEOL + "Found with parameters:";
		sResult += sEOL + "Short term adjustment: " + to_string((*inp_sShelf.mcMinPointPtr)[RebonatoParams::SHORTTERMADJ]);
		sResult += sEOL + "Hump factor: " + to_string((*inp_sShelf.mcMinPointPtr)[RebonatoParams::HUMPFACTOR]);
		sResult += sEOL + "Decay rate: " + to_string((*inp_sShelf.mcMinPointPtr)[RebonatoParams::DECAYRATE]);
		sResult += sEOL + "Long term volatility: " + to_string((*inp_sShelf.mcMinPointPtr)[RebonatoParams::LONGTERMVOL]);

		return sResult;
	}
};
#endif