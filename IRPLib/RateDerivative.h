#ifndef INCLUDE_H_RATEDERIVATIVES
#define INCLUDE_H_RATEDERIVATIVES

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "Option.h"
#include "RateIndex.h"

SMARTSTRUCT(SPriceTerms, (real mdExpiry; CVectorRealPtr mcPvFactorsPtr; CVectorRealPtr mcImpliedPvPtr;), (mdExpiry, mcPvFactorsPtr, mcImpliedPvPtr))
typedef CSmartVector<SPriceTermsPtr> SPriceTermsLst;
typedef CSmartPtr<SPriceTermsLst> SPriceTermsLstPtr;

typedef real(*FBlack76)(const real&, const real&, const real&, const real&, const real&);

template<typename TUnderlying, OPTIONTYPE TOptionType, FBlack76 F> class CRateOption : public COption < TUnderlying, TOptionType >
{
public:
	typedef TUnderlying		underlying_type;

	typedef COption<TUnderlying, TOptionType>			_Mybase;
	typedef CRateOption<TUnderlying, TOptionType, F>	_Myt;

	CRateOption(void) :_Mybase(), msTermsPtr(CreateSmart<SPriceTerms>()) 
	{
		msTermsPtr->mcPvFactorsPtr = CreateSmart<CVectorReal>();
		msTermsPtr->mcImpliedPvPtr = CreateSmart<CVectorReal>();
	}

	CRateOption(CSmartPtr<underlying_type> inp_cUnderlyingPtr) :_Myt() 
	{
		this->SetUnderlying(inp_cUnderlyingPtr); 
	}

	inline real CalcAtm(void) const
	{
		return (*msTermsPtr->mcImpliedPvPtr)[0] / (*msTermsPtr->mcPvFactorsPtr)[0];
	}

	inline real CalcPrice(real inp_dBlackVol) const
	{
		return F(msTermsPtr->mdExpiry, 1.0, (*msTermsPtr->mcImpliedPvPtr)[0], (*msTermsPtr->mcPvFactorsPtr)[0] * this->mdStrike, inp_dBlackVol);
	}

	inline SPriceTerms& GetPricingTerms(void) const { return *msTermsPtr; }

	inline real ImputeVol(real inp_dPrice) const
	{
		real dVol;
		auto foo = [&](const real& inp_dBlackVol){return CalcPrice(inp_dBlackVol) - inp_dPrice; };

		return (Solvers::root(foo, 0.01, 0.5, 0.00000001, dVol) ? dVol : 0.0001);
	}

	static FBlack76 PriceFn(void) { return F; }

	inline void SetExpiry(CDate inp_dtExpiry)
	{
		_Mybase::SetExpiry(inp_dtExpiry);
		this->mcUnderlyingPtr->SetSchedule(this->mdtExpiry);
	}

	inline void SetPricingTerms(const CYieldCurve& inp_cDiscount) const
	{
		msTermsPtr->mdExpiry = fin::ActualActualISDA(inp_cDiscount.GetAsOfDate(), this->mdtExpiry);

		this->mcUnderlyingPtr->GetPvFactors(inp_cDiscount, *msTermsPtr->mcPvFactorsPtr);
		this->mcUnderlyingPtr->ImputeAnnualizedRates(inp_cDiscount, *msTermsPtr->mcImpliedPvPtr);
		*msTermsPtr->mcImpliedPvPtr *= *msTermsPtr->mcPvFactorsPtr;
	}

protected:
	SPriceTermsPtr	msTermsPtr;

	DERIVED_SPUD(msTermsPtr)
};

template<typename TUnderlying, OPTIONTYPE TOptionType, FBlack76 F> struct CRateOptionChain : public COption< TUnderlying, TOptionType >
{
public:
	typedef TUnderlying		underlying_type;

	typedef COption<TUnderlying, TOptionType>				_Mybase;
	typedef CRateOptionChain<TUnderlying, TOptionType, F>	_Myt;

	CRateOptionChain(void) :_Myt(CreateSmart<underlying_type>(), 1) {}

	CRateOptionChain(CSmartPtr<underlying_type> inp_sUnderlyingPtr, uint32_t inp_iFixings) :_Mybase(), mcHlpr(inp_iFixings)
	{
		msTermsLstPtr = CreateSmart<SPriceTermsLst>();
		mcResetLstPtr = CreateSmart<CVectorDate>();

		SetUnderlying(inp_sUnderlyingPtr, inp_iFixings);
	}

	inline real CalcAtm(void) const
	{
		real dNum(0.0), dDen(0.0);
		FOREACH((*msTermsLstPtr), itr)
		{
			SPriceTermsPtr sTermsPtr(*itr);
			dNum += (*sTermsPtr->mcImpliedPvPtr)[0];
			dDen += (*sTermsPtr->mcPvFactorsPtr)[0];
		}

		return dNum / dDen;
	}

	inline real CalcPrice(real inp_dBlackVol) const
	{
		real dPrice(0.0);
		FOREACH((*msTermsLstPtr), itr)
		{
			SPriceTermsPtr sTermsPtr(*itr);
			dPrice += F(sTermsPtr->mdExpiry, 1.0, (*sTermsPtr->mcImpliedPvPtr)[0], (*sTermsPtr->mcPvFactorsPtr)[0] * this->mdStrike, inp_dBlackVol);
		}

		return dPrice;
	}

	inline uint32_t	GetCapletCount(void) const { return static_cast<uint32_t>(mcResetLstPtr->size()); }

	inline CVectorDate& GetResetLst(void) const { return *mcResetLstPtr; }

	inline SPriceTermsLst& GetPricingTermsLst(void) const { return *msTermsLstPtr; }

	inline SCpnLst& GetSchedule(void) const { return mcHlpr.GetCoupons(); }

	inline real ImputeVol(real inp_dPrice) const
	{
		real dVol;
		auto foo = [&](const real& inp_dBlackVol){return CalcPrice(inp_dBlackVol) - inp_dPrice; };

		return (Solvers::root(foo, 0.01, 0.5, 0.00000001, dVol) ? dVol : 0.0001);
	}

	static inline FBlack76 PriceFn(void) { return F; }

	inline void SetExpiry(CDate inp_dtExpiry)
	{
		_Mybase::SetExpiry(inp_dtExpiry);
		mcHlpr.SetSchedule(this->mdtExpiry);

		for (SCpnLst::size_type iDate(0); iDate < mcResetLstPtr->size(); iDate++)
		{
			(*mcResetLstPtr)[iDate] = mcHlpr.GetCoupons()[iDate].mdtBop;
			fin::SubtractBusinessDays((*mcResetLstPtr)[iDate], mcHlpr.GetConfig().miSettleDays, *mcHlpr.GetConfig().mcHolidaysPtr);
		}
	}

	inline void SetPricingTerms(const CYieldCurve& inp_cDiscount)
	{
		for (SCpnLst::size_type iDate(0); iDate < mcResetLstPtr->size(); iDate++)
		{
			this->mcUnderlyingPtr->SetSchedule((*mcResetLstPtr)[iDate]);

			SPriceTermsPtr sTermsPtr((*msTermsLstPtr)[iDate]);
			sTermsPtr->mdExpiry = fin::ActualActualISDA(inp_cDiscount.GetAsOfDate(), (*mcResetLstPtr)[iDate]);

			this->mcUnderlyingPtr->GetPvFactors(inp_cDiscount, *sTermsPtr->mcPvFactorsPtr);
			this->mcUnderlyingPtr->ImputeAnnualizedRates(inp_cDiscount, *sTermsPtr->mcImpliedPvPtr);
			*sTermsPtr->mcImpliedPvPtr *= *sTermsPtr->mcPvFactorsPtr;
		}
	}

	inline void SetUnderlying(CSmartPtr<underlying_type> inp_cUnderlyingPtr, uint32_t inp_iFixings) 
	{
		_Mybase::SetUnderlying(inp_cUnderlyingPtr);

		SRateConfig cfg(this->mcUnderlyingPtr->GetConfig());
		cfg.msPeriod = cfg.msTenor;
		cfg.msTenor.miUnitCount *= inp_iFixings;
		
		mcHlpr = CRateInstrument(inp_iFixings);
		mcHlpr.Config(cfg);

		msTermsLstPtr->resize(inp_iFixings);
		mcResetLstPtr->resize(inp_iFixings);

		for (size_t i(0); i < msTermsLstPtr->size(); i++)
		{
			if (!(*msTermsLstPtr)[i])
			{
				(*msTermsLstPtr)[i] = CreateSmart<SPriceTerms>();
				(*msTermsLstPtr)[i]->mcPvFactorsPtr = CreateSmart<CVectorReal>();
				(*msTermsLstPtr)[i]->mcImpliedPvPtr = CreateSmart<CVectorReal>();
			}
		}
	}

protected:
	CRateInstrument		mcHlpr;
	CVectorDatePtr		mcResetLstPtr;
	SPriceTermsLstPtr	msTermsLstPtr;

	DERIVED_SPUD(mcHlpr, mcResetLstPtr, msTermsLstPtr)
};

namespace RateDerivativeTools {};
#endif