#ifndef INCLUDE_H_RATEINDEX
#define INCLUDE_H_RATEINDEX

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "FinTools.h"
#include "HolidayCalendar.h"
#include "YieldCurve.h"

SMARTSTRUCT(SRateConfig, 
	(BUSINESS_DAY_CONVENTION mBusDayConv; DATE_GENERATION mDateGen; DAY_COUNT_CONVENTION mDayCountConv; uint32_t miSettleDays; SPeriod msPeriod; SPeriod msTenor; CHolidayCalendarPtr mcHolidaysPtr),
	(mBusDayConv, mDateGen, mDayCountConv, miSettleDays, msPeriod, msTenor, mcHolidaysPtr))

class CRateInstrument : public CContainer<FRM_TYPELIST::NONSCHEMATIC>
{
public:
	typedef CRateInstrument	_Myt;

	CRateInstrument(uint32_t inp_iCoupons) :msConfigPtr(CreateSmart<SRateConfig>()), msCouponsPtr(CreateSmart<SCpnLst>(inp_iCoupons)) { init(); }

	virtual inline void CalcDates(CDate inp_dtTrade, CDate& inp_dtSettlement, CDate& inp_dtMaturity) const
	{
		inp_dtSettlement = mcDateAdjPtr->AddBusinessDays(inp_dtTrade, msConfigPtr->miSettleDays);
		inp_dtMaturity = mcDateAdjPtr->Adjust(inp_dtSettlement + msConfigPtr->msTenor);
	}

	inline void Config(const SRateConfig& inp_sConfig) { *msConfigPtr = inp_sConfig; init(); }

	inline SRateConfig& GetConfig(void) const { return *msConfigPtr; }

	inline CDateAdjPtr GetDateAdj(void) const { return mcDateAdjPtr; }

	inline SCpnLst& GetCoupons(void) const { return *msCouponsPtr; }

	inline CYearFracPtr GetYearFrac(void) const { return mcYearFracPtr; }

	inline void SetSchedule(CDate inp_dtTrade) const
	{
		CDate dtSettlement, dtMaturity;
		CalcDates(inp_dtTrade, dtSettlement, dtMaturity);
		setCoupons(dtSettlement, dtMaturity);
	}

protected:
	SRateConfigPtr	msConfigPtr;

	CDateAdjPtr		mcDateAdjPtr;
	CYearFracPtr	mcYearFracPtr;

	SCpnLstPtr		msCouponsPtr;

	void init(void)
	{
		mcDateAdjPtr = CFinTools::CreateDateAdjustor(msConfigPtr->mBusDayConv, msConfigPtr->mcHolidaysPtr);
		mcYearFracPtr = CFinTools::CreateYearFrac(msConfigPtr->mDayCountConv, msConfigPtr->msPeriod);
	}

	virtual void setCoupons(CDate inp_dtSettlement, CDate inp_dtMaturity) const
	{
		CFinTools::CreateCouponScheduler(msConfigPtr->mDateGen, mcDateAdjPtr, mcYearFracPtr, inp_dtSettlement, inp_dtSettlement, inp_dtMaturity, msConfigPtr->msPeriod, msCouponsPtr);
	}

	virtual bool deserialize(SSerial& inp_sSerial)
	{
		if (!SPUD::deserializeTools(inp_sSerial, msConfigPtr, msCouponsPtr)) return false;
		init();

		return true;
	}
	virtual void pack(SPack& inp_sPack) const { SPUD::packTools(inp_sPack, msConfigPtr, msCouponsPtr); }
	virtual SPack::size_type packSize(void) const { return SPUD::packSizeTools(msConfigPtr, msCouponsPtr); }
	virtual void serialize(SSerial& inp_sSerial) const { SPUD::serializeTools(inp_sSerial, msConfigPtr, msCouponsPtr); }
	virtual bool unpack(SPack& inp_sPack)
	{
		if (!SPUD::unpackTools(inp_sPack, msConfigPtr, msCouponsPtr)) return false;
		init();

		return true;
	}
};

class IRateIndex : public CRateInstrument
{
public:
	typedef CRateInstrument	_Mybase;
	typedef IRateIndex		_Myt;

	virtual void Bootstrap(real inp_dQuoteRate, CYieldCurve& inp_cDiscount) const = 0;

	virtual void Calibrate(real inp_dQuoteRate, const CYieldCurve& inp_cDiscount) = 0;

	virtual void GetPvFactors(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cFactors) const = 0;

	virtual void ImputeAnnualizedRates(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cResult) const = 0;

	virtual void ImputePeriodicRates(const CYieldCurve& inp_cDiscount, CVectorReal& inp_cResult) const = 0;

protected:
	IRateIndex(uint32_t inp_iCoupons) :_Mybase(inp_iCoupons) {}
};

typedef CSimpleMap<MARKET_INDEX, IRateIndex*> CRateMaster;
typedef CSmartPtr<CRateMaster> CRateMasterPtr;
#endif
