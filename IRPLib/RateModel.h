#ifndef INCLUDE_H_RATEMODEL
#define INCLUDE_H_RATEMODEL

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateTools.h"

class IRateModel
{
public:
	virtual bool Run(const CYieldCurve& inp_cInitialCurve, CYieldCurve& inp_cPathDiscount, CYieldCurveLst& inp_cCurves) = 0;
};
typedef CSmartPtr<IRateModel> CRateModelPtr;
#endif