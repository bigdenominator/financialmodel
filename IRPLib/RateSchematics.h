#ifndef INCLUDE_H_RATESCHEMATICS
#define INCLUDE_H_RATESCHEMATICS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RebonatoVolatility.h"

enum IRP_TYPELIST :TYPELIST_t {
	SIRPCONFIG = 2001,
	SPRICEOASCONFIG,
	SREBONATO
};

SCHEMATIC(SIrpConfig, IRP_TYPELIST::SIRPCONFIG, (RATE_MODELS mlModelId; uint64_t mlSeed;  bool mbAdjConvexity; CIndexQuoteLstPtr mcQuotesPtr; CRateShockLstPtr mcShockLstPtr; CIndexHistoryPtr mcHistoryPtr; CRebonatoParamsPtr mcRebonatoParamsPtr;), (mlModelId, mlSeed, mbAdjConvexity, mcQuotesPtr, mcShockLstPtr, mcHistoryPtr, mcRebonatoParamsPtr))
SCHEMATIC(SPriceOasConfig, IRP_TYPELIST::SPRICEOASCONFIG, (size_t miSimsPerScenario; CYieldCurvePtr mcDiscountPtr;), (miSimsPerScenario, mcDiscountPtr))
SCHEMATIC(SRebonato, IRP_TYPELIST::SREBONATO, (SLiborDerivativeLstsPtr mcLiborDerivativeLstsPtr; CIndexQuoteLstPtr mcQuotesPtr; CYieldCurvePtr mcPricingPtr;  CRebonatoPtr mcRebonatoPtr), (mcLiborDerivativeLstsPtr, mcQuotesPtr, mcPricingPtr, mcRebonatoPtr))
#endif