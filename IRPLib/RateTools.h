#ifndef INCLUDE_H_RATETOOLS
#define INCLUDE_H_RATETOOLS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "LiborIndexes.h"

typedef CSmartMap<CDate, CYieldCurvePtr> CYieldCurveLst;
typedef CSmartPtr<CYieldCurveLst> CYieldCurveLstPtr;

typedef CSmartMap<MARKET_INDEX, real> CIndexQuoteLst;
typedef CSmartPtr<CIndexQuoteLst> CIndexQuoteLstPtr;

SMARTSTRUCT(SIndexHistoryEntry, (CDate mdtAsOf; real mdRate;), (mdtAsOf, mdRate))
typedef CSmartMultiMap<MARKET_INDEX, SIndexHistoryEntryPtr> CIndexHistory;
typedef CSmartPtr<CIndexHistory> CIndexHistoryPtr;

SMARTSTRUCT(SRateShockPoint, (PERIODS mUnitType; uint32_t miUnitCount; real mdShock), (mUnitType, miUnitCount, mdShock))
typedef CSmartVector<SRateShockPointPtr>	CRateShock;
typedef CSmartPtr<CRateShock> CRateShockPtr;

typedef CSmartMap<uint32_t, CRateShockPtr> CRateShockLst;
typedef CSmartPtr<CRateShockLst> CRateShockLstPtr;


namespace RateTools
{
	template<typename T> static inline void Bootstrap(MARKET_INDEX inp_MarketIndex, const CIndexQuoteLst& inp_cQuoteLst, const T& inp_cRateLst, CYieldCurve& inp_cBootstrap)
	{
		if ((inp_cQuoteLst.find(inp_MarketIndex) != inp_cQuoteLst.cend()) && (inp_cRateLst.find(inp_MarketIndex) != inp_cRateLst.cend()))
		{
			inp_cRateLst.at(inp_MarketIndex)->Bootstrap(inp_cQuoteLst.at(inp_MarketIndex), inp_cBootstrap);
		}
	}

	template<typename T> static inline void Calibrate(MARKET_INDEX inp_MarketIndex, const CIndexQuoteLst& inp_cQuoteLst, const T& inp_cRateLst, const CYieldCurve& inp_cBootstrap)
	{
		if ((inp_cQuoteLst.find(inp_MarketIndex) != inp_cQuoteLst.cend()) && (inp_cRateLst.find(inp_MarketIndex) != inp_cRateLst.cend()))
		{
			inp_cRateLst.at(inp_MarketIndex)->Calibrate(inp_cQuoteLst.at(inp_MarketIndex), inp_cBootstrap);
		}
	}

	static inline void InitializeRates(const CIndexQuoteLst& inp_cQuoteLst, const SLiborLsts& inp_sLiborLsts, CYieldCurve& inp_cBootstrap)
	{
		Bootstrap(MARKET_INDEX::LIBOR_ON, inp_cQuoteLst, *inp_sLiborLsts.mcLiborLstPtr, inp_cBootstrap);
		Bootstrap(MARKET_INDEX::LIBOR_3M, inp_cQuoteLst, *inp_sLiborLsts.mcLiborLstPtr, inp_cBootstrap);

		const uint32_t iEdfStart(static_cast<uint32_t>(MARKET_INDEX::EDF1));
		const uint32_t iEdfEnd(static_cast<uint32_t>(MARKET_INDEX::EDF40));
		for (uint32_t iIndx(iEdfStart); iIndx <= iEdfEnd; iIndx++)
		{
			Bootstrap(static_cast<MARKET_INDEX>(iIndx), inp_cQuoteLst, *inp_sLiborLsts.mcEdfLstPtr, inp_cBootstrap);
		}

		const uint32_t iSwapStart(static_cast<uint32_t>(MARKET_INDEX::SWAP_1Y));
		const uint32_t iSwapEnd(static_cast<uint32_t>(MARKET_INDEX::SWAP_30Y));
		for (uint32_t iIndx(iSwapStart); iIndx <= iSwapEnd; iIndx++)
		{
			Bootstrap(static_cast<MARKET_INDEX>(iIndx), inp_cQuoteLst, *inp_sLiborLsts.mcSwapLstPtr, inp_cBootstrap);
		}
		
		Calibrate(MARKET_INDEX::LIBOR_1M, inp_cQuoteLst, *inp_sLiborLsts.mcLiborLstPtr, inp_cBootstrap);
		Calibrate(MARKET_INDEX::LIBOR_6M, inp_cQuoteLst, *inp_sLiborLsts.mcLiborLstPtr, inp_cBootstrap);
		Calibrate(MARKET_INDEX::LIBOR_9M, inp_cQuoteLst, *inp_sLiborLsts.mcLiborLstPtr, inp_cBootstrap);
		Calibrate(MARKET_INDEX::LIBOR_12M, inp_cQuoteLst, *inp_sLiborLsts.mcLiborLstPtr, inp_cBootstrap);
	}

	static inline bool BuildShock(size_t inp_iScenarioNum, const CRateShock& inp_cShockDef, CYieldCurve& inp_cShock)
	{
		const CDate	dtAsOf(inp_cShock.GetAsOfDate());

		CVectorReal	cTemp(inp_cShock.GetSims(), 0.0);
		FOREACH(inp_cShockDef, itr)
		{
			cTemp[inp_iScenarioNum] = (*itr)->mdShock;
			inp_cShock.AddZeroRates(dtAsOf.Add((*itr)->mUnitType, (*itr)->miUnitCount), cTemp);
			cTemp[inp_iScenarioNum] = 0.0;
		}

		return true;
	}

	static inline bool BuildShocks(const CRateShockLst& inp_cShockDefLst, CYieldCurve& inp_cShock)
	{
		FOREACH(inp_cShockDefLst, itr)
		{
			if (itr->first <= inp_cShock.GetSims())
			{
				if (!BuildShock(itr->first, *itr->second, inp_cShock)) return false;
			}
		}

		return true;
	}
}
#endif
