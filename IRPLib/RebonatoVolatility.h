#ifndef INCLUDE_H_REBONATOVOLATILITY
#define INCLUDE_H_REBONATOVOLATILITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "LiborMarketModel.h"

typedef CArrayReal<4> CRebonatoParams;
typedef CSmartPtr<CRebonatoParams> CRebonatoParamsPtr;

enum RebonatoParams :size_t { SHORTTERMADJ, HUMPFACTOR, DECAYRATE, LONGTERMVOL };

class CRebonato : public LmmVolSurface, public CContainer<FRM_TYPELIST::NONSCHEMATIC>
{
public:
	typedef LmmVolSurface	_Mybase;
	typedef CRebonato		_Myt;

	CRebonato(void) :_Mybase(), mcParamsPtr(CreateSmart<CRebonatoParams>())
	{
		mdLongTermCorr = 0.3;
		mdCorrDecay = 0.4;
		mdOneLessLongTermCorr = 1.0 - mdLongTermCorr;
	}

	CRebonato(const CRebonatoParams& inp_cParams) :_Myt() { Set(inp_cParams); }

	void GetFactors(const real& inp_dAsOf, const real& inp_dExpiry, real& inp_dFactor1, real& inp_dFactor2) const
	{
		real dVol(calcInstantaneousVol(inp_dExpiry - inp_dAsOf) * d_SQRT2);
		real dCorr(calcCorrelation(inp_dAsOf, inp_dExpiry));
		real dAdj(sqrt(1.0 - dCorr * dCorr));

		inp_dFactor1 = dVol * (dCorr + dAdj);
		inp_dFactor2 = dVol * (dCorr - dAdj);
	}

	CRebonatoParams& GetParams(void) const { return *mcParamsPtr; }

	real IntegrateCovariance(const real& inp_dFrom, const real& inp_dTo, const real& inp_dExpiryRate1, const real& inp_dExpiryRate2) const
	{
		return indefiniteIntegralCovariance(inp_dTo, inp_dExpiryRate1, inp_dExpiryRate2) - indefiniteIntegralCovariance(inp_dFrom, inp_dExpiryRate1, inp_dExpiryRate2);
	}

	real IntegrateVariance(const real& inp_dFrom, const real& inp_dTo, const real& inp_dExpiryRate) const
	{
		return indefiniteIntegralVariance(inp_dTo, inp_dExpiryRate) - indefiniteIntegralVariance(inp_dFrom, inp_dExpiryRate);
	}

	void Set(const real& inp_dShortTermAdj, const real& inp_dHumpFactor, const real& inp_dDecayRate, const real& inp_dLongTermVol)
	{
		(*mcParamsPtr)[RebonatoParams::SHORTTERMADJ] = inp_dShortTermAdj;
		(*mcParamsPtr)[RebonatoParams::HUMPFACTOR] = inp_dHumpFactor;
		(*mcParamsPtr)[RebonatoParams::DECAYRATE] = inp_dDecayRate;
		(*mcParamsPtr)[RebonatoParams::LONGTERMVOL] = inp_dLongTermVol;

		updateParams();
	}

	void Set(const CRebonatoParams& inp_cParams)
	{
		*mcParamsPtr = inp_cParams;

		updateParams();
	}

protected:
	CRebonatoParamsPtr	mcParamsPtr;

	real	mdLongTermCorr;
	real	mdCorrDecay;

	real	mdOneLessLongTermCorr;

	real	mdSub1;
	real	mdSub2;
	real	mdSub3;

	real	mdCof1;
	real	mdCof2;
	real	mdCof3;

	real calcCorrelation(const real& inp_dExpiryRate1, const real& inp_dExpiryRate2) const
	{
		return mdLongTermCorr + mdOneLessLongTermCorr * exp(-mdCorrDecay * abs(inp_dExpiryRate1 - inp_dExpiryRate2));
	}

	real calcInstantaneousVol(const real& inp_dExpiry) const
	{
		return (*mcParamsPtr)[RebonatoParams::LONGTERMVOL] + ((*mcParamsPtr)[RebonatoParams::SHORTTERMADJ] + (*mcParamsPtr)[RebonatoParams::HUMPFACTOR] * inp_dExpiry) * exp(-(*mcParamsPtr)[RebonatoParams::DECAYRATE] * inp_dExpiry);
	}

	real indefiniteIntegralCovariance(const real& inp_dAsOf, const real& inp_dExpiryRate1, const real& inp_dExpiryRate2) const
	{
		const real dAlpha1(-(*mcParamsPtr)[RebonatoParams::DECAYRATE] * (inp_dExpiryRate1 - inp_dAsOf));
		const real dAlpha2(-(*mcParamsPtr)[RebonatoParams::DECAYRATE] * (inp_dExpiryRate2 - inp_dAsOf));

		const real dBeta1(exp(dAlpha1));
		const real dBeta2(exp(dAlpha2));

		real dReturn(mdSub1);
		dReturn += mdSub2 * (1 - dAlpha1 - dAlpha2);
		dReturn += mdSub3 * (1 + 2 * dAlpha1 * dAlpha2 - dAlpha1 - dAlpha2);
		dReturn *= dBeta1 * dBeta2;
		dReturn += mdCof1 * inp_dAsOf;
		dReturn += mdCof2 * (dBeta1 + dBeta2);
		dReturn += mdCof3 * (dBeta1 * (1 - dAlpha1) + dBeta2 * (1 - dAlpha2));

		return dReturn;
	}

	real indefiniteIntegralVariance(const real& inp_dAsOf, const real& inp_dExpiryRate) const
	{
		const real dAlpha(-(*mcParamsPtr)[RebonatoParams::DECAYRATE] * (inp_dExpiryRate - inp_dAsOf));
		const real dAlphaX2(2 * dAlpha);
		const real dBeta(exp(dAlpha));

		real dReturn(mdSub1);
		dReturn += mdSub2 * (1 - dAlphaX2);
		dReturn += mdSub3 * (1 + 2 * dAlpha * dAlpha - dAlphaX2);
		dReturn *= dBeta * dBeta;
		dReturn += mdCof1 * inp_dAsOf;
		dReturn += mdCof2 * 2 * dBeta;
		dReturn += mdCof3 * 2 * dBeta * (1 - dAlpha);

		return dReturn;
	}

	void updateParams(void)
	{
		real dDecayInv1, dDecayInv2, dDecayInv3;

		if ((*mcParamsPtr)[RebonatoParams::DECAYRATE] != 0.0)
		{
			dDecayInv1 = 1.0 / (*mcParamsPtr)[RebonatoParams::DECAYRATE];
			dDecayInv2 = dDecayInv1 / (*mcParamsPtr)[RebonatoParams::DECAYRATE];
			dDecayInv3 = dDecayInv2 / (*mcParamsPtr)[RebonatoParams::DECAYRATE];
		}
		else dDecayInv1 = dDecayInv2 = dDecayInv3 = 0.0;

		mdSub1 = 0.5 * (*mcParamsPtr)[RebonatoParams::SHORTTERMADJ] * (*mcParamsPtr)[RebonatoParams::SHORTTERMADJ] * dDecayInv1;
		mdSub2 = 0.5 * (*mcParamsPtr)[RebonatoParams::SHORTTERMADJ] * (*mcParamsPtr)[RebonatoParams::HUMPFACTOR] * dDecayInv2;
		mdSub3 = 0.25 * (*mcParamsPtr)[RebonatoParams::HUMPFACTOR] * (*mcParamsPtr)[RebonatoParams::HUMPFACTOR] * dDecayInv3;

		mdCof1 = (*mcParamsPtr)[RebonatoParams::LONGTERMVOL] * (*mcParamsPtr)[RebonatoParams::LONGTERMVOL];
		mdCof2 = (*mcParamsPtr)[RebonatoParams::SHORTTERMADJ] * (*mcParamsPtr)[RebonatoParams::LONGTERMVOL] * dDecayInv1;
		mdCof3 = (*mcParamsPtr)[RebonatoParams::HUMPFACTOR] * (*mcParamsPtr)[RebonatoParams::LONGTERMVOL] * dDecayInv2;
	}

	SPUD(mcParamsPtr, mdLongTermCorr, mdCorrDecay, mdOneLessLongTermCorr, mdSub1, mdSub2, mdSub3, mdCof1, mdCof2, mdCof3)
};
typedef CSmartPtr<CRebonato> CRebonatoPtr;
#endif