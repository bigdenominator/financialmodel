#ifndef INCLUDE_H_YIELDCURVE
#define INCLUDE_H_YIELDCURVE

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "FinTools.h"

enum class YIELDCURVETYPE { DISCOUNT_CURVE, ZERO_CURVE };

class CYieldCurve : public CDataTable
{
public:
	typedef CDataTable	_Mybase;
	typedef CYieldCurve	_Myt;

	CYieldCurve(void) :_Mybase() {}

	CYieldCurve(const CDate& inp_dtAsOf, const size_t& inp_iSims) :_Mybase(), mdtAsOf(inp_dtAsOf), miSize(inp_iSims) {}

	CYieldCurve(const _Myt& rhs) :_Mybase(rhs), mdtAsOf(rhs.mdtAsOf), miSize(rhs.miSize) {}

	CYieldCurve(_Myt&& rhs) :_Mybase(std::forward<_Mybase>(rhs)), mdtAsOf(rhs.mdtAsOf), miSize(rhs.miSize) {}

	_Myt& operator =(const _Myt& rhs)
	{
		_Mybase::operator=(rhs);
		mdtAsOf = rhs.mdtAsOf;
		miSize = rhs.miSize;

		return *this; 
	}

	_Myt& operator +=(const _Myt& rhs)
	{
		if (!rhs.empty())
		{
			CUniquePtr<CVectorReal> cTempPtr(CreateUnique<CVectorReal>(miSize));
			FOREACH((*this), itr)
			{
				rhs.GetZeroRates(itr->first, *cTempPtr);
				*itr->second += *cTempPtr;
			}
		}
		return *this;
	}

	_Myt& operator -=(const _Myt& rhs)
	{
		if (!rhs.empty())
		{
			CUniquePtr<CVectorReal> cTempPtr(CreateUnique<CVectorReal>(miSize));
			FOREACH((*this), itr)
			{
				rhs.GetZeroRates(itr->first, *cTempPtr);
				*itr->second -= *cTempPtr;
			}
		}
		return *this;
	}

	bool AddDiscountFactor(const CDate& inp_dtFrom, const CDate& inp_dtTo, real inp_dDiscountFactor)
	{
		const real dNewZero(discountFactorToRate(inp_dtFrom, inp_dtTo, inp_dDiscountFactor));
		
		if (inp_dtFrom == mdtAsOf)
		{
			return AddZeroRate(inp_dtTo, dNewZero);
		}
		else if (empty())
		{
			return AddZeroRate(inp_dtFrom, dNewZero) && AddZeroRate(inp_dtTo, dNewZero);
		}
		else
		{
			CVectorReal dZeros(GetZeroRates(inp_dtFrom));
			AddZeroRates(inp_dtFrom, dZeros);

			const real d_End(1.0 / fin::ActualActualISDA(mdtAsOf, inp_dtTo));
			const real dRatio(fin::ActualActualISDA(mdtAsOf, inp_dtFrom) * d_End);
			
			return AddZeroRates(inp_dtTo, dRatio * dZeros - CMath::ln(inp_dDiscountFactor) * d_End);
		}
	}

	bool AddDiscountFactors(const CDate& inp_dtFrom, const CDate& inp_dtTo, const CVectorReal& inp_cDiscountFactors)
	{
		if (inp_dtFrom == mdtAsOf)
		{
			return AddZeroRates(inp_dtTo, discountFactorToRate(mdtAsOf, inp_dtTo, inp_cDiscountFactors));
		}
		else if (empty())
		{
			AddZeroRates(inp_dtFrom, discountFactorToRate(inp_dtFrom, inp_dtTo, inp_cDiscountFactors));
			return AddZeroRates(inp_dtTo, discountFactorToRate(inp_dtFrom, inp_dtTo, inp_cDiscountFactors));
		}
		else
		{
			CVectorReal dZeros(GetZeroRates(inp_dtFrom));
			AddZeroRates(inp_dtFrom, dZeros);

			const real d_End(1.0 / fin::ActualActualISDA(mdtAsOf, inp_dtTo));
			const real dStart(fin::ActualActualISDA(mdtAsOf, inp_dtFrom));

			return AddZeroRates(inp_dtTo, (dStart * dZeros - std::ln(inp_cDiscountFactors)) * d_End);
		}
	}

	_Myt& AddSpread(const _Myt& inp_cSpread, bool inp_bAbsoluteTime = false)
	{
		if (inp_bAbsoluteTime)
		{
			*this += inp_cSpread;
		}
		else
		{
			const int32_t lOffset(inp_cSpread.GetAsOfDate() - mdtAsOf);
			
			CUniquePtr<CVectorReal> cTempPtr(CreateUnique<CVectorReal>(miSize));
			FOREACH((*this), itr)
			{
				inp_cSpread.GetZeroRates(itr->first + lOffset, *cTempPtr);
				*itr->second += *cTempPtr;
			}
		}
		return *this;
	}

	bool AddZeroRate(const CDate& inp_dtRate, real inp_dRateDecimal)
	{
		if (inp_dtRate > mdtAsOf)
		{
			add(inp_dtRate, CreateSmart<CVectorReal>(miSize, inp_dRateDecimal));
			return true;
		}
		return false;
	}

	template<typename T> bool AddZeroRates(const CDate& inp_dtRate, const T& inp_cRateDecimal)
	{
		if (inp_dtRate > mdtAsOf && inp_cRateDecimal.size() == miSize)
		{
			add(inp_dtRate, CreateSmart<CVectorReal>(inp_cRateDecimal));
			return true;
		}
		return false;
	}

	CDataTablePtr AsDiscountRates(void) const
	{
		CDataTablePtr cDataPtr(CreateSmart<CDataTable>());
		for (auto itr(this->begin()); itr != this->cend(); itr++)
		{
			cDataPtr->add(itr->first, CreateSmart<CVectorReal>(rateToDiscountFactor(mdtAsOf, itr->first, *itr->second)));
		}
		return cDataPtr;
	}

	CDataTablePtr AsZeroRates(void) const
	{
		CDataTablePtr cDataPtr(CreateSmart<CDataTable>());
		for (auto itr(this->begin()); itr != this->cend(); itr++)
		{
			cDataPtr->add(itr->first, CreateSmart<CVectorReal>(*itr->second));
		}
		return cDataPtr;
	}

	_Myt& ExpandSimulations(const size_t& inp_iExpansionFactor)
	{
		miSize *= inp_iExpansionFactor;

		for (auto itr(begin()); itr != cend(); itr++)
		{
			CVectorRealPtr cNewPtr(CreateSmart<CVectorReal>(miSize));
			cNewPtr->repeatElements(*itr->second);

			itr->second = cNewPtr;
		}
		return *this;
	}

	CDate GetAsOfDate(void) const { return(mdtAsOf); }

	CVectorReal GetDiscountFactors(const CDate& inp_dtAsOf) const
	{
		CVectorReal tmp(GetZeroRates(inp_dtAsOf));
		tmp = rateToDiscountFactor(mdtAsOf, inp_dtAsOf, tmp);

		return tmp;
	}

	void GetDiscountFactors(const CDate& inp_dtAsOf, CVectorReal& inp_cTarget) const
	{
		inp_cTarget = rateToDiscountFactor(mdtAsOf, inp_dtAsOf, GetZeroRates(inp_dtAsOf));
	}

	size_t GetSims(void) const { return miSize; }

	auto GetZeroRates(const CDate& inp_dtMaturity) const -> decltype(Solvers::InterpolateLinear(*this, inp_dtMaturity, false))
	{
		return Solvers::InterpolateLinear(*this, inp_dtMaturity, false);
	}

	void GetZeroRates(const CDate& inp_dtMaturity, CVectorReal& inp_cTarget) const
	{
		inp_cTarget = GetZeroRates(inp_dtMaturity);
	}

	_Myt& SubtractSpread(const _Myt& inp_cSpread, bool inp_bAbsoluteTime = false)
	{
		if (inp_bAbsoluteTime)
		{
			*this -= inp_cSpread;
		}
		else
		{
			const int32_t lOffset(inp_cSpread.GetAsOfDate() - mdtAsOf);

			CUniquePtr<CVectorReal> cTempPtr(CreateUnique<CVectorReal>(miSize));
			FOREACH((*this), itr)
			{
				inp_cSpread.GetZeroRates(itr->first + lOffset, *cTempPtr);
				*itr->second -= *cTempPtr;
			}
		}
		return *this;
	}

protected:
	DERIVED_SPUD(mdtAsOf, miSize)

private:
	typedef CRateCmpd<COMPOUNDTYPE::CONTINUOUS> cmpd_t;

	CDate		mdtAsOf;
	size_t		miSize;

	template<typename T> static inline auto discountFactorToRate(const CDate& inp_dtStart, const CDate& inp_dtEnd, const T& inp_dDiscountFactor) -> decltype(cmpd_t::ImplyPeriodicRateFromDiscount(inp_dDiscountFactor, fin::ActualActualISDA(inp_dtStart, inp_dtEnd)))
	{
		return cmpd_t::ImplyPeriodicRateFromDiscount(inp_dDiscountFactor, fin::ActualActualISDA(inp_dtStart, inp_dtEnd));
	}

	template<typename T> static inline auto rateToDiscountFactor(const CDate& inp_dtStart, const CDate& inp_dtEnd, const T& inp_dRateDecimal) -> decltype(cmpd_t::CalcDiscountFactor(inp_dRateDecimal, fin::ActualActualISDA(inp_dtStart, inp_dtEnd)))
	{
		return cmpd_t::CalcDiscountFactor(inp_dRateDecimal, fin::ActualActualISDA(inp_dtStart, inp_dtEnd));
	}

	inline void	add(CDate inp_lIndex, CVectorRealPtr inp_TData) { _Mybase::add(inp_lIndex, inp_TData); }
};
typedef CSmartPtr<CYieldCurve> CYieldCurvePtr;

#endif
