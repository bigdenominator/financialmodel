ifndef SETUP_FINIRP
SETUP_FINIRP = TRUE

# Financial model depends on framework
include ../../utilities/Make/common.mk
include ../financial.mk

# Specifics to build static / dynamic libraryi
FINIRP_DIR	= $(FINANCIAL_ROOT)/IRPLib
INCLUDE_DIRS	+= -I$(FINIRP_DIR)

FINIRP			:= IRPLib.bd
FINIRP_VER_MAJOR	= 1
FINIRP_VER_MINOR	= 0
FINIRP_VER_RELEASE	= 1
FINIRP_EXT		= $(FINIRP_VER_MAJOR).$(FINIRP_VER_MINOR).$(FINIRP_VER_RELEASE)

FINIRP_REF_REL		= $(FIN_BIN_REL)/$(FINIRP)
FINIRP_LINK_REL		= $(FINIRP_REF_REL).$(FINIRP_EXT)

FINIRP_REF_DBG		= $(FIN_BIN_DBG)/$(FINIRP)
FINIRP_LINK_DBG		= $(FINIRP_REF_DBG).$(FINIRP_EXT)

endif
