#ifndef INCLUDE_H_PDUMMYIRP
#define INCLUDE_H_PDUMMYIRP

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "RateSchematics.h"
#include "Schematics.h"


DEF_NODE(PIrp, SIrp, (), (SPriceOasConfig), (SGlobals, SIrpConfig))
{
public:
	bool Initialize(void)
	{
		SGlobalsPtr		sGlobalsPtr;
		SIrpConfigPtr	sConfigPtr;

		if (!View(sGlobalsPtr)) return false;
		if (!View(sConfigPtr)) return false;

		return Initialize(sGlobalsPtr, sConfigPtr);
	}

	bool Initialize(SGlobalsPtr inp_sGlobalsPtr, SIrpConfigPtr inp_sConfigPtr)
	{
		auto itr(iterators::GetItr(inp_sConfigPtr->mcQuotesPtr));
		
		const size_t iSims(inp_sGlobalsPtr->miScenarios * inp_sGlobalsPtr->miSimsPerScenario);
		const real dRate(itr.find(MARKET_INDEX::LIBOR_12M) ? itr.value() : 0.06);
		CDate dtRate(inp_sGlobalsPtr->mdtMarket);

		CRandomNumberGenerator cRng(inp_sConfigPtr->mlSeed);
		CNormal cDist(0, 0.0005); // made up distribution of periodic rate shocks

		CVectorReal cCurrentRates(iSims, dRate);
		CVectorReal cCdf(iSims);
		CVectorReal cShocks(iSims);
		CVectorReal cFactor(iSims);

		mcRatesPtr = CreateSmart<CDataTable>();
		mcRatesPtr->add(dtRate, CreateSmart<CVectorReal>(cCurrentRates));
		for (int i(0); i < FWD_YEARS; i++)
		{
			dtRate.Add(PERIODS::YEAR, 1);

			cRng.GetNext(cCdf);
			cCurrentRates *= std::exp(cDist.InverseCDF(cCdf));

			mcRatesPtr->add(dtRate, CreateSmart<CVectorReal>(cCurrentRates));
		}

		SPriceOasConfigPtr sOasConfigPtr(CreateSmart<SPriceOasConfig>());
		sOasConfigPtr->miSimsPerScenario = inp_sGlobalsPtr->miSimsPerScenario;
		sOasConfigPtr->mcDiscountPtr = CreateSmart<CYieldCurve>(inp_sGlobalsPtr->mdtMarket, iSims);
		sOasConfigPtr->mcDiscountPtr->AddZeroRate(dtRate.Add(PERIODS::YEAR, 1), dRate);

#ifndef UNITTEST
		Send(sOasConfigPtr);
#endif
		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		auto itr(iterators::GetItr(inp_sSchematicPtr->mcRatesPtr));
		LOOP(itr)
		{
			*itr.value() = Solvers::InterpolateLinear(*mcRatesPtr, itr.key(), false);
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	enum :int { FWD_YEARS = 30 };

	CDataTablePtr mcRatesPtr;
};
#endif
