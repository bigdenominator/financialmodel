#ifndef INCLUDE_H_PUBLISHER
#define INCLUDE_H_PUBLISHER

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "CommonDefs.h"

#include "PBondBehavior.h"
#include "PBondContract.h"
#include "PBondWorkflow.h"
#include "PDefault.h"
#include "PLossSeverity.h"
#include "PPrepay.h"
#include "PIrp.h"
#include "PPriceOAS.h"

PUBLISH(1005, (PBondBehavior, PBondContract, PBondWorkflow, PDefault, PLossSeverity, PPrepay, PIrp, PPriceOAS), ())
#endif
