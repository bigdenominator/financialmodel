// TestDriver.cpp : Defines the entry point for the console application.

#include <iostream>
#include "FinancialDefs.h"

void EXPECT_TRUE(bool inp_bTest) { std::cout << (inp_bTest ? "OK" : "Fail") << endl; }

template<typename T, typename U> void EXPECT_EQ(T lhs, U rhs) { EXPECT_TRUE(lhs == rhs); }

template<typename T> void EXPECT_GT(T lhs, T rhs) { std::cout << (lhs > rhs ? "OK" : "Fail") << endl; }

#include "PPriceOAS.h"
#include "YieldCurve.h"

class RateDerivativeToolsTests 
{
public:
	CHolidayCalendarLstPtr	mcCalendarsPtr;
	SLiborLstsPtr			msLiborLstsPtr;

	void SetUp(void)
	{
		mcCalendarsPtr = CHolidays::GetAll(2000, 2050);
		msLiborLstsPtr = CreateSmart<SLiborLsts>();
		RateIndexes::LoadLiborLsts(false, *mcCalendarsPtr, *msLiborLstsPtr);
	}

	void TearDown(void) {}

	void cap(void)
	{
		const real dTolerance(0.000001);

		const CDate dtMarket(20140101);

		CYieldCurvePtr cYcPtr(CreateSmart<CYieldCurve>(dtMarket, 1));
		cYcPtr->AddDiscountFactor(dtMarket, dtMarket.Add(PERIODS::MONTH, 6), 0.975365);
		cYcPtr->AddDiscountFactor(dtMarket, dtMarket.Add(PERIODS::MONTH, 12), 0.949999);
		cYcPtr->AddDiscountFactor(dtMarket, dtMarket.Add(PERIODS::MONTH, 18), 0.924837);
		cYcPtr->AddDiscountFactor(dtMarket, dtMarket.Add(PERIODS::MONTH, 24), 0.899541);
		cYcPtr->AddDiscountFactor(dtMarket, dtMarket.Add(PERIODS::MONTH, 30), 0.874550);
		cYcPtr->AddDiscountFactor(dtMarket, dtMarket.Add(PERIODS::MONTH, 36), 0.849939);

		CLiborRatePtr cLiborPtr(CreateSmart<CLiborRate>(SPeriod(FREQUENCY::SEMIANNUAL), *mcCalendarsPtr));
		SRateConfig cfg(cLiborPtr->GetConfig());
		cfg.mBusDayConv = BUSINESS_DAY_CONVENTION::UNADJUSTED;
		cfg.mDayCountConv = DAY_COUNT_CONVENTION::BOND_BASIS;
		cfg.miSettleDays = 0;
		cLiborPtr->Config(cfg);

		CCapPtr cTestPtr(CreateSmart<CCap>(cLiborPtr, 3));
		cTestPtr->SetExpiry(dtMarket.Add(PERIODS::MONTH, 6));
		cTestPtr->SetPricingTerms(*cYcPtr);
		cTestPtr->SetStrike(0.055);

		auto itr(cTestPtr->GetPricingTermsLst().begin());
		EXPECT_GT(dTolerance, fabs(fin::Black76Call((*itr)->mdExpiry, 1.0, (*(*itr)->mcImpliedPvPtr)[0], (*(*itr)->mcPvFactorsPtr)[0] * cTestPtr->GetStrike(), 0.1250) - 0.0005745));
		itr++;
		EXPECT_GT(dTolerance, fabs(fin::Black76Call((*itr)->mdExpiry, 1.0, (*(*itr)->mcImpliedPvPtr)[0], (*(*itr)->mcPvFactorsPtr)[0] * cTestPtr->GetStrike(), 0.1500) - 0.0013808));
		itr++;
		EXPECT_GT(dTolerance, fabs(fin::Black76Call((*itr)->mdExpiry, 1.0, (*(*itr)->mcImpliedPvPtr)[0], (*(*itr)->mcPvFactorsPtr)[0] * cTestPtr->GetStrike(), 0.1650) - 0.0023022));

		EXPECT_GT(dTolerance, fabs(cTestPtr->CalcPrice(0.1503) - 0.0042575));
		EXPECT_GT(dTolerance, fabs(cTestPtr->ImputeVol(0.0042575) - 0.1503));
	}

	void capSchedule(void)
	{
		const CDate dtMarket(20030211);

		CLiborRatePtr cLibor3Ptr(CreateSmart<CLiborRate>(SPeriod(FREQUENCY::QUARTERLY), *mcCalendarsPtr));
		cLibor3Ptr->SetSchedule(dtMarket);

		CDate dtExpiry(cLibor3Ptr->GetCoupons().front().mdtEop);
		fin::SubtractBusinessDays(dtExpiry, cLibor3Ptr->GetConfig().miSettleDays, *cLibor3Ptr->GetConfig().mcHolidaysPtr);

		CCapPtr cThisPtr(CreateSmart<CCap>(cLibor3Ptr, 4 * 5 - 1));
		cThisPtr->SetExpiry(dtExpiry);

		EXPECT_TRUE(cThisPtr->GetResetLst()[0] == CDate(20030509));
		EXPECT_TRUE(cThisPtr->GetResetLst()[1] == CDate(20030811));
		EXPECT_TRUE(cThisPtr->GetResetLst()[2] == CDate(20031111));
		EXPECT_TRUE(cThisPtr->GetResetLst()[3] == CDate(20040211));
		EXPECT_TRUE(cThisPtr->GetResetLst()[4] == CDate(20040511));
		EXPECT_TRUE(cThisPtr->GetResetLst()[5] == CDate(20040811));
		EXPECT_TRUE(cThisPtr->GetResetLst()[6] == CDate(20041111));
		EXPECT_TRUE(cThisPtr->GetResetLst()[7] == CDate(20050210));
		EXPECT_TRUE(cThisPtr->GetResetLst()[8] == CDate(20050511));
		EXPECT_TRUE(cThisPtr->GetResetLst()[9] == CDate(20050811));
		EXPECT_TRUE(cThisPtr->GetResetLst()[10] == CDate(20051110));
		EXPECT_TRUE(cThisPtr->GetResetLst()[11] == CDate(20060209));
		EXPECT_TRUE(cThisPtr->GetResetLst()[12] == CDate(20060511));
		EXPECT_TRUE(cThisPtr->GetResetLst()[13] == CDate(20060810));
		EXPECT_TRUE(cThisPtr->GetResetLst()[14] == CDate(20061109));
		EXPECT_TRUE(cThisPtr->GetResetLst()[15] == CDate(20070209));
		EXPECT_TRUE(cThisPtr->GetResetLst()[16] == CDate(20070510));
		EXPECT_TRUE(cThisPtr->GetResetLst()[17] == CDate(20070809));
		EXPECT_TRUE(cThisPtr->GetResetLst()[18] == CDate(20071109));

		std::cout << "resets" << endl;
		FOREACH(cThisPtr->GetResetLst(), itr)
		{
			std::cout << to_string(*itr) << endl;
		}

		std::cout << "cpns" << endl;
		FOREACH(cThisPtr->GetResetLst(), itr)
		{
			cThisPtr->GetUnderlying()->SetSchedule(*itr);
			std::cout << to_string(cThisPtr->GetUnderlying()->GetCoupons().front().mdtBop) << " - " << to_string(cThisPtr->GetUnderlying()->GetCoupons().front().mdtEop) << endl;
		}

		std::cout << "hlpr" << endl;
		FOREACH(cThisPtr->GetSchedule(), itr)
		{
			std::cout << to_string(itr->mdtBop) << " - " << to_string(itr->mdtEop) << endl;
		}
	}

	void swaption(void)
	{
		// adapted from Hull Seventh Edition, example 28.4
		const CDate dtMarket(20140101);
		const uint32_t iExpiry(2 * 5);
		const uint32_t iTenor(2 * 3);
		const uint32_t iMaturity(iExpiry + iTenor);

		CYieldCurvePtr cYcPtr(CreateSmart<CYieldCurve>(dtMarket, 1));
		for (uint32_t iPeriod(1); iPeriod <= iMaturity; iPeriod++)
		{
			cYcPtr->AddDiscountFactor(dtMarket, dtMarket.Add(PERIODS::MONTH, iPeriod * 6), exp(-0.03 * static_cast<real>(iPeriod)));
		}

		CSwapRatePtr cSwapPtr(CreateSmart<CSwapRate>(3, CreateSmart<CLiborRate>(SPeriod(FREQUENCY::QUARTERLY), *mcCalendarsPtr)));
		SRateConfig cfg(cSwapPtr->GetConfig());
		cfg.mBusDayConv = BUSINESS_DAY_CONVENTION::UNADJUSTED;
		cfg.miSettleDays = 0;
		cSwapPtr->Config(cfg);

		CSwaptionPtr cTestPtr(CreateSmart<CSwaption>(cSwapPtr));
		cTestPtr->SetExpiry(dtMarket.Add(PERIODS::YEAR, 5));
		cTestPtr->SetPricingTerms(*cYcPtr);
		cTestPtr->SetStrike(0.062);

		EXPECT_GT(0.0001, fabs(cTestPtr->CalcAtm() - 0.0609));
		EXPECT_GT(0.0001, fabs(cTestPtr->CalcPrice(0.2) - 0.0207));
		EXPECT_GT(0.0001, fabs(cTestPtr->ImputeVol(0.0207) - 0.2));
	}
};


int main(int argc, char* argv[])
{
	RateDerivativeToolsTests This;
	This.SetUp();
	This.cap();
	This.capSchedule();
	This.swaption();
	return 0;
}