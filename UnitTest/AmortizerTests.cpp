#ifndef INCLUDE_H_AMORTIZERTESTS
#define INCLUDE_H_AMORTIZERTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "FinTools.h"

class AmortizerTests : public testing::Test
{
public:
	const size_t	miSims = 10;
	const uint32_t	miPeriodsPerYear = 12;

	const real		mdTolerance = 0.0000001;

	CVectorReal	mcRndmRates;
	CVectorReal	mcRndmBalances;

	CVectorReal	mcInt;
	CVectorReal	mcPrin;
	CVectorReal	mcPmt;
	CVectorReal	mcEop;

	CVectorReal	mcPrinSum; 
	CVectorReal	mcWorking;
	CVectorReal	mcZero;

	template<typename T> static real Compare(const CVectorReal& lhs, const T& rhs)
	{
		return std::sum(std::pow(lhs - rhs, 2));
	}

	void SetUp(void)
	{
		CRandomNumberGenerator cRNG(12345);

		mcRndmRates = CVectorReal(miSims);
		cRNG.GetNext(mcRndmRates);
		mcRndmRates /= 12.0;

		mcRndmBalances = CVectorReal(miSims);
		cRNG.GetNext(mcRndmBalances);
		mcRndmBalances *= 1000000.0;

		mcInt = CVectorReal(miSims);
		mcPrin = CVectorReal(miSims);
		mcPmt = CVectorReal(miSims);
		mcEop = CVectorReal(miSims);
		mcPrinSum = CVectorReal(miSims, 0.0);
		mcWorking = CVectorReal(miSims);
		mcZero = CVectorReal(miSims, 0.0);
	}

	void TearDown(void) {}
};

TEST_F(AmortizerTests, Annuity)
{
	uint32_t iPeriodCnt(30 * miPeriodsPerYear);

	CMatrixReal cAllPmts(iPeriodCnt);

	CAmortizerPtr cAmortPtr(CFinTools::CreateAmortizer(AMORTIZATIONTYPE::ANNUITY, iPeriodCnt, mcRndmRates, mcRndmBalances, -1.0));
	for (uint32_t iPeriod(0); iPeriod < iPeriodCnt; iPeriod++)
	{
		cAmortPtr->Age(mcRndmRates);

		mcInt = cAmortPtr->GetInterest();
		mcPrin = cAmortPtr->GetPrincipal();
		mcPmt = cAmortPtr->GetPayment();
		mcEop = cAmortPtr->GetEndingBalance();

		mcPrinSum += mcPrin;
		cAllPmts[iPeriod] = mcPmt;

		EXPECT_GT(mdTolerance, Compare(mcInt, (mcEop + mcPrin) * mcRndmRates));
		EXPECT_GT(mdTolerance, Compare(mcPmt, mcInt + mcPrin));
	}
	EXPECT_GT(mdTolerance, Compare(mcEop, mcZero));
	EXPECT_GT(mdTolerance, Compare(mcPrinSum, mcRndmBalances));

	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		CVectorReal cThisSim(iPeriodCnt - 1);
		for (uint32_t iPeriod(0); iPeriod < cThisSim.size(); iPeriod++)
		{
			cThisSim[iPeriod] = cAllPmts[iPeriod][iSim];
		}
		EXPECT_GT(mdTolerance, statistics::GetVariancePopulation(cThisSim));
	}
}

TEST_F(AmortizerTests, Bullet)
{
	uint32_t iPeriodCnt(30 * miPeriodsPerYear);

	CAmortizerPtr cAmortPtr(CFinTools::CreateAmortizer(AMORTIZATIONTYPE::BULLET, iPeriodCnt, mcRndmRates, mcRndmBalances, -1.0));
	for (uint32_t iPeriod(0); iPeriod < iPeriodCnt; iPeriod++)
	{
		cAmortPtr->Age(mcRndmRates);

		mcInt = cAmortPtr->GetInterest();
		mcPrin = cAmortPtr->GetPrincipal();
		mcPmt = cAmortPtr->GetPayment();
		mcEop = cAmortPtr->GetEndingBalance();

		mcPrinSum += mcPrin;

		EXPECT_GT(mdTolerance, Compare(mcInt, (mcEop + mcPrin) * mcRndmRates));

		if (iPeriod == iPeriodCnt - 1) EXPECT_GT(mdTolerance, Compare(mcPrin, mcRndmBalances));
		else EXPECT_GT(mdTolerance, Compare(mcPrin, mcZero));

		EXPECT_GT(mdTolerance, Compare(mcPmt, mcInt + mcPrin));
	}
	EXPECT_GT(mdTolerance, Compare(mcEop, mcZero));
	EXPECT_GT(mdTolerance, Compare(mcPrinSum, mcRndmBalances));
}

TEST_F(AmortizerTests, StraightLine)
{
	uint32_t iPeriodCnt(30 * miPeriodsPerYear);

	CVectorReal cPriorPrin(mcRndmBalances / iPeriodCnt);

	CAmortizerPtr cAmortPtr(CFinTools::CreateAmortizer(AMORTIZATIONTYPE::STRAIGHTLINE, iPeriodCnt, mcRndmRates, mcRndmBalances, -1.0));
	for (uint32_t iPeriod(0); iPeriod < iPeriodCnt; iPeriod++)
	{
		cAmortPtr->Age(mcRndmRates);

		mcInt = cAmortPtr->GetInterest();
		mcPrin = cAmortPtr->GetPrincipal();
		mcPmt = cAmortPtr->GetPayment();
		mcEop = cAmortPtr->GetEndingBalance();

		mcPrinSum += mcPrin;

		EXPECT_GT(mdTolerance, Compare(mcInt, (mcEop + mcPrin) * mcRndmRates));
		EXPECT_GT(mdTolerance, Compare(mcPrin, mcRndmBalances/ static_cast<real>(iPeriodCnt)));
		EXPECT_GT(mdTolerance, Compare(mcPmt, mcInt + mcPrin));

		EXPECT_GT(mdTolerance, Compare(mcPrin, cPriorPrin));
		cPriorPrin = mcPrin;
	}
	EXPECT_GT(mdTolerance, Compare(mcEop, mcZero));
	EXPECT_GT(mdTolerance, Compare(mcPrinSum, mcRndmBalances));
}
#endif