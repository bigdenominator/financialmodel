#ifndef INCLUDE_H_COUPONSCHEDTESTS
#define INCLUDE_H_COUPONSCHEDTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "FinTools.h"
#include "HolidayCalendar.h"

class CouponScheduleTests : public testing::Test
{
public:
	CHolidayCalendarPtr mcCalendarPtr;

	const real mdTolerance = 0.0000001;

	typedef SCpnLst::size_type size_type;

	void SetUp(void)
	{
		mcCalendarPtr = CHolidays::GetCalendar(HOLIDAYCALENDARS::LONDON, 2014, 2025);
	}

	void TearDown(void) {}
};

TEST_F(CouponScheduleTests, BackwardMortgage)
{
	int32_t iMonths(360);

	CDate dtOrig(20140715);
	CDate dtIntStart(20140801);
	CDate dtMaturity(dtIntStart.Add(PERIODS::MONTH, iMonths));
	
	SPeriodPtr sPeriodPtr(CreateSmart<SPeriod>(FREQUENCY::MONTHLY));
	CDateAdjPtr cAdjPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::UNADJUSTED, mcCalendarPtr));
	CYearFracPtr cFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::BOND_BASIS, *sPeriodPtr));
	
	SCpnLstPtr sTestPtr(CreateSmart<SCpnLst>(iMonths));

	CCpnSchedPtr cCpnSchedPtr(CFinTools::CreateCouponScheduler(DATE_GENERATION::BACKWARD, cAdjPtr, cFracPtr, dtIntStart, dtOrig, dtMaturity, *sPeriodPtr, sTestPtr));

	CDate dtCpn(dtIntStart.Add(PERIODS::MONTH, 1));
	for (auto itr(iterators::GetItr(sTestPtr)); !itr.isEnd(); itr++)
	{
		EXPECT_EQ(dtCpn, itr->mdtEop);
		EXPECT_EQ(1.0 / 12.0, itr->mdCoupon);

		dtCpn.Move(PERIODS::MONTH, 1);
	}

	EXPECT_GT(0.00001, fabs(cCpnSchedPtr->GetAccrual(dtOrig) + 16.0 / 360.0));
}

TEST_F(CouponScheduleTests, BackwardTNote)
{
	// CUSIP 912828WJ5
	// https://www.treasurydirect.gov/instit/annceresult/press/preanre/2014/A_20140703_1.pdf

	CDate dtDated(20140515), dtReopen(20140715), dtMaturity(20240515);
	
	SPeriodPtr sPeriodPtr(CreateSmart<SPeriod>(FREQUENCY::SEMIANNUAL));
	CDateAdjPtr cAdjPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::UNADJUSTED, mcCalendarPtr));
	CYearFracPtr cFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_ACT_ICMA, *sPeriodPtr));

	SCpnLstPtr sTestPtr(CreateSmart<SCpnLst>(20));

	CCpnSchedPtr cCpnSchedPtr(CFinTools::CreateCouponScheduler(DATE_GENERATION::BACKWARD, cAdjPtr, cFracPtr, dtDated, dtReopen, dtMaturity, *sPeriodPtr, sTestPtr));

	CDate dtCpn(20141115);
	for (auto itr(iterators::GetItr(sTestPtr)); !itr.isEnd(); itr++)
	{
		EXPECT_EQ(dtCpn, itr->mdtEop);
		EXPECT_EQ(0.5, itr->mdCoupon);

		dtCpn.Move(PERIODS::MONTH, 6);
	}

	EXPECT_GT(0.00001, fabs(cCpnSchedPtr->GetAccrual(dtReopen) * 0.025 * 1000 - 4.14402));
}

TEST_F(CouponScheduleTests, BackwardTBill)
{
	// CUSIP 912796AR0
	// https://www.treasurydirect.gov/instit/annceresult/press/preanre/2013/A_20131031_1.pdf

	CDate dtDated(20131107), dtMaturity(20140206);
	
	SPeriodPtr sPeriodPtr(CreateSmart<SPeriod>(FREQUENCY::SINGLE));
	CDateAdjPtr cAdjPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::UNADJUSTED, mcCalendarPtr));
	CYearFracPtr cFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_360, *sPeriodPtr));

	SCpnLstPtr sTestPtr(CreateSmart<SCpnLst>(1));

	CCpnSchedPtr cCpnSchedPtr(CFinTools::CreateCouponScheduler(DATE_GENERATION::BACKWARD, cAdjPtr, cFracPtr, dtDated, dtDated, dtMaturity, *sPeriodPtr, sTestPtr));

	EXPECT_TRUE((*sTestPtr)[0].mdtEop == CDate(20140206));
	EXPECT_GT(0.00001, fabs((*sTestPtr)[0].mdCoupon - 91.0 / 360.0));
	EXPECT_GT(0.00001, fabs(cCpnSchedPtr->GetAccrual(CDate(20131107))));
}

TEST_F(CouponScheduleTests, ForwardTBill)
{
	// CUSIP 912796AR0
	// https://www.treasurydirect.gov/instit/annceresult/press/preanre/2013/A_20131031_1.pdf

	CDate dtDated(20131107), dtMaturity(20140206);
	
	SPeriodPtr sPeriodPtr(CreateSmart<SPeriod>(FREQUENCY::SINGLE));
	CDateAdjPtr cAdjPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::UNADJUSTED, mcCalendarPtr));
	CYearFracPtr cFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_365F, *sPeriodPtr));

	SCpnLstPtr sTestPtr(CreateSmart<SCpnLst>(1));

	CCpnSchedPtr cCpnSchedPtr(CFinTools::CreateCouponScheduler(DATE_GENERATION::FORWARD, cAdjPtr, cFracPtr, dtDated, dtDated, dtMaturity, *sPeriodPtr, sTestPtr));

	EXPECT_TRUE((*sTestPtr)[0].mdtEop == CDate(20140206));
	EXPECT_GT(0.00001, fabs((*sTestPtr)[0].mdCoupon - 91.0 / 365.0));
	EXPECT_GT(0.00001, fabs(cCpnSchedPtr->GetAccrual(CDate(20131107))));
}

TEST_F(CouponScheduleTests, ForwardSwap)
{
	// http://www.teraexchange.com/TeraExchange%20-%2040%202%20Filing%20-%202014-03%20Listing%20of%20USD%20LIBOR%20Fixed%20for%20Float%20Interest%20Rate%20Swaps.pdf
	// http://www.derivativepricing.com/blog/bid/78428/Interest-Rate-Swap-Tutorial-Part-2-of-5-Fixed-Legs

	int32_t iPmts(10);

	CDate dtOrig(20111114);
	CDate dtIntStart(dtOrig);
	CDate dtMaturity(dtIntStart.Add(PERIODS::MONTH, 6 * iPmts));

	SPeriodPtr sPeriodPtr(CreateSmart<SPeriod>(FREQUENCY::SEMIANNUAL));
	CDateAdjPtr cAdjPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING, mcCalendarPtr));
	CYearFracPtr cFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::BOND_BASIS, *sPeriodPtr));

	SCpnLstPtr sTestPtr(CreateSmart<SCpnLst>(iPmts));

	CCpnSchedPtr cCpnSchedPtr(CFinTools::CreateCouponScheduler(DATE_GENERATION::FORWARD, cAdjPtr, cFracPtr, dtIntStart, dtOrig, dtMaturity, *sPeriodPtr, sTestPtr));
	EXPECT_EQ(iPmts, sTestPtr->size());

	EXPECT_TRUE((*sTestPtr)[0].mdtEop == CDate(20120514));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[0].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[1].mdtEop == CDate(20121114));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[1].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[2].mdtEop == CDate(20130514));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[2].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[3].mdtEop == CDate(20131114));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[3].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[4].mdtEop == CDate(20140514));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[4].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[5].mdtEop == CDate(20141114));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[5].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[6].mdtEop == CDate(20150514));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[6].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[7].mdtEop == CDate(20151116));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[7].mdCoupon - 182.0 / 360.0));

	EXPECT_TRUE((*sTestPtr)[8].mdtEop == CDate(20160516));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[8].mdCoupon - 0.5));

	EXPECT_TRUE((*sTestPtr)[9].mdtEop == CDate(20161114));
	EXPECT_GT(mdTolerance, fabs((*sTestPtr)[9].mdCoupon - 178.0 / 360.0));
}

TEST_F(CouponScheduleTests, CalcAge)
{
	CDate dtDated(20150101), dtMaturity(20180101);
	
	SPeriodPtr sPeriodPtr(CreateSmart<SPeriod>(FREQUENCY::SEMIANNUAL));
	CDateAdjPtr cAdjPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::UNADJUSTED, mcCalendarPtr));
	CYearFracPtr cFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::BOND_BASIS, *sPeriodPtr));

	SCpnLstPtr sTestPtr(CreateSmart<SCpnLst>(6));

	CCpnSchedPtr cTestPtr(CFinTools::CreateCouponScheduler(DATE_GENERATION::FORWARD, cAdjPtr, cFracPtr, dtDated, dtDated, dtMaturity, *sPeriodPtr, sTestPtr));

	EXPECT_EQ(0, cTestPtr->GetAge(20141231));
	EXPECT_EQ(0, cTestPtr->GetAge(20150630));
	EXPECT_EQ(1, cTestPtr->GetAge(20150701));
	EXPECT_EQ(1, cTestPtr->GetAge(20150702));
	EXPECT_EQ(5, cTestPtr->GetAge(20171231));
	EXPECT_EQ(6, cTestPtr->GetAge(20180101));
	EXPECT_EQ(6, cTestPtr->GetAge(20180201));
}

TEST_F(CouponScheduleTests, EurodollarFutures)
{
	int32_t iPeriods(40);

	CDate dtOrig(20151020);
	CDate dtMaturity(dtOrig.Add(PERIODS::QUARTER, iPeriods));

	SPeriodPtr sPeriodPtr(CreateSmart<SPeriod>(FREQUENCY::QUARTERLY));
	CDateAdjPtr cAdjPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::UNADJUSTED, mcCalendarPtr));
	CYearFracPtr cFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_ACT_ISDA, *sPeriodPtr));

	SCpnLstPtr sTestPtr(CreateSmart<SCpnLst>(iPeriods));

	CCpnSchedPtr cCpnSchedPtr(CFinTools::CreateCouponScheduler(DATE_GENERATION::THIRDWED, cAdjPtr, cFracPtr, dtOrig, dtOrig, dtMaturity, *sPeriodPtr, sTestPtr));

	// See CME for the following settlement dates
	CVectorDatePtr cDatesPtr(CreateSmart<CVectorDate>(iPeriods));
	(*cDatesPtr)[0] = CDate(20151214);
	(*cDatesPtr)[1] = CDate(20160314);
	(*cDatesPtr)[2] = CDate(20160613);
	(*cDatesPtr)[3] = CDate(20160919);
	(*cDatesPtr)[4] = CDate(20161219);
	(*cDatesPtr)[5] = CDate(20170313);
	(*cDatesPtr)[6] = CDate(20170619);
	(*cDatesPtr)[7] = CDate(20170918);
	(*cDatesPtr)[8] = CDate(20171218);
	(*cDatesPtr)[9] = CDate(20180319);
	(*cDatesPtr)[10] = CDate(20180618);
	(*cDatesPtr)[11] = CDate(20180917);
	(*cDatesPtr)[12] = CDate(20181217);
	(*cDatesPtr)[13] = CDate(20190318);
	(*cDatesPtr)[14] = CDate(20190617);
	(*cDatesPtr)[15] = CDate(20190916);
	(*cDatesPtr)[16] = CDate(20191216);
	(*cDatesPtr)[17] = CDate(20200316);
	(*cDatesPtr)[18] = CDate(20200615);
	(*cDatesPtr)[19] = CDate(20200914);
	(*cDatesPtr)[20] = CDate(20201214);
	(*cDatesPtr)[21] = CDate(20210315);
	(*cDatesPtr)[22] = CDate(20210614);
	(*cDatesPtr)[23] = CDate(20210913);
	(*cDatesPtr)[24] = CDate(20211213);
	(*cDatesPtr)[25] = CDate(20220314);
	(*cDatesPtr)[26] = CDate(20220613);
	(*cDatesPtr)[27] = CDate(20220919);
	(*cDatesPtr)[28] = CDate(20221219);
	(*cDatesPtr)[29] = CDate(20230313);
	(*cDatesPtr)[30] = CDate(20230619);
	(*cDatesPtr)[31] = CDate(20230918);
	(*cDatesPtr)[32] = CDate(20231218);
	(*cDatesPtr)[33] = CDate(20240318);
	(*cDatesPtr)[34] = CDate(20240617);
	(*cDatesPtr)[35] = CDate(20240916);
	(*cDatesPtr)[36] = CDate(20241216);
	(*cDatesPtr)[37] = CDate(20250317);
	(*cDatesPtr)[38] = CDate(20250616);
	(*cDatesPtr)[39] = CDate(20250915);

	size_type iPeriod(0);

	auto foo = [&](SCpnLst::reference inp_Value)
	{
		fin::SubtractBusinessDays(inp_Value.mdtBop, 2, *mcCalendarPtr);
		EXPECT_TRUE((*cDatesPtr)[iPeriod] == inp_Value.mdtBop);
		iPeriod++;
	};
	sTestPtr->foreach(foo);
}
#endif