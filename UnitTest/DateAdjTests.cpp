#ifndef INCLUDE_H_DATEADJTESTS
#define INCLUDE_H_DATEADJTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "FinTools.h"

class DateAdjTest : public testing::Test
{
public:
	CHolidayCalendarPtr mcCalendarPtr;

	void SetUp(void)
	{
		mcCalendarPtr = CreateSmart<CHolidayCalendar>();
		for (int32_t iYear(2014); iYear < 2025; iYear++)
		{
			mcCalendarPtr->add(CDate(iYear, MONTHSOFYEAR::JANUARY, 1), HOLIDAYS::NEWYEARS_DAY);
			mcCalendarPtr->add(CDate(iYear, MONTHSOFYEAR::JULY, 4), HOLIDAYS::JULY4);
			mcCalendarPtr->add(CDate(iYear, MONTHSOFYEAR::DECEMBER, 25), HOLIDAYS::CHRISTMAS);
		}
	}

	void TearDown(void) {}
};

TEST_F(DateAdjTest, Following)
{
	CDate dtTest;

	CSmartPtr<IDateAdj> cTestPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::FOLLOWING, mcCalendarPtr));
	dtTest = 20140101;	EXPECT_TRUE(dtTest + 1 == cTestPtr->Adjust(dtTest));
	dtTest = 20140704;	EXPECT_TRUE(dtTest + 3 == cTestPtr->Adjust(dtTest));
	dtTest = 20141225;	EXPECT_TRUE(dtTest + 1 == cTestPtr->Adjust(dtTest));

	dtTest = 20150103;	EXPECT_TRUE(dtTest + 2 == cTestPtr->Adjust(dtTest));
	dtTest = 20160807;	EXPECT_TRUE(dtTest + 1 == cTestPtr->Adjust(dtTest));
}

TEST_F(DateAdjTest, ModifiedFollowing)
{
	CDate dtTest;

	CSmartPtr<IDateAdj> cTestPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING, mcCalendarPtr));
	dtTest = 20140101;	EXPECT_TRUE(dtTest + 1 == cTestPtr->Adjust(dtTest));
	dtTest = 20140704;	EXPECT_TRUE(dtTest + 3 == cTestPtr->Adjust(dtTest));
	dtTest = 20141225;	EXPECT_TRUE(dtTest + 1 == cTestPtr->Adjust(dtTest));

	dtTest = 20150103;	EXPECT_TRUE(dtTest + 2 == cTestPtr->Adjust(dtTest));
	dtTest = 20160807;	EXPECT_TRUE(dtTest + 1 == cTestPtr->Adjust(dtTest));

	dtTest = 20171231;	EXPECT_TRUE(dtTest - 2 == cTestPtr->Adjust(dtTest));
}

TEST_F(DateAdjTest, Preceding)
{
	CDate dtTest;

	CSmartPtr<IDateAdj> cTestPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::PRECEDING, mcCalendarPtr));
	dtTest = 20140101;	EXPECT_TRUE(dtTest - 1 == cTestPtr->Adjust(dtTest));
	dtTest = 20140704;	EXPECT_TRUE(dtTest - 1 == cTestPtr->Adjust(dtTest));
	dtTest = 20141225;	EXPECT_TRUE(dtTest - 1 == cTestPtr->Adjust(dtTest));
	dtTest = 20180101;	EXPECT_TRUE(dtTest - 3 == cTestPtr->Adjust(dtTest));

	dtTest = 20150103;	EXPECT_TRUE(dtTest - 1 == cTestPtr->Adjust(dtTest));
	dtTest = 20160807;	EXPECT_TRUE(dtTest - 2 == cTestPtr->Adjust(dtTest));
}

TEST_F(DateAdjTest, Unadjusted)
{
	CDate dtBase(20140614L);

	CSmartPtr<IDateAdj> cTestPtr(CFinTools::CreateDateAdjustor(BUSINESS_DAY_CONVENTION::UNADJUSTED, mcCalendarPtr));
	for (uint16_t iCnt(0); iCnt < 10000; iCnt++, dtBase++)
	{
		EXPECT_TRUE(dtBase == cTestPtr->Adjust(dtBase));
	}
}
#endif