#ifndef INCLUDE_H_HOLIDAYCALENDARTESTS
#define INCLUDE_H_HOLIDAYCALENDARTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "HolidayCalendar.h"

class HolidayCalendarTest : public testing::Test
{
public:
	typedef ItrSelector<CHolidayCalendarLst>::iterator HolidayLstItr;
	typedef ItrSelector<CHolidayCalendar>::iterator HolidayItr;

	HolidayLstItr	mcCalsItr;
	HolidayItr		mcThisItr;

	bool HasCalendar(HOLIDAYCALENDARS inp_HolidayCalendar)
	{
		if (!mcCalsItr.find(inp_HolidayCalendar)) return false;

		mcThisItr = iterators::GetItr(mcCalsItr.value());
		return true;
	}

	void SetUp(void) { mcCalsItr = iterators::GetItr(CHolidays::GetAll(2014, 2025)); }

	void TearDown(void) {}

	uint32_t CountInYear(HolidayItr inp_cCalItr, int32_t inp_iYear)
	{
		uint32_t iCnt(0);
		LOOP(inp_cCalItr)
		{
			if (inp_cCalItr.key().Year() == inp_iYear) iCnt++;
		}
		return iCnt;
	}
};

TEST_F(HolidayCalendarTest, Bond)
{
	// http://www.sifma.org/services/holiday-schedule/

	EXPECT_TRUE(HasCalendar(HOLIDAYCALENDARS::BOND));

	//2014
	EXPECT_EQ(11, CountInYear(mcThisItr, 2014));
	EXPECT_TRUE(mcThisItr.find(CDate(20140101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140120)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140217)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140418)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140526)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140704)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140901)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141013)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141111)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141127)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141225)));

	//2015
	EXPECT_EQ(11, CountInYear(mcThisItr, 2015));
	EXPECT_TRUE(mcThisItr.find(CDate(20150101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150119)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150216)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150403)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150525)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150703)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150907)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151012)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151111)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151126)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151225)));
}

TEST_F(HolidayCalendarTest, London)
{
	// http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=LSE

	EXPECT_TRUE(HasCalendar(HOLIDAYCALENDARS::LONDON));

	//2015
	EXPECT_EQ(8, CountInYear(mcThisItr, 2015));
	EXPECT_TRUE(mcThisItr.find(CDate(20150101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150403)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150406)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150504)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150525)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150831)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151225)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151228)));

	//2021
	EXPECT_EQ(8, CountInYear(mcThisItr, 2021));
	EXPECT_TRUE(mcThisItr.find(CDate(20210101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210402)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210405)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210503)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210531)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210830)));
	EXPECT_TRUE(mcThisItr.find(CDate(20211227)));
	EXPECT_TRUE(mcThisItr.find(CDate(20211228)));
}

TEST_F(HolidayCalendarTest, NYSE)
{
	// http://markets.on.nytimes.com/research/markets/holidays/holidays.asp?display=market&exchange=NYQ

	EXPECT_TRUE(HasCalendar(HOLIDAYCALENDARS::NYSE));

	//2014
	EXPECT_EQ(9, CountInYear(mcThisItr, 2014));
	EXPECT_TRUE(mcThisItr.find(CDate(20140101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140120)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140217)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140418)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140526)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140704)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140901)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141127)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141225)));

	//2021
	EXPECT_EQ(9, HolidayCalendarTest::CountInYear(mcThisItr, 2021));
	EXPECT_TRUE(mcThisItr.find(CDate(20210101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210118)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210215)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210402)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210531)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210705)));
	EXPECT_TRUE(mcThisItr.find(CDate(20210906)));
	EXPECT_TRUE(mcThisItr.find(CDate(20211125)));
	EXPECT_TRUE(mcThisItr.find(CDate(20211224)));
}

TEST_F(HolidayCalendarTest, Public)
{
	// http://www.redcort.com/us-federal-bank-holidays

	EXPECT_TRUE(HasCalendar(HOLIDAYCALENDARS::PUBLIC));

	//2014
	EXPECT_EQ(10, CountInYear(mcThisItr, 2014));
	EXPECT_TRUE(mcThisItr.find(CDate(20140101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140120)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140217)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140526)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140704)));
	EXPECT_TRUE(mcThisItr.find(CDate(20140901)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141013)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141111)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141127)));
	EXPECT_TRUE(mcThisItr.find(CDate(20141225)));

	//2015
	EXPECT_EQ(10, HolidayCalendarTest::CountInYear(mcThisItr, 2015));
	EXPECT_TRUE(mcThisItr.find(CDate(20150101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150119)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150216)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150525)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150703)));
	EXPECT_TRUE(mcThisItr.find(CDate(20150907)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151012)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151111)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151126)));
	EXPECT_TRUE(mcThisItr.find(CDate(20151225)));

	//2022
	EXPECT_EQ(9, HolidayCalendarTest::CountInYear(mcThisItr, 2022));
	EXPECT_FALSE(mcThisItr.find(CDate(20220101)));
	EXPECT_TRUE(mcThisItr.find(CDate(20220117)));
	EXPECT_TRUE(mcThisItr.find(CDate(20220221)));
	EXPECT_TRUE(mcThisItr.find(CDate(20220530)));
	EXPECT_TRUE(mcThisItr.find(CDate(20220704)));
	EXPECT_TRUE(mcThisItr.find(CDate(20220905)));
	EXPECT_TRUE(mcThisItr.find(CDate(20221010)));
	EXPECT_TRUE(mcThisItr.find(CDate(20221111)));
	EXPECT_TRUE(mcThisItr.find(CDate(20221124)));
	EXPECT_TRUE(mcThisItr.find(CDate(20221226)));
}
#endif