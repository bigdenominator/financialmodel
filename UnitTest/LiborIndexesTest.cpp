#ifndef INCLUDE_H_LIBORINDEXESTESTS
#define INCLUDE_H_LIBORINDEXESTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "LiborIndexes.h"

class RateIndexTest : public testing::Test
{
public:
	const real mdTolerance = 0.000001;
	
	CHolidayCalendarLstPtr	mcCalendarsPtr;
	SLiborLstsPtr			msDictsPtr;

	void SetUp(void)
	{
		mcCalendarsPtr = CHolidays::GetAll(2000, 2099);
		msDictsPtr = CreateSmart<SLiborLsts>();

		RateIndexes::LoadLiborLsts(false, *mcCalendarsPtr, *msDictsPtr);
	}

	void TearDown(void) {}
};

TEST_F(RateIndexTest, Libor)
{
	// http://www.derivativepricing.com/blogpage.asp?id=4

	CDate dtMarket(20111110);
	CYieldCurvePtr cYcPtr(CreateSmart<CYieldCurve>(dtMarket, 1));

	CLiborRatePtr cLibor0Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_ON]);
	CLiborRatePtr cLibor1Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_1M]);
	CLiborRatePtr cLibor3Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_3M]);
	CLiborRatePtr cLibor6Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_6M]);
	CLiborRatePtr cLibor9Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_9M]);
	CLiborRatePtr cLibor12Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_12M]);

	cLibor0Ptr->Bootstrap(0.00141, *cYcPtr);
	cLibor1Ptr->Bootstrap(0.00249, *cYcPtr);
	cLibor3Ptr->Bootstrap(0.00457, *cYcPtr);
	cLibor6Ptr->Bootstrap(0.00654, *cYcPtr);
	cLibor9Ptr->Bootstrap(0.00808, *cYcPtr);
	cLibor12Ptr->Bootstrap(0.00913, *cYcPtr);

	auto itrDf(iterators::GetItr(cYcPtr->AsDiscountRates()));
	EXPECT_TRUE(itrDf.find(CDate(20111111)));	EXPECT_GT(mdTolerance, fabs(0.99999608 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20111114)));	EXPECT_GT(mdTolerance, fabs(0.99998433 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20111214)));	EXPECT_GT(mdTolerance, fabs(0.99977688 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20120214)));	EXPECT_GT(mdTolerance, fabs(0.99881783 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20120514)));	EXPECT_GT(mdTolerance, fabs(0.99668895 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20120814)));	EXPECT_GT(mdTolerance, fabs(0.99387224 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20121114)));	EXPECT_GT(mdTolerance, fabs(0.99078768 - (*itrDf.value())[0]));

	CVectorRealPtr cRatesPtr(CreateSmart<CVectorReal>(cYcPtr->GetSims()));

	// Set
	cLibor0Ptr->SetSchedule(dtMarket);
	cLibor1Ptr->SetSchedule(dtMarket);
	cLibor3Ptr->SetSchedule(dtMarket);
	cLibor6Ptr->SetSchedule(dtMarket);
	cLibor9Ptr->SetSchedule(dtMarket);
	cLibor12Ptr->SetSchedule(dtMarket);

	// Periodic
	cLibor0Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(1.0 / 0.99999608 - 1.0 - (*cRatesPtr)[0]));

	cLibor1Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99977688 - 1.0 - (*cRatesPtr)[0]));

	cLibor3Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99881783 - 1.0 - (*cRatesPtr)[0]));

	cLibor6Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99668895 - 1.0 - (*cRatesPtr)[0]));

	cLibor9Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99387224 - 1.0 - (*cRatesPtr)[0]));

	cLibor12Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99078768 - 1.0 - (*cRatesPtr)[0]));

	// Annualized
	cLibor0Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00141 - (*cRatesPtr)[0]));

	cLibor1Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00249 - (*cRatesPtr)[0]));

	cLibor3Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00457 - (*cRatesPtr)[0]));

	cLibor6Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00654 - (*cRatesPtr)[0]));

	cLibor9Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00808 - (*cRatesPtr)[0]));

	cLibor12Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00913 - (*cRatesPtr)[0]));
}

TEST_F(RateIndexTest, EDF)
{
	// Based upon:
	// http://faculty.weatherhead.case.edu/ritchken/textbook/Fixed_Income/chap_revised_5.pdf
	// But adjusted for 2-day Libor settlement (i.e., textbook uses Mondays, while Wednesday is "proper"

	CDate dtMarket(20040115);
	CYieldCurvePtr cYcPtr(CreateSmart<CYieldCurve>(dtMarket, 1));

	CEdfRatePtr cEdf1Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF1]);
	CEdfRatePtr cEdf2Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF2]);
	CEdfRatePtr cEdf3Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF3]);
	CEdfRatePtr cEdf4Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF4]);
	CEdfRatePtr cEdf5Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF5]);
	CEdfRatePtr cEdf6Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF6]);
	CEdfRatePtr cEdf7Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF7]);
	CEdfRatePtr cEdf8Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF8]);
	CEdfRatePtr cEdf9Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF9]);
	CEdfRatePtr cEdf10Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF10]);
	CEdfRatePtr cEdf11Ptr((*msDictsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF11]);

	cEdf1Ptr->Bootstrap(0.0116, *cYcPtr);
	cEdf2Ptr->Bootstrap(0.0128, *cYcPtr);
	cEdf3Ptr->Bootstrap(0.0153, *cYcPtr);
	cEdf4Ptr->Bootstrap(0.0189, *cYcPtr);
	cEdf5Ptr->Bootstrap(0.02285, *cYcPtr);
	cEdf6Ptr->Bootstrap(0.02685, *cYcPtr);
	cEdf7Ptr->Bootstrap(0.03025, *cYcPtr);
	cEdf8Ptr->Bootstrap(0.0331, *cYcPtr);
	cEdf9Ptr->Bootstrap(0.03535, *cYcPtr);
	cEdf10Ptr->Bootstrap(0.03745, *cYcPtr);
	cEdf11Ptr->Bootstrap(0.03945, *cYcPtr);


	auto itrDf(iterators::GetItr(cYcPtr->AsDiscountRates()));
	EXPECT_TRUE(itrDf.find(CDate(20040317)));	EXPECT_GT(mdTolerance, fabs(0.998007133873281 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20040616)));	EXPECT_GT(mdTolerance, fabs(0.995089310882814 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20040915))); EXPECT_GT(mdTolerance, fabs(0.991880027947942 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20041215))); EXPECT_GT(mdTolerance, fabs(0.988058710883600 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20050316)));	EXPECT_GT(mdTolerance, fabs(0.983360705114913 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20050615)));	EXPECT_GT(mdTolerance, fabs(0.977713459332516 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20050921))); EXPECT_GT(mdTolerance, fabs(0.970619042977553 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20051221))); EXPECT_GT(mdTolerance, fabs(0.963253498345413 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20060315)));	EXPECT_GT(mdTolerance, fabs(0.955870988080802 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20060621)));	EXPECT_GT(mdTolerance, fabs(0.946760261486723 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20060920))); EXPECT_GT(mdTolerance, fabs(0.937881777586925 - (*itrDf.value())[0]));
	EXPECT_TRUE(itrDf.find(CDate(20061220))); EXPECT_GT(mdTolerance, fabs(0.928621486736069 - (*itrDf.value())[0]));

	CVectorRealPtr cRatesPtr(CreateSmart<CVectorReal>(1));

	// Set
	cEdf1Ptr->SetSchedule(dtMarket);
	cEdf2Ptr->SetSchedule(dtMarket);
	cEdf3Ptr->SetSchedule(dtMarket);
	cEdf4Ptr->SetSchedule(dtMarket);
	cEdf5Ptr->SetSchedule(dtMarket);
	cEdf6Ptr->SetSchedule(dtMarket);
	cEdf7Ptr->SetSchedule(dtMarket);
	cEdf8Ptr->SetSchedule(dtMarket);
	cEdf9Ptr->SetSchedule(dtMarket);
	cEdf10Ptr->SetSchedule(dtMarket);
	cEdf11Ptr->SetSchedule(dtMarket);

	// Periodic
	cEdf1Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.998007133873281 / 0.995089310882814 - 1.0 - (*cRatesPtr)[0]));

	cEdf2Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.995089310882814 / 0.991880027947942 - 1.0 - (*cRatesPtr)[0]));

	cEdf3Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.991880027947942 / 0.988058710883600 - 1.0 - (*cRatesPtr)[0]));

	cEdf4Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.988058710883600 / 0.983360705114913 - 1.0 - (*cRatesPtr)[0]));

	cEdf5Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.983360705114913 / 0.977713459332516 - 1.0 - (*cRatesPtr)[0]));

	cEdf6Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.977713459332516 / 0.970619042977553 - 1.0 - (*cRatesPtr)[0]));

	cEdf7Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.970619042977553 / 0.963253498345413 - 1.0 - (*cRatesPtr)[0]));

	cEdf8Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.963253498345413 / 0.955870988080802 - 1.0 - (*cRatesPtr)[0]));

	cEdf9Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.955870988080802 / 0.946760261486723 - 1.0 - (*cRatesPtr)[0]));

	cEdf10Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.946760261486723 / 0.937881777586925 - 1.0 - (*cRatesPtr)[0]));

	cEdf11Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.937881777586925 / 0.928621486736069 - 1.0 - (*cRatesPtr)[0]));

	// Annualized
	cEdf1Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0116 - (*cRatesPtr)[0]));

	cEdf2Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0128 - (*cRatesPtr)[0]));

	cEdf3Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0153 - (*cRatesPtr)[0]));

	cEdf4Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0189 - (*cRatesPtr)[0]));

	cEdf5Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.02285 - (*cRatesPtr)[0]));

	cEdf6Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.02685 - (*cRatesPtr)[0]));

	cEdf7Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.03025 - (*cRatesPtr)[0]));

	cEdf8Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0331 - (*cRatesPtr)[0]));

	cEdf9Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.03535 - (*cRatesPtr)[0]));

	cEdf10Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.03745 - (*cRatesPtr)[0]));

	cEdf11Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.03945 - (*cRatesPtr)[0]));
}

TEST_F(RateIndexTest, Swap)
{
	// http://www.derivativepricing.com/blogpage.asp?id=2

	CDate dtMarket(20111110);
	CYieldCurvePtr cYcPtr(CreateSmart<CYieldCurve>(dtMarket, 1));

	CLiborRatePtr cLibor0Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_ON]);
	CLiborRatePtr cLibor1Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_1M]);
	CLiborRatePtr cLibor3Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_3M]);
	CLiborRatePtr cLibor6Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_6M]);
	CLiborRatePtr cLibor9Ptr((*msDictsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_9M]);
	CSwapRatePtr cSwap1Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_1Y]);
	CSwapRatePtr cSwap2Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_2Y]);
	CSwapRatePtr cSwap3Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_3Y]);
	CSwapRatePtr cSwap4Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_4Y]);
	CSwapRatePtr cSwap5Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_5Y]);
	CSwapRatePtr cSwap7Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_7Y]);
	CSwapRatePtr cSwap10Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_10Y]);
	CSwapRatePtr cSwap30Ptr((*msDictsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_30Y]);

	cLibor0Ptr->Bootstrap(0.00141, *cYcPtr);
	cLibor1Ptr->Bootstrap(0.00249, *cYcPtr);
	cLibor3Ptr->Bootstrap(0.00457, *cYcPtr);
	cLibor6Ptr->Bootstrap(0.00654, *cYcPtr);
	cLibor9Ptr->Bootstrap(0.00808, *cYcPtr); 
	cSwap1Ptr->Bootstrap(0.0058, *cYcPtr); 
	cSwap2Ptr->Bootstrap(0.0060, *cYcPtr); 
	cSwap3Ptr->Bootstrap(0.0072, *cYcPtr); 
	cSwap4Ptr->Bootstrap(0.0096, *cYcPtr); 
	cSwap5Ptr->Bootstrap(0.0124, *cYcPtr); 
	cSwap7Ptr->Bootstrap(0.0173, *cYcPtr);
	cSwap10Ptr->Bootstrap(0.0219, *cYcPtr); 
	cSwap30Ptr->Bootstrap(0.0283, *cYcPtr); 

	CVectorRealPtr cRatesPtr(CreateSmart<CVectorReal>(1));

	// Set
	cLibor0Ptr->SetSchedule(dtMarket);
	cLibor1Ptr->SetSchedule(dtMarket);
	cLibor3Ptr->SetSchedule(dtMarket);
	cLibor6Ptr->SetSchedule(dtMarket);
	cLibor9Ptr->SetSchedule(dtMarket);
	cSwap1Ptr->SetSchedule(dtMarket);
	cSwap2Ptr->SetSchedule(dtMarket);
	cSwap3Ptr->SetSchedule(dtMarket);
	cSwap4Ptr->SetSchedule(dtMarket);
	cSwap5Ptr->SetSchedule(dtMarket);
	cSwap7Ptr->SetSchedule(dtMarket);
	cSwap10Ptr->SetSchedule(dtMarket);
	cSwap30Ptr->SetSchedule(dtMarket);
	
	// Periodic
	cLibor0Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(1.0 / 0.99999608 - 1.0 - (*cRatesPtr)[0]));

	cLibor1Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99977688 - 1.0 - (*cRatesPtr)[0]));

	cLibor3Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99881783 - 1.0 - (*cRatesPtr)[0]));

	cLibor6Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99668895 - 1.0 - (*cRatesPtr)[0]));

	cLibor9Ptr->ImputePeriodicRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.99998433 / 0.99387224 - 1.0 - (*cRatesPtr)[0]));

	// Annualized
	cLibor0Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00141 - (*cRatesPtr)[0]));

	cLibor1Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00249 - (*cRatesPtr)[0]));

	cLibor3Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00457 - (*cRatesPtr)[0]));

	cLibor6Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00654 - (*cRatesPtr)[0]));

	cLibor9Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.00808 - (*cRatesPtr)[0]));
	
	cSwap1Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0058 - (*cRatesPtr)[0]));
	
	cSwap2Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0060 - (*cRatesPtr)[0]));
	
	cSwap3Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0072 - (*cRatesPtr)[0]));
	
	cSwap4Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0096 - (*cRatesPtr)[0]));
	
	cSwap5Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0124 - (*cRatesPtr)[0]));
	
	cSwap7Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0173 - (*cRatesPtr)[0]));
	
	cSwap10Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0219 - (*cRatesPtr)[0]));
	
	cSwap30Ptr->ImputeAnnualizedRates(*cYcPtr, *cRatesPtr);
	EXPECT_GT(mdTolerance, fabs(0.0283 - (*cRatesPtr)[0]));
}
#endif