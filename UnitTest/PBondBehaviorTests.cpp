#ifndef INCLUDE_H_BEHAVIORTESTS
#define INCLUDE_H_BEHAVIORTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "PBondBehavior.h"

class BehaviorTests : public testing::Test
{
public:
	const size_t	miSims = 1;

	const real		mdTolerance = 0.0000001;
	const real		mdBalBop = 100000;
	const real		mdRate = 0.1;
	const real		mdPpmtRate = 0.375;
	const real		mdDftRate = 0.125;
	const real		mdLossSev = 0.1;

	const CDate		mdtSettle = 20141201;

	SBondBehaviorPtr msThisPtr;

	void SetUp(void)
	{
		const int iPeriods(4);

		CDate dtThis(mdtSettle);

		CVectorRealPtr cWorkingPtr(CreateSmart<CVectorReal>(miSims));

		msThisPtr = CreateSmart<SBondBehavior>();
		msThisPtr->mStatus = LOANSTATUS::CURRENT;
		msThisPtr->mcDetailPtr = CreateSmart<CMetricDetail>();
		for (CMetricDetail::value_type& tblPtr : *msThisPtr->mcDetailPtr)
		{
			tblPtr = CreateSmart<CMetricDetail::value_type::element_type>();
		}

		// settlement
		(*msThisPtr->mcDetailPtr)[METRICS::PROB_DFLT]->add(dtThis, CreateSmart<CVectorReal>(miSims, CMath::ln(2 * mdDftRate)));
		(*msThisPtr->mcDetailPtr)[METRICS::PROB_PPMT]->add(dtThis, CreateSmart<CVectorReal>(miSims, CMath::ln(2 * mdPpmtRate)));
		(*msThisPtr->mcDetailPtr)[METRICS::LGD]->add(dtThis, CreateSmart<CVectorReal>(miSims, mdLossSev));
		(*msThisPtr->mcDetailPtr)[METRICS::BAL_EOP]->add(dtThis, CreateSmart<CVectorReal>(miSims, mdBalBop));
		(*msThisPtr->mcDetailPtr)[METRICS::INT]->add(dtThis, CreateSmart<CVectorReal>(miSims, 0.0));
		(*msThisPtr->mcDetailPtr)[METRICS::PRIN]->add(dtThis, CreateSmart<CVectorReal>(miSims, 0.0));
		
		// periods
		auto itrBal(iterators::GetItr((*msThisPtr->mcDetailPtr)[METRICS::BAL_EOP]));
		for (int iPeriod(0); iPeriod < iPeriods; iPeriod++)
		{
			dtThis.Move(PERIODS::MONTH, 1, false);

			itrBal.end()--;
			CVectorRealPtr cLgdPtr(CreateSmart<CVectorReal>(miSims, mdLossSev));
			CVectorRealPtr cProbDfltPtr(CreateSmart<CVectorReal>(miSims, CMath::ln(2 * mdDftRate)));
			CVectorRealPtr cProbPpmtPtr(CreateSmart<CVectorReal>(miSims, CMath::ln(2 * mdPpmtRate)));
			CVectorRealPtr cPrinPtr(CreateSmart<CVectorReal>(miSims, mdBalBop / static_cast<real>(iPeriods)));
			CVectorRealPtr cIntPtr(CreateSmart<CVectorReal>(miSims));
			*cIntPtr = *itrBal.value() * mdRate;
			CVectorRealPtr cBalPtr(CreateSmart<CVectorReal>(miSims));
			*cBalPtr = *itrBal.value() - *cPrinPtr;

			(*msThisPtr->mcDetailPtr)[METRICS::PROB_DFLT]->add(dtThis, cProbDfltPtr);
			(*msThisPtr->mcDetailPtr)[METRICS::PROB_PPMT]->add(dtThis, cProbPpmtPtr);
			(*msThisPtr->mcDetailPtr)[METRICS::LGD]->add(dtThis, cLgdPtr);
			(*msThisPtr->mcDetailPtr)[METRICS::PRIN]->add(dtThis, cPrinPtr);
			(*msThisPtr->mcDetailPtr)[METRICS::INT]->add(dtThis, cIntPtr);
			(*msThisPtr->mcDetailPtr)[METRICS::BAL_EOP]->add(dtThis, cBalPtr);
		}

		PBondBehavior pTest;
		pTest.Initialize();
		pTest.Process(msThisPtr);
	}

	void TearDown(void) {}
};

TEST_F(BehaviorTests, AllBehaviorTests)
{
	real dSched, dBop, dEop(mdBalBop), dPrin, dInt, dPmt, dPpmt, dDft, dLoss, dRecov, dSurvival(1.0);

	auto itrDtl(iterators::GetConstItr(msThisPtr->mcDetailPtr));

	LOOP(itrDtl)
	{
		EXPECT_EQ((itrDtl.key() == static_cast<CMetricDetail::size_type>(METRICS::RATES) ? 0 : 5), (*itrDtl)->size());
	}

	CDate dtThis(mdtSettle);

	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::BAL_EOP))[dtThis])[iSim] - mdBalBop)); 
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::DFLT))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::INT))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LGD))[dtThis])[iSim] - mdLossSev));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LOSS))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::NET_CF))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PPMT))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PRIN))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_DFLT))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_PPMT))[dtThis])[iSim] - 0.0));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::RECOVERY))[dtThis])[iSim] - 0.0));
	}

	dtThis.Move(PERIODS::MONTH, 1, false);

	dSched = 75000;
	dBop = dEop;
	dDft = dBop * mdDftRate;
	dLoss = dDft * mdLossSev;
	dRecov = dDft - dLoss;
	dPpmt = dSurvival * mdPpmtRate * dSched;
	dInt = (dBop - dDft) * mdRate;

	dSurvival *= (1.0 - mdPpmtRate - mdDftRate);
	dEop = dSurvival * dSched;
	dPrin = (dBop - dEop) - dDft - dPpmt;
	dPmt = dPrin + dInt + dPpmt + dRecov;
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::BAL_EOP))[dtThis])[iSim] - dEop));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::DFLT))[dtThis])[iSim] - dDft));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::INT))[dtThis])[iSim] - dInt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LGD))[dtThis])[iSim] - mdLossSev));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LOSS))[dtThis])[iSim] - dLoss));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::NET_CF))[dtThis])[iSim] - dPmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PPMT))[dtThis])[iSim] - dPpmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PRIN))[dtThis])[iSim] - dPrin));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_DFLT))[dtThis])[iSim] - mdDftRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_PPMT))[dtThis])[iSim] - mdPpmtRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::RECOVERY))[dtThis])[iSim] - dRecov));
	}

	dtThis.Move(PERIODS::MONTH, 1, false);

	dSched = 50000;
	dBop = dEop;
	dDft = dBop * mdDftRate;
	dLoss = dDft * mdLossSev;
	dRecov = dDft - dLoss;
	dPpmt = dSurvival * mdPpmtRate * dSched;
	dInt = (dBop - dDft) * mdRate;

	dSurvival *= (1.0 - mdPpmtRate - mdDftRate);
	dEop = dSurvival * dSched;
	dPrin = (dBop - dEop) - dDft - dPpmt;
	dPmt = dPrin + dInt + dPpmt + dRecov;
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::BAL_EOP))[dtThis])[iSim] - dEop));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::DFLT))[dtThis])[iSim] - dDft));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::INT))[dtThis])[iSim] - dInt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LGD))[dtThis])[iSim] - mdLossSev));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LOSS))[dtThis])[iSim] - dLoss));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::NET_CF))[dtThis])[iSim] - dPmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PPMT))[dtThis])[iSim] - dPpmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PRIN))[dtThis])[iSim] - dPrin));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_DFLT))[dtThis])[iSim] - mdDftRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_PPMT))[dtThis])[iSim] - mdPpmtRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::RECOVERY))[dtThis])[iSim] - dRecov));
	}

	dtThis.Move(PERIODS::MONTH, 1, false);

	dSched = 25000;
	dBop = dEop;
	dDft = dBop * mdDftRate;
	dLoss = dDft * mdLossSev;
	dRecov = dDft - dLoss;
	dPpmt = dSurvival * mdPpmtRate * dSched;
	dInt = (dBop - dDft) * mdRate;

	dSurvival *= (1.0 - mdPpmtRate - mdDftRate);
	dEop = dSurvival * dSched;
	dPrin = (dBop - dEop) - dDft - dPpmt;
	dPmt = dPrin + dInt + dPpmt + dRecov;
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::BAL_EOP))[dtThis])[iSim] - dEop));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::DFLT))[dtThis])[iSim] - dDft));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::INT))[dtThis])[iSim] - dInt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LGD))[dtThis])[iSim] - mdLossSev));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LOSS))[dtThis])[iSim] - dLoss));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::NET_CF))[dtThis])[iSim] - dPmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PPMT))[dtThis])[iSim] - dPpmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PRIN))[dtThis])[iSim] - dPrin));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_DFLT))[dtThis])[iSim] - mdDftRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_PPMT))[dtThis])[iSim] - mdPpmtRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::RECOVERY))[dtThis])[iSim] - dRecov));
	}

	dtThis.Move(PERIODS::MONTH, 1, false);

	dSched = 0;
	dBop = dEop;
	dDft = dBop * mdDftRate;
	dLoss = dDft * mdLossSev;
	dRecov = dDft - dLoss;
	dPpmt = dSurvival * mdPpmtRate * dSched;
	dInt = (dBop - dDft) * mdRate;

	dSurvival *= (1.0 - mdPpmtRate - mdDftRate);
	dEop = dSurvival * dSched;
	dPrin = (dBop - dEop) - dDft - dPpmt;
	dPmt = dPrin + dInt + dPpmt + dRecov;
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::BAL_EOP))[dtThis])[iSim] - dEop));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::DFLT))[dtThis])[iSim] - dDft));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::INT))[dtThis])[iSim] - dInt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LGD))[dtThis])[iSim] - mdLossSev));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::LOSS))[dtThis])[iSim] - dLoss));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::NET_CF))[dtThis])[iSim] - dPmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PPMT))[dtThis])[iSim] - dPpmt));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PRIN))[dtThis])[iSim] - dPrin));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_DFLT))[dtThis])[iSim] - mdDftRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::PROB_PPMT))[dtThis])[iSim] - mdPpmtRate));
		EXPECT_GT(mdTolerance, fabs((*(*msThisPtr->mcDetailPtr->at(METRICS::RECOVERY))[dtThis])[iSim] - dRecov));
	}
}
#endif