#ifndef INCLUDE_H_CONTRACTTESTS
#define INCLUDE_H_CONTRACTTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "PBondContract.h"

enum class BOND_TEMPLATE { CORPORATE, LIBOR, BOND_MORTGAGE, SWAP };

class ContractTests : public testing::Test
{
public:
	enum ITRS { BAL, INT, PRIN, RATE };

	const size_t	miSims = 10;
	
	const real		mdTolerance = 0.0000001;
	
	PBondContract	mpBond;

	CVectorRealPtr	mcWorkingPtr;

	void setBasic(SBondDefPtr inp_sDefPtr)
	{
		inp_sDefPtr->miLookbackDays = 45;
		inp_sDefPtr->mdRoundingFactor = 0.00125;
		inp_sDefPtr->mdFirstRelCapFloor = 0.02;
		inp_sDefPtr->mdPeriodicRelCapFloor = 0.02;
		inp_sDefPtr->mdLifeRelCapFloor = 0.05;
		inp_sDefPtr->mRoundingType = ROUNDINGTYPE::NEAREST;

		inp_sDefPtr->mlCounterpartyId;
		inp_sDefPtr->mlPropertyId;

		inp_sDefPtr->mRtppSubModel;
	}

	void setTemplate(SBondDefPtr inp_sDefPtr, BOND_TEMPLATE inp_BondTemplate)
	{
		switch (inp_BondTemplate)
		{
		case BOND_TEMPLATE::LIBOR:
			inp_sDefPtr->mBusDayConv = BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING;
			inp_sDefPtr->mDateGen = DATE_GENERATION::EOM;
			inp_sDefPtr->mFreq = FREQUENCY::SINGLE;
			inp_sDefPtr->mHolidayCalendar = HOLIDAYCALENDARS::LONDON;
			inp_sDefPtr->mDayCountConv = DAY_COUNT_CONVENTION::ACT_360;

			inp_sDefPtr->mAmortType = AMORTIZATIONTYPE::BULLET;

			inp_sDefPtr->miPayPeriodsPerReset = 1;
			inp_sDefPtr->mResetIndex = MARKET_INDEX::LIBOR_3M;
			inp_sDefPtr->mdMargin = 0.0;
			break;
		case BOND_TEMPLATE::BOND_MORTGAGE:
			inp_sDefPtr->mBusDayConv = BUSINESS_DAY_CONVENTION::UNADJUSTED;
			inp_sDefPtr->mDateGen = DATE_GENERATION::BACKWARD;
			inp_sDefPtr->mFreq = FREQUENCY::MONTHLY;
			inp_sDefPtr->mHolidayCalendar = HOLIDAYCALENDARS::PUBLIC;
			inp_sDefPtr->mDayCountConv = DAY_COUNT_CONVENTION::BOND_BASIS;

			inp_sDefPtr->mAmortType = AMORTIZATIONTYPE::ANNUITY;

			inp_sDefPtr->miPayPeriodsPerReset = 12;
			inp_sDefPtr->mResetIndex = MARKET_INDEX::LIBOR_12M;
			inp_sDefPtr->mdMargin = 0.0225;
			break;
		case BOND_TEMPLATE::SWAP:
			inp_sDefPtr->mBusDayConv = BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING;
			inp_sDefPtr->mDateGen = DATE_GENERATION::FORWARD;
			inp_sDefPtr->mFreq = FREQUENCY::SEMIANNUAL;
			inp_sDefPtr->mHolidayCalendar = HOLIDAYCALENDARS::LONDON;
			inp_sDefPtr->mDayCountConv = DAY_COUNT_CONVENTION::BOND_BASIS;

			inp_sDefPtr->mAmortType = AMORTIZATIONTYPE::BULLET;

			inp_sDefPtr->miPayPeriodsPerReset = 2;
			inp_sDefPtr->mResetIndex = MARKET_INDEX::LIBOR_6M;
			inp_sDefPtr->mdMargin = 0.0;
			break;
		default:
			inp_sDefPtr->mBusDayConv = BUSINESS_DAY_CONVENTION::MODIFIED_FOLLOWING;
			inp_sDefPtr->mDateGen = DATE_GENERATION::BACKWARD;
			inp_sDefPtr->mFreq = FREQUENCY::SEMIANNUAL;
			inp_sDefPtr->mHolidayCalendar = HOLIDAYCALENDARS::PUBLIC;
			inp_sDefPtr->mDayCountConv = DAY_COUNT_CONVENTION::BOND_BASIS;

			inp_sDefPtr->mAmortType = AMORTIZATIONTYPE::BULLET;

			inp_sDefPtr->miPayPeriodsPerReset = 2;
			inp_sDefPtr->mResetIndex = MARKET_INDEX::LIBOR_6M;
			inp_sDefPtr->mdMargin = 0.005;
			break;
		}
	}

	SBondContractPtr Build(BOND_TEMPLATE inp_BondTemplate, CDate inp_dtDated, CDate inp_dtIssue, CDate inp_dtMaturity, uint32_t inp_iAmortLife, uint32_t inp_iPayPeriods, uint32_t inp_iFirstReset, real inp_dOrigBal, real inp_dOrigRate, real inp_dCurrBal, real inp_dCurrRate, real inp_dCurrPmt)
	{
		SBondDefPtr sDefPtr(CreateSmart<SBondDef>());
		setBasic(sDefPtr);
		setTemplate(sDefPtr, inp_BondTemplate);

		sDefPtr->mdtDated = inp_dtDated;
		sDefPtr->mdtIssue = inp_dtIssue;
		sDefPtr->mdtMaturity = inp_dtMaturity;

		sDefPtr->miAmortPeriods = inp_iAmortLife;
		sDefPtr->miPayPeriods = inp_iPayPeriods;
		sDefPtr->miFirstReset = inp_iFirstReset;

		sDefPtr->mdBalOrigination = inp_dOrigBal;
		sDefPtr->mdRateOrigination = inp_dOrigRate;

		sDefPtr->mdBalCurrent = inp_dCurrBal;
		sDefPtr->mdRateCurrent = inp_dCurrRate;
		sDefPtr->mdPmtCurrent = inp_dCurrPmt;

		SBondContractPtr sResultPtr(CreateSmart<SBondContract>());
		sResultPtr->msBondDefPtr = sDefPtr;
		sResultPtr->mcBalEopPtr = CreateSmart<CDataTable>();
		sResultPtr->mcIntPtr = CreateSmart<CDataTable>();
		sResultPtr->mcPrinPtr = CreateSmart<CDataTable>();
		sResultPtr->mcRatesPtr = CreateSmart<CDataTable>();

		return sResultPtr;
	}

	void SetUp(void)
	{
		SGlobalsPtr sGlobalsPtr(CreateSmart<SGlobals>());
		sGlobalsPtr->mcCalendarsPtr = CHolidays::GetAll(1950, 2099);
		sGlobalsPtr->mdtMarket = 20150101;
		sGlobalsPtr->miScenarios = 1;
		sGlobalsPtr->miSimsPerScenario = miSims;

		mpBond.Initialize(sGlobalsPtr);

		mcWorkingPtr = CreateSmart<CVectorReal>(miSims);
	}

	void TearDown(void) {}
};

TEST_F(ContractTests, NewFixedRateMortgage)
{
	SBondContractPtr sContractPtr(Build(BOND_TEMPLATE::BOND_MORTGAGE, CDate(20150201), CDate(20150115), CDate(20450201), 360, 360, 360, 250000, 0.06, 250000, 0.06, -1.0));

	EXPECT_EQ(PROCESS_STATUS::DONE, mpBond.Process(sContractPtr));

	EXPECT_EQ(361, sContractPtr->mcBalEopPtr->size());
	EXPECT_EQ(361, sContractPtr->mcIntPtr->size());
	EXPECT_EQ(361, sContractPtr->mcPrinPtr->size());
	EXPECT_EQ(361, sContractPtr->mcRatesPtr->size());

	auto itrs(iterators::MakeBundleConst(
		sContractPtr->mcBalEopPtr,
		sContractPtr->mcIntPtr,
		sContractPtr->mcPrinPtr,
		sContractPtr->mcRatesPtr));

	*mcWorkingPtr = *sContractPtr->mcBalEopPtr->front().second;
	LOOP(itrs)
	{
		*mcWorkingPtr -= *itrs.At<ITRS::BAL>().value();
		*mcWorkingPtr -= *itrs.At<ITRS::PRIN>().value();
		for (size_t iSim(0); iSim < miSims; iSim++)
		{
			EXPECT_GT(mdTolerance, fabs((*mcWorkingPtr)[iSim]));
		}

		*mcWorkingPtr = *itrs.At<ITRS::BAL>().value();
	}

	itrs.begin()++;
	*mcWorkingPtr = *itrs.At<ITRS::INT>().value() + *itrs.At<ITRS::PRIN>().value();
	for (itrs++; !itrs.isEnd(); itrs++)
	{
		*mcWorkingPtr -= *itrs.At<ITRS::INT>().value();
		*mcWorkingPtr -= *itrs.At<ITRS::PRIN>().value();

		for (long iSim(0); iSim < miSims; iSim++)
		{
			EXPECT_GT(mdTolerance, fabs((*mcWorkingPtr)[iSim]));
		}

		*mcWorkingPtr = *itrs.At<ITRS::INT>().value() + *itrs.At<ITRS::PRIN>().value();
	}
}

TEST_F(ContractTests, SeasonedFixedRateMortgage)
{
	SBondContractPtr sContractPtr(Build(BOND_TEMPLATE::BOND_MORTGAGE, CDate(20130201), CDate(20130115), CDate(20430201), 360, 360, 360, 250000, 0.06, 250000, 0.06, -1.0));

	EXPECT_EQ(PROCESS_STATUS::DONE, mpBond.Process(sContractPtr));

	EXPECT_EQ(361 - 23, sContractPtr->mcBalEopPtr->size());
	EXPECT_EQ(361 - 23, sContractPtr->mcIntPtr->size());
	EXPECT_EQ(361 - 23, sContractPtr->mcPrinPtr->size());
	EXPECT_EQ(361 - 23, sContractPtr->mcRatesPtr->size());

	auto itrs(iterators::MakeBundleConst(
		sContractPtr->mcBalEopPtr,
		sContractPtr->mcIntPtr,
		sContractPtr->mcPrinPtr,
		sContractPtr->mcRatesPtr));

	*mcWorkingPtr = *sContractPtr->mcBalEopPtr->front().second;
	LOOP(itrs)
	{
		*mcWorkingPtr -= *itrs.At<ITRS::BAL>().value();
		*mcWorkingPtr -= *itrs.At<ITRS::PRIN>().value();
		for (size_t iSim(0); iSim < miSims; iSim++)
		{
			EXPECT_GT(mdTolerance, fabs((*mcWorkingPtr)[iSim]));
		}
		*mcWorkingPtr = *itrs.At<ITRS::BAL>().value();
	}

	itrs.begin()++;
	*mcWorkingPtr = *itrs.At<ITRS::INT>().value() + *itrs.At<ITRS::PRIN>().value();
	for (itrs++; !itrs.isEnd(); itrs++)
	{
		*mcWorkingPtr -= *itrs.At<ITRS::INT>().value();
		*mcWorkingPtr -= *itrs.At<ITRS::PRIN>().value();

		for (long iSim(0); iSim < miSims; iSim++)
		{
			EXPECT_GT(mdTolerance, fabs((*mcWorkingPtr)[iSim]));
		}

		*mcWorkingPtr = *itrs.At<ITRS::INT>().value() + *itrs.At<ITRS::PRIN>().value();
	}
}

TEST_F(ContractTests, CurtailedFixedRateMortgage)
{
	SBondContractPtr sContractPtr(Build(BOND_TEMPLATE::BOND_MORTGAGE, CDate(20130201), CDate(20130115), CDate(20430201), 360, 360, 360, 250000, 0.06, 225000, 0.06, fin::AnnuityPmtPerPv(1.0 / 1.005, 360) * 250000));
	// current balance should be 243,949.71648070 -> curtailment of 18,949.71648070

	EXPECT_EQ(PROCESS_STATUS::DONE, mpBond.Process(sContractPtr));

	EXPECT_EQ(361 - 23, sContractPtr->mcBalEopPtr->size());
	EXPECT_EQ(361 - 23, sContractPtr->mcIntPtr->size());
	EXPECT_EQ(361 - 23, sContractPtr->mcPrinPtr->size());
	EXPECT_EQ(361 - 23, sContractPtr->mcRatesPtr->size());

	auto itrs(iterators::MakeBundleConst(
		sContractPtr->mcBalEopPtr,
		sContractPtr->mcIntPtr,
		sContractPtr->mcPrinPtr,
		sContractPtr->mcRatesPtr));

	*mcWorkingPtr = *sContractPtr->mcBalEopPtr->front().second;
	LOOP(itrs)
	{
		*mcWorkingPtr -= *itrs.At<ITRS::BAL>().value();
		*mcWorkingPtr -= *itrs.At<ITRS::PRIN>().value();
		for (size_t iSim(0); iSim < miSims; iSim++)
		{
			EXPECT_GT(mdTolerance, fabs((*mcWorkingPtr)[iSim]));
		}
		*mcWorkingPtr = *itrs.At<ITRS::BAL>().value();
	}

	CDataTable::size_type iZeroCnt(0);
	LOOP(itrs)
	{
		if (fabs((*itrs.At<ITRS::BAL>().value())[0]) < mdTolerance) iZeroCnt++;
	}
	EXPECT_TRUE(iZeroCnt == 59);
}

//TEST_F(ContractTests, NewAdjustableRateMortgage)
//{
//	SBondContractPtr sContractPtr(Build(BOND_TEMPLATE::BOND_MORTGAGE, CDate(20150201), CDate(20150115), CDate(20450201), 360, 360, 60, 250000, 0.05, 250000, 0.05, -1.0));
//
//	mpBond.msSchematicPtr = sContractPtr;
//	EXPECT_TRUE(mpBond.Process());
//
//	EXPECT_EQ(361, sContractPtr->mcBalEopPtr->size());
//	EXPECT_EQ(361, sContractPtr->mcIntPtr->size());
//	EXPECT_EQ(361, sContractPtr->mcPrinPtr->size());
//	EXPECT_EQ(361, sContractPtr->mcRatesPtr->size());
//
//	itrs.At<ITRS::BAL>().attach(sContractPtr->mcBalEopPtr);
//	itrs.At<ITRS::INT>().attach(sContractPtr->mcIntPtr);
//	itrs.At<ITRS::PRIN>().attach(sContractPtr->mcPrinPtr);
//	itrs.At<ITRS::RATE>().attach(sContractPtr->mcRatesPtr);
//
//	FOREACH(itrs)
//	{
//		*mcWorkingPtr -= *itrs.At<ITRS::BAL>().value();
//		*mcWorkingPtr -= *itrs.At<ITRS::PRIN>().value();
//		for (size_t iSim(0); iSim < miSims; iSim++)
//		{
//			EXPECT_GT(mdTolerance, fabs((*mcWorkingPtr)[iSim]));
//		}
//		*mcWorkingPtr = *itrs.At<ITRS::BAL>().value();
//	}
//
//	long iPeriod(0);
//	FOREACH(itrs)
//	{
//		if (iPeriod < 61) EXPECT_GT(mdTolerance, fabs((*itrs.At<ITRS::RATE>().value())[0] - 0.05));
//		else if (iPeriod < 73) EXPECT_GT(mdTolerance, fabs((*itrs.At<ITRS::RATE>().value())[0] - 0.05 - 0.02));
//		else EXPECT_GT(mdTolerance, fabs((*itrs.At<ITRS::RATE>().value())[0] - 0.06 - 0.0225));
//
//		iPeriod++;
//	}
//}
#endif