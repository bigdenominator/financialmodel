#ifndef INCLUDE_H_IRPTESTS
#define INCLUDE_H_IRPTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "PInterestRates.h"


class IrpTests : public testing::Test
{
public:
	const real mdTolerance = 0.0000001;

	SGlobalsPtr		msGlobalsPtr;
	SIrpConfigPtr	msConfigPtr;
	SIrpPtr			msIrpPtr;

	void SetUp(void)
	{
		msGlobalsPtr = CreateSmart<SGlobals>();
		msGlobalsPtr->mdtMarket = CDate(20111110);
		msGlobalsPtr->miScenarios = 1;
		msGlobalsPtr->miSimsPerScenario = 10;
		msGlobalsPtr->mcCalendarsPtr = CHolidays::GetAll(1950, 2099);
		
		msConfigPtr = CreateSmart<SIrpConfig>();
		msConfigPtr->mbAdjConvexity = false;
		msConfigPtr->mcHistoryPtr = CreateSmart<CIndexHistory>();
		msConfigPtr->mcQuotesPtr = CreateSmart<CIndexQuoteLst>();

		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::LIBOR_ON, 0.00141);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::LIBOR_1M, 0.00249);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::LIBOR_3M, 0.00457);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::LIBOR_6M, 0.00654);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::LIBOR_9M, 0.00808);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::LIBOR_12M, 0.00913);

		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF1, 0.0116);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF2, 0.0128);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF3, 0.0153);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF4, 0.0189);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF5, 0.02285);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF6, 0.02685);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF7, 0.03025);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF8, 0.0331);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF9, 0.03535);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF10, 0.03745);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::EDF11, 0.03945);

		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_1Y, 0.0058);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_2Y, 0.0060);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_3Y, 0.0072);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_4Y, 0.0096);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_5Y, 0.0124);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_7Y, 0.0173);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_10Y, 0.0219);
		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::SWAP_30Y, 0.0283);

		msConfigPtr->mcQuotesPtr->add(MARKET_INDEX::PMMS30, 0.0400);

		msConfigPtr->mcShockLstPtr = CreateSmart<CRateShockLst>();

		msIrpPtr = CreateSmart<SIrp>();
		msIrpPtr->mcRatesPtr = CreateSmart<CDataTable>();
	}

	void TearDown(void) {}
};

TEST_F(IrpTests, ConstRateModel)
{
	const MARKET_INDEX RateIndex(MARKET_INDEX::LIBOR_3M);
	const real dRateIndex(msConfigPtr->mcQuotesPtr->at(RateIndex));
	const size_t iSims(msGlobalsPtr->miScenarios * msGlobalsPtr->miSimsPerScenario);

	msConfigPtr->mlModelId = RATE_MODELS::CONST_RATES;
	msIrpPtr->mMarketIndex = RateIndex;
	msIrpPtr->mcRatesPtr->add(msGlobalsPtr->mdtMarket, CreateSmart<CVectorReal>());
	msIrpPtr->mcRatesPtr->add(msGlobalsPtr->mdtMarket.Add(PERIODS::YEAR, 5), CreateSmart<CVectorReal>());

	PIrp pTest;
	pTest.Initialize(msGlobalsPtr, msConfigPtr);
	EXPECT_EQ(PROCESS_STATUS::DONE, pTest.Process(msIrpPtr));

	EXPECT_TRUE(msIrpPtr->mcRatesPtr->size() == 2);

	auto itr(iterators::GetConstItr(msIrpPtr->mcRatesPtr));

	itr.begin();
	EXPECT_TRUE(itr.key() == msGlobalsPtr->mdtMarket);
	for (size_t iSim(0); iSim < iSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - dRateIndex));
	}

	itr++;
	EXPECT_TRUE(itr.key() == msGlobalsPtr->mdtMarket.Add(PERIODS::YEAR, 5));
	for (size_t iSim(0); iSim < iSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - dRateIndex));
	}
}
#endif