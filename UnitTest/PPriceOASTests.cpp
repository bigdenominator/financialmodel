#ifndef INCLUDE_H_PRICEOASTESTS
#define INCLUDE_H_PRICEOASTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "PPriceOAS.h"
#include "YieldCurve.h"

class PPriceOASTest : public testing::Test
{
public:
	const size_t miTotalSims = 10;
	const size_t miScenarios = 2;
	const CDate		mdtMarket = 20150101;

	const real		mdTolerance = 0.0000001;
	const real		mdBalBop = 100000;
	const real		mdRate = 0.04;
	const real		mdOas = 0.01;
	const real		mdValue = 96297.6005375893;

	PPriceOAS		mpTest;
	SPriceOASPtr	msThisPtr;

	void SetUp(void)
	{
		msThisPtr = CreateSmart<SPriceOAS>();
		
		msThisPtr->msBondDefPtr = CreateSmart<SBondDef>();

		msThisPtr->mcNetCfPtr = CreateSmart<CDataTable>();
		msThisPtr->mcNetCfPtr->add(mdtMarket.Add(PERIODS::YEAR, 1), CreateSmart<CVectorReal>(miTotalSims, mdRate * mdBalBop));
		msThisPtr->mcNetCfPtr->add(mdtMarket.Add(PERIODS::YEAR, 2), CreateSmart<CVectorReal>(miTotalSims, mdRate * mdBalBop));
		msThisPtr->mcNetCfPtr->add(mdtMarket.Add(PERIODS::YEAR, 3), CreateSmart<CVectorReal>(miTotalSims, mdRate * mdBalBop));
		msThisPtr->mcNetCfPtr->add(mdtMarket.Add(PERIODS::YEAR, 4), CreateSmart<CVectorReal>(miTotalSims, (1.0 + mdRate) * mdBalBop));

		msThisPtr->mcOasPtr = CreateSmart<CVectorReal>();
		msThisPtr->mcRiskFreePtr = CreateSmart<CVectorReal>();
		msThisPtr->mcValuesPtr = CreateSmart<CVectorReal>();

		SPriceOasConfigPtr sConfigPtr(CreateSmart<SPriceOasConfig>());
		sConfigPtr->miSimsPerScenario = miTotalSims / miScenarios;
		sConfigPtr->mcDiscountPtr = CreateSmart<CYieldCurve>(mdtMarket, miTotalSims);

		real dDf(1.0);
		sConfigPtr->mcDiscountPtr->AddDiscountFactor(mdtMarket, mdtMarket.Add(PERIODS::YEAR, 1), dDf /= (1.0 + mdRate));
		sConfigPtr->mcDiscountPtr->AddDiscountFactor(mdtMarket, mdtMarket.Add(PERIODS::YEAR, 2), dDf /= (1.0 + mdRate));
		sConfigPtr->mcDiscountPtr->AddDiscountFactor(mdtMarket, mdtMarket.Add(PERIODS::YEAR, 3), dDf /= (1.0 + mdRate));
		sConfigPtr->mcDiscountPtr->AddDiscountFactor(mdtMarket, mdtMarket.Add(PERIODS::YEAR, 4), dDf /= (1.0 + mdRate));

		mpTest.Initialize(sConfigPtr);
	}

	void TearDown(void) {}
};

TEST_F(PPriceOASTest, OAS_2_Price)
{
	msThisPtr->msBondDefPtr->mOasCalcType = OAS_CALC_TYPE::OAS2PRICE;
	msThisPtr->msBondDefPtr->mdOas = mdOas;

	EXPECT_EQ(PROCESS_STATUS::DONE, mpTest.Process(msThisPtr));
	for (size_t iScenario(0); iScenario < miScenarios; iScenario++)
	{
		EXPECT_GT(mdTolerance, fabs((*msThisPtr->mcOasPtr)[iScenario] - mdOas));
		EXPECT_GT(mdTolerance, fabs((*msThisPtr->mcRiskFreePtr)[iScenario] - mdBalBop));
		EXPECT_GT(mdTolerance, fabs((*msThisPtr->mcValuesPtr)[iScenario] - mdValue));
	}
}

TEST_F(PPriceOASTest, Price_2_OAS)
{
	msThisPtr->msBondDefPtr->mOasCalcType = OAS_CALC_TYPE::PRICE2OAS;
	msThisPtr->msBondDefPtr->mdValue = mdValue;

	EXPECT_EQ(PROCESS_STATUS::DONE, mpTest.Process(msThisPtr));
	for (size_t iScenario(0); iScenario < miScenarios; iScenario++)
	{
		EXPECT_GT(mdTolerance, fabs((*msThisPtr->mcOasPtr)[iScenario] - mdOas));
		EXPECT_GT(mdTolerance, fabs((*msThisPtr->mcRiskFreePtr)[iScenario] - mdBalBop));
		EXPECT_GT(mdTolerance, fabs((*msThisPtr->mcValuesPtr)[iScenario] - mdValue));
	}
}
#endif