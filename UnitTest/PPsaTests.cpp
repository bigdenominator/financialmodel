#ifndef INCLUDE_H_PPSATEST
#define INCLUDE_H_PPSATEST

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "PPrepay.h"


class PsaTests : public testing::Test
{
public:
	const real mdTolerance = 0.0000001;
	const uint32_t miPeriods = 360;

	size_t	miSims;

	PPrepay			mpPsa;
	SGlobalsPtr		msGlobalsPtr;
	SPrepayPtr		msPsaPtr;

	ItrSelector<CDataTable>::iterator	mcPpmtItr;

	void AddCashflowDates(uint32_t inp_iAge)
	{
		msPsaPtr->mcCfPtr->add(msGlobalsPtr->mdtMarket.Add(PERIODS::MONTH, inp_iAge) + 1, CreateSmart<CVectorReal>(miSims));
		for (uint32_t iPeriod(inp_iAge + 1); iPeriod <= miPeriods; iPeriod++)
		{
			msPsaPtr->mcCfPtr->add(msGlobalsPtr->mdtMarket.Add(PERIODS::MONTH, iPeriod), CreateSmart<CVectorReal>(miSims));
		}
	}

	void SetUp(void)
	{
		msGlobalsPtr = CreateSmart<SGlobals>();
		msGlobalsPtr->mdtMarket = CDate(20111110);
		msGlobalsPtr->miScenarios = 1;
		msGlobalsPtr->miSimsPerScenario = 10;
		msGlobalsPtr->mcCalendarsPtr = CHolidays::GetAll(1950, 2099);

		mpPsa.Initialize();

		mcPpmtItr.attach(CreateSmart<CDataTable>());

		msPsaPtr = CreateSmart<SPrepay>();
		msPsaPtr->msBondDefPtr = CreateSmart<SBondDef>();
		msPsaPtr->mcCfPtr = CreateSmart<CDataTable>();
		msPsaPtr->mcXBetaPtr = mcPpmtItr.data();

		msPsaPtr->msBondDefPtr->miPayPeriods = miPeriods;

		miSims = msGlobalsPtr->miScenarios * msGlobalsPtr->miSimsPerScenario;
	}

	void TearDown(void) {}

	real Unwind(real inp_dXBeta)
	{
		real dSmm(CMath::exp(inp_dXBeta));
		dSmm /= (1 + dSmm);

		return fin::Smm2Cpr(dSmm);
	}
};

TEST_F(PsaTests, NewLoan)
{
	const uint32_t iAge(0);

	int32_t iPeriod(iAge + 1);
	real dPsaExpect, dPsaCalc;

	AddCashflowDates(iAge);

	EXPECT_EQ(PROCESS_STATUS::DONE, mpPsa.Process(msPsaPtr));
	EXPECT_EQ(miPeriods - iAge + 1, msPsaPtr->mcXBetaPtr->size());

	mcPpmtItr.begin();
	EXPECT_GT(mdTolerance, fabs(-13.0 - (*mcPpmtItr.value())[0]));
	for (mcPpmtItr++; !mcPpmtItr.isEnd(); mcPpmtItr++)
	{
		EXPECT_EQ(miSims, mcPpmtItr.value()->size());
		
		dPsaExpect = (iPeriod < 30 ? 0.002 * iPeriod : 0.06);
		dPsaCalc = Unwind((*mcPpmtItr.value())[0]);
		EXPECT_GT(mdTolerance, fabs(dPsaCalc - dPsaExpect));

		iPeriod++;
	}
}

TEST_F(PsaTests, SlightlySeasoned)
{
	const uint32_t iAge(15);

	int32_t iPeriod(iAge + 1);
	real dPsaExpect, dPsaCalc;

	AddCashflowDates(iAge);

	EXPECT_EQ(PROCESS_STATUS::DONE, mpPsa.Process(msPsaPtr));
	EXPECT_EQ(miPeriods - iAge + 1, msPsaPtr->mcXBetaPtr->size());

	mcPpmtItr.begin();
	EXPECT_GT(mdTolerance, fabs(-13.0 - (*mcPpmtItr.value())[0]));
	for (mcPpmtItr++; !mcPpmtItr.isEnd(); mcPpmtItr++)
	{
		EXPECT_EQ(miSims, mcPpmtItr.value()->size());

		dPsaExpect = (iPeriod < 30 ? 0.002 * iPeriod : 0.06);
		dPsaCalc = Unwind((*mcPpmtItr.value())[0]);
		EXPECT_GT(mdTolerance, fabs(dPsaCalc - dPsaExpect));

		iPeriod++;
	}
}

TEST_F(PsaTests, Seasoned)
{
	const uint32_t iAge(31);

	int32_t iPeriod(iAge + 1);
	real dPsaExpect, dPsaCalc;

	AddCashflowDates(iAge);

	EXPECT_EQ(PROCESS_STATUS::DONE, mpPsa.Process(msPsaPtr));
	EXPECT_EQ(miPeriods - iAge + 1, msPsaPtr->mcXBetaPtr->size());

	mcPpmtItr.begin();
	EXPECT_GT(mdTolerance, fabs(-13.0 - (*mcPpmtItr.value())[0]));
	for (mcPpmtItr++; !mcPpmtItr.isEnd(); mcPpmtItr++)
	{
		EXPECT_EQ(miSims, mcPpmtItr.value()->size());

		dPsaExpect = 0.06;
		dPsaCalc = Unwind((*mcPpmtItr.value())[0]);
		EXPECT_GT(mdTolerance, fabs(dPsaCalc - dPsaExpect));

		iPeriod++;
	}
}
#endif