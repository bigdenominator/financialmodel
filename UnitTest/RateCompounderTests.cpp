#ifndef INCLUDE_H_RATECOMPOUNDTESTS
#define INCLUDE_H_RATECOMPOUNDTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "FinTools.h"

class RateCmpTest : public testing::Test
{
public:
	typedef CRateCmpd<COMPOUNDTYPE::COMPOUND> cmpd_t;
	typedef CRateCmpd<COMPOUNDTYPE::CONTINUOUS> cont_t;
	typedef CRateCmpd<COMPOUNDTYPE::SIMPLE> smpl_t;

	const size_t	miSims = 1000;

	const real mdTolerance = 0.0000001;

	real mdPeriods;

	CVectorReal	mcPoint1;
	CVectorReal	mcRndmRates;
	
	CVectorReal	mcRndmDiscount;
	CVectorReal	mcRndmGrowth;

	CVectorReal	mcTemp;
	CVectorReal	mcTest;

	void SetUp(void)
	{
		mcPoint1 = CVectorReal(miSims, 0.1);

		CRandomNumberGenerator cRNG(12345);
		mcRndmRates = CVectorReal(miSims);
		cRNG.GetNext(mcRndmRates);
		mcRndmRates /= 10.0;

		mdPeriods = cRNG.GetNext() * 10;

		mcRndmGrowth = CVectorReal(std::exp(mcRndmRates * mdPeriods));
		mcRndmDiscount = CVectorReal(1.0 / mcRndmGrowth);

		mcTest = CVectorReal(miSims);
	}

	void TearDown(void) {}
};

TEST_F(RateCmpTest, CompoundCalcDiscountFactors)
{
	typedef cmpd_t	test_t;

	EXPECT_GT(mdTolerance, fabs(pow(1 + 0.01, -4.5) - test_t::CalcDiscountFactor(0.01, 4.5)));
	EXPECT_GT(mdTolerance, fabs(1.0 - test_t::CalcDiscountFactor(0.00, 4.5)));
	
	mcTest = test_t::CalcDiscountFactor(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - pow(1.0 + 0.1, -4.5)));
	}

	mcTest = test_t::CalcDiscountFactor(mcRndmRates, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::CalcDiscountFactor(mcRndmRates[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, CompoundCalcGrowthFactors)
{
	typedef cmpd_t	test_t;

	EXPECT_GT(mdTolerance, fabs(pow(1 + 0.02, 1.75) - test_t::CalcGrowthFactor(0.02, 1.75)));
	EXPECT_GT(mdTolerance, fabs(1.0 - test_t::CalcGrowthFactor(0.00, 1.75)));

	mcTest = test_t::CalcGrowthFactor(mcPoint1, 1.75);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - pow(1.0 + 0.1, 1.75)));
	}

	mcTest = test_t::CalcGrowthFactor(mcRndmRates, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::CalcGrowthFactor(mcRndmRates[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, CompoundImplyPeriodicRateFromDiscount)
{
	typedef cmpd_t	test_t;

	EXPECT_GT(mdTolerance, fabs(pow(0.9, -1.0 / 4.5) - 1.0 - test_t::ImplyPeriodicRateFromDiscount(0.9, 4.5)));
	EXPECT_GT(mdTolerance, fabs(0.0 - test_t::ImplyPeriodicRateFromDiscount(1.0, 4.5)));

	mcTest = test_t::ImplyPeriodicRateFromDiscount(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - pow(0.1, -1.0 / 4.5) + 1.0));
	}

	mcTest = test_t::ImplyPeriodicRateFromDiscount(mcRndmDiscount, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::ImplyPeriodicRateFromDiscount(mcRndmDiscount[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, CompoundImplyPeriodicRateFromGrowth)
{
	typedef cmpd_t	test_t;

	EXPECT_GT(mdTolerance, fabs(pow(0.9, 1.0 / 4.5) - 1.0 - test_t::ImplyPeriodicRateFromGrowth(0.9, 4.5)));
	EXPECT_GT(mdTolerance, fabs(0.0 - test_t::ImplyPeriodicRateFromGrowth(1.0, 4.5)));

	mcTest = test_t::ImplyPeriodicRateFromGrowth(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - pow(0.1, 1.0 / 4.5) + 1.0));
	}

	mcTest = test_t::ImplyPeriodicRateFromGrowth(mcRndmGrowth, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::ImplyPeriodicRateFromGrowth(mcRndmGrowth[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, CompoundConsistency)
{
	typedef cmpd_t	test_t;

	mcTest = test_t::ImplyPeriodicRateFromDiscount(test_t::CalcDiscountFactor(mcRndmRates, mdPeriods), mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - mcRndmRates[iSim]));
	}

	mcTest = test_t::ImplyPeriodicRateFromGrowth(test_t::CalcGrowthFactor(mcRndmRates, mdPeriods), mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - mcRndmRates[iSim]));
	}
}

TEST_F(RateCmpTest, ContinuousCalcDiscountFactors)
{
	typedef cont_t	test_t;

	EXPECT_GT(mdTolerance, fabs(CMath::exp(-0.01 * 4.5) - test_t::CalcDiscountFactor(0.01, 4.5)));
	EXPECT_GT(mdTolerance, fabs(1.0 - test_t::CalcDiscountFactor(0.00, 4.5)));
	
	mcTest = test_t::CalcDiscountFactor(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - CMath::exp(-0.1 * 4.5)));
	}
	
	mcTest = test_t::CalcDiscountFactor(mcRndmRates, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::CalcDiscountFactor(mcRndmRates[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, ContinuousCalcGrowthFactors)
{
	typedef cont_t	test_t;

	EXPECT_GT(mdTolerance, fabs(CMath::exp(0.02 * 1.75) - test_t::CalcGrowthFactor(0.02, 1.75)));
	EXPECT_GT(mdTolerance, fabs(1.0 - test_t::CalcGrowthFactor(0.00, 1.75)));

	mcTest = test_t::CalcGrowthFactor(mcPoint1, 1.75);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - CMath::exp(0.1 * 1.75)));
	}

	mcTest = test_t::CalcGrowthFactor(mcRndmRates, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::CalcGrowthFactor(mcRndmRates[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, ContinuousImplyPeriodicRateFromDiscount)
{
	typedef cont_t	test_t;

	EXPECT_GT(mdTolerance, fabs(CMath::ln(0.9) / -4.5 - test_t::ImplyPeriodicRateFromDiscount(0.9, 4.5)));
	EXPECT_GT(mdTolerance, fabs(0.0 - test_t::ImplyPeriodicRateFromDiscount(1.0, 4.5)));

	mcTest = test_t::ImplyPeriodicRateFromDiscount(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - CMath::ln(0.1) / -4.5));
	}

	mcTest = test_t::ImplyPeriodicRateFromDiscount(mcRndmDiscount, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::ImplyPeriodicRateFromDiscount(mcRndmDiscount[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, ContinuousImplyPeriodicRateFromGrowth)
{
	typedef cont_t	test_t;

	EXPECT_GT(mdTolerance, fabs(CMath::ln(0.9) / 4.5 - test_t::ImplyPeriodicRateFromGrowth(0.9, 4.5)));
	EXPECT_GT(mdTolerance, fabs(0.0 - test_t::ImplyPeriodicRateFromGrowth(1.0, 4.5)));

	mcTest = test_t::ImplyPeriodicRateFromGrowth(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - CMath::ln(0.1) / 4.5));
	}

	mcTest = test_t::ImplyPeriodicRateFromGrowth(mcRndmGrowth, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::ImplyPeriodicRateFromGrowth(mcRndmGrowth[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, ContinuousConsistency)
{
	typedef cont_t	test_t;

	mcTest = test_t::ImplyPeriodicRateFromDiscount(test_t::CalcDiscountFactor(mcRndmRates, mdPeriods), mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - mcRndmRates[iSim]));
	}

	mcTest = test_t::ImplyPeriodicRateFromGrowth(test_t::CalcGrowthFactor(mcRndmRates, mdPeriods), mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - mcRndmRates[iSim]));
	}
}

TEST_F(RateCmpTest, SimpleCalcDiscountFactors)
{
	typedef smpl_t	test_t;

	EXPECT_GT(mdTolerance, fabs(1.0 / (1 + 0.01 * 4.5) - test_t::CalcDiscountFactor(0.01, 4.5)));
	EXPECT_GT(mdTolerance, fabs(1.0 - test_t::CalcDiscountFactor(0.00, 4.5)));

	mcTest = test_t::CalcDiscountFactor(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - 1.0 / (1.0 + 0.1 * 4.5)));
	}

	mcTest = test_t::CalcDiscountFactor(mcRndmRates, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::CalcDiscountFactor(mcRndmRates[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, SimpleCalcGrowthFactors)
{
	typedef smpl_t	test_t;

	EXPECT_GT(mdTolerance, fabs((1 + 0.02 * 1.75) - test_t::CalcGrowthFactor(0.02, 1.75)));
	EXPECT_GT(mdTolerance, fabs(1.0 - test_t::CalcGrowthFactor(0.00, 1.75)));

	mcTest = test_t::CalcGrowthFactor(mcPoint1, 1.75);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - (1.0 + 0.1 * 1.75)));
	}

	mcTest = test_t::CalcGrowthFactor(mcRndmRates, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::CalcGrowthFactor(mcRndmRates[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, SimpleImplyPeriodicRateFromDiscount)
{
	typedef smpl_t	test_t;

	EXPECT_GT(mdTolerance, fabs((1.0 / 0.9 - 1.0) / 4.5 - test_t::ImplyPeriodicRateFromDiscount(0.9, 4.5)));
	EXPECT_GT(mdTolerance, fabs(0.0 - test_t::ImplyPeriodicRateFromDiscount(1.0, 4.5)));

	mcTest = test_t::ImplyPeriodicRateFromDiscount(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - (1.0 / 0.1 - 1.0) / 4.5));
	}

	mcTest = test_t::ImplyPeriodicRateFromDiscount(mcRndmDiscount, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::ImplyPeriodicRateFromDiscount(mcRndmDiscount[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, SimpleImplyPeriodicRateFromGrowth)
{
	typedef smpl_t	test_t;

	EXPECT_GT(mdTolerance, fabs((0.9 - 1.0) / 4.5 - test_t::ImplyPeriodicRateFromGrowth(0.9, 4.5)));
	EXPECT_GT(mdTolerance, fabs(0.0 - test_t::ImplyPeriodicRateFromGrowth(1.0, 4.5)));

	mcTest = test_t::ImplyPeriodicRateFromGrowth(mcPoint1, 4.5);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - (0.1 - 1.0) / 4.5));
	}

	mcTest = test_t::ImplyPeriodicRateFromGrowth(mcRndmGrowth, mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - test_t::ImplyPeriodicRateFromGrowth(mcRndmGrowth[iSim], mdPeriods)));
	}
}

TEST_F(RateCmpTest, SimpleConsistency)
{
	typedef smpl_t	test_t;

	mcTest = test_t::ImplyPeriodicRateFromDiscount(test_t::CalcDiscountFactor(mcRndmRates, mdPeriods), mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - mcRndmRates[iSim]));
	}
	mcTest = test_t::ImplyPeriodicRateFromGrowth(test_t::CalcGrowthFactor(mcRndmRates, mdPeriods), mdPeriods);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs(mcTest[iSim] - mcRndmRates[iSim]));
	}
}
#endif