#ifndef INCLUDE_H_INTERESTRATESHOCKTEST
#define INCLUDE_H_INTERESTRATESHOCKTEST

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "LiborIndexes.h"
#include "RateTools.h"

class RateToolsTests: public testing::Test
{
public:
	const CDate				mdtMarket = 20140101;

	CHolidayCalendarLstPtr	mcHolidaysPtr;
	SLiborLstsPtr			msLiborLstsPtr;
	CIndexQuoteLstPtr		mcQuotesPtr;
	CRateShockLstPtr		mcShocksPtr;

	void SetUp(void)
	{
		mcHolidaysPtr = CHolidays::GetAll(2010, 2050);
		msLiborLstsPtr = CreateSmart<SLiborLsts>();
		RateIndexes::LoadLiborLsts(false, *mcHolidaysPtr, *msLiborLstsPtr);

		mcQuotesPtr = CreateSmart<CIndexQuoteLst>();
		mcQuotesPtr->add(MARKET_INDEX::LIBOR_3M, 0.0025);
		mcQuotesPtr->add(MARKET_INDEX::EDF1, 0.004);
		mcQuotesPtr->add(MARKET_INDEX::SWAP_10Y, 0.05);

		mcShocksPtr = CreateSmart<CRateShockLst>();
		
		CRateShockPtr cRateShockPtr;
		SRateShockPointPtr sPointPtr;

		cRateShockPtr = CreateSmart<CRateShock>(1);
		(*cRateShockPtr)[0] = CreateSmart<SRateShockPoint>();
		(*cRateShockPtr)[0]->mUnitType = PERIODS::MONTH;
		(*cRateShockPtr)[0]->miUnitCount = 3;
		(*cRateShockPtr)[0]->mdShock = 0.01;

		mcShocksPtr->add(1, cRateShockPtr);
	}

	void TearDown(void) {}
};

TEST_F(RateToolsTests, ApplyShocks)
{
	const real mdTolerance = std::numeric_limits<double>::epsilon();
	const size_t iSims(2);

	CYieldCurvePtr mcYcPtr(CreateSmart<CYieldCurve>(mdtMarket, iSims));
	mcYcPtr->AddZeroRate(mcYcPtr->GetAsOfDate().Add(PERIODS::YEAR, 1), 0.05);
	mcYcPtr->AddZeroRate(mcYcPtr->GetAsOfDate().Add(PERIODS::YEAR, 1), 0.06);
	auto itrBase(iterators::GetItr(mcYcPtr->AsZeroRates()));

	CYieldCurvePtr mcShockPtr(CreateSmart<CYieldCurve>(mdtMarket, iSims));
	EXPECT_TRUE(RateTools::BuildShocks(*mcShocksPtr, *mcShockPtr));

	*mcYcPtr += *mcShockPtr;
	auto itrTest(iterators::GetItr(mcYcPtr->AsZeroRates()));

	EXPECT_EQ(itrBase.data()->size(), itrTest.data()->size());
	for (itrBase.begin(), itrTest.begin(); !itrBase.isEnd(); itrBase++, itrTest++)
	{
		EXPECT_GT(mdTolerance, fabs((*itrTest.value())[0] - (*itrBase.value())[0] - 0.00));
		EXPECT_GT(mdTolerance, fabs((*itrTest.value())[1] - (*itrBase.value())[1] - 0.01));
	}
}

TEST_F(RateToolsTests, Bootstrap)
{
	const real dTol(0.000001);
	
	CVectorRealPtr cRatesPtr(CreateSmart<CVectorReal>(1));
	CYieldCurvePtr cCurvePtr(CreateSmart<CYieldCurve>(mdtMarket, 1));

	RateTools::InitializeRates(*mcQuotesPtr, *msLiborLstsPtr, *cCurvePtr);

	(*msLiborLstsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_3M]->SetSchedule(mdtMarket);
	(*msLiborLstsPtr->mcLiborLstPtr)[MARKET_INDEX::LIBOR_3M]->ImputeAnnualizedRates(*cCurvePtr, *cRatesPtr);
	EXPECT_GT(dTol, fabs((*cRatesPtr)[0] - 0.0025));

	(*msLiborLstsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF1]->SetSchedule(mdtMarket);
	(*msLiborLstsPtr->mcEdfLstPtr)[MARKET_INDEX::EDF1]->ImputeAnnualizedRates(*cCurvePtr, *cRatesPtr);
	EXPECT_GT(dTol, fabs((*cRatesPtr)[0] - 0.004));

	(*msLiborLstsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_10Y]->SetSchedule(mdtMarket);
	(*msLiborLstsPtr->mcSwapLstPtr)[MARKET_INDEX::SWAP_10Y]->ImputeAnnualizedRates(*cCurvePtr, *cRatesPtr);
	EXPECT_GT(dTol, fabs((*cRatesPtr)[0] - 0.05));
}

TEST_F(RateToolsTests, BuildShock)
{
	const real mdTolerance = std::numeric_limits<double>::epsilon();
	const size_t iSims(2);

	CYieldCurvePtr mcYcPtr(CreateSmart<CYieldCurve>(mdtMarket, iSims));
	
	EXPECT_TRUE(RateTools::BuildShock(1, *mcShocksPtr->at(1), *mcYcPtr));

	auto itrTest(iterators::GetItr(mcYcPtr->AsZeroRates()));
	EXPECT_EQ(1, itrTest.data()->size());

	itrTest.begin();
	EXPECT_GT(mdTolerance, fabs((*itrTest.value())[0] - 0.00));
	EXPECT_GT(mdTolerance, fabs((*itrTest.value())[1] - 0.01));
}
#endif
