#ifndef INCLUDE_H_SCHEMATICSTESTS
#define INCLUDE_H_SCHEMATICSTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "RateSchematics.h"
#include "Schematics.h"
#include "UserSchematics.h"

class SchematicsTest : public testing::Test
{
public:
	typedef CSmartMap<TYPELIST_t, string>	MySerialLst;
	typedef CSmartPtr<MySerialLst>			MySerialLstPtr;
	typedef MapItr<MySerialLst>				MySerialLstItr;

	MySerialLstItr	mcSerialLstItr;

	void SetUp(void)
	{
		mcSerialLstItr.attach(CreateSmart<MySerialLst>());
	}

	template<typename T> void RunTest(TYPELIST_t inp_iType)
	{
		CSmartPtr<T> sObj1Ptr(CreateSmart<T>());
		CSmartPtr<T> sObj2Ptr(CreateSmart<T>());

		SSerial sBefore;
		SSerial sAfter;

		if (mcSerialLstItr.find(inp_iType))
		{
			sBefore.msBuffer = mcSerialLstItr->second;

			sObj1Ptr->Deserialize(sBefore);
			CopyViaPack(*sObj1Ptr, *sObj2Ptr);
			sObj2Ptr->Serialize(sAfter);

			EXPECT_EQ(sBefore.msBuffer, sAfter.msBuffer);
		}
	}
};

static	const vector<string>	msTestIndxList = { string("SGLOBALS"), string("SBOND"), string("SBONDBEHAVIOR"), string("SBONDCONTRACT"), string("SBONDDEF"),
                                                   string("SCOUNTERPARTY"), string("SDEFAULT"), string("SDEFAULTCONFIG"), string("SHPI"), string("SHPICONFIG"),
                                                   string("SHPIPCACONFIG"), string("SIRP"), string("SIRPCONFIG"), string("SLMMCONFIG"), string("SLTV"),
                                                   string("SPRICEOAS"), string("SPROPERTY"), string("SREFITURNOVER"), string("SREFITURNOVERCONFIG"),
                                                   string("SSEVERITY"), string("SSEVERITYCONFIG"), string("SWORKFLOW") };

// Standard
TEST_F(SchematicsTest, SBondTest)
{
	RunTest<SBond>(TYPELIST::SBOND);
}

TEST_F(SchematicsTest, SBondBehaviorTest)
{
	RunTest<SBondBehavior>(TYPELIST::SBONDBEHAVIOR);
}

TEST_F(SchematicsTest, SBondContractTest)
{
	RunTest<SBondContract>(TYPELIST::SBONDCONTRACT);
}

TEST_F(SchematicsTest, SBondDefTest)
{
	RunTest<SBondDef>(TYPELIST::SBONDDEF);
}

TEST_F(SchematicsTest, SBondCounterPartyTest)
{
	RunTest<SCounterparty>(TYPELIST::SCOUNTERPARTY);
}

TEST_F(SchematicsTest, SDefaultTest)
{
	RunTest<SDefault>(TYPELIST::SDEFAULT);
}

TEST_F(SchematicsTest, SGlobalsTest)
{
	RunTest<SGlobals>(TYPELIST::SGLOBALS);
}

TEST_F(SchematicsTest, SIrpTest)
{
	RunTest<SIrp>(TYPELIST::SIRP);
}

TEST_F(SchematicsTest, SPrepayTest)
{
	RunTest<SPrepay>(TYPELIST::SPREPAY);
}

TEST_F(SchematicsTest, SPriceOASTest)
{
	RunTest<SPriceOAS>(TYPELIST::SPRICEOAS);
}

TEST_F(SchematicsTest, SPropertyTest)
{
	RunTest<SProperty>(TYPELIST::SPROPERTY);
}


TEST_F(SchematicsTest, SSeverityTest)
{
	RunTest<SSeverity>(TYPELIST::SSEVERITY);
}


// IRP
TEST_F(SchematicsTest, SIrpConfigTest)
{
	RunTest<SIrpConfig>(IRP_TYPELIST::SIRPCONFIG);
}

TEST_F(SchematicsTest, SRebonatoTest)
{
	RunTest<SRebonato>(IRP_TYPELIST::SREBONATO);
}


// User
TEST_F(SchematicsTest, SMyDefaultTest)
{
	RunTest<SMyDefault>(TYPELIST::SDEFAULT);
}

TEST_F(SchematicsTest, SMyDefaultSettingTest)
{
	RunTest<SMyDefaultConfig>(USER_TYPELIST::SDEFAULTCONFIG);
}

TEST_F(SchematicsTest, SHpiTest)
{
	RunTest<SHpi>(USER_TYPELIST::SHPI);
}

TEST_F(SchematicsTest, SHpiPcaTest)
{
	RunTest<SHpiPca>(USER_TYPELIST::SHPIPCA);
}

TEST_F(SchematicsTest, SHpiConfigTest)
{
	RunTest<SHpiConfig>(USER_TYPELIST::SHPICONFIG);
}

TEST_F(SchematicsTest, SHpiPcaConfigTest)
{
	RunTest<SHpiPcaConfig>(USER_TYPELIST::SHPIPCACONFIG);
}

TEST_F(SchematicsTest, SMyPrepayConfigTest)
{
	RunTest<SRtppConfig>(USER_TYPELIST::SPREPAYCONFIG);
}

TEST_F(SchematicsTest, SMyPrepayTest)
{
	RunTest<SRtpp>(TYPELIST::SPREPAY);
}

TEST_F(SchematicsTest, SMySeverityConfigTest)
{
	RunTest<SMySeverityConfig>(USER_TYPELIST::SSEVERITYCONFIG);
}

TEST_F(SchematicsTest, SMySeverityTest)
{
	RunTest<SSeverity>(TYPELIST::SSEVERITY);
}
#endif