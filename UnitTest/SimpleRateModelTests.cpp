#ifndef INCLUDE_H_CONSTRATEMODELTESTS
#define INCLUDE_H_CONSTRATEMODELTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "ConstantRateModel.h"
#include "ForwardRollModel.h"

class SimpleRateModelTests : public testing::Test
{
public:
	const size_t miSims = 10;

	const real		mdTolerance = 0.0000001;
	const CDate		mdtMarket = 20130102;

	CYieldCurvePtr		mcBootstrapPtr;
	CYieldCurvePtr		mcDiscountPtr;
	CYieldCurveLstPtr	mcCurvesPtr;

	void SetUp(void)
	{
		mcBootstrapPtr = CreateSmart<CYieldCurve>(mdtMarket, 1);
		mcBootstrapPtr->AddZeroRate(mdtMarket.Add(PERIODS::YEAR, 1), 0.1);
		mcBootstrapPtr->AddZeroRate(mdtMarket.Add(PERIODS::YEAR, 2), 0.2);

		mcDiscountPtr = CreateSmart<CYieldCurve>(mdtMarket, miSims);
		mcCurvesPtr = CreateSmart<CYieldCurveLst>();
	}

	void TearDown(void) {}
};

TEST_F(SimpleRateModelTests, Constant)
{
	CreateSmart<CConstantRates>()->Run(*mcBootstrapPtr, *mcDiscountPtr, *mcCurvesPtr);

	CVectorRealPtr cRatesPtr(CreateSmart<CVectorReal>(miSims));

	EXPECT_EQ(1, mcCurvesPtr->size());

	// first and only yield curve
	CYieldCurvePtr cYcPtr(mcCurvesPtr->front().second);

	EXPECT_TRUE(cYcPtr->GetSims() == miSims);
	EXPECT_TRUE(cYcPtr->GetAsOfDate() == mdtMarket);
	
	*cRatesPtr = cYcPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 1));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.1));
	}

	*cRatesPtr = cYcPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 2));
	for (long iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.2));
	}

	// path discount curve
	EXPECT_TRUE(mcDiscountPtr->GetSims() == miSims);
	EXPECT_TRUE(mcDiscountPtr->GetAsOfDate() == mdtMarket);

	*cRatesPtr = mcDiscountPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 1));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.1));
	}

	*cRatesPtr = mcDiscountPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 2));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.2));
	}
}

TEST_F(SimpleRateModelTests, ForwardRoll)
{
	CreateSmart<CForwardRoll>()->Run(*mcBootstrapPtr, *mcDiscountPtr, *mcCurvesPtr);

	CVectorRealPtr cRatesPtr(CreateSmart<CVectorReal>(miSims));

	EXPECT_EQ(2, mcCurvesPtr->size());

	// first
	CYieldCurvePtr cYcPtr(mcCurvesPtr->front().second);

	EXPECT_EQ(miSims, cYcPtr->GetSims());
	EXPECT_TRUE(cYcPtr->GetAsOfDate() == mdtMarket);

	*cRatesPtr = cYcPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 1));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.1));
	}

	*cRatesPtr = cYcPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 2));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.2));
	}

	// next
	cYcPtr = mcCurvesPtr->upper_bound(mdtMarket)->second;
	EXPECT_EQ(miSims, cYcPtr->GetSims());
	EXPECT_TRUE(cYcPtr->GetAsOfDate() == mdtMarket.Add(PERIODS::YEAR, 1));

	*cRatesPtr = cYcPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 2));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.3));
	}

	// path discount curve
	EXPECT_EQ(miSims, cYcPtr->GetSims());
	EXPECT_TRUE(mcDiscountPtr->GetAsOfDate() == mdtMarket);

	*cRatesPtr = mcDiscountPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 1));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.1));
	}

	*cRatesPtr = mcDiscountPtr->GetZeroRates(mdtMarket.Add(PERIODS::YEAR, 2));
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*cRatesPtr)[iSim] - 0.2));
	}
}
#endif