#ifndef INCLUDE_H_YEARFRACTESTS
#define INCLUDE_H_YEARFRACTESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "FinTools.h"

// http://www.deltaquants.com/day-count-conventions.html
// http://eclipsesoftware.biz/DayCountConventions.html
// http://www.opengamma.com/sites/default/files/interest-rate-instruments-and-market-conventions.pdf

TEST(YearFrac, Actual360)
{
	const real dTolerance = 0.0000001;

	CYearFracPtr cYearFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_360, SPeriod(FREQUENCY::SEMIANNUAL)));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071228), CDate(20080228)) - 0.172222222222222));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071228), CDate(20080229)) - 0.175));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071031), CDate(20081130)) - 1.1000000000000));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20080201), CDate(20090531)) - 1.34722222222222));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080228)) - 0.172222222222222));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080229)) - 0.175));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071031), CDate(20071031).Add(PERIODS::MONTH, 6), CDate(20081130)) - 1.1000000000000));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20080201), CDate(20080201).Add(PERIODS::MONTH, 6), CDate(20090531)) - 1.34722222222222));
}

TEST(YearFrac, Actual365F)
{
	const real dTolerance = 0.0000001;

	CYearFracPtr cYearFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_365F, SPeriod(FREQUENCY::SEMIANNUAL)));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071228), CDate(20080228)) - 0.16986301369863));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071228), CDate(20080229)) - 0.172602739726027));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071031), CDate(20081130)) - 1.08493150684932));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20080201), CDate(20090531)) - 1.32876712328767));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080228)) - 0.16986301369863));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080229)) - 0.172602739726027));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071031), CDate(20071031).Add(PERIODS::MONTH, 6), CDate(20081130)) - 1.08493150684932));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20080201), CDate(20080201).Add(PERIODS::MONTH, 6), CDate(20090531)) - 1.32876712328767));
}

TEST(YearFrac, ActualActualICMA)
{
	const real dTolerance = 0.0000001;

	CYearFracPtr cYearFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_ACT_ICMA, SPeriod(FREQUENCY::SEMIANNUAL)));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20070815), CDate(20080215)) - 0.5));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20070815), CDate(20080215), CDate(20070823)) - 8.0 * 0.5 / 184.0));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20031101), CDate(20040501)) - 0.5));
}

TEST(YearFrac, ActualActualISDA)
{
	const real dTolerance = 0.0000001;

	CYearFracPtr cYearFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_ACT_ISDA, SPeriod(FREQUENCY::SEMIANNUAL)));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071215), CDate(20080110)) - 0.07116551));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071215), CDate(20080110), CDate(20080110)) - 0.07116551));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071215), CDate(20080110), CDate(20071215)) - 0.0));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20101230), CDate(20110102)) - 3.0 / 365.0));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20111230), CDate(20120102)) - (2.0 / 365.0 + 1.0 / 366.0)));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20101230), CDate(20130102)) - (2.0 / 365.0 + 2.0 + 1.0 / 365.0)));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20031101), CDate(20040501)) - (61.0 / 365.0 + 121.0 / 366.0)));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071228), CDate(20080228)) - 0.16942884946478));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071228), CDate(20080229)) - 0.172161089901939));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20071031), CDate(20081130)) - 1.08243131970956));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20080201), CDate(20090531)) - 1.32625945055768));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080228)) - 0.16942884946478));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080229)) - 0.172161089901939));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071031), CDate(20071031).Add(PERIODS::MONTH, 6), CDate(20081130)) - 1.08243131970956));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20080201), CDate(20080201).Add(PERIODS::MONTH, 6), CDate(20090531)) - 1.32625945055768));
	
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20101230), CDate(20130102)) + cYearFracPtr->GetCoupon(CDate(20130102), CDate(20101230))));
}

TEST(YearFrac, BondBasis)
{
	const real dTolerance = 0.0000001;

	CYearFracPtr cYearFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::BOND_BASIS, SPeriod(FREQUENCY::SEMIANNUAL)));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20080228).Add(PERIODS::MONTH, 6), CDate(20080228)) - 0.166666666666667));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080229)) - 0.169444444444444));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071031), CDate(20071031).Add(PERIODS::MONTH, 6), CDate(20081130)) - 1.08333333333333));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20080201), CDate(20080201).Add(PERIODS::MONTH, 6), CDate(20090531)) - 1.33333333333333));

	cYearFracPtr = CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::BOND_BASIS, SPeriod(FREQUENCY::MONTHLY));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20090315), CDate(20090315).Add(PERIODS::MONTH, 1), CDate(20090401)) - 16.0 / 360.0));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetCoupon(CDate(20090315), CDate(20090315).Add(PERIODS::MONTH, 1)) - 30.0 / 360.0));
}

TEST(YearFrac, EurobondBasis)
{
	const real dTolerance = 0.0000001;

	CYearFracPtr cYearFracPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::EUROBOND_BASIS, SPeriod(FREQUENCY::SEMIANNUAL)));

	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080228)) - 0.166666666666667));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071228), CDate(20071228).Add(PERIODS::MONTH, 6), CDate(20080229)) - 0.169444444444444));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20071031), CDate(20071031).Add(PERIODS::MONTH, 6), CDate(20081130)) - 1.08333333333333));
	EXPECT_GT(dTolerance, fabs(cYearFracPtr->GetYearFrac(CDate(20080201), CDate(20080201).Add(PERIODS::MONTH, 6), CDate(20090531)) - 1.33055555555556));
}
#endif