#ifndef INCLUDE_H_YIELDCURVETESTS
#define INCLUDE_H_YIELDCURVETESTS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "unittest/UnitTestDefs.h"

#include "YieldCurve.h"

class YieldCurveTests : public testing::Test
{
public:
	const size_t miSims = 10;

	const real		mdTolerance = 0.00000001;
	const CDate		mdtMarket = CDate(20140101);

	CDate			mdt1;
	CDate			mdt2;

	real			mdFrac1;
	real			mdFrac2;

	CYieldCurvePtr	mcYc1Ptr;
	CYieldCurvePtr	mcYc2Ptr;

	CVectorRealPtr	mcSampleDataPtr;
	CVectorRealPtr	mcTempPtr;
	CVectorRealPtr	mcTestPtr;

	void SetUp(void)
	{
		mdt1 = mdtMarket.Add(PERIODS::YEAR, 1);
		mdt2 = mdtMarket.Add(PERIODS::YEAR, 2);

		CYearFracPtr cYfPtr(CFinTools::CreateYearFrac(DAY_COUNT_CONVENTION::ACT_ACT_ISDA, SPeriod(FREQUENCY::SINGLE)));
		mdFrac1 = cYfPtr->GetYearFrac(mdtMarket, mdt1, mdt1);
		mdFrac2 = cYfPtr->GetYearFrac(mdtMarket, mdt2, mdt2);

		mcYc1Ptr = CreateSmart<CYieldCurve>(mdtMarket, miSims);
		mcYc1Ptr->AddZeroRate(mdt1, 0.05);

		mcYc2Ptr = CreateSmart<CYieldCurve>(mdtMarket, miSims);
		mcYc2Ptr->AddZeroRate(mdt2, 0.06);

		mcSampleDataPtr = CreateSmart<CVectorReal>(miSims);
		for (size_t iSim(0); iSim < miSims; iSim++)
		{
			(*mcSampleDataPtr)[iSim] = 0.5 + static_cast<real>(iSim) * 0.25 / static_cast<real>(miSims);
		}

		mcTestPtr = CreateSmart<CVectorReal>(miSims);
		mcTempPtr = CreateSmart<CVectorReal>(miSims);
	}

	void TearDown(void) {}
};

TEST_F(YieldCurveTests, CreateInstance)
{
	CYieldCurvePtr cTestPtr(CreateSmart<CYieldCurve>(CDate(20140101), 10));

	EXPECT_TRUE(cTestPtr->GetAsOfDate() == CDate(20140101));
	EXPECT_TRUE(cTestPtr->GetSims() == 10);
}

TEST_F(YieldCurveTests, OperatorDiffEq)
{
	*mcYc2Ptr -= *mcYc1Ptr;
	CDataTablePtr cDataPtr(mcYc2Ptr->AsZeroRates());
	auto itr(iterators::GetItr(cDataPtr));

	EXPECT_TRUE(cDataPtr->size() == 1);
	EXPECT_TRUE(itr.key() == mdt2);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - 0.01));
	}
}

TEST_F(YieldCurveTests, OperatorEqual)
{
	*mcYc2Ptr = *mcYc1Ptr;

	SSerial s1, s2;
	mcYc1Ptr->Serialize(s1);
	mcYc2Ptr->Serialize(s2);
	EXPECT_EQ(s1.msBuffer, s2.msBuffer);
}

TEST_F(YieldCurveTests, OperatorSumEq)
{
	*mcYc1Ptr += *mcYc2Ptr;
	CDataTablePtr cDataPtr(mcYc1Ptr->AsZeroRates());
	auto itr(iterators::GetItr(cDataPtr));

	EXPECT_TRUE(cDataPtr->size() == 1);
	EXPECT_TRUE(itr.key() == mdt1);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - 0.11));
	}
}

TEST_F(YieldCurveTests, AddDiscountFactor)
{
	mcYc1Ptr->AddDiscountFactor(mdt1, mdt2, 0.8);

	mcYc1Ptr->GetDiscountFactors(mdt2, *mcTestPtr);
	mcYc1Ptr->GetDiscountFactors(mdt1, *mcTempPtr);
	*mcTestPtr /= *mcTempPtr;

	for (size_t iSim(0); iSim < mcYc1Ptr->GetSims(); iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[iSim] - 0.8));
	}
}

TEST_F(YieldCurveTests, AddDiscountFactors)
{
	mcYc1Ptr->AddDiscountFactors(mdt1, mdt2, *mcSampleDataPtr);

	mcYc1Ptr->GetDiscountFactors(mdt2, *mcTestPtr);
	mcYc1Ptr->GetDiscountFactors(mdt1, *mcTempPtr);
	*mcTestPtr /= *mcTempPtr;

	for (size_t iSim(0); iSim < mcYc1Ptr->GetSims(); iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[iSim] - (*mcSampleDataPtr)[iSim]));
	}
}

TEST_F(YieldCurveTests, AddSpread)
{
	CDataTablePtr cDataPtr(mcYc1Ptr->AddSpread(*mcYc2Ptr).AsZeroRates());
	auto itr(iterators::GetItr(cDataPtr));

	EXPECT_TRUE(cDataPtr->size() == 1);
	EXPECT_TRUE(itr.key() == mdt1);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - 0.11));
	}
}

TEST_F(YieldCurveTests, AddZeroRate)
{
	// validate that SetUp data was added
	mcYc1Ptr->GetZeroRates(mdt1, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - 0.05));

	mcYc2Ptr->GetZeroRates(mdt2, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - 0.06));
}

TEST_F(YieldCurveTests, AddZeroRates)
{
	mcYc1Ptr->AddZeroRates(mdt2, *mcSampleDataPtr);

	mcYc1Ptr->GetZeroRates(mdt2, *mcTestPtr);
	for (size_t iSim(0); iSim < mcYc1Ptr->GetSims(); iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[iSim] - (*mcSampleDataPtr)[iSim]));
	}
}

TEST_F(YieldCurveTests, ExpandSimulations)
{
	const size_t iSimCntOrig(mcYc1Ptr->GetSims());

	mcYc1Ptr->AddDiscountFactors(mdtMarket, mdt2, *mcSampleDataPtr);
	
	CYieldCurve cYc(*mcYc1Ptr);
	cYc.ExpandSimulations(10);
	
	CVectorRealPtr cTestPtr(CreateSmart<CVectorReal>(cYc.GetSims()));
	cYc.GetDiscountFactors(mdt2, *cTestPtr);

	const size_t iSimCntNew(cYc.GetSims());
	EXPECT_TRUE(iSimCntNew == 10 * iSimCntOrig);
	EXPECT_TRUE(cYc.GetSims() == iSimCntNew);
	EXPECT_TRUE(cTestPtr->size() == iSimCntNew);
	EXPECT_TRUE(mcYc1Ptr->GetSims() == iSimCntOrig);
	EXPECT_TRUE(mcSampleDataPtr->size() == iSimCntOrig);

	size_t lMod;
	for (size_t iSim(0); iSim < iSimCntNew; iSim++)
	{
		lMod = (iSim - (iSim % 10)) / 10; 
		EXPECT_GT(mdTolerance, fabs((*cTestPtr)[iSim] - (*mcSampleDataPtr)[lMod]));
	}
}

TEST_F(YieldCurveTests, GetAsOfDate)
{
	EXPECT_TRUE(mcYc1Ptr->GetAsOfDate() == mdtMarket);
}

TEST_F(YieldCurveTests, AsZeroRates)
{
	mcYc1Ptr->AddZeroRates(mdt2, *mcSampleDataPtr);

	CDataTablePtr cDataPtr(mcYc1Ptr->AsZeroRates());
	auto itr(iterators::GetItr(cDataPtr));

	EXPECT_TRUE(cDataPtr->size() == 2);

	itr.begin();
	EXPECT_TRUE(itr.key() == mdt1);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - 0.05));
	}

	itr++;
	EXPECT_TRUE(itr.key() == mdt2);
	for (size_t iSim(0); iSim < mcYc1Ptr->GetSims(); iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - (*mcSampleDataPtr)[iSim]));
	}
}

TEST_F(YieldCurveTests, GetDiscountFactors)
{
	mcYc1Ptr->GetDiscountFactors(mdt1, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - exp(-0.05 * mdFrac1)));
	
	mcYc1Ptr->GetDiscountFactors(mdt2, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - exp(-0.05 * mdFrac2)));


	mcYc2Ptr->GetDiscountFactors(mdt1, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - exp(-0.06 * mdFrac1)));

	mcYc2Ptr->GetDiscountFactors(mdt2, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - exp(-0.06 * mdFrac2)));
}

TEST_F(YieldCurveTests, GetSims)
{
	EXPECT_TRUE(mcYc1Ptr->GetSims() == miSims);
}

TEST_F(YieldCurveTests, GetZeroRates)
{
	mcYc1Ptr->GetZeroRates(mdt1, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - 0.05));

	mcYc1Ptr->GetZeroRates(mdt2, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - 0.05));


	mcYc2Ptr->GetZeroRates(mdt1, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - 0.06));

	mcYc2Ptr->GetZeroRates(mdt2, *mcTestPtr);
	EXPECT_GT(mdTolerance, fabs((*mcTestPtr)[0] - 0.06));
}

TEST_F(YieldCurveTests, SubtractSpread)
{
	CDataTablePtr cDataPtr(mcYc2Ptr->SubtractSpread(*mcYc1Ptr).AsZeroRates());
	auto itr(iterators::GetItr(cDataPtr));

	EXPECT_TRUE(cDataPtr->size() == 1);
	EXPECT_TRUE(itr.key() == mdt2);
	for (size_t iSim(0); iSim < miSims; iSim++)
	{
		EXPECT_GT(mdTolerance, fabs((*itr.value())[iSim] - 0.01));
	}
}
#endif