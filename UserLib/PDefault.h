#ifndef INCLUDE_H_PDEFAULT
#define INCLUDE_H_PDEFAULT

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "UserSchematics.h"

static const string sBadDefaultSubmodelLookup("No submodel found");

SIMPLESTRUCT(SDefaultPkt,
	(MyDefaultCoefPtr	msModelPtr;
	SMyDefaultPtr		msMyPtr;

	CUniquePtr<CVectorReal>	mcPriorPmtPtr;
	CUniquePtr<CVectorReal>	mcThisPmtPtr;
	CUniquePtr<CVectorReal>	mcThisLtvPtr;
	CUniquePtr<CVectorReal>	mcThisPayShockPtr;

	size_t		miSims;
	real		mdConst;
	uint32_t	miAge;));

DEF_NODE(PDefault, SMyDefault, (), (), (SMyDefaultConfig))
{
public:
	bool Initialize(void)
	{
		SMyDefaultConfigPtr	sConfigPtr;

		if (!View(sConfigPtr)) return false;
		
		return Initialize(sConfigPtr);
	}

	bool Initialize(SMyDefaultConfigPtr inp_sConfigPtr)
	{
		mcModelsItr.attach(inp_sConfigPtr->mcModelDataPtr);

		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		if (!mcModelsItr.find(inp_sSchematicPtr->msBondDefPtr->mDfltSubModel))
		{
			FEEDBACK(GetFeedback(), 5001, sBadDefaultSubmodelLookup + ": BondID(" + to_string(inp_sSchematicPtr->msBondDefPtr->mlBondId) + ") DefaultSubmodel(" + to_string(inp_sSchematicPtr->msBondDefPtr->mDfltSubModel) + ")");
			return PROCESS_STATUS::FAIL;
		}
		else
		{
			SDefaultPktPtr	msShelfPtr(CreateSmart<SDefaultPkt>());
			msShelfPtr->msModelPtr = mcModelsItr.value();
			msShelfPtr->msMyPtr = inp_sSchematicPtr;

			initialize(*msShelfPtr);
			
			return finalize(msShelfPtr);
		}
	}

	static inline void age(SDefaultPkt& inp_sShelf)
	{
		inp_sShelf.miAge++;
	}

	static inline real calcConstTerm(const SBondDef& inp_sBondDef, const SCounterparty& inp_sCounterparty, const SProperty& inp_sProperty, const MyDefaultCoef& inp_Default)
	{
		return inp_Default[MyDefault::INTERCEPT]
			+ (inp_sBondDef.mStatus == LOANSTATUS::CURRENT ? 0.0 : inp_Default[MyDefault::DLQ_ADJ])
			+ calcLoanOriginatorTypeTerm(inp_sBondDef.mLoanOriginatorType, inp_Default)
			+ calcLoanPurposeTypeTerm(inp_sBondDef.mLoanPurposeType, inp_Default)
			+ calcProjectLegalStructureTypeTerm(inp_sProperty.mProjectLegalStructureType, inp_Default)
			+ calcPropertyUsageTypeTerm(inp_sProperty.mPropertyUsageType, inp_Default)
			+ calcFicoTerm(inp_sCounterparty, inp_Default)
			;
	}

	static inline real calcFicoTerm(const SCounterparty& inp_sCounterparty, const MyDefaultCoef& inp_Default)
	{
		return inp_Default[MyDefault::FICO] * transformFico(inp_sCounterparty.mdFico);
	}

	static inline auto calcFirstPmt(const CDataTable& inp_cInt, const CDataTable& inp_cPrin) -> decltype(*(++inp_cInt.begin())->second + *(++inp_cPrin.begin())->second)
	{
			return *(++inp_cInt.begin())->second + *(++inp_cPrin.begin())->second;
	}

	static inline real calcLoanOriginatorTypeTerm(LOANORIGINATORTYPE inp_LoanOriginatorType, const MyDefaultCoef& inp_Default)
	{
		switch (inp_LoanOriginatorType)
		{
		case LOANORIGINATORTYPE::BROKER:		return inp_Default[MyDefault::BROKER];
		case LOANORIGINATORTYPE::CORRESPONDENT:	return inp_Default[MyDefault::CORR];
		default:								return inp_Default[MyDefault::LENDER];
		}
	}

	static inline real calcLoanPurposeTypeTerm(LOANPURPOSETYPE inp_mLoanPurposeType, const MyDefaultCoef& inp_Default)
	{
		switch (inp_mLoanPurposeType)
		{
		case LOANPURPOSETYPE::PURCHASE:	return inp_Default[MyDefault::PURCHASE];
		default:						return inp_Default[MyDefault::REFI];
		}
	}

	static inline real calcProjectLegalStructureTypeTerm(PROJECTLEGALSTRUCTURETYPE inp_ProjectLegalStructureType, const MyDefaultCoef& inp_Default)
	{
		switch (inp_ProjectLegalStructureType)
		{
		case PROJECTLEGALSTRUCTURETYPE::COOPERATIVE:	return inp_Default[MyDefault::COOP];
		case PROJECTLEGALSTRUCTURETYPE::CONDOMINIUM:	return inp_Default[MyDefault::CONDO];
		default:										return inp_Default[MyDefault::STANDARD];
		}
	}

	static inline real calcPropertyUsageTypeTerm(PROPERTYUSAGETYPE inp_PropertyUsageType, const MyDefaultCoef& inp_Default)
	{
		switch (inp_PropertyUsageType)
		{
		case PROPERTYUSAGETYPE::INVESTMENT:	return inp_Default[MyDefault::INVESTMENT];
		case PROPERTYUSAGETYPE::SECONDHOME:	return inp_Default[MyDefault::SECOND];
		default:							return inp_Default[MyDefault::PRIMARY];
		}
	}

	static inline real calcStaticTerm(const SDefaultPkt& inp_sShelf)
	{
		return inp_sShelf.mdConst + (*inp_sShelf.msModelPtr)[MyDefault::AGE] * transformAge(inp_sShelf.miAge);
	}

	static inline auto calcXBeta(const SDefaultPkt& inp_sShelf) -> decltype(*inp_sShelf.mcThisLtvPtr * (*inp_sShelf.msModelPtr)[MyDefault::LTV] + *inp_sShelf.mcThisPayShockPtr * (*inp_sShelf.msModelPtr)[MyDefault::PAY_SHOCK] + calcStaticTerm(inp_sShelf))
	{
		return *inp_sShelf.mcThisLtvPtr * (*inp_sShelf.msModelPtr)[MyDefault::LTV] + *inp_sShelf.mcThisPayShockPtr * (*inp_sShelf.msModelPtr)[MyDefault::PAY_SHOCK] + calcStaticTerm(inp_sShelf);
	}

	static PROCESS_STATUS finalize(SDefaultPktPtr inp_sShelfPtr)
	{
		enum HERE :size_t { BAL, HPI, INT, PRIN };

		auto itrs(iterators::MakeBundleConst(
			inp_sShelfPtr->msMyPtr->mcBalEopPtr,
			inp_sShelfPtr->msMyPtr->mcHomeValuesPtr,
			inp_sShelfPtr->msMyPtr->mcIntPtr,
			inp_sShelfPtr->msMyPtr->mcPrinPtr));

		itrs.begin();
		*inp_sShelfPtr->mcThisPmtPtr = calcFirstPmt(*inp_sShelfPtr->msMyPtr->mcIntPtr, *inp_sShelfPtr->msMyPtr->mcPrinPtr);
		inp_sShelfPtr->msMyPtr->mcXBetaPtr->add(itrs.At<HERE::BAL>().key(), CreateSmart<CVectorReal>(inp_sShelfPtr->miSims, dNOPPMTXBETA));

		for (itrs++; !itrs.isEnd(); itrs++)
		{
			std::swap(inp_sShelfPtr->mcPriorPmtPtr, inp_sShelfPtr->mcThisPmtPtr);
			*inp_sShelfPtr->mcThisPmtPtr = *itrs.At<HERE::INT>().value() + *itrs.At<HERE::PRIN>().value();
			*inp_sShelfPtr->mcThisLtvPtr = getCurrentLtv(*itrs.At<HERE::BAL>().value(), *itrs.At<HERE::HPI>().value());
			*inp_sShelfPtr->mcThisPayShockPtr = getCurrentPayShock(*inp_sShelfPtr->mcThisPmtPtr, *inp_sShelfPtr->mcPriorPmtPtr);

			inp_sShelfPtr->msMyPtr->mcXBetaPtr->add(itrs.At<HERE::BAL>().key(), CreateSmart<CVectorReal>(calcXBeta(*inp_sShelfPtr)));

			age(*inp_sShelfPtr);
		}

		return PROCESS_STATUS::DONE;
	}

	static inline auto getCurrentLtv(const CVectorReal& inp_cThisBal, const CVectorReal& inp_cThisHpi) -> decltype(CLogistic::StandardCDF((inp_cThisBal / inp_cThisHpi - 80) * 0.1428571428571429))
	{
		return CLogistic::StandardCDF((inp_cThisBal / inp_cThisHpi - 80) * 0.1428571428571429);
	}

	static inline auto getCurrentPayShock(const CVectorReal& inp_cThisPmt, const CVectorReal& inp_cPriorPmt) -> decltype(CLogistic::StandardCDF(inp_cThisPmt / inp_cPriorPmt))
	{
		return CLogistic::StandardCDF(inp_cThisPmt / inp_cPriorPmt);
	}

	static inline void initialize(SDefaultPkt& inp_sShelf)
	{
		inp_sShelf.miSims = inp_sShelf.msMyPtr->mcBalEopPtr->begin()->second->size();
		inp_sShelf.mcPriorPmtPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);
		inp_sShelf.mcThisPmtPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);
		inp_sShelf.mcThisLtvPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);
		inp_sShelf.mcThisPayShockPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);

		inp_sShelf.mdConst = calcConstTerm(*inp_sShelf.msMyPtr->msBondDefPtr, *inp_sShelf.msMyPtr->msCounterpartyPtr, *inp_sShelf.msMyPtr->msPropertyPtr, *inp_sShelf.msModelPtr);
		inp_sShelf.miAge = inp_sShelf.msMyPtr->msBondDefPtr->miPayPeriods - static_cast<uint32_t>(inp_sShelf.msMyPtr->mcBalEopPtr->size()) + 1;
	}

	static inline real transformAge(const uint32_t& inp_iAge)
	{
		return CMath::ln(1.0 + static_cast<real>(inp_iAge));
	}

	static inline real transformFico(const real& inp_dFico)
	{
		return CLogistic::StandardCDF((inp_dFico - 720.0) * 0.01);
	}

protected:
	ItrSelector<CMyDefaultCoefLst>::const_iterator	mcModelsItr;
};
#endif
