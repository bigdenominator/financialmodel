#ifndef INCLUDE_H_PHOMEVALUES
#define INCLUDE_H_PHOMEVALUES

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "UserSchematics.h"


DEF_NODE(PHomeValues, SHpi, (SHpiPca), (), (SHpiConfig))
{
public:
	typedef PHomeValues _Myt;

	bool Initialize(void)
	{
		SHpiConfigPtr	sConfigPtr;

		if (!View(sConfigPtr)) return false;

		return Initialize(sConfigPtr);
	}

	bool Initialize(SHpiConfigPtr inp_sConfigPtr)
	{
		mModelId = inp_sConfigPtr->mModelId;

		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		SHpiPcaPtr sPcaPtr;
		switch (mModelId)
		{
		case HPI_MODELS::HPI_PCA:
		default:
			sPcaPtr = CreateSmart<SHpiPca>();
			sPcaPtr->msPropertyPtr = inp_sSchematicPtr->msPropertyPtr;
			sPcaPtr->mcHomeValuesPtr = inp_sSchematicPtr->mcHomeValuesPtr;

			return Request(sPcaPtr, &_Myt::complete);
		}
	}

protected:
	HPI_MODELS mModelId;

	static PROCESS_STATUS complete(void) { return PROCESS_STATUS::DONE; }
};
#endif