#ifndef INCLUDE_H_PCAMODEL
#define INCLUDE_H_PCAMODEL

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "UserSchematics.h"


DEF_NODE(PHpiPca, SHpiPca, (), (), (SGlobals, SHpiConfig, SHpiPcaConfig))
{
public:
	enum :uint32_t { FACTORS = 4 };

	bool Initialize(void)
	{
		SGlobalsPtr			sGlobalsPtr;
		SHpiConfigPtr		sHpiPtr;
		SHpiPcaConfigPtr	sConfigPtr;

		if (!View(sGlobalsPtr)) return false;
		if (!View(sHpiPtr)) return false;
		if (!View(sConfigPtr)) return false;
		
		return Initialize(sGlobalsPtr, sHpiPtr, sConfigPtr);
	}

	bool Initialize(SGlobalsPtr inp_sGlobalsPtr, SHpiConfigPtr inp_sHpiPtr, SHpiPcaConfigPtr inp_sConfigPtr)
	{
		if (inp_sHpiPtr->mcHistoryPtr->empty())
		{
			FEEDBACK(GetFeedback(), 6000, "HPI History is empty.")
			return GetFeedback();
		}

		const size_t iSimsPerScenario(inp_sGlobalsPtr->miSimsPerScenario);

		mcHistoryPtr = inp_sHpiPtr->mcHistoryPtr;

		msMeansPtr = inp_sConfigPtr->mcMeansPtr;
		msLoadings1Ptr = inp_sConfigPtr->mcPca1Ptr->mcFactorLoadingsPtr;
		msLoadings2Ptr = inp_sConfigPtr->mcPca2Ptr->mcFactorLoadingsPtr;
		msLoadings3Ptr = inp_sConfigPtr->mcPca3Ptr->mcFactorLoadingsPtr;
		msLoadings4Ptr = inp_sConfigPtr->mcPca4Ptr->mcFactorLoadingsPtr;

		mcItrFactors.At<HERE::PC1>().attach(CreateSmart<CDataTable>());
		mcItrFactors.At<HERE::PC2>().attach(CreateSmart<CDataTable>());
		mcItrFactors.At<HERE::PC3>().attach(CreateSmart<CDataTable>());
		mcItrFactors.At<HERE::PC4>().attach(CreateSmart<CDataTable>());

		mcStateDataPtr = CreateSmart<CStateDataTables>();

		mcGrowthPtr = CreateSmart<CVectorReal>(iSimsPerScenario);
		mcScalePtr = CreateSmart<CVectorReal>(iSimsPerScenario * inp_sGlobalsPtr->miScenarios);

		const uint32_t iPeriods(YEARS * SPeriod::PeriodsPerYear(mFrequency));

		CRandomNumberGenerator cRNG(inp_sConfigPtr->mlSeed);
		SPeriod sPeriod(mFrequency);

		CDate dtCurrent((inp_sHpiPtr->mcHistoryPtr->cend()--)->first);
		for (uint32_t iPeriod(0); iPeriod < iPeriods; iPeriod++)
		{
			dtCurrent += sPeriod;

			CVectorRealPtr sPca1Ptr(CreateSmart<CVectorReal>(iSimsPerScenario));
			CVectorRealPtr sPca2Ptr(CreateSmart<CVectorReal>(iSimsPerScenario));
			CVectorRealPtr sPca3Ptr(CreateSmart<CVectorReal>(iSimsPerScenario));
			CVectorRealPtr sPca4Ptr(CreateSmart<CVectorReal>(iSimsPerScenario));

			cRNG.GetNext(*sPca1Ptr);
			cRNG.GetNext(*sPca2Ptr);
			cRNG.GetNext(*sPca3Ptr);
			cRNG.GetNext(*sPca4Ptr);

			for (size_t iSim(0); iSim < iSimsPerScenario; iSim++)
			{
				CMath::BoxMuller((*sPca1Ptr)[iSim], (*sPca2Ptr)[iSim], (*sPca1Ptr)[iSim], (*sPca2Ptr)[iSim]);
				CMath::BoxMuller((*sPca3Ptr)[iSim], (*sPca4Ptr)[iSim], (*sPca3Ptr)[iSim], (*sPca4Ptr)[iSim]);
			}

			*sPca1Ptr *= inp_sConfigPtr->mcPca1Ptr->mdStdDev;
			*sPca2Ptr *= inp_sConfigPtr->mcPca2Ptr->mdStdDev;
			*sPca3Ptr *= inp_sConfigPtr->mcPca3Ptr->mdStdDev;
			*sPca4Ptr *= inp_sConfigPtr->mcPca4Ptr->mdStdDev;

			mcItrFactors.At<HERE::PC1>().data()->add(dtCurrent, sPca1Ptr);
			mcItrFactors.At<HERE::PC2>().data()->add(dtCurrent, sPca2Ptr);
			mcItrFactors.At<HERE::PC3>().data()->add(dtCurrent, sPca3Ptr);
			mcItrFactors.At<HERE::PC4>().data()->add(dtCurrent, sPca4Ptr);
		}

		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		const size_type iState(static_cast<size_type>(inp_sSchematicPtr->msPropertyPtr->mFipsStateCode));

		if ((*mcStateDataPtr)[iState]->empty())
		{
			addHistory(iState, mcScalePtr->size());
			buildState(iState, mcScalePtr->size());
		}

		*mcScalePtr = setScale(*(*mcStateDataPtr)[iState], *inp_sSchematicPtr->msPropertyPtr);

		auto itr(iterators::GetItr(inp_sSchematicPtr->mcHomeValuesPtr));
		LOOP(itr)
		{
			*itr.value() = Solvers::InterpolateLinear(*(*mcStateDataPtr)[iState], itr.key(), true) * *mcScalePtr;
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	typedef BundleHlpr<CDataTable, CDataTable, CDataTable, CDataTable>::const_type MyBundle;
	typedef CSmartArray<CDataTablePtr, STATE_CODE_COUNT::value> CStateDataTables;
	typedef CSmartPtr<CStateDataTables> CStateDataTablesPtr;
	typedef CStateDataTables::size_type size_type;

	enum HERE { PC1, PC2, PC3, PC4 };
	enum :uint32_t { YEARS = 30 };

	const FREQUENCY	mFrequency = FREQUENCY::QUARTERLY;

	MyBundle			mcItrFactors;

	CStateDataPtr		msMeansPtr;
	CStateDataPtr		msLoadings1Ptr; 
	CStateDataPtr		msLoadings2Ptr;
	CStateDataPtr		msLoadings3Ptr;
	CStateDataPtr		msLoadings4Ptr;
	CStateDataSeriesPtr	mcHistoryPtr;
	CStateDataTablesPtr	mcStateDataPtr;

	CVectorRealPtr		mcGrowthPtr;
	CVectorRealPtr		mcScalePtr;

	inline void addHistory(const size_type& inp_iState, size_t inp_iSims)
	{
		FOREACH((*mcHistoryPtr), itr)
		{
			(*mcStateDataPtr)[inp_iState]->add(itr->first, CreateSmart<CVectorReal>(inp_iSims, (*itr->second)[inp_iState]));
		}
	}

	inline void buildState(const size_type& inp_iState, size_t inp_iSims)
	{
		LOOP(mcItrFactors)
		{
			*mcGrowthPtr = (*msMeansPtr)[inp_iState];
			
			*mcGrowthPtr += *mcItrFactors.At<HERE::PC1>().value() * (*msLoadings1Ptr)[inp_iState];
			*mcGrowthPtr += *mcItrFactors.At<HERE::PC2>().value() * (*msLoadings2Ptr)[inp_iState];
			*mcGrowthPtr += *mcItrFactors.At<HERE::PC3>().value() * (*msLoadings3Ptr)[inp_iState];
			*mcGrowthPtr += *mcItrFactors.At<HERE::PC4>().value() * (*msLoadings4Ptr)[inp_iState];
			*mcGrowthPtr = std::exp(*mcGrowthPtr);

			CVectorRealPtr cNewPtr(CreateSmart<CVectorReal>(inp_iSims));
			cNewPtr->repeat(*mcGrowthPtr);
			*cNewPtr *= *(*mcStateDataPtr)[inp_iState]->back().second;

			(*mcStateDataPtr)[inp_iState]->add(mcItrFactors.At<HERE::PC1>().key(), cNewPtr);
		}
	}

	static inline auto setScale(const CDataTable& inp_cData, const SProperty& inp_sProperty) -> decltype(inp_sProperty.mdtValue / Solvers::InterpolateLinear(inp_cData, inp_sProperty.mdtValue, true))
	{
		return inp_sProperty.mdtValue / Solvers::InterpolateLinear(inp_cData, inp_sProperty.mdtValue, true);
	}
};
#endif