#ifndef INCLUDE_H_PLOSSSEVERITY
#define INCLUDE_H_PLOSSSEVERITY

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "Schematics.h"

#include "UserSchematics.h"


DEF_NODE(PLossSeverity, SMySeverity, (), (), (SMySeverityConfig))
{
public:
	bool Initialize(void)
	{
		SMySeverityConfigPtr	sConfigPtr;

		if (!View(sConfigPtr)) return false;
		
		return Initialize(sConfigPtr);
	}

	bool Initialize(SMySeverityConfigPtr inp_sConfigPtr)
	{
		mcDistressedPricesPtr = inp_sConfigPtr->mcDistressedPricesPtr;
		mcForeclosureCostPtr = inp_sConfigPtr->mcForeclosureCostPtr;

		return true;
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		enum HERE { BAL, HPI };

		const CStateData::size_type lState(static_cast<CStateData::size_type>(inp_sSchematicPtr->mFipsStateCode));
		const real dCost((*mcForeclosureCostPtr)[lState]);
		const real dDistressPrice((*mcDistressedPricesPtr)[lState]);

		auto itr(iterators::MakeBundleConst(inp_sSchematicPtr->mcBalEopPtr, inp_sSchematicPtr->mcHomeValuesPtr));

		LOOP(itr)
		{
			CVectorRealPtr cSeverityPtr(CreateSmart<CVectorReal>(*itr.At<HERE::BAL>().value() - (*itr.At<HERE::HPI>().value() * dDistressPrice - dCost)));

			for (CVectorReal::size_type iSim(0); iSim < cSeverityPtr->size(); iSim++)
			{
				(*cSeverityPtr)[iSim] = ((*itr.At<HERE::BAL>().value())[iSim] == 0.0 ? 0.0 : (*cSeverityPtr)[iSim] / (*itr.At<HERE::BAL>().value())[iSim]);
			}
			(*cSeverityPtr |= 0.0) &= 1.0;

			inp_sSchematicPtr->mcSeverityPtr->add(itr.At<HERE::BAL>().key(), cSeverityPtr);
		}

		return PROCESS_STATUS::DONE;
	}

protected:
	CStateDataPtr	mcDistressedPricesPtr;
	CStateDataPtr	mcForeclosureCostPtr;
};
#endif