#ifndef INCLUDE_H_PREFITURNOVER
#define INCLUDE_H_PREFITURNOVER

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "UserSchematics.h"

static const string sBadRtppSubmodelLookup("No submodel found");


SIMPLESTRUCT(SPrepayPkt,
	(SRtppModelPtr	msModelPtr;
	SRtppPtr		msMyPtr;
	SIrpPtr			msIrpPtr;
	
	CUniquePtr<CVectorReal>	mcThisLtvPtr;
	CUniquePtr<CVectorReal>	mcThisNasPtr;
	CUniquePtr<CVectorReal>	mcOrigSpdsPtr;

	size_t			miSims;
	real			mdConst;
	real			mdLtv;
	real			mdNas;
  	uint32_t		miAge;
	MONTHSOFYEAR	mMonth;
	int32_t			miMonthsSinceFirstReset;))

DEF_NODE(PPrepay, SRtpp, (SIrp), (), (SRtppConfig))
{
public:
	typedef PPrepay _Myt;

	bool Initialize(void)
	{
		SRtppConfigPtr	sConfigPtr;

		if (!View(sConfigPtr)) return false;
		
		return Initialize(sConfigPtr);
	}

	bool Initialize(SRtppConfigPtr inp_sConfigPtr)
	{
		mcModelsItr.attach(inp_sConfigPtr->mcModelLstPtr);

		return true;

		//CTimeSeriesPtr	cJumboSpreadsPtr(CTimeSeries>());
		//cJumboSpreadsPtr->Add(20070701, 0.25);
		//cJumboSpreadsPtr->Add(20080701, 2.00);
		//cJumboSpreadsPtr->Add(20090101, 2.00);
		//cJumboSpreadsPtr->Add(20090701, 1.25);
		//cJumboSpreadsPtr->Add(20110701, 1.25);
		//cJumboSpreadsPtr->Add(20120701, 0.25);
	}

	PROCESS_STATUS Process(SSchematicPtr inp_sSchematicPtr)
	{
		if (!mcModelsItr.find(inp_sSchematicPtr->msBondDefPtr->mRtppSubModel))
		{
			FEEDBACK(GetFeedback(), 5001, sBadRtppSubmodelLookup + ": BondID(" + to_string(inp_sSchematicPtr->msBondDefPtr->mlBondId) + ") RtppSubmodel(" + to_string(inp_sSchematicPtr->msBondDefPtr->mRtppSubModel) + ")");
			return PROCESS_STATUS::FAIL;
		}
		else
		{
			SPrepayPktPtr	msShelfPtr(CreateSmart<SPrepayPkt>());
			msShelfPtr->msModelPtr = mcModelsItr.value();
			msShelfPtr->msMyPtr = inp_sSchematicPtr;

			initialize(*msShelfPtr);

			return Request(msShelfPtr->msIrpPtr, &_Myt::finalize, msShelfPtr);
		}
	}

	static inline void age(SPrepayPkt& inp_sShelf)
	{
		inp_sShelf.miAge++;
		inp_sShelf.miMonthsSinceFirstReset++;
	}
	
	static inline real calcConstTerm(const SBondDef& inp_sBondDef, const SCounterparty& inp_sCounterparty, const SProperty& inp_sProperty, const RtppCoef& inp_RtppCoef)
	{
		return inp_RtppCoef[Rtpp::INTERCEPT]
			+ (inp_sBondDef.mStatus == LOANSTATUS::CURRENT ? 0.0 : inp_RtppCoef[Rtpp::DLQ_ADJ])
			+ calcLoanOriginatorTypeTerm(inp_sBondDef.mLoanOriginatorType, inp_RtppCoef)
			+ calcLoanPurposeTypeTerm(inp_sBondDef.mLoanPurposeType, inp_RtppCoef)
			+ calcProjectLegalStructureTypeTerm(inp_sProperty.mProjectLegalStructureType, inp_RtppCoef)
			+ calcPropertyUsageTypeTerm(inp_sProperty.mPropertyUsageType, inp_RtppCoef)
			+ calcFicoTerm(inp_sCounterparty, inp_RtppCoef)
			;
	}

	static inline real calcFicoTerm(const SCounterparty& inp_sCounterparty, const RtppCoef& inp_RtppCoef)
	{
		return inp_RtppCoef[Rtpp::FICO] * transformFico(inp_sCounterparty.mdFico);
	}

	static inline real calcFirstResetPostFactor(const int32_t& inp_iMonthSinceFirstReset)
	{
		return splineLinearUnit(inp_iMonthSinceFirstReset, 0, 6);
	}

	static inline real calcFirstResetPreFactor(const int32_t& inp_iMonthSinceFirstReset)
	{
		return splineLinearUnit(inp_iMonthSinceFirstReset, -6, 6);
	}

	static inline real calcLoanOriginatorTypeTerm(LOANORIGINATORTYPE inp_LoanOriginatorType, const RtppCoef& inp_RtppCoef)
	{
		switch (inp_LoanOriginatorType)
		{
		case LOANORIGINATORTYPE::BROKER:		return inp_RtppCoef[Rtpp::BROKER];
		case LOANORIGINATORTYPE::CORRESPONDENT:	return inp_RtppCoef[Rtpp::CORR];
		default:								return inp_RtppCoef[Rtpp::LENDER];
		}
	}

	static inline real calcLoanPurposeTypeTerm(LOANPURPOSETYPE inp_LoanPurposeType, const RtppCoef& inp_RtppCoef)
	{
		switch (inp_LoanPurposeType)
		{
		case LOANPURPOSETYPE::PURCHASE:	return inp_RtppCoef[Rtpp::PURCHASE];
		default:						return inp_RtppCoef[Rtpp::REFI];
		}
	}

	static inline real calcMonthTerm(MONTHSOFYEAR inp_Month, const RtppCoef& inp_RtppCoef)
	{
		switch (inp_Month)
		{
		case MONTHSOFYEAR::JANUARY:		return inp_RtppCoef[Rtpp::JAN];
		case MONTHSOFYEAR::FEBRUARY:	return inp_RtppCoef[Rtpp::FEB];
		case MONTHSOFYEAR::MARCH:		return inp_RtppCoef[Rtpp::MAR];
		case MONTHSOFYEAR::APRIL:		return inp_RtppCoef[Rtpp::APR];
		case MONTHSOFYEAR::MAY:			return inp_RtppCoef[Rtpp::MAY];
		case MONTHSOFYEAR::JUNE:		return inp_RtppCoef[Rtpp::JUN];
		case MONTHSOFYEAR::JULY:		return inp_RtppCoef[Rtpp::JUL];
		case MONTHSOFYEAR::AUGUST:		return inp_RtppCoef[Rtpp::AUG];
		case MONTHSOFYEAR::SEPTEMBER:	return inp_RtppCoef[Rtpp::SEP];
		case MONTHSOFYEAR::OCTOBER:		return inp_RtppCoef[Rtpp::OCT];
		case MONTHSOFYEAR::NOVEMBER:	return inp_RtppCoef[Rtpp::NOV];
		default:						return inp_RtppCoef[Rtpp::DEC];
		}
	}

	static inline auto calcOriginationSpread(const SBondDef& inp_sBondDef, const CVectorReal& inp_cRates) -> decltype(inp_sBondDef.mdRateOrigination - inp_cRates)
	{
		return inp_sBondDef.mdRateOrigination - inp_cRates;
	}

	static inline real calcProjectLegalStructureTypeTerm(PROJECTLEGALSTRUCTURETYPE inp_ProjectLegalStructureType, const RtppCoef& inp_RtppCoef)
	{
		switch (inp_ProjectLegalStructureType)
		{
		case PROJECTLEGALSTRUCTURETYPE::COOPERATIVE:	return inp_RtppCoef[Rtpp::COOP];
		case PROJECTLEGALSTRUCTURETYPE::CONDOMINIUM:	return inp_RtppCoef[Rtpp::CONDO];
		default:										return inp_RtppCoef[Rtpp::STANDARD];
		}
	}

	static inline real calcPropertyUsageTypeTerm(PROPERTYUSAGETYPE inp_PropertyUsageType, const RtppCoef& inp_RtppCoef)
	{
		switch (inp_PropertyUsageType)
		{
		case PROPERTYUSAGETYPE::INVESTMENT:	return inp_RtppCoef[Rtpp::INVESTMENT];
		case PROPERTYUSAGETYPE::SECONDHOME:	return inp_RtppCoef[Rtpp::SECOND];
		default:							return inp_RtppCoef[Rtpp::PRIMARY];
		}
	}

	static inline real calcStaticTerm(const SPrepayPkt& inp_sShelf)
	{
		return inp_sShelf.mdConst
			+ calcMonthTerm(inp_sShelf.mMonth, *inp_sShelf.msModelPtr->mcCoeffPtr)
			+ (*inp_sShelf.msModelPtr->mcCoeffPtr)[Rtpp::AGE] * transformAge(inp_sShelf.miAge)
			+ (*inp_sShelf.msModelPtr->mcCoeffPtr)[Rtpp::SUMMER] * calcSummerIndicator(inp_sShelf.mMonth)
			+ (*inp_sShelf.msModelPtr->mcCoeffPtr)[Rtpp::PRE_RESET] * calcFirstResetPreFactor(inp_sShelf.miMonthsSinceFirstReset)
			+ (*inp_sShelf.msModelPtr->mcCoeffPtr)[Rtpp::POST_RESET] * calcFirstResetPostFactor(inp_sShelf.miMonthsSinceFirstReset);
	}

	static inline real calcSummerIndicator(MONTHSOFYEAR inp_Month)
	{
		switch (inp_Month)
		{
		case MONTHSOFYEAR::APRIL:
		case MONTHSOFYEAR::MAY:
		case MONTHSOFYEAR::JUNE:
		case MONTHSOFYEAR::JULY:
		case MONTHSOFYEAR::AUGUST:
		case MONTHSOFYEAR::SEPTEMBER:
			return 1.0;
			break;
		default:
			return 0.0;
			break;
		}
	}

	static inline auto calcXBeta(const SPrepayPkt& inp_sShelf) -> decltype(*inp_sShelf.mcThisLtvPtr * inp_sShelf.mdLtv + *inp_sShelf.mcThisNasPtr * inp_sShelf.mdNas + calcStaticTerm(inp_sShelf))
	{
		return *inp_sShelf.mcThisLtvPtr * inp_sShelf.mdLtv + *inp_sShelf.mcThisNasPtr * inp_sShelf.mdNas + calcStaticTerm(inp_sShelf);
	}

	static PROCESS_STATUS finalize(SPrepayPktPtr inp_sShelfPtr)
	{
		enum HERE :size_t { BAL, HPI, LOANRATE, MKTRATE };

		auto itrs(iterators::MakeBundleConst(
			inp_sShelfPtr->msMyPtr->mcBalEopPtr,
			inp_sShelfPtr->msMyPtr->mcHomeValuesPtr,
			inp_sShelfPtr->msMyPtr->mcRatesPtr,
			inp_sShelfPtr->msIrpPtr->mcRatesPtr));

		itrs.begin();
		*inp_sShelfPtr->mcOrigSpdsPtr = calcOriginationSpread(*inp_sShelfPtr->msMyPtr->msBondDefPtr, *itrs.At<HERE::MKTRATE>().value());
		inp_sShelfPtr->msMyPtr->mcXBetaPtr->add(itrs.At<HERE::BAL>().key(), CreateSmart<CVectorReal>(inp_sShelfPtr->miSims, dNOPPMTXBETA));

		for (itrs++; !itrs.isEnd(); itrs++)
		{
			inp_sShelfPtr->mMonth = itrs.At<HERE::BAL>().key().Month();

			*inp_sShelfPtr->mcThisLtvPtr = getCurrentLtv(*itrs.At<HERE::BAL>().value(), *itrs.At<HERE::HPI>().value());
			*inp_sShelfPtr->mcThisNasPtr = getCurrentNas(*itrs.At<HERE::LOANRATE>().value(), *itrs.At<HERE::MKTRATE>().value(), *inp_sShelfPtr->mcOrigSpdsPtr);

			inp_sShelfPtr->msMyPtr->mcXBetaPtr->add(itrs.At<HERE::BAL>().key(), CreateSmart<CVectorReal>(calcXBeta(*inp_sShelfPtr)));

			age(*inp_sShelfPtr);
		}

		return PROCESS_STATUS::DONE;
	}

	static inline auto getCurrentLtv(const CVectorReal& inp_cThisBal, const CVectorReal& inp_cThisHpi) -> decltype(CLogistic::StandardCDF((inp_cThisBal / inp_cThisHpi - 80) * 0.1428571428571429))
	{
		return CLogistic::StandardCDF((inp_cThisBal / inp_cThisHpi - 80) * 0.1428571428571429);
	}

	static inline auto getCurrentNas(const CVectorReal& inp_cThisLoanRate, const CVectorReal& inp_cThisMktRate, const CVectorReal& inp_cOrigSpread) -> decltype(CLogistic::StandardCDF(((inp_cThisLoanRate - inp_cThisMktRate) - inp_cOrigSpread) * 0.5))
	{
		return CLogistic::StandardCDF(((inp_cThisLoanRate - inp_cThisMktRate) - inp_cOrigSpread) * 0.5);
	}

	static inline void initialize(SPrepayPkt& inp_sShelf)
	{
		inp_sShelf.miSims = inp_sShelf.msMyPtr->mcBalEopPtr->begin()->second->size();
		inp_sShelf.mcOrigSpdsPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);
		inp_sShelf.mcThisLtvPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);
		inp_sShelf.mcThisNasPtr = CreateUnique<CVectorReal>(inp_sShelf.miSims);

		inp_sShelf.msIrpPtr = CreateSmart<SIrp>();
		inp_sShelf.msIrpPtr->mMarketIndex = inp_sShelf.msMyPtr->msBondDefPtr->mResetIndex;
		inp_sShelf.msIrpPtr->mcRatesPtr = CreateSmart<CDataTable>();

		inp_sShelf.msIrpPtr->mcRatesPtr->add(inp_sShelf.msMyPtr->msBondDefPtr->mdtIssue, CreateSmart<CVectorReal>());
		FOREACH((*inp_sShelf.msMyPtr->mcBalEopPtr), itr)
		{
			inp_sShelf.msIrpPtr->mcRatesPtr->add(itr->first, CreateSmart<CVectorReal>());
		}

		inp_sShelf.mdLtv = (*inp_sShelf.msModelPtr->mcCoeffPtr)[Rtpp::LTV];
		inp_sShelf.mdNas = (*inp_sShelf.msModelPtr->mcCoeffPtr)[Rtpp::NAS];
		switch (inp_sShelf.msMyPtr->msBondDefPtr->mLoanOriginatorType)
		{
		case LOANORIGINATORTYPE::BROKER:
		case LOANORIGINATORTYPE::CORRESPONDENT:
			inp_sShelf.mdNas += (*inp_sShelf.msModelPtr->mcCoeffPtr)[Rtpp::NAS_TPO];
		}

		inp_sShelf.mdConst = calcConstTerm(*inp_sShelf.msMyPtr->msBondDefPtr, *inp_sShelf.msMyPtr->msCounterpartyPtr, *inp_sShelf.msMyPtr->msPropertyPtr, *inp_sShelf.msModelPtr->mcCoeffPtr);
		inp_sShelf.miAge = inp_sShelf.msMyPtr->msBondDefPtr->miPayPeriods - static_cast<uint32_t>(inp_sShelf.msMyPtr->mcBalEopPtr->size()) + 1;
		inp_sShelf.miMonthsSinceFirstReset = inp_sShelf.miAge - inp_sShelf.msMyPtr->msBondDefPtr->miFirstReset;
	}

	static inline real splineLinearUnit(const real& inp_dValue, const real& inp_dSplineLoc, const real& inp_dSplineScale)
	{
		return min(1.0, max(0.0, (inp_dValue - inp_dSplineLoc) / inp_dSplineScale));
	}

	static inline real transformAge(const uint32_t& inp_iAge)
	{
		return CMath::ln(1.0 + static_cast<real>(inp_iAge));
	}

	static inline real transformFico(const real& inp_dFico)
	{
		return CLogistic::StandardCDF((inp_dFico - 720.0) * 0.01);
	}

protected:
	ItrSelector<CRtppModelLst>::const_iterator	mcModelsItr;
};
#endif