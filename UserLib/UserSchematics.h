#ifndef INCLUDE_H_USCHEMATICS
#define INCLUDE_H_USCHEMATICS

/* (C) Copyright Big Denominator, LLC 2016.
*  Use, modification and distribution are subject to the Big Denominator SDK License, Version 1.0.
*  (See accompanying file LICENSE.txt or copy at http://www.bigdenominator.com/license-sdk)
*/

#include "FinancialDefs.h"

#include "Schematics.h"

enum USER_TYPELIST :TYPELIST_t {
	SDEFAULTCONFIG = 3001,
	SHPICONFIG,
	SHPIPCACONFIG,
	SHPI,
	SHPIPCA,
	SPREPAYCONFIG,
	SSEVERITYCONFIG
};

//SDEFAULT
typedef CArrayReal<17> MyDefaultCoef;
namespace MyDefault
{
	enum :size_t
	{
		INTERCEPT, DLQ_ADJ,
		BROKER, CORR, LENDER, PURCHASE, REFI,
		COOP, CONDO, STANDARD, INVESTMENT, PRIMARY, SECOND,
		FICO, AGE, LTV, PAY_SHOCK
	};
}

typedef CSmartPtr<MyDefaultCoef> MyDefaultCoefPtr;

typedef CSmartMap<DEFAULT_SUBMODELS, MyDefaultCoefPtr> CMyDefaultCoefLst;
typedef CSmartPtr<CMyDefaultCoefLst> CMyDefaultCoefLstPtr;

SCHEMATIC(SMyDefaultConfig, USER_TYPELIST::SDEFAULTCONFIG, (CMyDefaultCoefLstPtr mcModelDataPtr;), (mcModelDataPtr))
SCHEMATIC(SMyDefault, TYPELIST::SDEFAULT,
(SBondDefPtr msBondDefPtr; SCounterpartyPtr msCounterpartyPtr; SPropertyPtr msPropertyPtr; CDataTablePtr mcHomeValuesPtr; CDataTablePtr mcBalEopPtr; CDataTablePtr mcIntPtr; CDataTablePtr mcPrinPtr; CDataTablePtr mcXBetaPtr;),
(msBondDefPtr, msCounterpartyPtr, msPropertyPtr, mcHomeValuesPtr, mcBalEopPtr, mcIntPtr, mcPrinPtr, mcXBetaPtr))

//SHPI
SCHEMATIC(SHpiConfig, USER_TYPELIST::SHPICONFIG, (HPI_MODELS mModelId; CStateDataSeriesPtr mcHistoryPtr;), (mModelId, mcHistoryPtr))
SCHEMATIC(SHpiPcaConfig, USER_TYPELIST::SHPIPCACONFIG, (uint64_t mlSeed; CStateDataPtr mcMeansPtr; SStatePcaPtr mcPca1Ptr; SStatePcaPtr mcPca2Ptr; SStatePcaPtr mcPca3Ptr; SStatePcaPtr mcPca4Ptr;), (mlSeed, mcMeansPtr, mcPca1Ptr, mcPca2Ptr, mcPca3Ptr, mcPca4Ptr))

SCHEMATIC(SHpi, USER_TYPELIST::SHPI, (SPropertyPtr msPropertyPtr; CDataTablePtr mcHomeValuesPtr;), (msPropertyPtr, mcHomeValuesPtr))
SCHEMATIC(SHpiPca, USER_TYPELIST::SHPIPCA, (SPropertyPtr msPropertyPtr; CDataTablePtr mcHomeValuesPtr;), (msPropertyPtr, mcHomeValuesPtr))

//SREFITURNOVER
typedef CArrayReal<17> RtppCoef;
namespace Rtpp
{
	enum :size_t
	{
		INTERCEPT, DLQ_ADJ,
		BROKER, CORR, LENDER, PURCHASE, REFI,
		COOP, CONDO, STANDARD, INVESTMENT, PRIMARY, SECOND,
		FICO, AGE, SUMMER,
		JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC,
		PRE_RESET, POST_RESET, LTV, MEDIA, NAS, NAS_TPO
	};
}

typedef CSmartPtr<RtppCoef> RtppCoefPtr;
SMARTSTRUCT(SRtppModel, (MARKET_INDEX mIndexRate; RtppCoefPtr mcCoeffPtr;), (mIndexRate, mcCoeffPtr))

typedef CSmartMap<RTPP_SUBMODELS, SRtppModelPtr> CRtppModelLst;
typedef CSmartPtr<CRtppModelLst> CRtppModelLstPtr;

SCHEMATIC(SRtppConfig, USER_TYPELIST::SPREPAYCONFIG, (CRtppModelLstPtr mcModelLstPtr;), (mcModelLstPtr))
SCHEMATIC(SRtpp, TYPELIST::SPREPAY,
	(SBondDefPtr msBondDefPtr; SCounterpartyPtr msCounterpartyPtr; SPropertyPtr msPropertyPtr; CDataTablePtr mcHomeValuesPtr; CDataTablePtr mcBalEopPtr; CDataTablePtr mcRatesPtr; CDataTablePtr mcXBetaPtr;),
	(msBondDefPtr, msCounterpartyPtr, msPropertyPtr, mcHomeValuesPtr, mcBalEopPtr, mcRatesPtr, mcXBetaPtr))

//SSEVERITY
SCHEMATIC(SMySeverityConfig, USER_TYPELIST::SSEVERITYCONFIG, (CStateDataPtr mcDistressedPricesPtr; CStateDataPtr mcForeclosureCostPtr;), (mcDistressedPricesPtr, mcForeclosureCostPtr))
SCHEMATIC(SMySeverity, TYPELIST::SSEVERITY, (STATE_CODES mFipsStateCode; CDataTablePtr mcBalEopPtr; CDataTablePtr mcHomeValuesPtr; CDataTablePtr mcSeverityPtr;), (mFipsStateCode, mcBalEopPtr, mcHomeValuesPtr, mcSeverityPtr))
#endif